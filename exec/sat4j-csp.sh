#!/bin/bash

##########################################################################
# This script runs Sat4j-CSP from its modular jar.                       #
#                                                                        #
# If Sat4j-CSP and its dependencies are not stored in the default        #
# directory, you must specify its location using:                        #
#      export SAT4J_CSP_HOME="/path/to/sat4j-csp"                        #
#                                                                        #
# If options must be passed to the Java Virtual Machine (and NOT to      #
# Sat4j-CSP), you must specify them by exporting either JAVA_OPTS or     #
# JVM_ARGS set with the appropriate value.                               #
##########################################################################

if [ -z "$SAT4J_CSP_HOME" ]
then
    # Using the default directory for Sat4j-CSP.
    SAT4J_CSP_HOME="dist/home"
fi

if [ ! -d "$SAT4J_CSP_HOME" ]
then
    # Sat4j-CSP directory does not exist.
    echo "$0: $SAT4J_CSP_HOME: No such directory or not a directory."
    exit 127
fi

# Running Sat4j-CSP.
java $JAVA_OPTS $JVM_ARGS \
    -p "$SAT4J_CSP_HOME" \
    -m org.ow2.sat4j.csppb/org.sat4j.csp.LauncherCSP \
    $@
