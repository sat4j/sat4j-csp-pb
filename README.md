# Sat4j-PB-CSP, a CSP solver based on the Sat4j platform

[![pipeline status](https://gitlab.ow2.org/sat4j/sat4j-csp-pb/badges/main/pipeline.svg)](https://gitlab.ow2.org/sat4j/sat4j-csp-pb/-/commits/main)
[![Latest Release](https://gitlab.ow2.org/sat4j/sat4j-csp-pb/-/badges/release.svg)](https://gitlab.ow2.org/sat4j/sat4j-csp-pb/-/releases)

*Sat4j-PB-CSP* is a pseudo-Boolean based implementation of a CSP solver.

## Editing the source

Sat4j-PB-CSP is developed using [Java 11](https://openjdk.java.net/projects/jdk/11/)
or higher, [Gradle](https://gradle.org/) and
[Eclipse IDE](https://www.eclipse.org/).

Installing Gradle is **not** required, as the scripts `gradlew` or
`gradlew.bat` can do the job on their own.

However, you need to install at least the JDK 11 to compile the source code,
and it is recommended to install the latest version of Eclipse if you want to
edit it.

To do so, after having installed all the needed tools, you will need to clone
the project, and then to tell Gradle to generate Eclipse's configuration files
(an access to the internet is required, since dependencies may have to be
downloaded).

```bash
$ git clone https://gitlab.ow2.org/sat4j/sat4j-pb-csp.git
$ cd sat4j-pb-csp
$ ./gradlew eclipse
```

Once this has been done, you will be able to import the `sat4j-csp` project into
Eclipse, by choosing `File > Import... > Existing Projects into Workspace`.
Then, locate the `sat4j-pb-csp` repository on your computer.
Validate, and you are done: the project should now be available in Eclipse.

## Building Sat4j-PB-CSP from source

To build Sat4j-PB-CSP from its source code, execute the following command:

```bash
$ ./gradlew csp
```

This will create a directory `dist/jars` containing the jars of Sat4j-PB-CSP
and its dependencies.
It will also produce a gzipped-tarball `dist/sat4j-csp.tgz`, containing
all these jars, bundled with a script for executing Sat4j-PB-CSP, and this
README.

## Executing Sat4j-PB-CSP

After having built Sat4j-PB-CSP, you may want to execute it from the command line.

Sat4j-PB-CSP provides a *wrapper* bash script `sat4j-csp.sh` (available in the
`exec` directory) to make easier its execution from its jars and required
modules.

By default, this script considers that the jars of Sat4j-CSP are stored in their
output directory, that is `dist/home`.

If you have moved them, or if you do not want to execute the script from
Sat4j-PB-CSP's repository, you will need to export a variable `SAT4J_CSP_HOME`
set with the path of the directory containing the jars (you may put this
export into your `.bashrc` to stop worrying about it).
You may also edit the script to alter its default behavior.

Once everything is properly set, you can run Sat4j-PB-CSP as follows:

```bash
$ ./exec/sat4j-csp.sh [options] <path/to/instance.xml>
```

To get the details of all possible options, you can also run:

```bash
$ ./exec/sat4j-csp.sh -h
```

