/**
 * The {@code org.ow2.sat4j.csppb} module provides a CSP solver based on
 * the Sat4j platform.
 *
 * @author Thibault Falque
 * @author Romain Wallon
 *
 * @version 0.1.0
 */

module org.ow2.sat4j.csppb {

    // Exported packages of Sat4j-CSP.

    exports org.sat4j.csp;

    exports org.sat4j.csp.constraints;

    exports org.sat4j.csp.constraints.encoder;

    exports org.sat4j.csp.constraints.encoder.alldifferent;

    exports org.sat4j.csp.constraints.encoder.cardinality;

    exports org.sat4j.csp.constraints.encoder.channel;

    exports org.sat4j.csp.constraints.encoder.count;

    exports org.sat4j.csp.constraints.encoder.cumulative;

    exports org.sat4j.csp.constraints.encoder.element;

    exports org.sat4j.csp.constraints.encoder.extension;

    exports org.sat4j.csp.constraints.encoder.intension;

    exports org.sat4j.csp.constraints.encoder.minmax;

    exports org.sat4j.csp.constraints.encoder.multiplication;

    exports org.sat4j.csp.constraints.encoder.nooverlap;

    exports org.sat4j.csp.constraints.encoder.nvalues;

    exports org.sat4j.csp.constraints.encoder.order;

    exports org.sat4j.csp.constraints.encoder.sum;

    exports org.sat4j.csp.constraints.intension;

    exports org.sat4j.csp.core;

    exports org.sat4j.csp.reader;

    exports org.sat4j.csp.utils;

    exports org.sat4j.csp.variables;

    // Required (Sat4j) modules for solving SAT and PB problems.

    requires transitive org.ow2.sat4j.core;

    requires transitive org.ow2.sat4j.pb;

    // Required modules for parsing the command line.

    requires fr.cril.cli;

    opens org.sat4j.csp to fr.cril.cli;

    // Required modules for parsing XCSP instances.

    requires java.xml;

    requires xcsp3.tools;

}
