/**
 * Sat4j-CSP, a CSP solver based on the Sat4j platform.
 * Copyright (c) 2021-2022 - Exakis Nelite, Univ Artois & CNRS.
 * All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 * If not, see {@link http://www.gnu.org/licenses}.
 */

package org.sat4j.csp.reader;

import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.sat4j.csp.core.ICSPSolver;
import org.sat4j.csp.utils.UncheckedContradictionException;
import org.sat4j.reader.ParseFormatException;
import org.sat4j.reader.Reader;
import org.sat4j.specs.ContradictionException;
import org.sat4j.specs.IProblem;
import org.xml.sax.SAXException;

/**
 * The XCSP3Reader is a {@link Reader} that allows to feed an XCSP3 instance to
 * an {@link ICSPSolver}.
 *
 * @author Thibault Falque
 * @author Romain Wallon
 *
 * @version 0.1.0
 */
public class XCSP3Reader extends Reader {

    /**
     * The solver to which the read constraints will be added.
     */
    private ICSPSolver solver;

    /**
     * The callback that will be notified while reading the input instance.
     */
    private UglyXCallback callback;

    /**
     * Creates a new XCSP3Reader.
     *
     * @param solver The solver to which the read constraints will be added.
     */
    public XCSP3Reader(ICSPSolver solver) {
        this.solver = solver;
        this.callback = new UglyXCallback(new XCSP3Listener(solver));
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.reader.Reader#parseInstance(java.io.InputStream)
     */
    @Override
    public IProblem parseInstance(InputStream in)
            throws ContradictionException, ParseFormatException, IOException {
        try {
            var document = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(in);
            callback.loadInstance(document);
            return solver;

        } catch (UncheckedContradictionException e) {
            // Throwing instead a ContradictionException, to follow Sat4j's conventions.
            throw new ContradictionException(e);

        } catch (ParserConfigurationException | SAXException e) {
            // There has been an XML parsing problem.
            throw new ParseFormatException(e);

        } catch (UnsupportedOperationException | IOException e) {
            // We simply re-throw the exception.
            // We still need to catch it though, otherwise it will be caught below...
            throw e;

        } catch (Exception e) {
            // An error occurred while reading, so we throw an IOException.
            // We cannot catch a narrower Exception, since loadInstance() throws Exception...
            throw new IOException(e);
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.reader.Reader#decode(int[])
     */
    @Override
    public String decode(int[] model) {
        var list = new StringBuilder(" ");
        var values = new StringBuilder(" ");

        for (var entry : solver.getVariables().entrySet()) {
            list.append(entry.getKey()).append(' ');
            values.append(entry.getValue().decode(model)).append(' ');
        }

        return "<instantiation>\n"
                + "v    <list>" + list + "</list>\n"
                + "v    <values>" + values + "</values>\n"
                + "v </instantiation>";
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.reader.Reader#decode(int[], java.io.PrintWriter)
     */
    @Override
    public void decode(int[] model, PrintWriter out) {
        out.print(decode(model));
    }

}
