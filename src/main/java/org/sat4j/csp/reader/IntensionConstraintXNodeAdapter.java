/**
 * Sat4j-CSP, a CSP solver based on the Sat4j platform.
 * Copyright (c) 2021-2022 - Exakis Nelite, Univ Artois & CNRS.
 * All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 * If not, see {@link http://www.gnu.org/licenses}.
 */

package org.sat4j.csp.reader;

import static java.util.stream.Collectors.toList;

import java.math.BigInteger;
import java.util.Map;
import java.util.stream.Stream;

import org.sat4j.csp.constraints.ArithmeticOperator;
import org.sat4j.csp.constraints.BooleanOperator;
import org.sat4j.csp.constraints.Operator;
import org.sat4j.csp.constraints.RelationalOperator;
import org.sat4j.csp.constraints.encoder.intension.IIntensionConstraintVisitor;
import org.sat4j.csp.constraints.intension.BinaryIntensionConstraint;
import org.sat4j.csp.constraints.intension.ConstantIntensionConstraint;
import org.sat4j.csp.constraints.intension.IIntensionConstraint;
import org.sat4j.csp.constraints.intension.IfThenElseIntensionConstraint;
import org.sat4j.csp.constraints.intension.NaryIntensionConstraint;
import org.sat4j.csp.constraints.intension.UnaryIntensionConstraint;
import org.sat4j.csp.constraints.intension.VariableIntensionConstraint;
import org.sat4j.specs.ContradictionException;
import org.xcsp.common.Types.TypeExpr;
import org.xcsp.common.predicates.XNode;

/**
 * The IntensionConstraintXNodeAdapter is a (lazy) adapter for an {@link XNode}
 * representation of an {@link IIntensionConstraint}.
 *
 * @author Thibault Falque
 * @author Romain Wallon
 *
 * @version 0.1.0
 */
final class IntensionConstraintXNodeAdapter implements IIntensionConstraint {

    /**
     * The mapping between {@link TypeExpr} and unary operators.
     */
    private static final Map<TypeExpr, Operator> UNARY_OPERATORS = Map.of(
            TypeExpr.NEG, ArithmeticOperator.NEG,
            TypeExpr.ABS, ArithmeticOperator.ABS,
            TypeExpr.SQR, ArithmeticOperator.SQR,
            TypeExpr.NOT, BooleanOperator.NOT);

    /**
     * The mapping between {@link TypeExpr} and binary operators.
     */
    private static final Map<TypeExpr, Operator> BINARY_OPERATORS = Map.ofEntries(
            Map.entry(TypeExpr.SUB, ArithmeticOperator.SUB),
            Map.entry(TypeExpr.DIV, ArithmeticOperator.DIV),
            Map.entry(TypeExpr.MOD, ArithmeticOperator.MOD),
            Map.entry(TypeExpr.POW, ArithmeticOperator.POW),
            Map.entry(TypeExpr.DIST, ArithmeticOperator.DIST),
            Map.entry(TypeExpr.IMP, BooleanOperator.IMPL),
            Map.entry(TypeExpr.LT, RelationalOperator.LT),
            Map.entry(TypeExpr.LE, RelationalOperator.LE),
            Map.entry(TypeExpr.NE, RelationalOperator.NEQ),
            Map.entry(TypeExpr.GE, RelationalOperator.GE),
            Map.entry(TypeExpr.GT, RelationalOperator.GT));

    /**
     * The mapping between {@link TypeExpr} and n-ary operators.
     */
    private static final Map<TypeExpr, Operator> NARY_OPERATORS = Map.of(
            TypeExpr.ADD, ArithmeticOperator.ADD,
            TypeExpr.MUL, ArithmeticOperator.MULT,
            TypeExpr.MIN, ArithmeticOperator.MIN,
            TypeExpr.MAX, ArithmeticOperator.MAX,
            TypeExpr.AND, BooleanOperator.AND,
            TypeExpr.OR, BooleanOperator.OR,
            TypeExpr.XOR, BooleanOperator.XOR,
            TypeExpr.IFF, BooleanOperator.EQUIV,
            TypeExpr.EQ, RelationalOperator.EQ);

    /**
     * The adapted {@link XNode}.
     */
    private final XNode<?> adaptee;

    /**
     * Creates a new IntensionConstraintXNodeAdapter.
     *
     * @param adaptee The {@link XNode} to adapt.
     */
    IntensionConstraintXNodeAdapter(XNode<?> adaptee) {
        this.adaptee = adaptee;
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.constraints.intension.IIntensionConstraint#accept(org.sat4j.csp.
     * constraints.encoder.intension.IIntensionConstraintVisitor)
     */
    @Override
    public void accept(IIntensionConstraintVisitor visitor) throws ContradictionException {
        var type = adaptee.type;

        if (type == TypeExpr.LONG) {
            // The adapted node is a constant.
            var self = new ConstantIntensionConstraint(BigInteger.valueOf(adaptee.val(0)));
            self.accept(visitor);
            return;
        }

        if (type == TypeExpr.VAR) {
            // The adapted node is a variable.
            var self = new VariableIntensionConstraint(adaptee.var(0).id());
            self.accept(visitor);
            return;
        }

        if (type == TypeExpr.IF) {
            // The adapted node is an alternative.
            var adaptedCondition = new IntensionConstraintXNodeAdapter(adaptee.sons[0]);
            var adaptedIfTrue = new IntensionConstraintXNodeAdapter(adaptee.sons[1]);
            var adaptedIfFalse = new IntensionConstraintXNodeAdapter(adaptee.sons[2]);
            var self = new IfThenElseIntensionConstraint(adaptedCondition, adaptedIfTrue, adaptedIfFalse);
            self.accept(visitor);
            return;
        }

        var unary = UNARY_OPERATORS.get(type);
        if (unary != null) {
            // The adapted node is a unary constraint.
            var adaptedChild = new IntensionConstraintXNodeAdapter(adaptee.sons[0]);
            var self = new UnaryIntensionConstraint(unary, adaptedChild);
            self.accept(visitor);
            return;
        }

        var binary = BINARY_OPERATORS.get(type);
        if (binary != null) {
            // The adapted node is a binary constraint.
            var adaptedLeft = new IntensionConstraintXNodeAdapter(adaptee.sons[0]);
            var adaptedRight = new IntensionConstraintXNodeAdapter(adaptee.sons[1]);
            var self = new BinaryIntensionConstraint(binary, adaptedLeft, adaptedRight);
            self.accept(visitor);
            return;
        }

        var nary = NARY_OPERATORS.get(type);
        if (nary != null) {
            // The adapted node is an n-ary constraint.
            var adaptedChildren = Stream.of(adaptee.sons)
                    .map(IntensionConstraintXNodeAdapter::new)
                    .collect(toList());
            var self = new NaryIntensionConstraint(nary, adaptedChildren);
            self.accept(visitor);
            return;
        }

        // The node is of an unsupported type.
        throw new UnsupportedOperationException("Unknown intension constraint of type: " + type);
    }

}
