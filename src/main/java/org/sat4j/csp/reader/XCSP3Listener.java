/**
 * Sat4j-CSP, a CSP solver based on the Sat4j platform.
 * Copyright (c) 2021-2022 - Exakis Nelite, Univ Artois & CNRS.
 * All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 * If not, see {@link http://www.gnu.org/licenses}.
 */

package org.sat4j.csp.reader;

import java.math.BigInteger;

import org.sat4j.csp.constraints.ArithmeticOperator;
import org.sat4j.csp.constraints.BooleanOperator;
import org.sat4j.csp.constraints.RelationalOperator;
import org.sat4j.csp.constraints.SetBelongingOperator;
import org.sat4j.csp.constraints.intension.IIntensionConstraint;
import org.sat4j.csp.core.ICSPSolver;
import org.sat4j.specs.ContradictionException;
import org.sat4j.specs.IVec;
import org.sat4j.specs.IVecInt;

/**
 * The XCSP3Listener is an implementation of {@link IXCSP3Listener} that feeds the read
 * constraints to an {@link ICSPSolver}.
 *
 * @author Thibault Falque
 * @author Romain Wallon
 *
 * @version 0.1.0
 */
public class XCSP3Listener implements IXCSP3Listener {

    /**
     * The solver to which the read constraints must be added.
     */
    private ICSPSolver solver;

    /**
     * Creates a new XCSP3Listener.
     *
     * @param solver The solver to which the read constraints must be added.
     */
    public XCSP3Listener(ICSPSolver solver) {
        this.solver = solver;
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.reader.IXCSP3Listener#newVariable(java.lang.String, int, int)
     */
    @Override
    public void newVariable(String id, int min, int max) {
        solver.newVariable(id, min, max);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.reader.IXCSP3Listener#newVariable(java.lang.String,
     * java.math.BigInteger, java.math.BigInteger)
     */
    @Override
    public void newVariable(String id, BigInteger min, BigInteger max) {
        solver.newVariable(id, min, max);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.reader.IXCSP3Listener#newVariable(java.lang.String,
     * org.sat4j.specs.IVecInt)
     */
    @Override
    public void newVariable(String id, IVecInt values) {
        solver.newVariable(id, values);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.reader.IXCSP3Listener#newVariable(java.lang.String,
     * org.sat4j.specs.IVec)
     */
    @Override
    public void newVariable(String id, IVec<BigInteger> values) {
        solver.newVariable(id, values);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.reader.IXCSP3Listener#addInstantiation(java.lang.String, int)
     */
    @Override
    public void addInstantiation(String variable, int value) throws ContradictionException {
        solver.addInstantiation(variable, value);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.reader.IXCSP3Listener#addInstantiation(java.lang.String,
     * java.math.BigInteger)
     */
    @Override
    public void addInstantiation(String variable, BigInteger value) throws ContradictionException {
        solver.addInstantiation(variable, value);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.reader.IXCSP3Listener#addInstantiation(org.sat4j.specs.IVec,
     * org.sat4j.specs.IVecInt)
     */
    @Override
    public void addInstantiation(IVec<String> variables, IVecInt values)
            throws ContradictionException {
        solver.addInstantiation(variables, values);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.reader.IXCSP3Listener#addInstantiation(org.sat4j.specs.IVec,
     * org.sat4j.specs.IVec)
     */
    @Override
    public void addInstantiation(IVec<String> variables, IVec<BigInteger> values)
            throws ContradictionException {
        solver.addInstantiation(variables, values);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.reader.IXCSP3Listener#addClause(org.sat4j.specs.IVec,
     * org.sat4j.specs.IVec)
     */
    @Override
    public void addClause(IVec<String> positive, IVec<String> negative)
            throws ContradictionException {
        solver.addClause(positive, negative);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.reader.IXCSP3Listener#addLogical(org.sat4j.csp.constraints.
     * BooleanOperator, org.sat4j.specs.IVec)
     */
    @Override
    public void addLogical(BooleanOperator operator, IVec<String> variables)
            throws ContradictionException {
        solver.addLogical(operator, variables);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.reader.IXCSP3Listener#addLogical(java.lang.String, boolean,
     * org.sat4j.csp.constraints.BooleanOperator, org.sat4j.specs.IVec)
     */
    @Override
    public void addLogical(String variable, boolean equiv, BooleanOperator operator,
            IVec<String> variables) throws ContradictionException {
        solver.addLogical(variable, equiv, operator, variables);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.reader.IXCSP3Listener#addLogical(java.lang.String,
     * java.lang.String, org.sat4j.csp.constraints.RelationalOperator,
     * java.math.BigInteger)
     */
    @Override
    public void addLogical(String variable, String left, RelationalOperator operator,
            BigInteger right) throws ContradictionException {
        solver.addLogical(variable, left, operator, right);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.reader.IXCSP3Listener#addLogical(java.lang.String,
     * java.lang.String, org.sat4j.csp.constraints.RelationalOperator, java.lang.String)
     */
    @Override
    public void addLogical(String variable, String left, RelationalOperator operator, String right)
            throws ContradictionException {
        solver.addLogical(variable, left, operator, right);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.reader.IXCSP3Listener#addAllDifferent(org.sat4j.specs.IVec)
     */
    @Override
    public void addAllDifferent(IVec<String> variables) throws ContradictionException {
        solver.addAllDifferent(variables);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.reader.IXCSP3Listener#addAllDifferent(org.sat4j.specs.IVec,
     * org.sat4j.specs.IVec)
     */
    @Override
    public void addAllDifferent(IVec<String> variables, IVec<BigInteger> except)
            throws ContradictionException {
        solver.addAllDifferent(variables, except);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.sat4j.csp.reader.IXCSP3Listener#addAllDifferentMatrix(org.sat4j.specs.IVec)
     */
    @Override
    public void addAllDifferentMatrix(IVec<IVec<String>> variableMatrix)
            throws ContradictionException {
        solver.addAllDifferentMatrix(variableMatrix);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.sat4j.csp.reader.IXCSP3Listener#addAllDifferentMatrix(org.sat4j.specs.IVec,
     * org.sat4j.specs.IVec)
     */
    @Override
    public void addAllDifferentMatrix(IVec<IVec<String>> variableMatrix, IVec<BigInteger> except)
            throws ContradictionException {
        solver.addAllDifferentMatrix(variableMatrix, except);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.reader.IXCSP3Listener#addAllDifferentList(org.sat4j.specs.IVec)
     */
    @Override
    public void addAllDifferentList(IVec<IVec<String>> variableLists)
            throws ContradictionException {
        solver.addAllDifferentList(variableLists);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.sat4j.csp.reader.IXCSP3Listener#addAllDifferentIntension(org.sat4j.specs.IVec)
     */
    @Override
    public void addAllDifferentIntension(IVec<IIntensionConstraint> intensionConstraints)
            throws ContradictionException {
        solver.addAllDifferentIntension(intensionConstraints);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.reader.IXCSP3Listener#
     * addCardinalityWithConstantValuesAndConstantCounts(org.sat4j.specs.IVec,
     * org.sat4j.specs.IVec, org.sat4j.specs.IVec, boolean)
     */
    @Override
    public void addCardinalityWithConstantValuesAndConstantCounts(IVec<String> variables,
            IVec<BigInteger> values, IVec<BigInteger> occurs, boolean closed)
            throws ContradictionException {
        solver.addCardinalityWithConstantValuesAndConstantCounts(
                variables, values, occurs, closed);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.reader.IXCSP3Listener#
     * addCardinalityWithConstantValuesAndConstantIntervalCounts(org.sat4j.specs.IVec,
     * org.sat4j.specs.IVec, org.sat4j.specs.IVec, org.sat4j.specs.IVec, boolean)
     */
    @Override
    public void addCardinalityWithConstantValuesAndConstantIntervalCounts(IVec<String> variables,
            IVec<BigInteger> values, IVec<BigInteger> occursMin, IVec<BigInteger> occursMax,
            boolean closed) throws ContradictionException {
        solver.addCardinalityWithConstantValuesAndConstantIntervalCounts(
                variables, values, occursMin, occursMax, closed);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.reader.IXCSP3Listener#
     * addCardinalityWithConstantValuesAndVariableCounts(org.sat4j.specs.IVec,
     * org.sat4j.specs.IVec, org.sat4j.specs.IVec, boolean)
     */
    @Override
    public void addCardinalityWithConstantValuesAndVariableCounts(IVec<String> variables,
            IVec<BigInteger> values, IVec<String> occurs, boolean closed)
            throws ContradictionException {
        solver.addCardinalityWithConstantValuesAndVariableCounts(
                variables, values, occurs, closed);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.reader.IXCSP3Listener#
     * addCardinalityWithVariableValuesAndConstantCounts(org.sat4j.specs.IVec,
     * org.sat4j.specs.IVec, org.sat4j.specs.IVec, boolean)
     */
    @Override
    public void addCardinalityWithVariableValuesAndConstantCounts(IVec<String> variables,
            IVec<String> values, IVec<BigInteger> occurs, boolean closed)
            throws ContradictionException {
        solver.addCardinalityWithVariableValuesAndConstantCounts(
                variables, values, occurs, closed);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.reader.IXCSP3Listener#
     * addCardinalityWithVariableValuesAndConstantIntervalCounts(org.sat4j.specs.IVec,
     * org.sat4j.specs.IVec, org.sat4j.specs.IVec, org.sat4j.specs.IVec, boolean)
     */
    @Override
    public void addCardinalityWithVariableValuesAndConstantIntervalCounts(IVec<String> variables,
            IVec<String> values, IVec<BigInteger> occursMin, IVec<BigInteger> occursMax,
            boolean closed)
            throws ContradictionException {
        solver.addCardinalityWithVariableValuesAndConstantIntervalCounts(
                variables, values, occursMin, occursMax, closed);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.reader.IXCSP3Listener#
     * addCardinalityWithVariableValuesAndVariableCounts(org.sat4j.specs.IVec,
     * org.sat4j.specs.IVec, org.sat4j.specs.IVec, boolean)
     */
    @Override
    public void addCardinalityWithVariableValuesAndVariableCounts(IVec<String> variables,
            IVec<String> values, IVec<String> occurs, boolean closed)
            throws ContradictionException {
        solver.addCardinalityWithVariableValuesAndVariableCounts(
                variables, values, occurs, closed);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.reader.IXCSP3Listener#addChannel(org.sat4j.specs.IVec, int)
     */
    @Override
    public void addChannel(IVec<String> variables, int startIndex) throws ContradictionException {
        solver.addChannel(variables, startIndex);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.reader.IXCSP3Listener#addChannel(org.sat4j.specs.IVec, int,
     * java.lang.String)
     */
    @Override
    public void addChannel(IVec<String> variables, int startIndex, String value)
            throws ContradictionException {
        solver.addChannel(variables, startIndex, value);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.reader.IXCSP3Listener#addChannel(org.sat4j.specs.IVec, int,
     * org.sat4j.specs.IVec, int)
     */
    @Override
    public void addChannel(IVec<String> variables, int startIndex, IVec<String> otherVariables,
            int otherStartIndex) throws ContradictionException {
        solver.addChannel(variables, startIndex, otherVariables, otherStartIndex);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.reader.IXCSP3Listener#addAtLeast(org.sat4j.specs.IVec,
     * java.math.BigInteger, java.math.BigInteger)
     */
    @Override
    public void addAtLeast(IVec<String> variables, BigInteger value, BigInteger count)
            throws ContradictionException {
        solver.addAtLeast(variables, value, count);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.reader.IXCSP3Listener#addExactly(org.sat4j.specs.IVec,
     * java.math.BigInteger, java.math.BigInteger)
     */
    @Override
    public void addExactly(IVec<String> variables, BigInteger value, BigInteger count)
            throws ContradictionException {
        solver.addExactly(variables, value, count);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.reader.IXCSP3Listener#addExactly(org.sat4j.specs.IVec,
     * java.math.BigInteger, java.lang.String)
     */
    @Override
    public void addExactly(IVec<String> variables, BigInteger value, String count)
            throws ContradictionException {
        solver.addExactly(variables, value, count);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.reader.IXCSP3Listener#addAmong(org.sat4j.specs.IVec,
     * org.sat4j.specs.IVec, java.math.BigInteger)
     */
    @Override
    public void addAmong(IVec<String> variables, IVec<BigInteger> values, BigInteger count)
            throws ContradictionException {
        solver.addAmong(variables, values, count);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.reader.IXCSP3Listener#addAmong(org.sat4j.specs.IVec,
     * org.sat4j.specs.IVec, java.lang.String)
     */
    @Override
    public void addAmong(IVec<String> variables, IVec<BigInteger> values, String count)
            throws ContradictionException {
        solver.addAmong(variables, values, count);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.reader.IXCSP3Listener#addAtMost(org.sat4j.specs.IVec,
     * java.math.BigInteger, java.math.BigInteger)
     */
    @Override
    public void addAtMost(IVec<String> variables, BigInteger value, BigInteger count)
            throws ContradictionException {
        solver.addAtMost(variables, value, count);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.sat4j.csp.reader.IXCSP3Listener#addCountWithConstantValues(org.sat4j.specs.
     * IVec, org.sat4j.specs.IVec, org.sat4j.csp.constraints.RelationalOperator,
     * java.math.BigInteger)
     */
    @Override
    public void addCountWithConstantValues(IVec<String> variables, IVec<BigInteger> values,
            RelationalOperator operator, BigInteger count) throws ContradictionException {
        solver.addCountWithConstantValues(variables, values, operator, count);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.sat4j.csp.reader.IXCSP3Listener#addCountWithConstantValues(org.sat4j.specs.
     * IVec, org.sat4j.specs.IVec, org.sat4j.csp.constraints.RelationalOperator,
     * java.lang.String)
     */
    @Override
    public void addCountWithConstantValues(IVec<String> variables, IVec<BigInteger> values,
            RelationalOperator operator, String count) throws ContradictionException {
        solver.addCountWithConstantValues(variables, values, operator, count);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.sat4j.csp.reader.IXCSP3Listener#addCountWithVariableValues(org.sat4j.specs.
     * IVec, org.sat4j.specs.IVec, org.sat4j.csp.constraints.RelationalOperator,
     * java.math.BigInteger)
     */
    @Override
    public void addCountWithVariableValues(IVec<String> variables, IVec<String> values,
            RelationalOperator operator, BigInteger count) throws ContradictionException {
        solver.addCountWithVariableValues(variables, values, operator, count);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.sat4j.csp.reader.IXCSP3Listener#addCountWithVariableValues(org.sat4j.specs.
     * IVec, org.sat4j.specs.IVec, org.sat4j.csp.constraints.RelationalOperator,
     * java.lang.String)
     */
    @Override
    public void addCountWithVariableValues(IVec<String> variables, IVec<String> values,
            RelationalOperator operator, String count) throws ContradictionException {
        solver.addCountWithVariableValues(variables, values, operator, count);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.sat4j.csp.reader.IXCSP3Listener#addCountIntensionWithConstantValues(org.sat4j.
     * specs.IVec, org.sat4j.specs.IVec, org.sat4j.csp.constraints.RelationalOperator,
     * java.math.BigInteger)
     */
    @Override
    public void addCountIntensionWithConstantValues(IVec<IIntensionConstraint> expressions,
            IVec<BigInteger> values, RelationalOperator operator, BigInteger count)
            throws ContradictionException {
        solver.addCountIntensionWithConstantValues(expressions, values, operator, count);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.sat4j.csp.reader.IXCSP3Listener#addCountIntensionWithConstantValues(org.sat4j.
     * specs.IVec, org.sat4j.specs.IVec, org.sat4j.csp.constraints.RelationalOperator,
     * java.lang.String)
     */
    @Override
    public void addCountIntensionWithConstantValues(IVec<IIntensionConstraint> expressions,
            IVec<BigInteger> values, RelationalOperator operator, String count)
            throws ContradictionException {
        solver.addCountIntensionWithConstantValues(expressions, values, operator, count);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.sat4j.csp.reader.IXCSP3Listener#addCumulativeConstantLengthsConstantHeights(org
     * .sat4j.specs.IVec, org.sat4j.specs.IVec, org.sat4j.specs.IVec,
     * org.sat4j.csp.constraints.RelationalOperator, java.math.BigInteger)
     */
    @Override
    public void addCumulativeConstantLengthsConstantHeights(IVec<String> origins,
            IVec<BigInteger> lengths, IVec<BigInteger> heights, RelationalOperator operator,
            BigInteger value) throws ContradictionException {
        solver.addCumulativeConstantLengthsConstantHeights(
                origins, lengths, heights, operator, value);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.sat4j.csp.reader.IXCSP3Listener#addCumulativeConstantLengthsConstantHeights(org
     * .sat4j.specs.IVec, org.sat4j.specs.IVec, org.sat4j.specs.IVec,
     * org.sat4j.specs.IVec, org.sat4j.csp.constraints.RelationalOperator,
     * java.math.BigInteger)
     */
    @Override
    public void addCumulativeConstantLengthsConstantHeights(IVec<String> origins,
            IVec<BigInteger> lengths, IVec<String> ends, IVec<BigInteger> heights,
            RelationalOperator operator, BigInteger value) throws ContradictionException {
        solver.addCumulativeConstantLengthsConstantHeights(
                origins, lengths, ends, heights, operator, value);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.sat4j.csp.reader.IXCSP3Listener#addCumulativeConstantLengthsConstantHeights(org
     * .sat4j.specs.IVec, org.sat4j.specs.IVec, org.sat4j.specs.IVec,
     * org.sat4j.csp.constraints.RelationalOperator, java.lang.String)
     */
    @Override
    public void addCumulativeConstantLengthsConstantHeights(IVec<String> origins,
            IVec<BigInteger> lengths, IVec<BigInteger> heights, RelationalOperator operator,
            String value) throws ContradictionException {
        solver.addCumulativeConstantLengthsConstantHeights(
                origins, lengths, heights, operator, value);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.sat4j.csp.reader.IXCSP3Listener#addCumulativeConstantLengthsConstantHeights(org
     * .sat4j.specs.IVec, org.sat4j.specs.IVec, org.sat4j.specs.IVec,
     * org.sat4j.specs.IVec, org.sat4j.csp.constraints.RelationalOperator,
     * java.lang.String)
     */
    @Override
    public void addCumulativeConstantLengthsConstantHeights(IVec<String> origins,
            IVec<BigInteger> lengths, IVec<String> ends, IVec<BigInteger> heights,
            RelationalOperator operator, String value) throws ContradictionException {
        solver.addCumulativeConstantLengthsConstantHeights(
                origins, lengths, ends, heights, operator, value);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.sat4j.csp.reader.IXCSP3Listener#addCumulativeConstantLengthsVariableHeights(org
     * .sat4j.specs.IVec, org.sat4j.specs.IVec, org.sat4j.specs.IVec,
     * org.sat4j.csp.constraints.RelationalOperator, java.math.BigInteger)
     */
    @Override
    public void addCumulativeConstantLengthsVariableHeights(IVec<String> origins,
            IVec<BigInteger> lengths, IVec<String> heights, RelationalOperator operator,
            BigInteger value) throws ContradictionException {
        solver.addCumulativeConstantLengthsVariableHeights(
                origins, lengths, heights, operator, value);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.sat4j.csp.reader.IXCSP3Listener#addCumulativeConstantLengthsVariableHeights(org
     * .sat4j.specs.IVec, org.sat4j.specs.IVec, org.sat4j.specs.IVec,
     * org.sat4j.specs.IVec, org.sat4j.csp.constraints.RelationalOperator,
     * java.math.BigInteger)
     */
    @Override
    public void addCumulativeConstantLengthsVariableHeights(IVec<String> origins,
            IVec<BigInteger> lengths, IVec<String> ends, IVec<String> heights,
            RelationalOperator operator, BigInteger value) throws ContradictionException {
        solver.addCumulativeConstantLengthsVariableHeights(
                origins, lengths, ends, heights, operator, value);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.sat4j.csp.reader.IXCSP3Listener#addCumulativeConstantLengthsVariableHeights(org
     * .sat4j.specs.IVec, org.sat4j.specs.IVec, org.sat4j.specs.IVec,
     * org.sat4j.csp.constraints.RelationalOperator, java.lang.String)
     */
    @Override
    public void addCumulativeConstantLengthsVariableHeights(IVec<String> origins,
            IVec<BigInteger> lengths, IVec<String> heights, RelationalOperator operator,
            String value) throws ContradictionException {
        solver.addCumulativeConstantLengthsVariableHeights(
                origins, lengths, heights, operator, value);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.sat4j.csp.reader.IXCSP3Listener#addCumulativeConstantLengthsVariableHeights(org
     * .sat4j.specs.IVec, org.sat4j.specs.IVec, org.sat4j.specs.IVec,
     * org.sat4j.specs.IVec, org.sat4j.csp.constraints.RelationalOperator,
     * java.lang.String)
     */
    @Override
    public void addCumulativeConstantLengthsVariableHeights(IVec<String> origins,
            IVec<BigInteger> lengths, IVec<String> ends, IVec<String> heights,
            RelationalOperator operator, String value) throws ContradictionException {
        solver.addCumulativeConstantLengthsVariableHeights(
                origins, lengths, ends, heights, operator, value);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.sat4j.csp.reader.IXCSP3Listener#addCumulativeVariableLengthsConstantHeights(org
     * .sat4j.specs.IVec, org.sat4j.specs.IVec, org.sat4j.specs.IVec,
     * org.sat4j.csp.constraints.RelationalOperator, java.math.BigInteger)
     */
    @Override
    public void addCumulativeVariableLengthsConstantHeights(IVec<String> origins,
            IVec<String> lengths, IVec<BigInteger> heights, RelationalOperator operator,
            BigInteger value) throws ContradictionException {
        solver.addCumulativeVariableLengthsConstantHeights(
                origins, lengths, heights, operator, value);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.sat4j.csp.reader.IXCSP3Listener#addCumulativeVariableLengthsConstantHeights(org
     * .sat4j.specs.IVec, org.sat4j.specs.IVec, org.sat4j.specs.IVec,
     * org.sat4j.specs.IVec, org.sat4j.csp.constraints.RelationalOperator,
     * java.math.BigInteger)
     */
    @Override
    public void addCumulativeVariableLengthsConstantHeights(IVec<String> origins,
            IVec<String> lengths, IVec<String> ends, IVec<BigInteger> heights,
            RelationalOperator operator, BigInteger value) throws ContradictionException {
        solver.addCumulativeVariableLengthsConstantHeights(
                origins, lengths, ends, heights, operator, value);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.sat4j.csp.reader.IXCSP3Listener#addCumulativeVariableLengthsConstantHeights(org
     * .sat4j.specs.IVec, org.sat4j.specs.IVec, org.sat4j.specs.IVec,
     * org.sat4j.csp.constraints.RelationalOperator, java.lang.String)
     */
    @Override
    public void addCumulativeVariableLengthsConstantHeights(IVec<String> origins,
            IVec<String> lengths, IVec<BigInteger> heights, RelationalOperator operator,
            String value) throws ContradictionException {
        solver.addCumulativeVariableLengthsConstantHeights(
                origins, lengths, heights, operator, value);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.sat4j.csp.reader.IXCSP3Listener#addCumulativeVariableLengthsConstantHeights(org
     * .sat4j.specs.IVec, org.sat4j.specs.IVec, org.sat4j.specs.IVec,
     * org.sat4j.specs.IVec, org.sat4j.csp.constraints.RelationalOperator,
     * java.lang.String)
     */
    @Override
    public void addCumulativeVariableLengthsConstantHeights(IVec<String> origins,
            IVec<String> lengths, IVec<String> ends, IVec<BigInteger> heights,
            RelationalOperator operator, String value) throws ContradictionException {
        solver.addCumulativeVariableLengthsConstantHeights(
                origins, lengths, ends, heights, operator, value);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.sat4j.csp.reader.IXCSP3Listener#addCumulativeVariableLengthsVariableHeights(org
     * .sat4j.specs.IVec, org.sat4j.specs.IVec, org.sat4j.specs.IVec,
     * org.sat4j.csp.constraints.RelationalOperator, java.math.BigInteger)
     */
    @Override
    public void addCumulativeVariableLengthsVariableHeights(IVec<String> origins,
            IVec<String> lengths, IVec<String> heights, RelationalOperator operator,
            BigInteger value) throws ContradictionException {
        solver.addCumulativeVariableLengthsVariableHeights(
                origins, lengths, heights, operator, value);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.sat4j.csp.reader.IXCSP3Listener#addCumulativeVariableLengthsVariableHeights(org
     * .sat4j.specs.IVec, org.sat4j.specs.IVec, org.sat4j.specs.IVec,
     * org.sat4j.specs.IVec, org.sat4j.csp.constraints.RelationalOperator,
     * java.math.BigInteger)
     */
    @Override
    public void addCumulativeVariableLengthsVariableHeights(IVec<String> origins,
            IVec<String> lengths, IVec<String> ends, IVec<String> heights,
            RelationalOperator operator, BigInteger value) throws ContradictionException {
        solver.addCumulativeVariableLengthsVariableHeights(
                origins, lengths, ends, heights, operator, value);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.sat4j.csp.reader.IXCSP3Listener#addCumulativeVariableLengthsVariableHeights(org
     * .sat4j.specs.IVec, org.sat4j.specs.IVec, org.sat4j.specs.IVec,
     * org.sat4j.csp.constraints.RelationalOperator, java.lang.String)
     */
    @Override
    public void addCumulativeVariableLengthsVariableHeights(IVec<String> origins,
            IVec<String> lengths, IVec<String> heights, RelationalOperator operator, String value)
            throws ContradictionException {
        solver.addCumulativeVariableLengthsVariableHeights(
                origins, lengths, heights, operator, value);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.sat4j.csp.reader.IXCSP3Listener#addCumulativeVariableLengthsVariableHeights(org
     * .sat4j.specs.IVec, org.sat4j.specs.IVec, org.sat4j.specs.IVec,
     * org.sat4j.specs.IVec, org.sat4j.csp.constraints.RelationalOperator,
     * java.lang.String)
     */
    @Override
    public void addCumulativeVariableLengthsVariableHeights(IVec<String> origins,
            IVec<String> lengths, IVec<String> ends, IVec<String> heights,
            RelationalOperator operator, String value) throws ContradictionException {
        solver.addCumulativeVariableLengthsVariableHeights(
                origins, lengths, ends, heights, operator, value);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.reader.IXCSP3Listener#addElement(org.sat4j.specs.IVec,
     * java.math.BigInteger)
     */
    @Override
    public void addElement(IVec<String> variables, BigInteger value) throws ContradictionException {
        solver.addElement(variables, value);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.reader.IXCSP3Listener#addElement(org.sat4j.specs.IVec,
     * java.lang.String)
     */
    @Override
    public void addElement(IVec<String> variables, String value) throws ContradictionException {
        solver.addElement(variables, value);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.sat4j.csp.reader.IXCSP3Listener#addElementConstantValues(org.sat4j.specs.IVec,
     * int, java.lang.String, java.lang.String)
     */
    @Override
    public void addElementConstantValues(IVec<BigInteger> values, int startIndex, String index,
            String variable) throws ContradictionException {
        solver.addElementConstantValues(values, startIndex, index, variable);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.sat4j.csp.reader.IXCSP3Listener#addElementConstantValues(org.sat4j.specs.IVec,
     * int, java.lang.String, java.math.BigInteger)
     */
    @Override
    public void addElementConstantValues(IVec<BigInteger> values, int startIndex, String index,
            BigInteger value) throws ContradictionException {
        solver.addElementConstantValues(values, startIndex, index, value);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.reader.IXCSP3Listener#addElement(org.sat4j.specs.IVec, int,
     * java.lang.String, java.math.BigInteger)
     */
    @Override
    public void addElement(IVec<String> variables, int startIndex, String index, BigInteger value)
            throws ContradictionException {
        solver.addElement(variables, startIndex, index, value);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.reader.IXCSP3Listener#addElement(org.sat4j.specs.IVec, int,
     * java.lang.String, java.lang.String)
     */
    @Override
    public void addElement(IVec<String> values, int startIndex, String index, String variable)
            throws ContradictionException {
        solver.addElement(values, startIndex, index, variable);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.sat4j.csp.reader.IXCSP3Listener#addElementConstantMatrix(org.sat4j.specs.IVec,
     * int, java.lang.String, int, java.lang.String, java.math.BigInteger)
     */
    @Override
    public void addElementConstantMatrix(IVec<IVec<BigInteger>> matrix, int startRowIndex,
            String rowIndex, int startColIndex, String colIndex, BigInteger value)
            throws ContradictionException {
        solver.addElementConstantMatrix(
                matrix, startRowIndex, rowIndex, startColIndex, colIndex, value);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.sat4j.csp.reader.IXCSP3Listener#addElementConstantMatrix(org.sat4j.specs.IVec,
     * int, java.lang.String, int, java.lang.String, java.lang.String)
     */
    @Override
    public void addElementConstantMatrix(IVec<IVec<BigInteger>> matrix, int startRowIndex,
            String rowIndex, int startColIndex, String colIndex, String value)
            throws ContradictionException {
        solver.addElementConstantMatrix(
                matrix, startRowIndex, rowIndex, startColIndex, colIndex, value);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.reader.IXCSP3Listener#addElementMatrix(org.sat4j.specs.IVec,
     * int, java.lang.String, int, java.lang.String, java.math.BigInteger)
     */
    @Override
    public void addElementMatrix(IVec<IVec<String>> matrix, int startRowIndex, String rowIndex,
            int startColIndex, String colIndex, BigInteger value) throws ContradictionException {
        solver.addElementMatrix(matrix, startRowIndex, rowIndex, startColIndex, colIndex, value);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.reader.IXCSP3Listener#addElementMatrix(org.sat4j.specs.IVec,
     * int, java.lang.String, int, java.lang.String, java.lang.String)
     */
    @Override
    public void addElementMatrix(IVec<IVec<String>> matrix, int startRowIndex, String rowIndex,
            int startColIndex, String colIndex, String value) throws ContradictionException {
        solver.addElementMatrix(matrix, startRowIndex, rowIndex, startColIndex, colIndex, value);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.reader.IXCSP3Listener#addSupport(java.lang.String,
     * org.sat4j.specs.IVec)
     */
    @Override
    public void addSupport(String variable, IVec<BigInteger> allowedValues)
            throws ContradictionException {
        solver.addSupport(variable, allowedValues);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.reader.IXCSP3Listener#addSupport(org.sat4j.specs.IVec,
     * org.sat4j.specs.IVec)
     */
    @Override
    public void addSupport(IVec<String> variableTuple, IVec<IVec<BigInteger>> allowedValues)
            throws ContradictionException {
        solver.addSupport(variableTuple, allowedValues);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.reader.IXCSP3Listener#addConflicts(java.lang.String,
     * org.sat4j.specs.IVec)
     */
    @Override
    public void addConflicts(String variable, IVec<BigInteger> forbiddenValues)
            throws ContradictionException {
        solver.addConflicts(variable, forbiddenValues);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.reader.IXCSP3Listener#addConflicts(org.sat4j.specs.IVec,
     * org.sat4j.specs.IVec)
     */
    @Override
    public void addConflicts(IVec<String> variableTuple, IVec<IVec<BigInteger>> forbiddenValues)
            throws ContradictionException {
        solver.addConflicts(variableTuple, forbiddenValues);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.reader.IXCSP3Listener#addIntension(org.sat4j.csp.constraints.
     * intension.IIntensionConstraint)
     */
    @Override
    public void addIntension(IIntensionConstraint constr) throws ContradictionException {
        solver.addIntension(constr);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.reader.IXCSP3Listener#addPrimitive(java.lang.String,
     * org.sat4j.csp.constraints.RelationalOperator, java.math.BigInteger)
     */
    @Override
    public void addPrimitive(String variable, RelationalOperator operator, BigInteger value)
            throws ContradictionException {
        solver.addPrimitive(variable, operator, value);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.reader.IXCSP3Listener#addPrimitive(java.lang.String,
     * org.sat4j.csp.constraints.ArithmeticOperator, java.math.BigInteger,
     * org.sat4j.csp.constraints.RelationalOperator, java.math.BigInteger)
     */
    @Override
    public void addPrimitive(String variable, ArithmeticOperator arithOp, BigInteger leftHandSide,
            RelationalOperator relOp, BigInteger rightHandSide) throws ContradictionException {
        solver.addPrimitive(variable, arithOp, leftHandSide, relOp, rightHandSide);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.reader.IXCSP3Listener#addPrimitive(java.lang.String,
     * org.sat4j.csp.constraints.ArithmeticOperator, java.lang.String,
     * org.sat4j.csp.constraints.RelationalOperator, java.math.BigInteger)
     */
    @Override
    public void addPrimitive(String variable, ArithmeticOperator arithOp, String leftHandSide,
            RelationalOperator relOp, BigInteger rightHandSide) throws ContradictionException {
        solver.addPrimitive(variable, arithOp, leftHandSide, relOp, rightHandSide);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.reader.IXCSP3Listener#addPrimitive(java.lang.String,
     * org.sat4j.csp.constraints.ArithmeticOperator, java.math.BigInteger,
     * org.sat4j.csp.constraints.RelationalOperator, java.lang.String)
     */
    @Override
    public void addPrimitive(String variable, ArithmeticOperator arithOp, BigInteger leftHandSide,
            RelationalOperator relOp, String rightHandSide) throws ContradictionException {
        solver.addPrimitive(variable, arithOp, leftHandSide, relOp, rightHandSide);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.reader.IXCSP3Listener#addPrimitive(java.lang.String,
     * org.sat4j.csp.constraints.ArithmeticOperator, java.lang.String,
     * org.sat4j.csp.constraints.RelationalOperator, java.lang.String)
     */
    @Override
    public void addPrimitive(String variable, ArithmeticOperator arithOp, String leftHandSide,
            RelationalOperator relOp, String rightHandSide) throws ContradictionException {
        solver.addPrimitive(variable, arithOp, leftHandSide, relOp, rightHandSide);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.reader.IXCSP3Listener#addPrimitive(org.sat4j.csp.constraints.
     * ArithmeticOperator, java.lang.String, java.lang.String)
     */
    @Override
    public void addPrimitive(ArithmeticOperator arithOp, String variable, String rightHandSide)
            throws ContradictionException {
        solver.addPrimitive(arithOp, variable, rightHandSide);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.reader.IXCSP3Listener#addPrimitive(java.lang.String,
     * org.sat4j.csp.constraints.SetBelongingOperator, org.sat4j.specs.IVec)
     */
    @Override
    public void addPrimitive(String variable, SetBelongingOperator operator,
            IVec<BigInteger> values) throws ContradictionException {
        solver.addPrimitive(variable, operator, values);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.reader.IXCSP3Listener#addPrimitive(java.lang.String,
     * org.sat4j.csp.constraints.SetBelongingOperator, java.math.BigInteger,
     * java.math.BigInteger)
     */
    @Override
    public void addPrimitive(String variable, SetBelongingOperator operator, BigInteger min,
            BigInteger max) throws ContradictionException {
        solver.addPrimitive(variable, operator, min, max);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.reader.IXCSP3Listener#addMinimum(org.sat4j.specs.IVec,
     * org.sat4j.csp.constraints.RelationalOperator, java.math.BigInteger)
     */
    @Override
    public void addMinimum(IVec<String> variables, RelationalOperator operator, BigInteger value)
            throws ContradictionException {
        solver.addMinimum(variables, operator, value);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.reader.IXCSP3Listener#addMinimumIntension(org.sat4j.specs.IVec,
     * org.sat4j.csp.constraints.RelationalOperator, java.math.BigInteger)
     */
    @Override
    public void addMinimumIntension(IVec<IIntensionConstraint> intensionConstraints,
            RelationalOperator operator, BigInteger value) throws ContradictionException {
        solver.addMinimumIntension(intensionConstraints, operator, value);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.reader.IXCSP3Listener#addMinimumIntension(org.sat4j.specs.IVec,
     * org.sat4j.csp.constraints.RelationalOperator, java.lang.String)
     */
    @Override
    public void addMinimumIntension(IVec<IIntensionConstraint> intensionConstraints,
            RelationalOperator operator, String value) throws ContradictionException {
        solver.addMinimumIntension(intensionConstraints, operator, value);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.reader.IXCSP3Listener#addMinimum(org.sat4j.specs.IVec,
     * org.sat4j.csp.constraints.RelationalOperator, java.lang.String)
     */
    @Override
    public void addMinimum(IVec<String> variables, RelationalOperator operator, String value)
            throws ContradictionException {
        solver.addMinimum(variables, operator, value);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.reader.IXCSP3Listener#addMaximum(org.sat4j.specs.IVec,
     * org.sat4j.csp.constraints.RelationalOperator, java.math.BigInteger)
     */
    @Override
    public void addMaximum(IVec<String> variables, RelationalOperator operator, BigInteger value)
            throws ContradictionException {
        solver.addMaximum(variables, operator, value);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.reader.IXCSP3Listener#addMaximumIntension(org.sat4j.specs.IVec,
     * org.sat4j.csp.constraints.RelationalOperator, java.math.BigInteger)
     */
    @Override
    public void addMaximumIntension(IVec<IIntensionConstraint> intensionConstraints,
            RelationalOperator operator, BigInteger value) throws ContradictionException {
        solver.addMaximumIntension(intensionConstraints, operator, value);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.reader.IXCSP3Listener#addMaximumIntension(org.sat4j.specs.IVec,
     * org.sat4j.csp.constraints.RelationalOperator, java.lang.String)
     */
    @Override
    public void addMaximumIntension(IVec<IIntensionConstraint> intensionConstraints,
            RelationalOperator operator, String value) throws ContradictionException {
        solver.addMaximumIntension(intensionConstraints, operator, value);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.reader.IXCSP3Listener#addMaximum(org.sat4j.specs.IVec,
     * org.sat4j.csp.constraints.RelationalOperator, java.lang.String)
     */
    @Override
    public void addMaximum(IVec<String> variables, RelationalOperator operator, String value)
            throws ContradictionException {
        solver.addMaximum(variables, operator, value);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.reader.IXCSP3Listener#addNoOverlap(org.sat4j.specs.IVec,
     * org.sat4j.specs.IVec)
     */
    @Override
    public void addNoOverlap(IVec<String> variables, IVec<BigInteger> length)
            throws ContradictionException {
        solver.addNoOverlap(variables, length);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.reader.IXCSP3Listener#addNoOverlap(org.sat4j.specs.IVec,
     * org.sat4j.specs.IVec, boolean)
     */
    @Override
    public void addNoOverlap(IVec<String> variables, IVec<BigInteger> length, boolean zeroIgnored)
            throws ContradictionException {
        solver.addNoOverlap(variables, length, zeroIgnored);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.sat4j.csp.reader.IXCSP3Listener#addNoOverlapVariableLength(org.sat4j.specs.
     * IVec, org.sat4j.specs.IVec)
     */
    @Override
    public void addNoOverlapVariableLength(IVec<String> variables, IVec<String> length)
            throws ContradictionException {
        solver.addNoOverlapVariableLength(variables, length);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.sat4j.csp.reader.IXCSP3Listener#addNoOverlapVariableLength(org.sat4j.specs.
     * IVec, org.sat4j.specs.IVec, boolean)
     */
    @Override
    public void addNoOverlapVariableLength(IVec<String> variables, IVec<String> length,
            boolean zeroIgnored) throws ContradictionException {
        solver.addNoOverlapVariableLength(variables, length, zeroIgnored);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.sat4j.csp.reader.IXCSP3Listener#addeMultiDimensionalNoOverlap(org.sat4j.specs.
     * IVec, org.sat4j.specs.IVec)
     */
    @Override
    public void addeMultiDimensionalNoOverlap(IVec<IVec<String>> variables,
            IVec<IVec<BigInteger>> length) throws ContradictionException {
        solver.addMultiDimensionalNoOverlap(variables, length);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.sat4j.csp.reader.IXCSP3Listener#addMultiDimensionalNoOverlap(org.sat4j.specs.
     * IVec, org.sat4j.specs.IVec, boolean)
     */
    @Override
    public void addMultiDimensionalNoOverlap(IVec<IVec<String>> variables,
            IVec<IVec<BigInteger>> length, boolean zeroIgnored) throws ContradictionException {
        solver.addMultiDimensionalNoOverlap(variables, length, zeroIgnored);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.sat4j.csp.reader.IXCSP3Listener#addMultiDimensionalNoOverlapVariableLength(org.
     * sat4j.specs.IVec, org.sat4j.specs.IVec)
     */
    @Override
    public void addMultiDimensionalNoOverlapVariableLength(IVec<IVec<String>> variables,
            IVec<IVec<String>> length) throws ContradictionException {
        solver.addMultiDimensionalNoOverlapVariableLength(variables, length);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.sat4j.csp.reader.IXCSP3Listener#addMultiDimensionalNoOverlapVariableLength(org.
     * sat4j.specs.IVec, org.sat4j.specs.IVec, boolean)
     */
    @Override
    public void addMultiDimensionalNoOverlapVariableLength(IVec<IVec<String>> variables,
            IVec<IVec<String>> length, boolean zeroIgnored) throws ContradictionException {
        solver.addMultiDimensionalNoOverlapVariableLength(variables, length, zeroIgnored);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.reader.IXCSP3Listener#addNValues(org.sat4j.specs.IVec,
     * org.sat4j.csp.constraints.RelationalOperator, java.math.BigInteger)
     */
    @Override
    public void addNValues(IVec<String> variables, RelationalOperator operator, BigInteger nb)
            throws ContradictionException {
        solver.addNValues(variables, operator, nb);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.reader.IXCSP3Listener#addNValuesExcept(org.sat4j.specs.IVec,
     * org.sat4j.csp.constraints.RelationalOperator, java.math.BigInteger,
     * org.sat4j.specs.IVec)
     */
    @Override
    public void addNValuesExcept(IVec<String> variables, RelationalOperator operator, BigInteger nb,
            IVec<BigInteger> except) throws ContradictionException {
        solver.addNValuesExcept(variables, operator, nb, except);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.reader.IXCSP3Listener#addNValues(org.sat4j.specs.IVec,
     * org.sat4j.csp.constraints.RelationalOperator, java.lang.String)
     */
    @Override
    public void addNValues(IVec<String> variables, RelationalOperator operator, String nb)
            throws ContradictionException {
        solver.addNValues(variables, operator, nb);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.reader.IXCSP3Listener#addNValuesExcept(org.sat4j.specs.IVec,
     * org.sat4j.csp.constraints.RelationalOperator, java.lang.String,
     * org.sat4j.specs.IVec)
     */
    @Override
    public void addNValuesExcept(IVec<String> variables, RelationalOperator operator, String nb,
            IVec<BigInteger> except) throws ContradictionException {
        solver.addNValuesExcept(variables, operator, nb, except);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.reader.IXCSP3Listener#addNValuesIntension(org.sat4j.specs.IVec,
     * org.sat4j.csp.constraints.RelationalOperator, java.math.BigInteger)
     */
    @Override
    public void addNValuesIntension(IVec<IIntensionConstraint> expressions,
            RelationalOperator operator, BigInteger nb) throws ContradictionException {
        solver.addNValuesIntension(expressions, operator, nb);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.reader.IXCSP3Listener#addNValuesIntension(org.sat4j.specs.IVec,
     * org.sat4j.csp.constraints.RelationalOperator, java.lang.String)
     */
    @Override
    public void addNValuesIntension(IVec<IIntensionConstraint> expressions,
            RelationalOperator operator, String nb) throws ContradictionException {
        solver.addNValuesIntension(expressions, operator, nb);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.reader.IXCSP3Listener#addOrdered(org.sat4j.specs.IVec,
     * org.sat4j.csp.constraints.RelationalOperator)
     */
    @Override
    public void addOrdered(IVec<String> variables, RelationalOperator operator)
            throws ContradictionException {
        solver.addOrdered(variables, operator);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.sat4j.csp.reader.IXCSP3Listener#addOrderedWithConstantLength(org.sat4j.specs.
     * IVec, org.sat4j.specs.IVec, org.sat4j.csp.constraints.RelationalOperator)
     */
    @Override
    public void addOrderedWithConstantLength(IVec<String> variables, IVec<BigInteger> lengths,
            RelationalOperator operator) throws ContradictionException {
        solver.addOrderedWithConstantLength(variables, lengths, operator);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.sat4j.csp.reader.IXCSP3Listener#addOrderedWithVariableLength(org.sat4j.specs.
     * IVec, org.sat4j.specs.IVec, org.sat4j.csp.constraints.RelationalOperator)
     */
    @Override
    public void addOrderedWithVariableLength(IVec<String> variables, IVec<String> lengths,
            RelationalOperator operator) throws ContradictionException {
        solver.addOrderedWithVariableLength(variables, lengths, operator);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.reader.IXCSP3Listener#addAllEqual(org.sat4j.specs.IVec)
     */
    @Override
    public void addAllEqual(IVec<String> variables) throws ContradictionException {
        solver.addAllEqual(variables);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.reader.IXCSP3Listener#addAllEqualIntension(org.sat4j.specs.IVec)
     */
    @Override
    public void addAllEqualIntension(IVec<IIntensionConstraint> expressions)
            throws ContradictionException {
        solver.addAllEqualIntension(expressions);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.reader.IXCSP3Listener#addNotAllEqual(org.sat4j.specs.IVec)
     */
    @Override
    public void addNotAllEqual(IVec<String> variables) throws ContradictionException {
        solver.addNotAllEqual(variables);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.reader.IXCSP3Listener#addLex(org.sat4j.specs.IVec,
     * org.sat4j.csp.constraints.RelationalOperator)
     */
    @Override
    public void addLex(IVec<IVec<String>> tuples, RelationalOperator operator)
            throws ContradictionException {
        solver.addLex(tuples, operator);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.reader.IXCSP3Listener#addLexMatrix(org.sat4j.specs.IVec,
     * org.sat4j.csp.constraints.RelationalOperator)
     */
    @Override
    public void addLexMatrix(IVec<IVec<String>> matrix, RelationalOperator operator)
            throws ContradictionException {
        solver.addLexMatrix(matrix, operator);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.reader.IXCSP3Listener#addSum(org.sat4j.specs.IVec,
     * org.sat4j.csp.constraints.RelationalOperator, java.math.BigInteger)
     */
    @Override
    public void addSum(IVec<String> variables, RelationalOperator operator, BigInteger value)
            throws ContradictionException {
        solver.addSum(variables, operator, value);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.reader.IXCSP3Listener#addSum(org.sat4j.specs.IVec,
     * org.sat4j.specs.IVec, org.sat4j.csp.constraints.RelationalOperator,
     * java.math.BigInteger)
     */
    @Override
    public void addSum(IVec<String> variables, IVec<BigInteger> coefficients,
            RelationalOperator operator, BigInteger value) throws ContradictionException {
        solver.addSum(variables, coefficients, operator, value);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.reader.IXCSP3Listener#addSum(org.sat4j.specs.IVec,
     * org.sat4j.csp.constraints.RelationalOperator, java.lang.String)
     */
    @Override
    public void addSum(IVec<String> variables, RelationalOperator operator, String rightVariable)
            throws ContradictionException {
        solver.addSum(variables, operator, rightVariable);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.reader.IXCSP3Listener#addSum(org.sat4j.specs.IVec,
     * org.sat4j.specs.IVec, org.sat4j.csp.constraints.RelationalOperator,
     * java.lang.String)
     */
    @Override
    public void addSum(IVec<String> variables, IVec<BigInteger> coefficients,
            RelationalOperator operator, String rightVariable) throws ContradictionException {
        solver.addSum(variables, coefficients, operator, rightVariable);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.reader.IXCSP3Listener#addSumIntension(org.sat4j.specs.IVec,
     * org.sat4j.csp.constraints.RelationalOperator, java.math.BigInteger)
     */
    @Override
    public void addSumIntension(IVec<IIntensionConstraint> intensionConstraints,
            RelationalOperator operator, BigInteger value) throws ContradictionException {
        solver.addSumIntension(intensionConstraints, operator, value);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.reader.IXCSP3Listener#addSumIntension(org.sat4j.specs.IVec,
     * org.sat4j.specs.IVec, org.sat4j.csp.constraints.RelationalOperator,
     * java.math.BigInteger)
     */
    @Override
    public void addSumIntension(IVec<IIntensionConstraint> intensionConstraints,
            IVec<BigInteger> coefficients, RelationalOperator operator, BigInteger value)
            throws ContradictionException {
        solver.addSumIntension(intensionConstraints, coefficients, operator, value);

    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.reader.IXCSP3Listener#addSumIntension(org.sat4j.specs.IVec,
     * org.sat4j.csp.constraints.RelationalOperator, java.lang.String)
     */
    @Override
    public void addSumIntension(IVec<IIntensionConstraint> intensionConstraints,
            RelationalOperator operator, String rightVariable) throws ContradictionException {
        solver.addSumIntension(intensionConstraints, operator, rightVariable);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.reader.IXCSP3Listener#addSumIntension(org.sat4j.specs.IVec,
     * org.sat4j.specs.IVec, org.sat4j.csp.constraints.RelationalOperator,
     * java.lang.String)
     */
    @Override
    public void addSumIntension(IVec<IIntensionConstraint> intensionConstraints,
            IVec<BigInteger> coefficients, RelationalOperator operator, String rightVariable)
            throws ContradictionException {
        solver.addSumIntension(intensionConstraints, coefficients, operator, rightVariable);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.sat4j.csp.reader.IXCSP3Listener#addSumWithVariableCoefficients(org.sat4j.specs.
     * IVec, org.sat4j.specs.IVec, org.sat4j.csp.constraints.RelationalOperator,
     * java.math.BigInteger)
     */
    @Override
    public void addSumWithVariableCoefficients(IVec<String> variables, IVec<String> coefficients,
            RelationalOperator operator, BigInteger value) throws ContradictionException {
        solver.addSumWithVariableCoefficients(variables, coefficients, operator, value);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.sat4j.csp.reader.IXCSP3Listener#addSumWithVariableCoefficients(org.sat4j.specs.
     * IVec, org.sat4j.specs.IVec, org.sat4j.csp.constraints.RelationalOperator,
     * java.lang.String)
     */
    @Override
    public void addSumWithVariableCoefficients(IVec<String> variables, IVec<String> coefficients,
            RelationalOperator operator, String rightVariable) throws ContradictionException {
        solver.addSumWithVariableCoefficients(variables, coefficients, operator, rightVariable);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.sat4j.csp.reader.IXCSP3Listener#addSumIntensionWithVariableCoefficients(org.
     * sat4j.specs.IVec, org.sat4j.specs.IVec,
     * org.sat4j.csp.constraints.RelationalOperator, java.math.BigInteger)
     */
    @Override
    public void addSumIntensionWithVariableCoefficients(
            IVec<IIntensionConstraint> intensionConstraints, IVec<String> coefficients,
            RelationalOperator operator, BigInteger value) throws ContradictionException {
        solver.addSumIntensionWithVariableCoefficients(
                intensionConstraints, coefficients, operator, value);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.sat4j.csp.reader.IXCSP3Listener#addSumIntensionWithVariableCoefficients(org.
     * sat4j.specs.IVec, org.sat4j.specs.IVec,
     * org.sat4j.csp.constraints.RelationalOperator, java.lang.String)
     */
    @Override
    public void addSumIntensionWithVariableCoefficients(
            IVec<IIntensionConstraint> intensionConstraints, IVec<String> coefficients,
            RelationalOperator operator, String rightVariable) throws ContradictionException {
        solver.addSumIntensionWithVariableCoefficients(
                intensionConstraints, coefficients, operator, rightVariable);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.reader.IXCSP3Listener#minimizeVariable(java.lang.String)
     */
    @Override
    public void minimizeVariable(String variable) {
        solver.minimizeVariable(variable);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.reader.IXCSP3Listener#maximizeVariable(java.lang.String)
     */
    @Override
    public void maximizeVariable(String variable) {
        solver.maximizeVariable(variable);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.sat4j.csp.reader.IXCSP3Listener#minimizeExpression(org.sat4j.csp.constraints.
     * intension.IIntensionConstraint)
     */
    @Override
    public void minimizeExpression(IIntensionConstraint expression) {
        solver.minimizeExpression(expression);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.sat4j.csp.reader.IXCSP3Listener#maximizeExpression(org.sat4j.csp.constraints.
     * intension.IIntensionConstraint)
     */
    @Override
    public void maximizeExpression(IIntensionConstraint expression) {
        solver.maximizeExpression(expression);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.reader.IXCSP3Listener#minimizeSum(org.sat4j.specs.IVec)
     */
    @Override
    public void minimizeSum(IVec<String> variables) {
        solver.minimizeSum(variables);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.reader.IXCSP3Listener#minimizeSum(org.sat4j.specs.IVec,
     * org.sat4j.specs.IVec)
     */
    @Override
    public void minimizeSum(IVec<String> variables, IVec<BigInteger> coefficients) {
        solver.minimizeSum(variables, coefficients);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.sat4j.csp.reader.IXCSP3Listener#minimizeExpressionSum(org.sat4j.specs.IVec)
     */
    @Override
    public void minimizeExpressionSum(IVec<IIntensionConstraint> expressions) {
        solver.minimizeExpressionSum(expressions);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.sat4j.csp.reader.IXCSP3Listener#minimizeExpressionSum(org.sat4j.specs.IVec,
     * org.sat4j.specs.IVec)
     */
    @Override
    public void minimizeExpressionSum(IVec<IIntensionConstraint> expressions,
            IVec<BigInteger> coefficients) {
        solver.minimizeExpressionSum(expressions, coefficients);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.reader.IXCSP3Listener#maximizeSum(org.sat4j.specs.IVec)
     */
    @Override
    public void maximizeSum(IVec<String> variables) {
        solver.maximizeSum(variables);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.reader.IXCSP3Listener#maximizeSum(org.sat4j.specs.IVec,
     * org.sat4j.specs.IVec)
     */
    @Override
    public void maximizeSum(IVec<String> variables, IVec<BigInteger> coefficients) {
        solver.maximizeSum(variables, coefficients);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.sat4j.csp.reader.IXCSP3Listener#maximizeExpressionSum(org.sat4j.specs.IVec)
     */
    @Override
    public void maximizeExpressionSum(IVec<IIntensionConstraint> expressions) {
        solver.maximizeExpressionSum(expressions);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.sat4j.csp.reader.IXCSP3Listener#maximizeExpressionSum(org.sat4j.specs.IVec,
     * org.sat4j.specs.IVec)
     */
    @Override
    public void maximizeExpressionSum(IVec<IIntensionConstraint> expressions,
            IVec<BigInteger> coefficients) {
        solver.maximizeExpressionSum(expressions, coefficients);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.reader.IXCSP3Listener#minimizeProduct(org.sat4j.specs.IVec)
     */
    @Override
    public void minimizeProduct(IVec<String> variables) {
        solver.minimizeProduct(variables);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.reader.IXCSP3Listener#minimizeProduct(org.sat4j.specs.IVec,
     * org.sat4j.specs.IVec)
     */
    @Override
    public void minimizeProduct(IVec<String> variables, IVec<BigInteger> coefficients) {
        solver.minimizeProduct(variables, coefficients);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.sat4j.csp.reader.IXCSP3Listener#minimizeExpressionProduct(org.sat4j.specs.IVec)
     */
    @Override
    public void minimizeExpressionProduct(IVec<IIntensionConstraint> expressions) {
        solver.minimizeExpressionProduct(expressions);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.sat4j.csp.reader.IXCSP3Listener#minimizeExpressionProduct(org.sat4j.specs.IVec,
     * org.sat4j.specs.IVec)
     */
    @Override
    public void minimizeExpressionProduct(IVec<IIntensionConstraint> expressions,
            IVec<BigInteger> coefficients) {
        solver.minimizeExpressionProduct(expressions, coefficients);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.reader.IXCSP3Listener#maximizeProduct(org.sat4j.specs.IVec)
     */
    @Override
    public void maximizeProduct(IVec<String> variables) {
        solver.maximizeProduct(variables);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.reader.IXCSP3Listener#maximizeProduct(org.sat4j.specs.IVec,
     * org.sat4j.specs.IVec)
     */
    @Override
    public void maximizeProduct(IVec<String> variables, IVec<BigInteger> coefficients) {
        solver.maximizeProduct(variables, coefficients);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.sat4j.csp.reader.IXCSP3Listener#maximizeExpressionProduct(org.sat4j.specs.IVec)
     */
    @Override
    public void maximizeExpressionProduct(IVec<IIntensionConstraint> expressions) {
        solver.maximizeExpressionProduct(expressions);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.sat4j.csp.reader.IXCSP3Listener#maximizeExpressionProduct(org.sat4j.specs.IVec,
     * org.sat4j.specs.IVec)
     */
    @Override
    public void maximizeExpressionProduct(IVec<IIntensionConstraint> expressions,
            IVec<BigInteger> coefficients) {
        solver.maximizeExpressionProduct(expressions, coefficients);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.reader.IXCSP3Listener#minimizeMinimum(org.sat4j.specs.IVec)
     */
    @Override
    public void minimizeMinimum(IVec<String> variables) {
        solver.minimizeMinimum(variables);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.reader.IXCSP3Listener#minimizeMinimum(org.sat4j.specs.IVec,
     * org.sat4j.specs.IVec)
     */
    @Override
    public void minimizeMinimum(IVec<String> variables, IVec<BigInteger> coefficients) {
        solver.minimizeMinimum(variables, coefficients);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.sat4j.csp.reader.IXCSP3Listener#minimizeExpressionMinimum(org.sat4j.specs.IVec)
     */
    @Override
    public void minimizeExpressionMinimum(IVec<IIntensionConstraint> expressions) {
        solver.minimizeExpressionMinimum(expressions);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.sat4j.csp.reader.IXCSP3Listener#minimizeExpressionMinimum(org.sat4j.specs.IVec,
     * org.sat4j.specs.IVec)
     */
    @Override
    public void minimizeExpressionMinimum(IVec<IIntensionConstraint> expressions,
            IVec<BigInteger> coefficients) {
        solver.minimizeExpressionMinimum(expressions, coefficients);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.reader.IXCSP3Listener#maximizeMinimum(org.sat4j.specs.IVec)
     */
    @Override
    public void maximizeMinimum(IVec<String> variables) {
        solver.maximizeMinimum(variables);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.reader.IXCSP3Listener#maximizeMinimum(org.sat4j.specs.IVec,
     * org.sat4j.specs.IVec)
     */
    @Override
    public void maximizeMinimum(IVec<String> variables, IVec<BigInteger> coefficients) {
        solver.maximizeMinimum(variables, coefficients);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.sat4j.csp.reader.IXCSP3Listener#maximizeExpressionMinimum(org.sat4j.specs.IVec)
     */
    @Override
    public void maximizeExpressionMinimum(IVec<IIntensionConstraint> expressions) {
        solver.maximizeExpressionMinimum(expressions);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.sat4j.csp.reader.IXCSP3Listener#maximizeExpressionMinimum(org.sat4j.specs.IVec,
     * org.sat4j.specs.IVec)
     */
    @Override
    public void maximizeExpressionMinimum(IVec<IIntensionConstraint> expressions,
            IVec<BigInteger> coefficients) {
        solver.maximizeExpressionMinimum(expressions, coefficients);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.reader.IXCSP3Listener#minimizeMaximum(org.sat4j.specs.IVec)
     */
    @Override
    public void minimizeMaximum(IVec<String> variables) {
        solver.minimizeMaximum(variables);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.reader.IXCSP3Listener#minimizeMaximum(org.sat4j.specs.IVec,
     * org.sat4j.specs.IVec)
     */
    @Override
    public void minimizeMaximum(IVec<String> variables, IVec<BigInteger> coefficients) {
        solver.minimizeMaximum(variables, coefficients);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.sat4j.csp.reader.IXCSP3Listener#minimizeExpressionMaximum(org.sat4j.specs.IVec)
     */
    @Override
    public void minimizeExpressionMaximum(IVec<IIntensionConstraint> expressions) {
        solver.minimizeExpressionMaximum(expressions);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.sat4j.csp.reader.IXCSP3Listener#minimizeExpressionMaximum(org.sat4j.specs.IVec,
     * org.sat4j.specs.IVec)
     */
    @Override
    public void minimizeExpressionMaximum(IVec<IIntensionConstraint> expressions,
            IVec<BigInteger> coefficients) {
        solver.minimizeExpressionMaximum(expressions, coefficients);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.reader.IXCSP3Listener#maximizeMaximum(org.sat4j.specs.IVec)
     */
    @Override
    public void maximizeMaximum(IVec<String> variables) {
        solver.maximizeMaximum(variables);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.reader.IXCSP3Listener#maximizeMaximum(org.sat4j.specs.IVec,
     * org.sat4j.specs.IVec)
     */
    @Override
    public void maximizeMaximum(IVec<String> variables, IVec<BigInteger> coefficients) {
        solver.maximizeMaximum(variables, coefficients);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.sat4j.csp.reader.IXCSP3Listener#maximizeExpressionMaximum(org.sat4j.specs.IVec)
     */
    @Override
    public void maximizeExpressionMaximum(IVec<IIntensionConstraint> expressions) {
        solver.maximizeExpressionMaximum(expressions);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.sat4j.csp.reader.IXCSP3Listener#maximizeExpressionMaximum(org.sat4j.specs.IVec,
     * org.sat4j.specs.IVec)
     */
    @Override
    public void maximizeExpressionMaximum(IVec<IIntensionConstraint> expressions,
            IVec<BigInteger> coefficients) {
        solver.maximizeExpressionMaximum(expressions, coefficients);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.reader.IXCSP3Listener#minimizeNValues(org.sat4j.specs.IVec)
     */
    @Override
    public void minimizeNValues(IVec<String> variables) {
        solver.minimizeNValues(variables);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.reader.IXCSP3Listener#minimizeNValues(org.sat4j.specs.IVec,
     * org.sat4j.specs.IVec)
     */
    @Override
    public void minimizeNValues(IVec<String> variables, IVec<BigInteger> coefficients) {
        solver.minimizeNValues(variables, coefficients);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.sat4j.csp.reader.IXCSP3Listener#minimizeExpressionNValues(org.sat4j.specs.IVec)
     */
    @Override
    public void minimizeExpressionNValues(IVec<IIntensionConstraint> expressions) {
        solver.minimizeExpressionNValues(expressions);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.sat4j.csp.reader.IXCSP3Listener#minimizeExpressionNValues(org.sat4j.specs.IVec,
     * org.sat4j.specs.IVec)
     */
    @Override
    public void minimizeExpressionNValues(IVec<IIntensionConstraint> expressions,
            IVec<BigInteger> coefficients) {
        solver.minimizeExpressionNValues(expressions, coefficients);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.reader.IXCSP3Listener#maximizeNValues(org.sat4j.specs.IVec)
     */
    @Override
    public void maximizeNValues(IVec<String> variables) {
        solver.maximizeNValues(variables);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.reader.IXCSP3Listener#maximizeNValues(org.sat4j.specs.IVec,
     * org.sat4j.specs.IVec)
     */
    @Override
    public void maximizeNValues(IVec<String> variables, IVec<BigInteger> coefficients) {
        solver.maximizeNValues(variables, coefficients);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.sat4j.csp.reader.IXCSP3Listener#maximizeExpressionNValues(org.sat4j.specs.IVec)
     */
    @Override
    public void maximizeExpressionNValues(IVec<IIntensionConstraint> expressions) {
        solver.maximizeExpressionNValues(expressions);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.sat4j.csp.reader.IXCSP3Listener#maximizeExpressionNValues(org.sat4j.specs.IVec,
     * org.sat4j.specs.IVec)
     */
    @Override
    public void maximizeExpressionNValues(IVec<IIntensionConstraint> expressions,
            IVec<BigInteger> coefficients) {
        solver.maximizeExpressionNValues(expressions, coefficients);
    }

}
