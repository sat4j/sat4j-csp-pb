/**
 * The {@code org.sat4j.csp.reader} package provides the classes for parsing
 * XCSP3 instances.
 *
 * @author Thibault Falque
 * @author Romain Wallon
 *
 * @version 0.1.0
 */

package org.sat4j.csp.reader;
