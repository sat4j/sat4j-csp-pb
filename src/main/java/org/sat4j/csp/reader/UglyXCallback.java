/**
 * Sat4j-CSP, a CSP solver based on the Sat4j platform.
 * Copyright (c) 2021-2022 - Exakis Nelite, Univ Artois & CNRS.
 * All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 * If not, see {@link http://www.gnu.org/licenses}.
 */

package org.sat4j.csp.reader;

import java.math.BigInteger;
import java.util.Map;
import java.util.Set;

import org.sat4j.core.Vec;
import org.sat4j.core.VecInt;
import org.sat4j.csp.constraints.ArithmeticOperator;
import org.sat4j.csp.constraints.BooleanOperator;
import org.sat4j.csp.constraints.Operator;
import org.sat4j.csp.constraints.RelationalOperator;
import org.sat4j.csp.constraints.SetBelongingOperator;
import org.sat4j.csp.constraints.intension.IIntensionConstraint;
import org.sat4j.csp.utils.UncheckedContradictionException;
import org.sat4j.specs.ContradictionException;
import org.sat4j.specs.IVec;
import org.xcsp.common.Condition;
import org.xcsp.common.Condition.ConditionIntset;
import org.xcsp.common.Condition.ConditionIntvl;
import org.xcsp.common.Constants;
import org.xcsp.common.Types.TypeArithmeticOperator;
import org.xcsp.common.Types.TypeConditionOperatorRel;
import org.xcsp.common.Types.TypeConditionOperatorSet;
import org.xcsp.common.Types.TypeEqNeOperator;
import org.xcsp.common.Types.TypeExpr;
import org.xcsp.common.Types.TypeFlag;
import org.xcsp.common.Types.TypeLogicalOperator;
import org.xcsp.common.Types.TypeObjective;
import org.xcsp.common.Types.TypeOperatorRel;
import org.xcsp.common.Types.TypeRank;
import org.xcsp.common.Types.TypeUnaryArithmeticOperator;
import org.xcsp.common.predicates.XNode;
import org.xcsp.common.predicates.XNodeParent;
import org.xcsp.parser.callbacks.XCallbacks2;
import org.xcsp.parser.entries.XVariables.XVarInteger;

/**
 * The UglyXCallback is an implementation of {@link XCallbacks2} that hides all
 * the gory implementation details required by this interface to make easier and
 * more flexible the parsing of XCSP3 instances.
 *
 * @author Thibault Falque
 * @author Romain Wallon
 *
 * @version 0.1.0
 */
final class UglyXCallback implements XCallbacks2 {

    /**
     * A map for translating a {@link TypeArithmeticOperator} to an
     * {@link ArithmeticOperator}.
     */
    private static final Map<TypeArithmeticOperator, ArithmeticOperator> TYPE_ARITH_OP_TO_ARITH_OP = Map.of(
            TypeArithmeticOperator.ADD, ArithmeticOperator.ADD,
            TypeArithmeticOperator.DIST, ArithmeticOperator.DIST,
            TypeArithmeticOperator.DIV, ArithmeticOperator.DIV,
            TypeArithmeticOperator.MOD, ArithmeticOperator.MOD,
            TypeArithmeticOperator.MUL, ArithmeticOperator.MULT,
            TypeArithmeticOperator.POW, ArithmeticOperator.POW,
            TypeArithmeticOperator.SUB, ArithmeticOperator.SUB);

    private static final Map<TypeUnaryArithmeticOperator, ArithmeticOperator> TYPE_UNARY_ARITH_OP_TO_ARITH_OP = Map.of(
            TypeUnaryArithmeticOperator.ABS, ArithmeticOperator.ABS,
            TypeUnaryArithmeticOperator.NEG, ArithmeticOperator.NEG,
            TypeUnaryArithmeticOperator.SQR, ArithmeticOperator.SQR);

    /**
     * A map for translating a {@link TypeLogicalOperator} to a {@link BooleanOperator}.
     */
    private static final Map<TypeLogicalOperator, BooleanOperator> TYPE_LOGIC_OP_TO_BOOL_OP = Map.of(
            TypeLogicalOperator.AND, BooleanOperator.AND,
            TypeLogicalOperator.IFF, BooleanOperator.EQUIV,
            TypeLogicalOperator.IMP, BooleanOperator.IMPL,
            TypeLogicalOperator.OR, BooleanOperator.OR,
            TypeLogicalOperator.XOR, BooleanOperator.XOR);

    /**
     * A map for translating a {@link TypeConditionOperatorRel} to a
     * {@link RelationalOperator}.
     */
    private static final Map<TypeConditionOperatorRel, RelationalOperator> TYPE_COND_OP_REL_TO_REL_OP = Map.of(
            TypeConditionOperatorRel.LT, RelationalOperator.LT,
            TypeConditionOperatorRel.LE, RelationalOperator.LE,
            TypeConditionOperatorRel.EQ, RelationalOperator.EQ,
            TypeConditionOperatorRel.NE, RelationalOperator.NEQ,
            TypeConditionOperatorRel.GT, RelationalOperator.GT,
            TypeConditionOperatorRel.GE, RelationalOperator.GE);

    /**
     * A map for translating a {@link TypeExpr} to a {@link RelationalOperator}.
     */
    private static final Map<TypeExpr, RelationalOperator> TYPE_EXPR_TO_REL_OP = Map.of(
            TypeExpr.LT, RelationalOperator.LT,
            TypeExpr.LE, RelationalOperator.LE,
            TypeExpr.EQ, RelationalOperator.EQ,
            TypeExpr.NE, RelationalOperator.NEQ,
            TypeExpr.GT, RelationalOperator.GT,
            TypeExpr.GE, RelationalOperator.GE);

    /**
     * A map for translating a {@link TypeOperatorRel} to a {@link RelationalOperator}.
     */
    private static final Map<TypeOperatorRel, RelationalOperator> TYPE_OP_REL_TO_REL_OP = Map.of(
            TypeOperatorRel.LT, RelationalOperator.LT,
            TypeOperatorRel.LE, RelationalOperator.LE,
            TypeOperatorRel.GT, RelationalOperator.GT,
            TypeOperatorRel.GE, RelationalOperator.GE);

    /**
     * A map for translating a {@link TypeConditionOperatorSet} to a
     * {@link SetBelongingOperator}.
     */
    private static final Map<TypeConditionOperatorSet, SetBelongingOperator> TYPE_COND_OP_SET_TO_SET_OP = Map.of(
            TypeConditionOperatorSet.IN, SetBelongingOperator.IN,
            TypeConditionOperatorSet.NOTIN, SetBelongingOperator.NOT_IN);

    /**
     * The listener to notify while reading an XCSP3 instance.
     */
    private IXCSP3Listener listener;

    /**
     * The implem object required for parsing XCSP3 instances.
     */
    private Implem implem;

    /**
     * Creates a new UglyXCallback.
     *
     * @param listener The listener to notify while reading an XCSP3 instance.
     */
    UglyXCallback(IXCSP3Listener listener) {
        this.listener = listener;
        this.implem = new Implem(this);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.xcsp.parser.callbacks.XCallbacks#implem()
     */
    @Override
    public Implem implem() {
        return implem;
    }

    /*
     * (non-Javadoc)
     *
     * @see org.xcsp.parser.callbacks.XCallbacks2#unimplementedCase(java.lang.Object[])
     */
    @Override
    public Object unimplementedCase(Object... objects) {
        throw new UnsupportedOperationException();
    }

    /*
     * (non-Javadoc)
     *
     * @see org.xcsp.parser.callbacks.XCallbacks2#buildVarInteger(org.xcsp.parser.entries.
     * XVariables.XVarInteger, int, int)
     */
    @Override
    public void buildVarInteger(XVarInteger x, int minValue, int maxValue) {
        listener.newVariable(x.id(), minValue, maxValue);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.xcsp.parser.callbacks.XCallbacks2#buildVarInteger(org.xcsp.parser.entries.
     * XVariables.XVarInteger, int[])
     */
    @Override
    public void buildVarInteger(XVarInteger x, int[] values) {
        listener.newVariable(x.id(), VecInt.of(values));
    }

    /*
     * (non-Javadoc)
     *
     * @see org.xcsp.parser.callbacks.XCallbacks2#buildCtrClause(java.lang.String,
     * org.xcsp.parser.entries.XVariables.XVarInteger[],
     * org.xcsp.parser.entries.XVariables.XVarInteger[])
     */
    @Override
    public void buildCtrClause(String id, XVarInteger[] pos, XVarInteger[] neg) {
        try {
            listener.addClause(toVariableIdentifiers(pos), toVariableIdentifiers(neg));
        } catch (ContradictionException e) {
            throw new UncheckedContradictionException(e);
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see org.xcsp.parser.callbacks.XCallbacks2#buildCtrLogic(java.lang.String,
     * org.xcsp.common.Types.TypeLogicalOperator,
     * org.xcsp.parser.entries.XVariables.XVarInteger[])
     */
    @Override
    public void buildCtrLogic(String id, TypeLogicalOperator op, XVarInteger[] vars) {
        try {
            listener.addLogical(TYPE_LOGIC_OP_TO_BOOL_OP.get(op), toVariableIdentifiers(vars));
        } catch (ContradictionException e) {
            throw new UncheckedContradictionException(e);
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see org.xcsp.parser.callbacks.XCallbacks2#buildCtrLogic(java.lang.String,
     * org.xcsp.parser.entries.XVariables.XVarInteger,
     * org.xcsp.common.Types.TypeEqNeOperator, org.xcsp.common.Types.TypeLogicalOperator,
     * org.xcsp.parser.entries.XVariables.XVarInteger[])
     */
    @Override
    public void buildCtrLogic(String id, XVarInteger x, TypeEqNeOperator op,
            TypeLogicalOperator lop, XVarInteger[] vars) {
        try {
            listener.addLogical(x.id(), op == TypeEqNeOperator.EQ,
                    TYPE_LOGIC_OP_TO_BOOL_OP.get(lop), toVariableIdentifiers(vars));
        } catch (ContradictionException e) {
            throw new UncheckedContradictionException(e);
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see org.xcsp.parser.callbacks.XCallbacks2#buildCtrLogic(java.lang.String,
     * org.xcsp.parser.entries.XVariables.XVarInteger,
     * org.xcsp.parser.entries.XVariables.XVarInteger,
     * org.xcsp.common.Types.TypeConditionOperatorRel, int)
     */
    @Override
    public void buildCtrLogic(String id, XVarInteger x, XVarInteger y,
            TypeConditionOperatorRel op, int k) {
        try {
            listener.addLogical(x.id(),
                    y.id(), TYPE_COND_OP_REL_TO_REL_OP.get(op), BigInteger.valueOf(k));
        } catch (ContradictionException e) {
            throw new UncheckedContradictionException(e);
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see org.xcsp.parser.callbacks.XCallbacks2#buildCtrLogic(java.lang.String,
     * org.xcsp.parser.entries.XVariables.XVarInteger,
     * org.xcsp.parser.entries.XVariables.XVarInteger,
     * org.xcsp.common.Types.TypeConditionOperatorRel,
     * org.xcsp.parser.entries.XVariables.XVarInteger)
     */
    @Override
    public void buildCtrLogic(String id, XVarInteger x, XVarInteger y,
            TypeConditionOperatorRel op, XVarInteger z) {
        try {
            listener.addLogical(x.id(),
                    y.id(), TYPE_COND_OP_REL_TO_REL_OP.get(op), z.id());
        } catch (ContradictionException e) {
            throw new UncheckedContradictionException(e);
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see org.xcsp.parser.callbacks.XCallbacks2#buildCtrAllDifferent(java.lang.String,
     * org.xcsp.parser.entries.XVariables.XVarInteger[])
     */
    @Override
    public void buildCtrAllDifferent(String id, XVarInteger[] list) {
        try {
            listener.addAllDifferent(toVariableIdentifiers(list));
        } catch (ContradictionException e) {
            throw new UncheckedContradictionException(e);
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.xcsp.parser.callbacks.XCallbacks2#buildCtrAllDifferentExcept(java.lang.String,
     * org.xcsp.parser.entries.XVariables.XVarInteger[], int[])
     */
    @Override
    public void buildCtrAllDifferentExcept(String id, XVarInteger[] list, int[] except) {
        try {
            listener.addAllDifferent(toVariableIdentifiers(list), toBigInteger(except));
        } catch (ContradictionException e) {
            throw new UncheckedContradictionException(e);
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.xcsp.parser.callbacks.XCallbacks2#buildCtrAllDifferentList(java.lang.String,
     * org.xcsp.parser.entries.XVariables.XVarInteger[][])
     */
    @Override
    public void buildCtrAllDifferentList(String id, XVarInteger[][] lists) {
        try {
            listener.addAllDifferentList(toVariableIdentifiers(lists));
        } catch (ContradictionException e) {
            throw new UncheckedContradictionException(e);
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.xcsp.parser.callbacks.XCallbacks2#buildCtrAllDifferentMatrix(java.lang.String,
     * org.xcsp.parser.entries.XVariables.XVarInteger[][])
     */
    @Override
    public void buildCtrAllDifferentMatrix(String id, XVarInteger[][] matrix) {
        try {
            listener.addAllDifferentMatrix(toVariableIdentifiers(matrix));
        } catch (ContradictionException e) {
            throw new UncheckedContradictionException(e);
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.xcsp.parser.callbacks.XCallbacks2#buildCtrAllDifferentMatrix(java.lang.String,
     * org.xcsp.parser.entries.XVariables.XVarInteger[][], int[])
     */
    @Override
    public void buildCtrAllDifferentMatrix(String id, XVarInteger[][] matrix, int[] except) {
        try {
            listener.addAllDifferentMatrix(toVariableIdentifiers(matrix), toBigInteger(except));
        } catch (ContradictionException e) {
            throw new UncheckedContradictionException(e);
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see org.xcsp.parser.callbacks.XCallbacks2#buildCtrAllDifferent(java.lang.String,
     * org.xcsp.common.predicates.XNode[])
     */
    @Override
    public void buildCtrAllDifferent(String id, XNode<XVarInteger>[] trees) {
        try {
            listener.addAllDifferentIntension(toIntensionConstraints(trees));
        } catch (ContradictionException e) {
            throw new UncheckedContradictionException(e);
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see org.xcsp.parser.callbacks.XCallbacks2#buildCtrAllEqual(java.lang.String,
     * org.xcsp.parser.entries.XVariables.XVarInteger[])
     */
    @Override
    public void buildCtrAllEqual(String id, XVarInteger[] list) {
        try {
            listener.addAllEqual(toVariableIdentifiers(list));
        } catch (ContradictionException e) {
            throw new UncheckedContradictionException(e);
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see org.xcsp.parser.callbacks.XCallbacks2#buildCtrAllEqual(java.lang.String,
     * org.xcsp.common.predicates.XNode[])
     */
    @Override
    public void buildCtrAllEqual(String id, XNode<XVarInteger>[] trees) {
        try {
            listener.addAllEqualIntension(toIntensionConstraints(trees));
        } catch (ContradictionException e) {
            throw new UncheckedContradictionException(e);
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see org.xcsp.parser.callbacks.XCallbacks2#buildCtrNotAllEqual(java.lang.String,
     * org.xcsp.parser.entries.XVariables.XVarInteger[])
     */
    @Override
    public void buildCtrNotAllEqual(String id, XVarInteger[] list) {
        try {
            listener.addNotAllEqual(toVariableIdentifiers(list));
        } catch (ContradictionException e) {
            throw new UncheckedContradictionException(e);
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see org.xcsp.parser.callbacks.XCallbacks2#buildCtrAmong(java.lang.String,
     * org.xcsp.parser.entries.XVariables.XVarInteger[], int[], int)
     */
    @Override
    public void buildCtrAmong(String id, XVarInteger[] list, int[] values, int k) {
        try {
            listener.addAmong(
                    toVariableIdentifiers(list), toBigInteger(values), BigInteger.valueOf(k));
        } catch (ContradictionException e) {
            throw new UncheckedContradictionException(e);
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see org.xcsp.parser.callbacks.XCallbacks2#buildCtrAmong(java.lang.String,
     * org.xcsp.parser.entries.XVariables.XVarInteger[], int[],
     * org.xcsp.parser.entries.XVariables.XVarInteger)
     */
    @Override
    public void buildCtrAmong(String id, XVarInteger[] list, int[] values, XVarInteger k) {
        try {
            listener.addAmong(toVariableIdentifiers(list), toBigInteger(values), k.id());
        } catch (ContradictionException e) {
            throw new UncheckedContradictionException(e);
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see org.xcsp.parser.callbacks.XCallbacks2#buildCtrAtLeast(java.lang.String,
     * org.xcsp.parser.entries.XVariables.XVarInteger[], int, int)
     */
    @Override
    public void buildCtrAtLeast(String id, XVarInteger[] list, int value, int k) {
        try {
            listener.addAtLeast(
                    toVariableIdentifiers(list), BigInteger.valueOf(value), BigInteger.valueOf(k));
        } catch (ContradictionException e) {
            throw new UncheckedContradictionException(e);
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see org.xcsp.parser.callbacks.XCallbacks2#buildCtrAtMost(java.lang.String,
     * org.xcsp.parser.entries.XVariables.XVarInteger[], int, int)
     */
    @Override
    public void buildCtrAtMost(String id, XVarInteger[] list, int value, int k) {
        try {
            listener.addAtMost(
                    toVariableIdentifiers(list), BigInteger.valueOf(value), BigInteger.valueOf(k));
        } catch (ContradictionException e) {
            throw new UncheckedContradictionException(e);
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see org.xcsp.parser.callbacks.XCallbacks2#buildCtrCardinality(java.lang.String,
     * org.xcsp.parser.entries.XVariables.XVarInteger[], boolean, int[], int[], int[])
     */
    @Override
    public void buildCtrCardinality(String id, XVarInteger[] list, boolean closed, int[] values,
            int[] occursMin, int[] occursMax) {
        try {
            listener.addCardinalityWithConstantValuesAndConstantIntervalCounts(
                    toVariableIdentifiers(list), toBigInteger(values), toBigInteger(occursMin),
                    toBigInteger(occursMax), closed);
        } catch (ContradictionException e) {
            throw new UncheckedContradictionException(e);
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see org.xcsp.parser.callbacks.XCallbacks2#buildCtrCardinality(java.lang.String,
     * org.xcsp.parser.entries.XVariables.XVarInteger[], boolean, int[], int[])
     */
    @Override
    public void buildCtrCardinality(String id, XVarInteger[] list, boolean closed, int[] values,
            int[] occurs) {
        try {
            listener.addCardinalityWithConstantValuesAndConstantCounts(toVariableIdentifiers(list),
                    toBigInteger(values), toBigInteger(occurs), closed);
        } catch (ContradictionException e) {
            throw new UncheckedContradictionException(e);
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see org.xcsp.parser.callbacks.XCallbacks2#buildCtrCardinality(java.lang.String,
     * org.xcsp.parser.entries.XVariables.XVarInteger[], boolean, int[],
     * org.xcsp.parser.entries.XVariables.XVarInteger[])
     */
    @Override
    public void buildCtrCardinality(String id, XVarInteger[] list, boolean closed, int[] values,
            XVarInteger[] occurs) {
        try {
            listener.addCardinalityWithConstantValuesAndVariableCounts(
                    toVariableIdentifiers(list), toBigInteger(values),
                    toVariableIdentifiers(occurs), closed);
        } catch (ContradictionException e) {
            throw new UncheckedContradictionException(e);
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see org.xcsp.parser.callbacks.XCallbacks2#buildCtrCardinality(java.lang.String,
     * org.xcsp.parser.entries.XVariables.XVarInteger[], boolean,
     * org.xcsp.parser.entries.XVariables.XVarInteger[], int[], int[])
     */
    @Override
    public void buildCtrCardinality(String id, XVarInteger[] list, boolean closed,
            XVarInteger[] values, int[] occursMin, int[] occursMax) {
        try {
            listener.addCardinalityWithVariableValuesAndConstantIntervalCounts(
                    toVariableIdentifiers(list), toVariableIdentifiers(values),
                    toBigInteger(occursMin), toBigInteger(occursMax), closed);
        } catch (ContradictionException e) {
            throw new UncheckedContradictionException(e);
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see org.xcsp.parser.callbacks.XCallbacks2#buildCtrCardinality(java.lang.String,
     * org.xcsp.parser.entries.XVariables.XVarInteger[], boolean,
     * org.xcsp.parser.entries.XVariables.XVarInteger[], int[])
     */
    @Override
    public void buildCtrCardinality(String id, XVarInteger[] list, boolean closed,
            XVarInteger[] values, int[] occurs) {
        try {
            listener.addCardinalityWithVariableValuesAndConstantCounts(
                    toVariableIdentifiers(list), toVariableIdentifiers(values),
                    toBigInteger(occurs), closed);
        } catch (ContradictionException e) {
            throw new UncheckedContradictionException(e);
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see org.xcsp.parser.callbacks.XCallbacks2#buildCtrCardinality(java.lang.String,
     * org.xcsp.parser.entries.XVariables.XVarInteger[], boolean,
     * org.xcsp.parser.entries.XVariables.XVarInteger[],
     * org.xcsp.parser.entries.XVariables.XVarInteger[])
     */
    @Override
    public void buildCtrCardinality(String id, XVarInteger[] list, boolean closed,
            XVarInteger[] values, XVarInteger[] occurs) {
        try {
            listener.addCardinalityWithVariableValuesAndVariableCounts(
                    toVariableIdentifiers(list), toVariableIdentifiers(values),
                    toVariableIdentifiers(occurs), closed);
        } catch (ContradictionException e) {
            throw new UncheckedContradictionException(e);
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see org.xcsp.parser.callbacks.XCallbacks2#buildCtrChannel(java.lang.String,
     * org.xcsp.parser.entries.XVariables.XVarInteger[], int,
     * org.xcsp.parser.entries.XVariables.XVarInteger)
     */
    @Override
    public void buildCtrChannel(String id, XVarInteger[] list, int startIndex, XVarInteger value) {
        try {
            listener.addChannel(toVariableIdentifiers(list), startIndex, value.id());
        } catch (ContradictionException e) {
            throw new UncheckedContradictionException(e);
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see org.xcsp.parser.callbacks.XCallbacks2#buildCtrChannel(java.lang.String,
     * org.xcsp.parser.entries.XVariables.XVarInteger[], int,
     * org.xcsp.parser.entries.XVariables.XVarInteger[], int)
     */
    @Override
    public void buildCtrChannel(String id, XVarInteger[] list1, int startIndex1,
            XVarInteger[] list2, int startIndex2) {
        try {
            listener.addChannel(toVariableIdentifiers(list1), startIndex1,
                    toVariableIdentifiers(list2), startIndex2);
        } catch (ContradictionException e) {
            throw new UncheckedContradictionException(e);
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see org.xcsp.parser.callbacks.XCallbacks2#buildCtrChannel(java.lang.String,
     * org.xcsp.parser.entries.XVariables.XVarInteger[], int)
     */
    @Override
    public void buildCtrChannel(String id, XVarInteger[] list, int startIndex) {
        try {
            listener.addChannel(toVariableIdentifiers(list), startIndex);
        } catch (ContradictionException e) {
            throw new UncheckedContradictionException(e);
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see org.xcsp.parser.callbacks.XCallbacks2#buildCtrCount(java.lang.String,
     * org.xcsp.parser.entries.XVariables.XVarInteger[], int[], org.xcsp.common.Condition)
     */
    @Override
    public void buildCtrCount(String id, XVarInteger[] list, int[] values, Condition condition) {
        buildCtrWithCondition(condition,
                (op, rhs) -> listener.addCountWithConstantValues(toVariableIdentifiers(list),
                        toBigInteger(values), op, rhs),
                (op, rhs) -> listener.addCountWithConstantValues(toVariableIdentifiers(list),
                        toBigInteger(values), op, rhs));
    }

    /*
     * (non-Javadoc)
     *
     * @see org.xcsp.parser.callbacks.XCallbacks2#buildCtrCount(java.lang.String,
     * org.xcsp.parser.entries.XVariables.XVarInteger[],
     * org.xcsp.parser.entries.XVariables.XVarInteger[], org.xcsp.common.Condition)
     */
    @Override
    public void buildCtrCount(String id, XVarInteger[] list, XVarInteger[] values,
            Condition condition) {
        buildCtrWithCondition(condition,
                (op, rhs) -> listener.addCountWithVariableValues(toVariableIdentifiers(list),
                        toVariableIdentifiers(values), op, rhs),
                (op, rhs) -> listener.addCountWithVariableValues(toVariableIdentifiers(list),
                        toVariableIdentifiers(values), op, rhs));
    }

    /*
     * (non-Javadoc)
     *
     * @see org.xcsp.parser.callbacks.XCallbacks2#buildCtrCount(java.lang.String,
     * org.xcsp.common.predicates.XNode[], int[], org.xcsp.common.Condition)
     */
    @Override
    public void buildCtrCount(String id, XNode<XVarInteger>[] trees, int[] values,
            Condition condition) {
        buildCtrWithCondition(condition,
                (op, rhs) -> listener.addCountIntensionWithConstantValues(
                        toIntensionConstraints(trees), toBigInteger(values), op, rhs),
                (op, rhs) -> listener.addCountIntensionWithConstantValues(
                        toIntensionConstraints(trees), toBigInteger(values), op, rhs));
    }

    /*
     * (non-Javadoc)
     *
     * @see org.xcsp.parser.callbacks.XCallbacks2#buildCtrCumulative(java.lang.String,
     * org.xcsp.parser.entries.XVariables.XVarInteger[], int[], int[],
     * org.xcsp.common.Condition)
     */
    @Override
    public void buildCtrCumulative(String id, XVarInteger[] origins, int[] lengths, int[] heights,
            Condition condition) {
        buildCtrWithCondition(condition,
                (op, rhs) -> listener.addCumulativeConstantLengthsConstantHeights(
                        toVariableIdentifiers(origins), toBigInteger(lengths),
                        toBigInteger(heights), op, rhs),
                (op, rhs) -> listener.addCumulativeConstantLengthsConstantHeights(
                        toVariableIdentifiers(origins), toBigInteger(lengths),
                        toBigInteger(heights), op, rhs));
    }

    /*
     * (non-Javadoc)
     *
     * @see org.xcsp.parser.callbacks.XCallbacks2#buildCtrCumulative(java.lang.String,
     * org.xcsp.parser.entries.XVariables.XVarInteger[], int[],
     * org.xcsp.parser.entries.XVariables.XVarInteger[], org.xcsp.common.Condition)
     */
    @Override
    public void buildCtrCumulative(String id, XVarInteger[] origins, int[] lengths,
            XVarInteger[] heights, Condition condition) {
        buildCtrWithCondition(condition,
                (op, rhs) -> listener.addCumulativeConstantLengthsVariableHeights(
                        toVariableIdentifiers(origins), toBigInteger(lengths),
                        toVariableIdentifiers(heights), op, rhs),
                (op, rhs) -> listener.addCumulativeConstantLengthsVariableHeights(
                        toVariableIdentifiers(origins), toBigInteger(lengths),
                        toVariableIdentifiers(heights), op, rhs));
    }

    /*
     * (non-Javadoc)
     *
     * @see org.xcsp.parser.callbacks.XCallbacks2#buildCtrCumulative(java.lang.String,
     * org.xcsp.parser.entries.XVariables.XVarInteger[], int[],
     * org.xcsp.parser.entries.XVariables.XVarInteger[], int[], org.xcsp.common.Condition)
     */
    @Override
    public void buildCtrCumulative(String id, XVarInteger[] origins, int[] lengths,
            XVarInteger[] ends, int[] heights, Condition condition) {
        buildCtrWithCondition(condition,
                (op, rhs) -> listener.addCumulativeConstantLengthsConstantHeights(
                        toVariableIdentifiers(origins), toBigInteger(lengths),
                        toVariableIdentifiers(ends), toBigInteger(heights), op, rhs),
                (op, rhs) -> listener.addCumulativeConstantLengthsConstantHeights(
                        toVariableIdentifiers(origins), toBigInteger(lengths),
                        toVariableIdentifiers(ends), toBigInteger(heights), op, rhs));
    }

    /*
     * (non-Javadoc)
     *
     * @see org.xcsp.parser.callbacks.XCallbacks2#buildCtrCumulative(java.lang.String,
     * org.xcsp.parser.entries.XVariables.XVarInteger[], int[],
     * org.xcsp.parser.entries.XVariables.XVarInteger[],
     * org.xcsp.parser.entries.XVariables.XVarInteger[], org.xcsp.common.Condition)
     */
    @Override
    public void buildCtrCumulative(String id, XVarInteger[] origins, int[] lengths,
            XVarInteger[] ends, XVarInteger[] heights, Condition condition) {
        buildCtrWithCondition(condition,
                (op, rhs) -> listener.addCumulativeConstantLengthsVariableHeights(
                        toVariableIdentifiers(origins), toBigInteger(lengths),
                        toVariableIdentifiers(ends), toVariableIdentifiers(heights), op, rhs),
                (op, rhs) -> listener.addCumulativeConstantLengthsVariableHeights(
                        toVariableIdentifiers(origins), toBigInteger(lengths),
                        toVariableIdentifiers(ends), toVariableIdentifiers(heights), op, rhs));
    }

    /*
     * (non-Javadoc)
     *
     * @see org.xcsp.parser.callbacks.XCallbacks2#buildCtrCumulative(java.lang.String,
     * org.xcsp.parser.entries.XVariables.XVarInteger[],
     * org.xcsp.parser.entries.XVariables.XVarInteger[], int[], org.xcsp.common.Condition)
     */
    @Override
    public void buildCtrCumulative(String id, XVarInteger[] origins, XVarInteger[] lengths,
            int[] heights, Condition condition) {
        buildCtrWithCondition(condition,
                (op, rhs) -> listener.addCumulativeVariableLengthsConstantHeights(
                        toVariableIdentifiers(origins), toVariableIdentifiers(lengths),
                        toBigInteger(heights), op, rhs),
                (op, rhs) -> listener.addCumulativeVariableLengthsConstantHeights(
                        toVariableIdentifiers(origins), toVariableIdentifiers(lengths),
                        toBigInteger(heights), op, rhs));
    }

    /*
     * (non-Javadoc)
     *
     * @see org.xcsp.parser.callbacks.XCallbacks2#buildCtrCumulative(java.lang.String,
     * org.xcsp.parser.entries.XVariables.XVarInteger[],
     * org.xcsp.parser.entries.XVariables.XVarInteger[],
     * org.xcsp.parser.entries.XVariables.XVarInteger[], org.xcsp.common.Condition)
     */
    @Override
    public void buildCtrCumulative(String id, XVarInteger[] origins, XVarInteger[] lengths,
            XVarInteger[] heights, Condition condition) {
        buildCtrWithCondition(condition,
                (op, rhs) -> listener.addCumulativeVariableLengthsVariableHeights(
                        toVariableIdentifiers(origins), toVariableIdentifiers(lengths),
                        toVariableIdentifiers(heights), op, rhs),
                (op, rhs) -> listener.addCumulativeVariableLengthsVariableHeights(
                        toVariableIdentifiers(origins), toVariableIdentifiers(lengths),
                        toVariableIdentifiers(heights), op, rhs));
    }

    /*
     * (non-Javadoc)
     *
     * @see org.xcsp.parser.callbacks.XCallbacks2#buildCtrCumulative(java.lang.String,
     * org.xcsp.parser.entries.XVariables.XVarInteger[],
     * org.xcsp.parser.entries.XVariables.XVarInteger[],
     * org.xcsp.parser.entries.XVariables.XVarInteger[], int[], org.xcsp.common.Condition)
     */
    @Override
    public void buildCtrCumulative(String id, XVarInteger[] origins, XVarInteger[] lengths,
            XVarInteger[] ends, int[] heights, Condition condition) {
        buildCtrWithCondition(condition,
                (op, rhs) -> listener.addCumulativeVariableLengthsConstantHeights(
                        toVariableIdentifiers(origins), toVariableIdentifiers(lengths),
                        toVariableIdentifiers(ends), toBigInteger(heights), op, rhs),
                (op, rhs) -> listener.addCumulativeVariableLengthsConstantHeights(
                        toVariableIdentifiers(origins), toVariableIdentifiers(lengths),
                        toVariableIdentifiers(ends), toBigInteger(heights), op, rhs));
    }

    /*
     * (non-Javadoc)
     *
     * @see org.xcsp.parser.callbacks.XCallbacks2#buildCtrCumulative(java.lang.String,
     * org.xcsp.parser.entries.XVariables.XVarInteger[],
     * org.xcsp.parser.entries.XVariables.XVarInteger[],
     * org.xcsp.parser.entries.XVariables.XVarInteger[],
     * org.xcsp.parser.entries.XVariables.XVarInteger[], org.xcsp.common.Condition)
     */
    @Override
    public void buildCtrCumulative(String id, XVarInteger[] origins, XVarInteger[] lengths,
            XVarInteger[] ends, XVarInteger[] heights, Condition condition) {
        buildCtrWithCondition(condition,
                (op, rhs) -> listener.addCumulativeVariableLengthsVariableHeights(
                        toVariableIdentifiers(origins), toVariableIdentifiers(lengths),
                        toVariableIdentifiers(ends), toVariableIdentifiers(heights), op, rhs),
                (op, rhs) -> listener.addCumulativeVariableLengthsVariableHeights(
                        toVariableIdentifiers(origins), toVariableIdentifiers(lengths),
                        toVariableIdentifiers(ends), toVariableIdentifiers(heights), op, rhs));
    }

    /*
     * (non-Javadoc)
     *
     * @see org.xcsp.parser.callbacks.XCallbacks2#buildCtrElement(java.lang.String,
     * int[][], int, org.xcsp.parser.entries.XVariables.XVarInteger, int,
     * org.xcsp.parser.entries.XVariables.XVarInteger, int)
     */
    @Override
    public void buildCtrElement(String id, int[][] matrix, int startRowIndex, XVarInteger rowIndex,
            int startColIndex, XVarInteger colIndex, Condition condition) {
        buildCtrWithCondition(condition,
                (op, rhs) -> listener.addElementConstantMatrix(
                        toBigInteger(matrix), startRowIndex, rowIndex.id(), startColIndex,
                        colIndex.id(), rhs),
                (op, rhs) -> listener.addElementConstantMatrix(
                        toBigInteger(matrix), startRowIndex, rowIndex.id(), startColIndex,
                        colIndex.id(), rhs));
    }

    /*
     * (non-Javadoc)
     *
     * @see org.xcsp.parser.callbacks.XCallbacks2#buildCtrElement(java.lang.String,
     * org.xcsp.parser.entries.XVariables.XVarInteger[], int)
     */
    @Override
    public void buildCtrElement(String id, XVarInteger[] list, Condition condition) {
        buildCtrWithCondition(condition,
                (op, rhs) -> listener.addElement(toVariableIdentifiers(list), rhs),
                (op, rhs) -> listener.addElement(toVariableIdentifiers(list), rhs));
    }

    /*
     * (non-Javadoc)
     *
     * @see org.xcsp.parser.callbacks.XCallbacks2#buildCtrElement(java.lang.String, int[],
     * int, org.xcsp.parser.entries.XVariables.XVarInteger,
     * org.xcsp.common.Types.TypeRank, org.xcsp.parser.entries.XVariables.XVarInteger)
     */
    @Override
    public void buildCtrElement(String id, int[] list, int startIndex, XVarInteger index,
            TypeRank rank, Condition condition) {
        if (rank != TypeRank.ANY) {
            throw new UnsupportedOperationException("Unsupported type rank for element: " + rank);
        }

        buildCtrWithCondition(condition,
                (op, rhs) -> listener.addElementConstantValues(
                        toBigInteger(list), startIndex, index.id(), rhs),
                (op, rhs) -> listener.addElementConstantValues(
                        toBigInteger(list), startIndex, index.id(), rhs));
    }

    /*
     * (non-Javadoc)
     *
     * @see org.xcsp.parser.callbacks.XCallbacks2#buildCtrElement(java.lang.String,
     * org.xcsp.parser.entries.XVariables.XVarInteger[], int,
     * org.xcsp.parser.entries.XVariables.XVarInteger, org.xcsp.common.Types.TypeRank,
     * int)
     */
    @Override
    public void buildCtrElement(String id, XVarInteger[] list, int startIndex, XVarInteger index,
            TypeRank rank, Condition condition) {
        if (rank != TypeRank.ANY) {
            throw new UnsupportedOperationException("Unsupported type rank for element: " + rank);
        }

        buildCtrWithCondition(condition,
                (op, rhs) -> listener.addElement(
                        toVariableIdentifiers(list), startIndex, index.id(), rhs),
                (op, rhs) -> listener.addElement(
                        toVariableIdentifiers(list), startIndex, index.id(), rhs));
    }

    /*
     * (non-Javadoc)
     *
     * @see org.xcsp.parser.callbacks.XCallbacks2#buildCtrElement(java.lang.String,
     * org.xcsp.parser.entries.XVariables.XVarInteger[][], int,
     * org.xcsp.parser.entries.XVariables.XVarInteger, int,
     * org.xcsp.parser.entries.XVariables.XVarInteger, int)
     */
    @Override
    public void buildCtrElement(String id, XVarInteger[][] matrix, int startRowIndex,
            XVarInteger rowIndex, int startColIndex, XVarInteger colIndex, Condition condition) {
        buildCtrWithCondition(condition,
                (op, rhs) -> listener.addElementMatrix(
                        toVariableIdentifiers(matrix), startRowIndex, rowIndex.id(), startColIndex,
                        colIndex.id(), rhs),
                (op, rhs) -> listener.addElementMatrix(
                        toVariableIdentifiers(matrix), startRowIndex, rowIndex.id(), startColIndex,
                        colIndex.id(), rhs));
    }

    /*
     * (non-Javadoc)
     *
     * @see org.xcsp.parser.callbacks.XCallbacks2#buildCtrExactly(java.lang.String,
     * org.xcsp.parser.entries.XVariables.XVarInteger[], int, int)
     */
    @Override
    public void buildCtrExactly(String id, XVarInteger[] list, int value, int k) {
        try {
            listener.addExactly(
                    toVariableIdentifiers(list), BigInteger.valueOf(value), BigInteger.valueOf(k));
        } catch (ContradictionException e) {
            throw new UncheckedContradictionException(e);
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see org.xcsp.parser.callbacks.XCallbacks2#buildCtrExactly(java.lang.String,
     * org.xcsp.parser.entries.XVariables.XVarInteger[], int,
     * org.xcsp.parser.entries.XVariables.XVarInteger)
     */
    @Override
    public void buildCtrExactly(String id, XVarInteger[] list, int value, XVarInteger k) {
        try {
            listener.addExactly(toVariableIdentifiers(list), BigInteger.valueOf(value), k.id());
        } catch (ContradictionException e) {
            throw new UncheckedContradictionException(e);
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see org.xcsp.parser.callbacks.XCallbacks2#buildCtrExtension(java.lang.String,
     * org.xcsp.parser.entries.XVariables.XVarInteger, int[], boolean, java.util.Set)
     */
    @Override
    public void buildCtrExtension(String id, XVarInteger x, int[] values, boolean positive,
            Set<TypeFlag> flags) {
        try {
            if (positive) {
                listener.addSupport(x.id(), toBigInteger(values));
            } else {
                listener.addConflicts(x.id(), toBigInteger(values));
            }
        } catch (ContradictionException e) {
            throw new UncheckedContradictionException(e);
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see org.xcsp.parser.callbacks.XCallbacks2#buildCtrExtension(java.lang.String,
     * org.xcsp.parser.entries.XVariables.XVarInteger[], int[][], boolean, java.util.Set)
     */
    @Override
    public void buildCtrExtension(String id, XVarInteger[] list, int[][] tuples, boolean positive,
            Set<TypeFlag> flags) {
        try {
            if (positive) {
                listener.addSupport(toVariableIdentifiers(list),
                        toBigInteger(tuples, flags.contains(TypeFlag.STARRED_TUPLES)));
            } else {
                listener.addConflicts(toVariableIdentifiers(list),
                        toBigInteger(tuples, flags.contains(TypeFlag.STARRED_TUPLES)));
            }
        } catch (ContradictionException e) {
            throw new UncheckedContradictionException(e);
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see org.xcsp.parser.callbacks.XCallbacks2#buildCtrInstantiation(java.lang.String,
     * org.xcsp.parser.entries.XVariables.XVarInteger[], int[])
     */
    @Override
    public void buildCtrInstantiation(String id, XVarInteger[] list, int[] values) {
        try {
            listener.addInstantiation(toVariableIdentifiers(list), toBigInteger(values));
        } catch (ContradictionException e) {
            throw new UncheckedContradictionException(e);
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see org.xcsp.parser.callbacks.XCallbacks2#buildCtrIntension(java.lang.String,
     * org.xcsp.parser.entries.XVariables.XVarInteger[],
     * org.xcsp.common.predicates.XNodeParent)
     */
    @Override
    public void buildCtrIntension(String id, XVarInteger[] scope, XNodeParent<XVarInteger> tree) {
        try {
            listener.addIntension(new IntensionConstraintXNodeAdapter(tree));
        } catch (ContradictionException e) {
            throw new UncheckedContradictionException(e);
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see org.xcsp.parser.callbacks.XCallbacks2#buildCtrLex(java.lang.String,
     * org.xcsp.parser.entries.XVariables.XVarInteger[][],
     * org.xcsp.common.Types.TypeOperatorRel)
     */
    @Override
    public void buildCtrLex(String id, XVarInteger[][] lists, TypeOperatorRel operator) {
        try {
            listener.addLex(toVariableIdentifiers(lists), TYPE_OP_REL_TO_REL_OP.get(operator));
        } catch (ContradictionException e) {
            throw new UncheckedContradictionException(e);
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see org.xcsp.parser.callbacks.XCallbacks2#buildCtrLexMatrix(java.lang.String,
     * org.xcsp.parser.entries.XVariables.XVarInteger[][],
     * org.xcsp.common.Types.TypeOperatorRel)
     */
    @Override
    public void buildCtrLexMatrix(String id, XVarInteger[][] matrix, TypeOperatorRel operator) {
        try {
            listener.addLexMatrix(
                    toVariableIdentifiers(matrix), TYPE_OP_REL_TO_REL_OP.get(operator));
        } catch (ContradictionException e) {
            throw new UncheckedContradictionException(e);
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see org.xcsp.parser.callbacks.XCallbacks2#buildCtrMaximum(java.lang.String,
     * org.xcsp.parser.entries.XVariables.XVarInteger[], org.xcsp.common.Condition)
     */
    @Override
    public void buildCtrMaximum(String id, XVarInteger[] list, Condition condition) {
        buildCtrWithCondition(condition,
                (op, rhs) -> listener.addMaximum(toVariableIdentifiers(list), op, rhs),
                (op, rhs) -> listener.addMaximum(toVariableIdentifiers(list), op, rhs));
    }

    /*
     * (non-Javadoc)
     *
     * @see org.xcsp.parser.callbacks.XCallbacks2#buildCtrMaximum(java.lang.String,
     * org.xcsp.common.predicates.XNode[], org.xcsp.common.Condition)
     */
    @Override
    public void buildCtrMaximum(String id, XNode<XVarInteger>[] trees, Condition condition) {
        buildCtrWithCondition(condition,
                (op, rhs) -> listener.addMaximumIntension(toIntensionConstraints(trees), op, rhs),
                (op, rhs) -> listener.addMaximumIntension(toIntensionConstraints(trees), op, rhs));
    }

    /*
     * (non-Javadoc)
     *
     * @see org.xcsp.parser.callbacks.XCallbacks2#buildCtrMinimum(java.lang.String,
     * org.xcsp.parser.entries.XVariables.XVarInteger[], org.xcsp.common.Condition)
     */
    @Override
    public void buildCtrMinimum(String id, XVarInteger[] list, Condition condition) {
        buildCtrWithCondition(condition,
                (op, rhs) -> listener.addMinimum(toVariableIdentifiers(list), op, rhs),
                (op, rhs) -> listener.addMinimum(toVariableIdentifiers(list), op, rhs));
    }

    /*
     * (non-Javadoc)
     *
     * @see org.xcsp.parser.callbacks.XCallbacks2#buildCtrMinimum(java.lang.String,
     * org.xcsp.common.predicates.XNode[], org.xcsp.common.Condition)
     */
    @Override
    public void buildCtrMinimum(String id, XNode<XVarInteger>[] trees, Condition condition) {
        buildCtrWithCondition(condition,
                (op, rhs) -> listener.addMinimumIntension(toIntensionConstraints(trees), op, rhs),
                (op, rhs) -> listener.addMinimumIntension(toIntensionConstraints(trees), op, rhs));
    }

    /*
     * (non-Javadoc)
     *
     * @see org.xcsp.parser.callbacks.XCallbacks2#buildCtrNValues(java.lang.String,
     * org.xcsp.parser.entries.XVariables.XVarInteger[], org.xcsp.common.Condition)
     */
    @Override
    public void buildCtrNValues(String id, XVarInteger[] list, Condition condition) {
        buildCtrWithCondition(condition,
                (op, rhs) -> listener.addNValues(toVariableIdentifiers(list), op, rhs),
                (op, rhs) -> listener.addNValues(toVariableIdentifiers(list), op, rhs));
    }

    /*
     * (non-Javadoc)
     *
     * @see org.xcsp.parser.callbacks.XCallbacks2#buildCtrNValuesExcept(java.lang.String,
     * org.xcsp.parser.entries.XVariables.XVarInteger[], int[], org.xcsp.common.Condition)
     */
    @Override
    public void buildCtrNValuesExcept(String id, XVarInteger[] list, int[] except,
            Condition condition) {
        buildCtrWithCondition(condition,
                (op, rhs) -> listener.addNValuesExcept(
                        toVariableIdentifiers(list), op, rhs, toBigInteger(except)),
                (op, rhs) -> listener.addNValuesExcept(
                        toVariableIdentifiers(list), op, rhs, toBigInteger(except)));
    }

    /*
     * (non-Javadoc)
     *
     * @see org.xcsp.parser.callbacks.XCallbacks2#buildCtrNValues(java.lang.String,
     * org.xcsp.common.predicates.XNode[], org.xcsp.common.Condition)
     */
    @Override
    public void buildCtrNValues(String id, XNode<XVarInteger>[] trees, Condition condition) {
        buildCtrWithCondition(condition,
                (op, rhs) -> listener.addNValuesIntension(toIntensionConstraints(trees), op, rhs),
                (op, rhs) -> listener.addNValuesIntension(toIntensionConstraints(trees), op, rhs));
    }

    /*
     * (non-Javadoc)
     *
     * @see org.xcsp.parser.callbacks.XCallbacks2#buildCtrNoOverlap(java.lang.String,
     * org.xcsp.parser.entries.XVariables.XVarInteger[], int[], boolean)
     */
    @Override
    public void buildCtrNoOverlap(String id, XVarInteger[] origins, int[] lengths,
            boolean zeroIgnored) {
        try {
            listener.addNoOverlap(
                    toVariableIdentifiers(origins), toBigInteger(lengths), zeroIgnored);
        } catch (ContradictionException e) {
            throw new UncheckedContradictionException(e);
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see org.xcsp.parser.callbacks.XCallbacks2#buildCtrNoOverlap(java.lang.String,
     * org.xcsp.parser.entries.XVariables.XVarInteger[],
     * org.xcsp.parser.entries.XVariables.XVarInteger[], boolean)
     */
    @Override
    public void buildCtrNoOverlap(String id, XVarInteger[] origins, XVarInteger[] lengths,
            boolean zeroIgnored) {
        try {
            listener.addNoOverlapVariableLength(
                    toVariableIdentifiers(origins), toVariableIdentifiers(lengths), zeroIgnored);
        } catch (ContradictionException e) {
            throw new UncheckedContradictionException(e);
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see org.xcsp.parser.callbacks.XCallbacks2#buildCtrNoOverlap(java.lang.String,
     * org.xcsp.parser.entries.XVariables.XVarInteger[][], int[][], boolean)
     */
    @Override
    public void buildCtrNoOverlap(String id, XVarInteger[][] origins, int[][] lengths,
            boolean zeroIgnored) {
        try {
            listener.addMultiDimensionalNoOverlap(
                    toVariableIdentifiers(origins), toBigInteger(lengths), zeroIgnored);
        } catch (ContradictionException e) {
            throw new UncheckedContradictionException(e);
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see org.xcsp.parser.callbacks.XCallbacks2#buildCtrNoOverlap(java.lang.String,
     * org.xcsp.parser.entries.XVariables.XVarInteger[][],
     * org.xcsp.parser.entries.XVariables.XVarInteger[][], boolean)
     */
    @Override
    public void buildCtrNoOverlap(String id, XVarInteger[][] origins, XVarInteger[][] lengths,
            boolean zeroIgnored) {
        try {
            listener.addMultiDimensionalNoOverlapVariableLength(
                    toVariableIdentifiers(origins), toVariableIdentifiers(lengths), zeroIgnored);
        } catch (ContradictionException e) {
            throw new UncheckedContradictionException(e);
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see org.xcsp.parser.callbacks.XCallbacks2#buildCtrOrdered(java.lang.String,
     * org.xcsp.parser.entries.XVariables.XVarInteger[], int[],
     * org.xcsp.common.Types.TypeOperatorRel)
     */
    @Override
    public void buildCtrOrdered(String id, XVarInteger[] list, int[] lengths,
            TypeOperatorRel operator) {
        try {
            listener.addOrderedWithConstantLength(toVariableIdentifiers(list),
                    toBigInteger(lengths), TYPE_OP_REL_TO_REL_OP.get(operator));
        } catch (ContradictionException e) {
            throw new UncheckedContradictionException(e);
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see org.xcsp.parser.callbacks.XCallbacks2#buildCtrOrdered(java.lang.String,
     * org.xcsp.parser.entries.XVariables.XVarInteger[],
     * org.xcsp.common.Types.TypeOperatorRel)
     */
    @Override
    public void buildCtrOrdered(String id, XVarInteger[] list, TypeOperatorRel operator) {
        try {
            listener.addOrdered(toVariableIdentifiers(list), TYPE_OP_REL_TO_REL_OP.get(operator));
        } catch (ContradictionException e) {
            throw new UncheckedContradictionException(e);
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see org.xcsp.parser.callbacks.XCallbacks2#buildCtrOrdered(java.lang.String,
     * org.xcsp.parser.entries.XVariables.XVarInteger[],
     * org.xcsp.parser.entries.XVariables.XVarInteger[],
     * org.xcsp.common.Types.TypeOperatorRel)
     */
    @Override
    public void buildCtrOrdered(String id, XVarInteger[] list, XVarInteger[] lengths,
            TypeOperatorRel operator) {
        try {
            listener.addOrderedWithVariableLength(toVariableIdentifiers(list),
                    toVariableIdentifiers(lengths), TYPE_OP_REL_TO_REL_OP.get(operator));
        } catch (ContradictionException e) {
            throw new UncheckedContradictionException(e);
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see org.xcsp.parser.callbacks.XCallbacks2#buildCtrPrimitive(java.lang.String,
     * org.xcsp.parser.entries.XVariables.XVarInteger,
     * org.xcsp.common.Types.TypeConditionOperatorRel, int)
     */
    @Override
    public void buildCtrPrimitive(String id, XVarInteger x, TypeConditionOperatorRel op, int k) {
        try {
            listener.addPrimitive(
                    x.id(), TYPE_COND_OP_REL_TO_REL_OP.get(op), BigInteger.valueOf(k));
        } catch (ContradictionException e) {
            throw new UncheckedContradictionException(e);
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see org.xcsp.parser.callbacks.XCallbacks2#buildCtrPrimitive(java.lang.String,
     * org.xcsp.parser.entries.XVariables.XVarInteger,
     * org.xcsp.common.Types.TypeArithmeticOperator, int,
     * org.xcsp.common.Types.TypeConditionOperatorRel, int)
     */
    @Override
    public void buildCtrPrimitive(String id, XVarInteger x, TypeArithmeticOperator aop, int p,
            TypeConditionOperatorRel op, int k) {
        try {
            listener.addPrimitive(x.id(), TYPE_ARITH_OP_TO_ARITH_OP.get(aop), BigInteger.valueOf(p),
                    TYPE_COND_OP_REL_TO_REL_OP.get(op), BigInteger.valueOf(k));
        } catch (ContradictionException e) {
            throw new UncheckedContradictionException(e);
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see org.xcsp.parser.callbacks.XCallbacks2#buildCtrPrimitive(java.lang.String,
     * org.xcsp.parser.entries.XVariables.XVarInteger,
     * org.xcsp.common.Types.TypeArithmeticOperator, int,
     * org.xcsp.common.Types.TypeConditionOperatorRel,
     * org.xcsp.parser.entries.XVariables.XVarInteger)
     */
    @Override
    public void buildCtrPrimitive(String id, XVarInteger x, TypeArithmeticOperator aop, int p,
            TypeConditionOperatorRel op, XVarInteger y) {
        try {
            listener.addPrimitive(x.id(), TYPE_ARITH_OP_TO_ARITH_OP.get(aop), BigInteger.valueOf(p),
                    TYPE_COND_OP_REL_TO_REL_OP.get(op), y.id());
        } catch (ContradictionException e) {
            throw new UncheckedContradictionException(e);
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see org.xcsp.parser.callbacks.XCallbacks2#buildCtrPrimitive(java.lang.String,
     * org.xcsp.parser.entries.XVariables.XVarInteger,
     * org.xcsp.common.Types.TypeArithmeticOperator,
     * org.xcsp.parser.entries.XVariables.XVarInteger,
     * org.xcsp.common.Types.TypeConditionOperatorRel, int)
     */
    @Override
    public void buildCtrPrimitive(String id, XVarInteger x, TypeArithmeticOperator aop,
            XVarInteger y, TypeConditionOperatorRel op, int k) {
        try {
            listener.addPrimitive(x.id(), TYPE_ARITH_OP_TO_ARITH_OP.get(aop), y.id(),
                    TYPE_COND_OP_REL_TO_REL_OP.get(op), BigInteger.valueOf(k));
        } catch (ContradictionException e) {
            throw new UncheckedContradictionException(e);
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see org.xcsp.parser.callbacks.XCallbacks2#buildCtrPrimitive(java.lang.String,
     * org.xcsp.parser.entries.XVariables.XVarInteger,
     * org.xcsp.common.Types.TypeArithmeticOperator,
     * org.xcsp.parser.entries.XVariables.XVarInteger,
     * org.xcsp.common.Types.TypeConditionOperatorRel,
     * org.xcsp.parser.entries.XVariables.XVarInteger)
     */
    @Override
    public void buildCtrPrimitive(String id, XVarInteger x, TypeArithmeticOperator aop,
            XVarInteger y, TypeConditionOperatorRel op, XVarInteger z) {
        try {
            listener.addPrimitive(x.id(), TYPE_ARITH_OP_TO_ARITH_OP.get(aop), y.id(),
                    TYPE_COND_OP_REL_TO_REL_OP.get(op), z.id());
        } catch (ContradictionException e) {
            throw new UncheckedContradictionException(e);
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see org.xcsp.parser.callbacks.XCallbacks2#buildCtrPrimitive(java.lang.String,
     * org.xcsp.parser.entries.XVariables.XVarInteger,
     * org.xcsp.common.Types.TypeUnaryArithmeticOperator,
     * org.xcsp.parser.entries.XVariables.XVarInteger)
     */
    @Override
    public void buildCtrPrimitive(String id, XVarInteger x, TypeUnaryArithmeticOperator aop,
            XVarInteger y) {
        try {
            listener.addPrimitive(TYPE_UNARY_ARITH_OP_TO_ARITH_OP.get(aop), y.id(), x.id());
        } catch (ContradictionException e) {
            throw new UncheckedContradictionException(e);
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see org.xcsp.parser.callbacks.XCallbacks2#buildCtrPrimitive(java.lang.String,
     * org.xcsp.parser.entries.XVariables.XVarInteger,
     * org.xcsp.common.Types.TypeConditionOperatorSet, int[])
     */
    @Override
    public void buildCtrPrimitive(String id, XVarInteger x, TypeConditionOperatorSet op, int[] t) {
        try {
            listener.addPrimitive(x.id(), TYPE_COND_OP_SET_TO_SET_OP.get(op), toBigInteger(t));
        } catch (ContradictionException e) {
            throw new UncheckedContradictionException(e);
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see org.xcsp.parser.callbacks.XCallbacks2#buildCtrPrimitive(java.lang.String,
     * org.xcsp.parser.entries.XVariables.XVarInteger,
     * org.xcsp.common.Types.TypeConditionOperatorSet, int, int)
     */
    @Override
    public void buildCtrPrimitive(String id, XVarInteger x, TypeConditionOperatorSet op,
            int min, int max) {
        try {
            listener.addPrimitive(x.id(), TYPE_COND_OP_SET_TO_SET_OP.get(op),
                    BigInteger.valueOf(min), BigInteger.valueOf(max));
        } catch (ContradictionException e) {
            throw new UncheckedContradictionException(e);
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see org.xcsp.parser.callbacks.XCallbacks2#buildCtrSum(java.lang.String,
     * org.xcsp.parser.entries.XVariables.XVarInteger[], org.xcsp.common.Condition)
     */
    @Override
    public void buildCtrSum(String id, XVarInteger[] list, Condition condition) {
        buildCtrWithCondition(condition,
                (op, rhs) -> listener.addSum(toVariableIdentifiers(list), op, rhs),
                (op, rhs) -> listener.addSum(toVariableIdentifiers(list), op, rhs));
    }

    /*
     * (non-Javadoc)
     *
     * @see org.xcsp.parser.callbacks.XCallbacks2#buildCtrSum(java.lang.String,
     * org.xcsp.parser.entries.XVariables.XVarInteger[], int[], org.xcsp.common.Condition)
     */
    @Override
    public void buildCtrSum(String id, XVarInteger[] list, int[] coeffs, Condition condition) {
        buildCtrWithCondition(condition,
                (op, rhs) -> listener.addSum(
                        toVariableIdentifiers(list), toBigInteger(coeffs), op, rhs),
                (op, rhs) -> listener.addSum(
                        toVariableIdentifiers(list), toBigInteger(coeffs), op, rhs));
    }

    /*
     * (non-Javadoc)
     *
     * @see org.xcsp.parser.callbacks.XCallbacks2#buildCtrSum(java.lang.String,
     * org.xcsp.common.predicates.XNode[], org.xcsp.common.Condition)
     */
    @Override
    public void buildCtrSum(String id, XNode<XVarInteger>[] trees, Condition condition) {
        buildCtrWithCondition(condition,
                (op, rhs) -> listener.addSumIntension(toIntensionConstraints(trees), op, rhs),
                (op, rhs) -> listener.addSumIntension(toIntensionConstraints(trees), op, rhs));
    }

    /*
     * (non-Javadoc)
     *
     * @see org.xcsp.parser.callbacks.XCallbacks2#buildCtrSum(java.lang.String,
     * org.xcsp.common.predicates.XNode[], int[], org.xcsp.common.Condition)
     */
    @Override
    public void buildCtrSum(String id, XNode<XVarInteger>[] trees, int[] coeffs,
            Condition condition) {
        buildCtrWithCondition(condition,
                (op, rhs) -> listener.addSumIntension(
                        toIntensionConstraints(trees), toBigInteger(coeffs), op, rhs),
                (op, rhs) -> listener.addSumIntension(
                        toIntensionConstraints(trees), toBigInteger(coeffs), op, rhs));
    }

    /*
     * (non-Javadoc)
     *
     * @see org.xcsp.parser.callbacks.XCallbacks2#buildCtrSum(java.lang.String,
     * org.xcsp.parser.entries.XVariables.XVarInteger[],
     * org.xcsp.parser.entries.XVariables.XVarInteger[], org.xcsp.common.Condition)
     */
    @Override
    public void buildCtrSum(String id, XVarInteger[] list, XVarInteger[] coeffs,
            Condition condition) {
        buildCtrWithCondition(condition,
                (op, rhs) -> listener.addSumWithVariableCoefficients(
                        toVariableIdentifiers(list), toVariableIdentifiers(coeffs), op, rhs),
                (op, rhs) -> listener.addSumWithVariableCoefficients(
                        toVariableIdentifiers(list), toVariableIdentifiers(coeffs), op, rhs));
    }

    /*
     * (non-Javadoc)
     *
     * @see org.xcsp.parser.callbacks.XCallbacks2#buildCtrSum(java.lang.String,
     * org.xcsp.common.predicates.XNode[],
     * org.xcsp.parser.entries.XVariables.XVarInteger[], org.xcsp.common.Condition)
     */
    @Override
    public void buildCtrSum(String id, XNode<XVarInteger>[] trees, XVarInteger[] coeffs,
            Condition condition) {
        buildCtrWithCondition(condition,
                (op, rhs) -> listener.addSumIntensionWithVariableCoefficients(
                        toIntensionConstraints(trees), toVariableIdentifiers(coeffs), op, rhs),
                (op, rhs) -> listener.addSumIntensionWithVariableCoefficients(
                        toIntensionConstraints(trees), toVariableIdentifiers(coeffs), op, rhs));
    }

    /*
     * (non-Javadoc)
     *
     * @see org.xcsp.parser.callbacks.XCallbacks2#buildObjToMinimize(java.lang.String,
     * org.xcsp.parser.entries.XVariables.XVarInteger)
     */
    @Override
    public void buildObjToMinimize(String id, XVarInteger x) {
        listener.minimizeVariable(x.id());
    }

    /*
     * (non-Javadoc)
     *
     * @see org.xcsp.parser.callbacks.XCallbacks2#buildObjToMaximize(java.lang.String,
     * org.xcsp.parser.entries.XVariables.XVarInteger)
     */
    @Override
    public void buildObjToMaximize(String id, XVarInteger x) {
        listener.maximizeVariable(x.id());
    }

    /*
     * (non-Javadoc)
     *
     * @see org.xcsp.parser.callbacks.XCallbacks2#buildObjToMinimize(java.lang.String,
     * org.xcsp.common.predicates.XNodeParent)
     */
    @Override
    public void buildObjToMinimize(String id, XNodeParent<XVarInteger> tree) {
        listener.minimizeExpression(new IntensionConstraintXNodeAdapter(tree));
    }

    /*
     * (non-Javadoc)
     *
     * @see org.xcsp.parser.callbacks.XCallbacks2#buildObjToMaximize(java.lang.String,
     * org.xcsp.common.predicates.XNodeParent)
     */
    @Override
    public void buildObjToMaximize(String id, XNodeParent<XVarInteger> tree) {
        listener.maximizeExpression(new IntensionConstraintXNodeAdapter(tree));
    }

    /*
     * (non-Javadoc)
     *
     * @see org.xcsp.parser.callbacks.XCallbacks2#buildObjToMinimize(java.lang.String,
     * org.xcsp.common.Types.TypeObjective,
     * org.xcsp.parser.entries.XVariables.XVarInteger[])
     */
    @Override
    public void buildObjToMinimize(String id, TypeObjective type, XVarInteger[] list) {
        if (type == TypeObjective.SUM) {
            listener.minimizeSum(toVariableIdentifiers(list));

        } else if (type == TypeObjective.PRODUCT) {
            listener.minimizeProduct(toVariableIdentifiers(list));

        } else if (type == TypeObjective.MINIMUM) {
            listener.minimizeMinimum(toVariableIdentifiers(list));

        } else if (type == TypeObjective.MAXIMUM) {
            listener.minimizeMaximum(toVariableIdentifiers(list));

        } else if (type == TypeObjective.NVALUES) {
            listener.minimizeNValues(toVariableIdentifiers(list));

        } else {
            throw new UnsupportedOperationException(
                    "Objective function of type " + type + " is not (yet?) supported");
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see org.xcsp.parser.callbacks.XCallbacks2#buildObjToMaximize(java.lang.String,
     * org.xcsp.common.Types.TypeObjective,
     * org.xcsp.parser.entries.XVariables.XVarInteger[])
     */
    @Override
    public void buildObjToMaximize(String id, TypeObjective type, XVarInteger[] list) {
        if (type == TypeObjective.SUM) {
            listener.maximizeSum(toVariableIdentifiers(list));

        } else if (type == TypeObjective.PRODUCT) {
            listener.maximizeProduct(toVariableIdentifiers(list));

        } else if (type == TypeObjective.MINIMUM) {
            listener.maximizeMinimum(toVariableIdentifiers(list));

        } else if (type == TypeObjective.MAXIMUM) {
            listener.maximizeMaximum(toVariableIdentifiers(list));

        } else if (type == TypeObjective.NVALUES) {
            listener.maximizeNValues(toVariableIdentifiers(list));

        } else {
            throw new UnsupportedOperationException(
                    "Objective function of type " + type + " is not (yet?) supported");
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see org.xcsp.parser.callbacks.XCallbacks2#buildObjToMinimize(java.lang.String,
     * org.xcsp.common.Types.TypeObjective,
     * org.xcsp.parser.entries.XVariables.XVarInteger[], int[])
     */
    @Override
    public void buildObjToMinimize(String id, TypeObjective type, XVarInteger[] list,
            int[] coeffs) {
        if (type == TypeObjective.SUM) {
            listener.minimizeSum(toVariableIdentifiers(list), toBigInteger(coeffs));

        } else if (type == TypeObjective.PRODUCT) {
            listener.minimizeProduct(toVariableIdentifiers(list), toBigInteger(coeffs));

        } else if (type == TypeObjective.MINIMUM) {
            listener.minimizeMinimum(toVariableIdentifiers(list), toBigInteger(coeffs));

        } else if (type == TypeObjective.MAXIMUM) {
            listener.minimizeMaximum(toVariableIdentifiers(list), toBigInteger(coeffs));

        } else if (type == TypeObjective.NVALUES) {
            listener.minimizeNValues(toVariableIdentifiers(list), toBigInteger(coeffs));

        } else {
            throw new UnsupportedOperationException(
                    "Objective function of type " + type + " is not (yet?) supported");
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see org.xcsp.parser.callbacks.XCallbacks2#buildObjToMaximize(java.lang.String,
     * org.xcsp.common.Types.TypeObjective,
     * org.xcsp.parser.entries.XVariables.XVarInteger[], int[])
     */
    @Override
    public void buildObjToMaximize(String id, TypeObjective type, XVarInteger[] list,
            int[] coeffs) {
        if (type == TypeObjective.SUM) {
            listener.maximizeSum(toVariableIdentifiers(list), toBigInteger(coeffs));

        } else if (type == TypeObjective.PRODUCT) {
            listener.maximizeProduct(toVariableIdentifiers(list), toBigInteger(coeffs));

        } else if (type == TypeObjective.MINIMUM) {
            listener.maximizeMinimum(toVariableIdentifiers(list), toBigInteger(coeffs));

        } else if (type == TypeObjective.MAXIMUM) {
            listener.maximizeMaximum(toVariableIdentifiers(list), toBigInteger(coeffs));

        } else if (type == TypeObjective.NVALUES) {
            listener.maximizeNValues(toVariableIdentifiers(list), toBigInteger(coeffs));

        } else {
            throw new UnsupportedOperationException(
                    "Objective function of type " + type + " is not (yet?) supported");
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see org.xcsp.parser.callbacks.XCallbacks2#buildObjToMinimize(java.lang.String,
     * org.xcsp.common.Types.TypeObjective, org.xcsp.common.predicates.XNode[])
     */
    @Override
    public void buildObjToMinimize(String id, TypeObjective type, XNode<XVarInteger>[] trees) {
        if (type == TypeObjective.SUM) {
            listener.minimizeExpressionSum(toIntensionConstraints(trees));

        } else if (type == TypeObjective.PRODUCT) {
            listener.minimizeExpressionProduct(toIntensionConstraints(trees));

        } else if (type == TypeObjective.MINIMUM) {
            listener.minimizeExpressionMinimum(toIntensionConstraints(trees));

        } else if (type == TypeObjective.MAXIMUM) {
            listener.minimizeExpressionMaximum(toIntensionConstraints(trees));

        } else if (type == TypeObjective.NVALUES) {
            listener.minimizeExpressionNValues(toIntensionConstraints(trees));

        } else {
            throw new UnsupportedOperationException(
                    "Objective function of type " + type + " is not (yet?) supported");
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see org.xcsp.parser.callbacks.XCallbacks2#buildObjToMaximize(java.lang.String,
     * org.xcsp.common.Types.TypeObjective, org.xcsp.common.predicates.XNode[])
     */
    @Override
    public void buildObjToMaximize(String id, TypeObjective type, XNode<XVarInteger>[] trees) {
        if (type == TypeObjective.SUM) {
            listener.maximizeExpressionSum(toIntensionConstraints(trees));

        } else if (type == TypeObjective.PRODUCT) {
            listener.maximizeExpressionProduct(toIntensionConstraints(trees));

        } else if (type == TypeObjective.MINIMUM) {
            listener.maximizeExpressionMinimum(toIntensionConstraints(trees));

        } else if (type == TypeObjective.MAXIMUM) {
            listener.maximizeExpressionMaximum(toIntensionConstraints(trees));

        } else if (type == TypeObjective.NVALUES) {
            listener.maximizeExpressionNValues(toIntensionConstraints(trees));

        } else {
            throw new UnsupportedOperationException(
                    "Objective function of type " + type + " is not (yet?) supported");
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see org.xcsp.parser.callbacks.XCallbacks2#buildObjToMinimize(java.lang.String,
     * org.xcsp.common.Types.TypeObjective, org.xcsp.common.predicates.XNode[], int[])
     */
    @Override
    public void buildObjToMinimize(String id, TypeObjective type, XNode<XVarInteger>[] trees,
            int[] coeffs) {
        if (type == TypeObjective.SUM) {
            listener.minimizeExpressionSum(toIntensionConstraints(trees), toBigInteger(coeffs));

        } else if (type == TypeObjective.PRODUCT) {
            listener.minimizeExpressionProduct(toIntensionConstraints(trees), toBigInteger(coeffs));

        } else if (type == TypeObjective.MINIMUM) {
            listener.minimizeExpressionMinimum(toIntensionConstraints(trees), toBigInteger(coeffs));

        } else if (type == TypeObjective.MAXIMUM) {
            listener.minimizeExpressionMaximum(toIntensionConstraints(trees), toBigInteger(coeffs));

        } else if (type == TypeObjective.NVALUES) {
            listener.minimizeExpressionNValues(toIntensionConstraints(trees), toBigInteger(coeffs));

        } else {
            throw new UnsupportedOperationException(
                    "Objective function of type " + type + " is not (yet?) supported");
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see org.xcsp.parser.callbacks.XCallbacks2#buildObjToMaximize(java.lang.String,
     * org.xcsp.common.Types.TypeObjective, org.xcsp.common.predicates.XNode[], int[])
     */
    @Override
    public void buildObjToMaximize(String id, TypeObjective type, XNode<XVarInteger>[] trees,
            int[] coeffs) {
        if (type == TypeObjective.SUM) {
            listener.maximizeExpressionSum(toIntensionConstraints(trees), toBigInteger(coeffs));

        } else if (type == TypeObjective.PRODUCT) {
            listener.maximizeExpressionProduct(toIntensionConstraints(trees), toBigInteger(coeffs));

        } else if (type == TypeObjective.MINIMUM) {
            listener.maximizeExpressionMinimum(toIntensionConstraints(trees), toBigInteger(coeffs));

        } else if (type == TypeObjective.MAXIMUM) {
            listener.maximizeExpressionMaximum(toIntensionConstraints(trees), toBigInteger(coeffs));

        } else if (type == TypeObjective.NVALUES) {
            listener.maximizeExpressionNValues(toIntensionConstraints(trees), toBigInteger(coeffs));

        } else {
            throw new UnsupportedOperationException(
                    "Objective function of type " + type + " is not (yet?) supported");
        }
    }

    /**
     * Transforms an array of {@code int} values into a vector of {@link BigInteger}.
     *
     * @param array The array to transform.
     *
     * @return The vector of {@link BigInteger}.
     */
    private static IVec<BigInteger> toBigInteger(int[] array) {
        return toBigInteger(array, false);
    }

    /**
     * Transforms an array of {@code int} values into a vector of {@link BigInteger}.
     *
     * @param array The array to transform
     * @param starred Whether {@link Constants#STAR_INT} must be interpreted as a
     *        {@code *}.
     *
     * @return The vector of {@link BigInteger}.
     */
    private static IVec<BigInteger> toBigInteger(int[] array, boolean starred) {
        var vec = new Vec<BigInteger>(array.length);
        for (var i : array) {
            if (starred && (i == Constants.STAR_INT)) {
                vec.push(null);

            } else {
                vec.push(BigInteger.valueOf(i));
            }
        }
        return vec;
    }

    /**
     * Transforms a matrix of {@code int} values into a vector-based matrix of
     * {@link BigInteger}.
     *
     * @param matrix The matrix to transform.
     *
     * @return The matrix of {@link BigInteger}.
     */
    private static IVec<IVec<BigInteger>> toBigInteger(int[][] matrix) {
        return toBigInteger(matrix, false);
    }

    /**
     * Transforms a matrix of {@code int} values into a vector-based matrix of
     * {@link BigInteger}.
     *
     * @param matrix The matrix to transform.
     * @param starred Whether {@link Constants#STAR_INT} must be interpreted as a
     *        {@code *}.
     *
     * @return The matrix of {@link BigInteger}.
     */
    private static IVec<IVec<BigInteger>> toBigInteger(int[][] matrix, boolean starred) {
        var vec = new Vec<IVec<BigInteger>>(matrix.length);
        for (var array : matrix) {
            vec.push(toBigInteger(array, starred));
        }
        return vec;
    }

    /**
     * Extracts the identifiers of the given variables.
     *
     * @param array The array of the variables to extract the identifiers of.
     *
     * @return The vector of the identifiers of the variables in the array.
     */
    private static IVec<String> toVariableIdentifiers(XVarInteger[] array) {
        var vec = new Vec<String>(array.length);
        for (var v : array) {
            vec.push(v.id());
        }
        return vec;
    }

    /**
     * Extracts the identifiers of the given variables.
     *
     * @param matrix The matrix of the variables to extract the identifiers of.
     *
     * @return The matrix of the identifiers of the variables in the matrix.
     */
    private static IVec<IVec<String>> toVariableIdentifiers(XVarInteger[][] matrix) {
        var vec = new Vec<IVec<String>>(matrix.length);
        for (var array : matrix) {
            vec.push(toVariableIdentifiers(array));
        }
        return vec;
    }

    /**
     * Adapts an array of {@link XNode} to a vector of intension constraints.
     *
     * @param nodes The nodes to adapt.
     *
     * @return The vector of intension constraints.
     */
    private static IVec<IIntensionConstraint> toIntensionConstraints(XNode<?>[] nodes) {
        var vec = new Vec<IIntensionConstraint>(nodes.length);
        for (var n : nodes) {
            vec.push(new IntensionConstraintXNodeAdapter(n));
        }
        return vec;
    }

    /**
     * The ConditionalConstraintBuilder is a private functional interface allowing to deal
     * with {@link Condition} objects, and especially to invoke the right method depending
     * on the type of the right-hand side of this {@link Condition} using lambda
     * expressions to avoid code duplication.
     *
     * This interface is only intended to be used by {@link UglyXCallback} instances,
     * hence its private status within this class.
     *
     * @param <O> The type of the operator used in the constraint.
     * @param <T> The type of the value of the right-hand side of the constraint.
     */
    @FunctionalInterface
    private static interface ConditionalConstraintBuilder<O extends Operator, T> {

        /**
         * Builds the constraint based on a {@link Condition} object.
         *
         * @param operator The operator of the condition.
         * @param rightHandSide The right-hand side of the condition.
         *
         * @throws ContradictionException If building the constraint results in a trivial
         *         inconsistency.
         */
        void buildCtr(O operator, T rightHandSide) throws ContradictionException;

    }

    /**
     * Builds a constraint that uses a condition.
     * The type of the right-hand side of the constraint will be used to choose the right
     * way of building the constraint.
     *
     * @param condition The condition of the constraint to build.
     * @param ifConstant The builder to use to build a constraint if the condition has a
     *        constant on its right-hand side.
     * @param ifVariable The builder to use to build a constraint if the condition has a
     *        variable on its right-hand side.
     *
     * @throws UnsupportedOperationException If the condition uses an unrecognized
     *         operator for the constraint to build.
     */
    private void buildCtrWithCondition(Condition condition,
            ConditionalConstraintBuilder<RelationalOperator, BigInteger> ifConstant,
            ConditionalConstraintBuilder<RelationalOperator, String> ifVariable) {
        buildCtrWithCondition(condition, ifConstant, ifVariable,
                (op, rhs) -> {
                    throw new UnsupportedOperationException(
                            "Sets are not supported (yet?) for this constraint");
                },
                (op, rhs) -> {
                    throw new UnsupportedOperationException(
                            "Sets are not supported (yet?) for this constraint");
                });
    }

    /**
     * Builds a constraint that uses a condition.
     * The type of the right-hand side of the constraint will be used to choose the right
     * way of building the constraint.
     *
     * @param condition The condition of the constraint to build.
     * @param ifConstant The builder to use to build a constraint if the condition has a
     *        constant on its right-hand side.
     * @param ifVariable The builder to use to build a constraint if the condition has a
     *        variable on its right-hand side.
     * @param ifRange The builder to use to build a constraint if the condition has a
     *        range on its right-hand side.
     * @param ifSetOfValues The builder to use to build a constraint if the condition has
     *        a set on its right-hand side.
     *
     * @throws UnsupportedOperationException If the condition uses an unrecognized
     *         operator.
     */
    private void buildCtrWithCondition(Condition condition,
            ConditionalConstraintBuilder<RelationalOperator, BigInteger> ifConstant,
            ConditionalConstraintBuilder<RelationalOperator, String> ifVariable,
            ConditionalConstraintBuilder<SetBelongingOperator, BigInteger[]> ifRange,
            ConditionalConstraintBuilder<SetBelongingOperator, IVec<BigInteger>> ifSetOfValues) {
        try {
            // Getting the operator involved in the condition.
            var operator = condition.operatorTypeExpr();

            // Considering the case where the condition uses a relational operator.
            if (operator.isRelationalOperator()) {
                buildCtrRelational(condition, ifConstant, ifVariable);
                return;
            }

            // Considering the case where the condition is on a set.
            if ((operator == TypeExpr.IN) || (operator == TypeExpr.NOTIN)) {
                buildCtrConditionSet(condition, ifRange, ifSetOfValues);
                return;
            }

            throw new UnsupportedOperationException(
                    "Unknown condition operator: " + operator);

        } catch (ContradictionException e) {
            throw new UncheckedContradictionException(e);
        }
    }

    /**
     * Builds a constraint that uses a relational-based condition.
     * The type of the right-hand side of the constraint will be used to choose the right
     * way of building the constraint.
     *
     * @param condition The condition of the constraint to build.
     * @param ifConstant The builder to use to build a constraint if the condition has a
     *        constant on its right-hand side.
     * @param ifVariable The builder to use to build a constraint if the condition has a
     *        variable on its right-hand side.
     *
     * @throws ContradictionException If building the constraint results in a trivial
     *         inconsistency.
     * @throws UnsupportedOperationException If the condition uses an unrecognized
     *         operator.
     */
    private void buildCtrRelational(Condition condition,
            ConditionalConstraintBuilder<RelationalOperator, BigInteger> ifConstant,
            ConditionalConstraintBuilder<RelationalOperator, String> ifVariable)
            throws ContradictionException {
        // Translating the operator to the appropriate type.
        var relOperator = TYPE_EXPR_TO_REL_OP.get(condition.operatorTypeExpr());

        // Checking whether the condition involves a variable.
        var variable = condition.involvedVar();
        if (variable == null) {
            // The right-hand side is a value.
            ifConstant.buildCtr(relOperator, BigInteger.valueOf((long) condition.rightTerm()));

        } else {
            // The right-hand side is a variable.
            ifVariable.buildCtr(relOperator, variable.id());
        }
    }

    /**
     * Builds a constraint that uses a set-based condition.
     * The type of the right-hand side of the constraint will be used to choose the right
     * way of building the constraint.
     *
     * @param condition The condition of the constraint to build.
     * @param ifRange The builder to use to build a constraint if the condition has a
     *        range on its right-hand side.
     * @param ifSetOfValues The builder to use to build a constraint if the condition has
     *        a set on its right-hand side.
     *
     * @throws ContradictionException If building the constraint results in a trivial
     *         inconsistency.
     * @throws UnsupportedOperationException If the condition uses an unrecognized
     *         operator.
     */
    private void buildCtrConditionSet(Condition condition,
            ConditionalConstraintBuilder<SetBelongingOperator, BigInteger[]> ifRange,
            ConditionalConstraintBuilder<SetBelongingOperator, IVec<BigInteger>> ifSetOfValues)
            throws ContradictionException {
        // Translating the operator to the appropriate type.
        var setOperator = SetBelongingOperator.IN;
        if (condition.operatorTypeExpr() == TypeExpr.NOTIN) {
            setOperator = SetBelongingOperator.NOT_IN;
        }

        if (condition instanceof ConditionIntvl) {
            // The set is represented with a range.
            var range = (ConditionIntvl) condition;
            var min = BigInteger.valueOf(range.min);
            var max = BigInteger.valueOf(range.max);
            ifRange.buildCtr(setOperator, new BigInteger[] { min, max });
            return;
        }

        if (condition instanceof ConditionIntset) {
            // The set is represented with an array of values.
            var set = (ConditionIntset) condition;
            ifSetOfValues.buildCtr(setOperator, toBigInteger(set.t));
            return;
        }

        throw new UnsupportedOperationException(
                "Unknown condition type: " + condition.getClass());
    }

}
