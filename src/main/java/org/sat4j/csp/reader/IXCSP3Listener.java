/**
 * Sat4j-CSP, a CSP solver based on the Sat4j platform.
 * Copyright (c) 2021-2022 - Exakis Nelite, Univ Artois & CNRS.
 * All rightVariables reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 * If not, see {@link http://www.gnu.org/licenses}.
 */

package org.sat4j.csp.reader;

import java.math.BigInteger;

import org.sat4j.csp.constraints.ArithmeticOperator;
import org.sat4j.csp.constraints.BooleanOperator;
import org.sat4j.csp.constraints.RelationalOperator;
import org.sat4j.csp.constraints.SetBelongingOperator;
import org.sat4j.csp.constraints.intension.IIntensionConstraint;
import org.sat4j.specs.ContradictionException;
import org.sat4j.specs.IVec;
import org.sat4j.specs.IVecInt;

/**
 * The IXCSP3Listener defines a listener interface for listening to the events triggered
 * when an XCSP3 instance is being read.
 *
 * @author Thibault Falque
 * @author Romain Wallon
 *
 * @version 0.1.0
 */
public interface IXCSP3Listener {

    /**
     * Notifies this listener that a new variable is to be created.
     *
     * @param id The identifier of the variable to create.
     * @param min The minimum value of the domain of the variable.
     * @param max The maximum value of the domain of the variable.
     */
    void newVariable(String id, int min, int max);

    /**
     * Notifies this listener that a new variable is to be created.
     *
     * @param id The identifier of the variable to create.
     * @param min The minimum value of the domain of the variable.
     * @param max The maximum value of the domain of the variable.
     */
    void newVariable(String id, BigInteger min, BigInteger max);

    /**
     * Notifies this listener that a new variable is to be created.
     *
     * @param id The identifier of the variable to create.
     * @param values The values of the domain of the variable.
     */
    void newVariable(String id, IVecInt values);

    /**
     * Notifies this listener that a new variable is to be created.
     *
     * @param id The identifier of the variable to create.
     * @param values The values of the domain of the variable.
     */
    void newVariable(String id, IVec<BigInteger> values);

    /**
     * Notifies this listener that an {@code instantiation} constraint is to be added.
     *
     * @param variable The variable to assign.
     * @param value The value to assign to the variable.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    void addInstantiation(String variable, int value) throws ContradictionException;

    /**
     * Notifies this listener that an {@code instantiation} constraint is to be added.
     *
     * @param variable The variable to assign.
     * @param value The value to assign to the variable.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    void addInstantiation(String variable, BigInteger value) throws ContradictionException;

    /**
     * Notifies this listener that an {@code instantiation} constraint is to be added.
     *
     * @param variables The variables to assign.
     * @param values The values to assign to the variables.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    void addInstantiation(IVec<String> variables, IVecInt values) throws ContradictionException;

    /**
     * Notifies this listener that a {@code clause} constraint is to be added.
     *
     * @param positive The (Boolean) variables appearing positively in the clause.
     * @param negative The (Boolean) variables appearing negatively in the clause.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    void addClause(IVec<String> positive, IVec<String> negative) throws ContradictionException;

    /**
     * Notifies this listener that a {@code logical} constraint is to be added.
     *
     * @param operator The Boolean operator to apply on the variables.
     * @param variables The (Boolean) variables on which the operator is applied.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    void addLogical(BooleanOperator operator, IVec<String> variables) throws ContradictionException;

    /**
     * Notifies this listener that a {@code logical} constraint is to be added.
     *
     * @param variable The (Boolean) variable whose assignment depends on the truth value
     *        of the logical operations.
     * @param equiv Whether {@code variable} must be equivalent to the truth value of the
     *        logical operations.
     * @param operator The Boolean operator to apply on the variables.
     * @param variables The (Boolean) variables on which the operator is applied.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    void addLogical(String variable, boolean equiv, BooleanOperator operator,
            IVec<String> variables) throws ContradictionException;

    /**
     * Notifies this listener that a {@code logical} constraint is to be added.
     *
     * @param variable The (Boolean) variable whose assignment depends on the truth value
     *        of the comparison between {@code left} and {@code right}.
     * @param left The variable on the left-hand side of the comparison.
     * @param operator The relational operator used to compare {@code left} and
     *        {@code right}.
     * @param right The value on the right-hand side of the comparison.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    void addLogical(String variable, String left, RelationalOperator operator, BigInteger right)
            throws ContradictionException;

    /**
     * Notifies this listener that a {@code logical} constraint is to be added.
     *
     * @param variable The (Boolean) variable whose assignment depends on the truth value
     *        of the comparison between {@code left} and {@code right}.
     * @param left The variable on the left-hand side of the comparison.
     * @param operator The relational operator used to compare {@code left} and
     *        {@code right}.
     * @param right The variable on the right-hand side of the comparison.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    void addLogical(String variable, String left, RelationalOperator operator, String right)
            throws ContradictionException;

    /**
     * Notifies this listener that an {@code instantiation} constraint is to be added.
     *
     * @param variables The variables to assign.
     * @param values The values to assign to the variables.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    void addInstantiation(IVec<String> variables, IVec<BigInteger> values)
            throws ContradictionException;

    /**
     * Notifies this listener that an {@code all-different} constraint is to be added.
     *
     * @param variables The variables that should all be different.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    void addAllDifferent(IVec<String> variables) throws ContradictionException;

    /**
     * Notifies this listener that an {@code all-different} constraint is to be added.
     *
     * @param variables The variables that should all be different.
     * @param except The values not to consider in the constraint.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    void addAllDifferent(IVec<String> variables, IVec<BigInteger> except)
            throws ContradictionException;

    /**
     * Notifies this listener that an {@code all-different} constraint is to be added.
     *
     * @param variableMatrix The matrix of variables that should all be different.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    void addAllDifferentMatrix(IVec<IVec<String>> variableMatrix)
            throws ContradictionException;

    /**
     * Notifies this listener that an {@code all-different} constraint is to be added.
     *
     * @param variableMatrix The matrix of variables that should all be different.
     * @param except The values not to consider in the constraint.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    void addAllDifferentMatrix(IVec<IVec<String>> variableMatrix, IVec<BigInteger> except)
            throws ContradictionException;

    /**
     * Notifies this listener that an {@code all-different} constraint is to be added.
     *
     * @param variableLists The lists of variables that should all be different.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    void addAllDifferentList(IVec<IVec<String>> variableLists) throws ContradictionException;

    /**
     * Notifies this listener that an {@code all-different} constraint is to be added.
     *
     * @param intensionConstraints The intension constraints that should all be different.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    void addAllDifferentIntension(IVec<IIntensionConstraint> intensionConstraints)
            throws ContradictionException;

    /**
     * Notifies this listener that a {@code cardinality} constraint is to be added.
     *
     * @param variables The variables to count the assignments of.
     * @param values The assignable values to count.
     * @param occurs The number of times each value can be assigned.
     * @param closed Whether only the values in {@code value} can be assigned to the
     *        variables.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    void addCardinalityWithConstantValuesAndConstantCounts(IVec<String> variables,
            IVec<BigInteger> values, IVec<BigInteger> occurs, boolean closed)
            throws ContradictionException;

    /**
     * Notifies this listener that a {@code cardinality} constraint is to be added.
     *
     * @param variables The variables to count the assignments of.
     * @param values The assignable values to count.
     * @param occursMin The minimum number of times each value can be assigned.
     * @param occursMax The maximum number of times each value can be assigned.
     * @param closed Whether only the values in {@code value} can be assigned to the
     *        variables.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    void addCardinalityWithConstantValuesAndConstantIntervalCounts(IVec<String> variables,
            IVec<BigInteger> values, IVec<BigInteger> occursMin, IVec<BigInteger> occursMax,
            boolean closed) throws ContradictionException;

    /**
     * Notifies this listener that a {@code cardinality} constraint is to be added.
     *
     * @param variables The variables to count the assignments of.
     * @param values The assignable values to count.
     * @param occurs The variables encoding the number of times each value can be
     *        assigned.
     * @param closed Whether only the values in {@code value} can be assigned to the
     *        variables.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    void addCardinalityWithConstantValuesAndVariableCounts(IVec<String> variables,
            IVec<BigInteger> values, IVec<String> occurs, boolean closed)
            throws ContradictionException;

    /**
     * Notifies this listener that a {@code cardinality} constraint is to be added.
     *
     * @param variables The variables to count the assignments of.
     * @param values The variables encoding the assignable values to count.
     * @param occurs The number of times each value can be assigned.
     * @param closed Whether only the values in {@code value} can be assigned to the
     *        variables.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    void addCardinalityWithVariableValuesAndConstantCounts(IVec<String> variables,
            IVec<String> values, IVec<BigInteger> occurs, boolean closed)
            throws ContradictionException;

    /**
     * Notifies this listener that a {@code cardinality} constraint is to be added.
     *
     * @param variables The variables to count the assignments of.
     * @param values The variables encoding the assignable values to count.
     * @param occursMin The minimum number of times each value can be assigned.
     * @param occursMax The maximum number of times each value can be assigned.
     * @param closed Whether only the values in {@code value} can be assigned to the
     *        variables.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    void addCardinalityWithVariableValuesAndConstantIntervalCounts(IVec<String> variables,
            IVec<String> values, IVec<BigInteger> occursMin, IVec<BigInteger> occursMax,
            boolean closed) throws ContradictionException;

    /**
     * Notifies this listener that a {@code cardinality} constraint is to be added.
     *
     * @param variables The variables to count the assignments of.
     * @param values The variables encoding the assignable values to count.
     * @param occurs The variables encoding the number of times each value can be
     *        assigned.
     * @param closed Whether only the values in {@code value} can be assigned to the
     *        variables.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    void addCardinalityWithVariableValuesAndVariableCounts(IVec<String> variables,
            IVec<String> values, IVec<String> occurs, boolean closed)
            throws ContradictionException;

    /**
     * Notifies this listener that a {@code channel} constraint is to be added.
     *
     * @param variables The variables appearing in the constraint.
     * @param startIndex The index at which the constraint starts.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    void addChannel(IVec<String> variables, int startIndex) throws ContradictionException;

    /**
     * Notifies this listener that a {@code channel} constraint is to be added.
     *
     * @param variables The variables among which exactly one should be satisfied starting
     *        from the given index.
     * @param startIndex The index at which the constraint starts.
     * @param value The variable containing the index of the satisfied variable.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    void addChannel(IVec<String> variables, int startIndex, String value)
            throws ContradictionException;

    /**
     * Notifies this listener that a {@code channel} constraint is to be added.
     *
     * @param variables The variables appearing in the constraint.
     * @param startIndex The index at which the constraint starts on the first vector of
     *        variables.
     * @param otherVariables The variables with which to channel the variables of the
     *        first vector.
     * @param otherStartIndex The index at which the constraint starts on the second
     *        vector of variables.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    void addChannel(IVec<String> variables, int startIndex,
            IVec<String> otherVariables, int otherStartIndex) throws ContradictionException;

    /**
     * Notifies this listener that an {@code at-least} constraint is to be added.
     *
     * @param variables The variables to count the assignments of.
     * @param value The value to count the assignments of.
     * @param count The minimum number of times the value can be assigned among the
     *        variables.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    void addAtLeast(IVec<String> variables, BigInteger value, BigInteger count)
            throws ContradictionException;

    /**
     * Notifies this listener that an {@code exactly} constraint is to be added.
     *
     * @param variables The variables to count the assignments of.
     * @param value The value to count the assignments of.
     * @param count The number of times the value can be assigned among the variables.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    void addExactly(IVec<String> variables, BigInteger value, BigInteger count)
            throws ContradictionException;

    /**
     * Notifies this listener that an {@code exactly} constraint is to be added.
     *
     * @param variables The variables to count the assignments of.
     * @param value The value to count the assignments of.
     * @param count The variable encoding the number of times the value can be assigned
     *        among the variables.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    void addExactly(IVec<String> variables, BigInteger value, String count)
            throws ContradictionException;

    /**
     * Notifies this listener that an {@code among} constraint is to be added.
     *
     * @param variables The variables to count the assignments of.
     * @param values The values to count the assignments of.
     * @param count The number of times the value can be assigned among the variables.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    void addAmong(IVec<String> variables, IVec<BigInteger> values,
            BigInteger count) throws ContradictionException;

    /**
     * Notifies this listener that an {@code among} constraint is to be added.
     *
     * @param variables The variables to count the assignments of.
     * @param values The values to count the assignments of.
     * @param count The variable encoding the number of times the values can be assigned
     *        among the variables.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    void addAmong(IVec<String> variables, IVec<BigInteger> values, String count)
            throws ContradictionException;

    /**
     * Notifies this listener that an {@code at-most} constraint is to be added.
     *
     * @param variables The variables to count the assignments of.
     * @param value The value to count the assignments of.
     * @param count The maximum number of times the value can be assigned among the
     *        variables.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    void addAtMost(IVec<String> variables, BigInteger value, BigInteger count)
            throws ContradictionException;

    /**
     * Notifies this listener that a {@code count} constraint is to be added.
     *
     * @param variables The variables to count the assignments of.
     * @param values The values to count the assignments of.
     * @param operator The operator to use to compare the number of assignments to their
     *        expected count.
     * @param count The number of times the values can be assigned among the variables.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    void addCountWithConstantValues(IVec<String> variables, IVec<BigInteger> values,
            RelationalOperator operator, BigInteger count) throws ContradictionException;

    /**
     * Notifies this listener that a {@code count} constraint is to be added.
     *
     * @param variables The variables to count the assignments of.
     * @param values The values to count the assignments of.
     * @param operator The operator to use to compare the number of assignments to their
     *        expected count.
     * @param count The variable encoding the number of times the values can be assigned
     *        among the variables.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    void addCountWithConstantValues(IVec<String> variables, IVec<BigInteger> values,
            RelationalOperator operator, String count) throws ContradictionException;

    /**
     * Notifies this listener that a {@code count} constraint is to be added.
     *
     * @param variables The variables to count the assignments of.
     * @param values The variables encoding the values to count the assignments of.
     * @param operator The operator to use to compare the number of assignments to their
     *        expected count.
     * @param count The number of times the values can be assigned among the variables.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    void addCountWithVariableValues(IVec<String> variables, IVec<String> values,
            RelationalOperator operator, BigInteger count) throws ContradictionException;

    /**
     * Notifies this listener that a {@code count} constraint is to be added.
     *
     * @param variables The variables to count the assignments of.
     * @param values The variables encoding the values to count the assignments of.
     * @param operator The operator to use to compare the number of assignments to their
     *        expected count.
     * @param count The variable encoding the number of times the values can be assigned
     *        among the variables.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    void addCountWithVariableValues(IVec<String> variables, IVec<String> values,
            RelationalOperator operator, String count) throws ContradictionException;

    /**
     * Notifies this listener that a {@code count} constraint is to be added.
     *
     * @param expressions The expressions to count the assignments of.
     * @param values The values to count the assignments of.
     * @param operator The operator to use to compare the number of assignments to
     *        their expected count.
     * @param count The variable encoding the number of times the values can be
     *        assigned among the variables.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    void addCountIntensionWithConstantValues(IVec<IIntensionConstraint> expressions,
            IVec<BigInteger> values, RelationalOperator operator, BigInteger count)
            throws ContradictionException;

    /**
     * Notifies this listener that a {@code count} constraint is to be added.
     *
     * @param expressions The expressions to count the assignments of.
     * @param values The values to count the assignments of.
     * @param operator The operator to use to compare the number of assignments to
     *        their expected count.
     * @param count The variable encoding the number of times the values can be
     *        assigned among the variables.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    void addCountIntensionWithConstantValues(IVec<IIntensionConstraint> expressions,
            IVec<BigInteger> values, RelationalOperator operator, String count)
            throws ContradictionException;

    /**
     * Notifies this listener that a {@code cumulative} constraint is to be added.
     *
     * @param origins The variables encoding the origins of the resources.
     * @param lengths The lengths of the tasks to assign.
     * @param heights The heights of the tasks to assign.
     * @param operator The operator to compare the cumulative use with.
     * @param value The value for the cumulative use.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    void addCumulativeConstantLengthsConstantHeights(IVec<String> origins,
            IVec<BigInteger> lengths, IVec<BigInteger> heights, RelationalOperator operator,
            BigInteger value) throws ContradictionException;

    /**
     * Notifies this listener that a {@code cumulative} constraint is to be added.
     *
     * @param origins The variables encoding the origins of the resources.
     * @param lengths The lengths of the tasks to assign.
     * @param ends The variables encoding the ends of the resources.
     * @param heights The heights of the tasks to assign.
     * @param operator The operator to compare the cumulative use with.
     * @param value The value for the cumulative use.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    void addCumulativeConstantLengthsConstantHeights(IVec<String> origins,
            IVec<BigInteger> lengths, IVec<String> ends, IVec<BigInteger> heights,
            RelationalOperator operator, BigInteger value) throws ContradictionException;

    /**
     * Notifies this listener that a {@code cumulative} constraint is to be added.
     *
     * @param origins The variables encoding the origins of the resources.
     * @param lengths The lengths of the tasks to assign.
     * @param heights The heights of the tasks to assign.
     * @param operator The operator to compare the cumulative use with.
     * @param value The variable encoding the value for the cumulative use.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    void addCumulativeConstantLengthsConstantHeights(IVec<String> origins,
            IVec<BigInteger> lengths, IVec<BigInteger> heights, RelationalOperator operator,
            String value) throws ContradictionException;

    /**
     * Notifies this listener that a {@code cumulative} constraint is to be added.
     *
     * @param origins The variables encoding the origins of the resources.
     * @param lengths The lengths of the tasks to assign.
     * @param ends The variables encoding the ends of the resources.
     * @param heights The heights of the tasks to assign.
     * @param operator The operator to compare the cumulative use with.
     * @param value The variable encoding the value for the cumulative use.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    void addCumulativeConstantLengthsConstantHeights(IVec<String> origins,
            IVec<BigInteger> lengths, IVec<String> ends, IVec<BigInteger> heights,
            RelationalOperator operator, String value) throws ContradictionException;

    /**
     * Notifies this listener that a {@code cumulative} constraint is to be added.
     *
     * @param origins The variables encoding the origins of the resources.
     * @param lengths The lengths of the tasks to assign.
     * @param heights The variable encoding the heights of the tasks to assign.
     * @param operator The operator to compare the cumulative use with.
     * @param value The value for the cumulative use.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    void addCumulativeConstantLengthsVariableHeights(IVec<String> origins,
            IVec<BigInteger> lengths, IVec<String> heights, RelationalOperator operator,
            BigInteger value) throws ContradictionException;

    /**
     * Notifies this listener that a {@code cumulative} constraint is to be added.
     *
     * @param origins The variables encoding the origins of the resources.
     * @param lengths The lengths of the tasks to assign.
     * @param ends The variables encoding the ends of the resources.
     * @param heights The variable encoding the heights of the tasks to assign.
     * @param operator The operator to compare the cumulative use with.
     * @param value The value for the cumulative use.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    void addCumulativeConstantLengthsVariableHeights(IVec<String> origins,
            IVec<BigInteger> lengths, IVec<String> ends, IVec<String> heights,
            RelationalOperator operator, BigInteger value) throws ContradictionException;

    /**
     * Notifies this listener that a {@code cumulative} constraint is to be added.
     *
     * @param origins The variables encoding the origins of the resources.
     * @param lengths The lengths of the tasks to assign.
     * @param heights The variable encoding the heights of the tasks to assign.
     * @param operator The operator to compare the cumulative use with.
     * @param value The variable encoding the value for the cumulative use.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    void addCumulativeConstantLengthsVariableHeights(IVec<String> origins,
            IVec<BigInteger> lengths, IVec<String> heights, RelationalOperator operator,
            String value) throws ContradictionException;

    /**
     * Notifies this listener that a {@code cumulative} constraint is to be added.
     *
     * @param origins The variables encoding the origins of the resources.
     * @param lengths The lengths of the tasks to assign.
     * @param ends The variables encoding the ends of the resources.
     * @param heights The variable encoding the heights of the tasks to assign.
     * @param operator The operator to compare the cumulative use with.
     * @param value The variable encoding the value for the cumulative use.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    void addCumulativeConstantLengthsVariableHeights(IVec<String> origins,
            IVec<BigInteger> lengths, IVec<String> ends, IVec<String> heights,
            RelationalOperator operator, String value) throws ContradictionException;

    /**
     * Notifies this listener that a {@code cumulative} constraint is to be added.
     *
     * @param origins The variables encoding the origins of the resources.
     * @param lengths The variables encoding the lengths of the tasks to assign.
     * @param heights The heights of the tasks to assign.
     * @param operator The operator to compare the cumulative use with.
     * @param value The value for the cumulative use.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    void addCumulativeVariableLengthsConstantHeights(IVec<String> origins,
            IVec<String> lengths, IVec<BigInteger> heights, RelationalOperator operator,
            BigInteger value) throws ContradictionException;

    /**
     * Notifies this listener that a {@code cumulative} constraint is to be added.
     *
     * @param origins The variables encoding the origins of the resources.
     * @param lengths The variables encoding the lengths of the tasks to assign.
     * @param ends The variables encoding the ends of the resources.
     * @param heights The heights of the tasks to assign.
     * @param operator The operator to compare the cumulative use with.
     * @param value The value for the cumulative use.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    void addCumulativeVariableLengthsConstantHeights(IVec<String> origins,
            IVec<String> lengths, IVec<String> ends, IVec<BigInteger> heights,
            RelationalOperator operator, BigInteger value) throws ContradictionException;

    /**
     * Notifies this listener that a {@code cumulative} constraint is to be added.
     *
     * @param origins The variables encoding the origins of the resources.
     * @param lengths The variables encoding the lengths of the tasks to assign.
     * @param heights The heights of the tasks to assign.
     * @param operator The operator to compare the cumulative use with.
     * @param value The variable encoding the value for the cumulative use.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    void addCumulativeVariableLengthsConstantHeights(IVec<String> origins,
            IVec<String> lengths, IVec<BigInteger> heights, RelationalOperator operator,
            String value) throws ContradictionException;

    /**
     * Notifies this listener that a {@code cumulative} constraint is to be added.
     *
     * @param origins The variables encoding the origins of the resources.
     * @param lengths The variables encoding the lengths of the tasks to assign.
     * @param ends The variables encoding the ends of the resources.
     * @param heights The heights of the tasks to assign.
     * @param operator The operator to compare the cumulative use with.
     * @param value The variable encoding the value for the cumulative use.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    void addCumulativeVariableLengthsConstantHeights(IVec<String> origins,
            IVec<String> lengths, IVec<String> ends, IVec<BigInteger> heights,
            RelationalOperator operator, String value) throws ContradictionException;

    /**
     * Notifies this listener that a {@code cumulative} constraint is to be added.
     *
     * @param origins The variables encoding the origins of the resources.
     * @param lengths The variables encoding the lengths of the tasks to assign.
     * @param heights The variables encoding the heights of the tasks to assign.
     * @param operator The operator to compare the cumulative use with.
     * @param value The value for the cumulative use.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    void addCumulativeVariableLengthsVariableHeights(IVec<String> origins,
            IVec<String> lengths, IVec<String> heights, RelationalOperator operator,
            BigInteger value) throws ContradictionException;

    /**
     * Notifies this listener that a {@code cumulative} constraint is to be added.
     *
     * @param origins The variables encoding the origins of the resources.
     * @param lengths The variables encoding the lengths of the tasks to assign.
     * @param ends The variables encoding the ends of the resources.
     * @param heights The variables encoding the heights of the tasks to assign.
     * @param operator The operator to compare the cumulative use with.
     * @param value The value for the cumulative use.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    void addCumulativeVariableLengthsVariableHeights(IVec<String> origins,
            IVec<String> lengths, IVec<String> ends, IVec<String> heights,
            RelationalOperator operator, BigInteger value) throws ContradictionException;

    /**
     * Notifies this listener that a {@code cumulative} constraint is to be added.
     *
     * @param origins The variables encoding the origins of the resources.
     * @param lengths The variables encoding the lengths of the tasks to assign.
     * @param heights The variables encoding the heights of the tasks to assign.
     * @param operator The operator to compare the cumulative use with.
     * @param value The variable encoding the value for the cumulative use.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    void addCumulativeVariableLengthsVariableHeights(IVec<String> origins,
            IVec<String> lengths, IVec<String> heights, RelationalOperator operator,
            String value) throws ContradictionException;

    /**
     * Notifies this listener that a {@code cumulative} constraint is to be added.
     *
     * @param origins The variables encoding the origins of the resources.
     * @param lengths The variables encoding the lengths of the tasks to assign.
     * @param ends The variables encoding the ends of the resources.
     * @param heights The variables encoding the heights of the tasks to assign.
     * @param operator The operator to compare the cumulative use with.
     * @param value The variable encoding the value for the cumulative use.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    void addCumulativeVariableLengthsVariableHeights(IVec<String> origins,
            IVec<String> lengths, IVec<String> ends, IVec<String> heights,
            RelationalOperator operator, String value) throws ContradictionException;

    /**
     * Notifies this listener that an {@code element} constraint is to be added.
     *
     * @param variables The variables appearing in the constraint.
     * @param value The value that must appear among the variables.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    void addElement(IVec<String> variables, BigInteger value)
            throws ContradictionException;

    /**
     * Notifies this listener that an {@code element} constraint is to be added.
     *
     * @param variables The variables appearing in the constraint.
     * @param value The variable encoding the value that must appear among the variables.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    void addElement(IVec<String> variables, String value) throws ContradictionException;

    /**
     * Notifies this listener that an {@code element} constraint is to be added.
     *
     * @param values The values among which to look for the variable.
     * @param startIndex The index at which to start looking for the variable.
     * @param index The index at which the variable appears in the values.
     * @param value The value to look for.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    void addElementConstantValues(IVec<BigInteger> values, int startIndex, String index,
            BigInteger value) throws ContradictionException;

    /**
     * Notifies this listener that an {@code element} constraint is to be added.
     *
     * @param values The values among which to look for the variable.
     * @param startIndex The index at which to start looking for the variable.
     * @param index The index at which the variable appears in the values.
     * @param variable The variable whose value is to be looked for.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    void addElementConstantValues(IVec<BigInteger> values, int startIndex, String index,
            String variable) throws ContradictionException;

    /**
     * Notifies this listener that an {@code element} constraint is to be added.
     *
     * @param variables The variables among which to look for the value.
     * @param startIndex The index at which to start looking for the value.
     * @param index The index at which the value appears in the variables.
     * @param value The value to look for.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    void addElement(IVec<String> variables, int startIndex, String index, BigInteger value)
            throws ContradictionException;

    /**
     * Notifies this listener that an {@code element} constraint is to be added.
     *
     * @param values The variables representing the values among which to look for the
     *        variable.
     * @param startIndex The index at which to start looking for the variable.
     * @param index The index at which the variable appears in the values.
     * @param variable The variable whose value is to be looked for.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    void addElement(IVec<String> values, int startIndex, String index, String variable)
            throws ContradictionException;

    /**
     * Notifies this listener that an {@code element} constraint is to be added.
     *
     * @param matrix The matrix of values among which the value must appear.
     * @param startRowIndex The index of the row starting from which the value must
     *        appear.
     * @param rowIndex The variable encoding the index of the row at which the value
     *        appears.
     * @param startColIndex The index of the column starting from which the value must
     *        appear.
     * @param colIndex The variable encoding the index of the column at which the value
     *        appears.
     * @param value The value to look for inside the matrix.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    void addElementConstantMatrix(IVec<IVec<BigInteger>> matrix, int startRowIndex,
            String rowIndex, int startColIndex, String colIndex, BigInteger value)
            throws ContradictionException;

    /**
     * Notifies this listener that an {@code element} constraint is to be added.
     *
     * @param matrix The matrix of values among which the value must appear.
     * @param startRowIndex The index of the row starting from which the value must
     *        appear.
     * @param rowIndex The variable encoding the index of the row at which the value
     *        appears.
     * @param startColIndex The index of the column starting from which the value must
     *        appear.
     * @param colIndex The variable encoding the index of the column at which the value
     *        appears.
     * @param value The variable encoding the value to look for inside the matrix.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    void addElementConstantMatrix(IVec<IVec<BigInteger>> matrix, int startRowIndex,
            String rowIndex, int startColIndex, String colIndex, String value)
            throws ContradictionException;

    /**
     * Notifies this listener that an {@code element} constraint is to be added.
     *
     * @param matrix The matrix of variables among which the value must be assigned.
     * @param startRowIndex The index of the row starting from which the value must
     *        appear.
     * @param rowIndex The variable encoding the index of the row at which the value
     *        appears.
     * @param startColIndex The index of the column starting from which the value must
     *        appear.
     * @param colIndex The variable encoding the index of the column at which the value
     *        appears.
     * @param value The variable encoding the value to look for inside the matrix.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    void addElementMatrix(IVec<IVec<String>> matrix, int startRowIndex, String rowIndex,
            int startColIndex, String colIndex, BigInteger value) throws ContradictionException;

    /**
     * Notifies this listener that an {@code element} constraint is to be added.
     *
     * @param matrix The matrix of variables among which the value must be assigned.
     * @param startRowIndex The index of the row starting from which the value must
     *        appear.
     * @param rowIndex The variable encoding the index of the row at which the value
     *        appears.
     * @param startColIndex The index of the column starting from which the value must
     *        appear.
     * @param colIndex The variable encoding the index of the column at which the value
     *        appears.
     * @param value The variable encoding the value to look for inside the matrix.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    void addElementMatrix(IVec<IVec<String>> matrix, int startRowIndex, String rowIndex,
            int startColIndex, String colIndex, String value) throws ContradictionException;

    /**
     * Notifies this listener that an {@code extension} constraint describing the support
     * of a tuple of variables is to be added.
     *
     * @param variable The variable for which the support is given.
     * @param allowedValues The values allowed for the variable.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    void addSupport(String variable, IVec<BigInteger> allowedValues)
            throws ContradictionException;

    /**
     * Notifies this listener that an {@code extension} constraint describing the support
     * of a tuple of variables is to be added.
     *
     * @param variableTuple The tuple of variables for which the support is given.
     * @param allowedValues The values allowed for the tuple variables.
     *        Values equal to {@code null} are interpreted as "any value" (as in so-called
     *        "starred tuples").
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    void addSupport(IVec<String> variableTuple, IVec<IVec<BigInteger>> allowedValues)
            throws ContradictionException;

    /**
     * Notifies this listener that an {@code extension} constraint describing the
     * conflicts of a tuple of variables is to be added.
     *
     * @param variable The variable for which the conflicts are given.
     * @param forbiddenValues The values forbidden for the variable.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    void addConflicts(String variable, IVec<BigInteger> forbiddenValues)
            throws ContradictionException;

    /**
     * Notifies this listener that an {@code extension} constraint describing the
     * conflicts of a tuple of variables is to be added.
     *
     * @param variableTuple The tuple of variables for which the conflicts are given.
     * @param forbiddenValues The values forbidden for the tuple variables.
     *        Values equal to {@code null} are interpreted as "any value" (as in so-called
     *        "starred tuples").
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    void addConflicts(IVec<String> variableTuple, IVec<IVec<BigInteger>> forbiddenValues)
            throws ContradictionException;

    /**
     * Notifies this listener that an {@code intension} constraint is to be added.
     *
     * @param constr The user-friendly representation of the constraint to add.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    void addIntension(IIntensionConstraint constr) throws ContradictionException;

    /**
     * Notifies this listener that a {@code primitive} constraint is to be added.
     *
     * @param variable The variable appearing in the constraint.
     * @param operator The operator used in the constraint.
     * @param value The value to compare the variable with.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    void addPrimitive(String variable, RelationalOperator operator, BigInteger value)
            throws ContradictionException;

    /**
     * Notifies this listener that a {@code primitive} constraint is to be added.
     *
     * @param variable The variable appearing in the constraint.
     * @param arithOp The arithmetic operator applied on the variable.
     * @param leftHandSide The value on the left-hand side of the constraint.
     * @param relOp The relational operator used to compare the right-hand side with the
     *        left-hand side.
     * @param rightHandSide The value on the right-hand side of the constraint.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    void addPrimitive(String variable, ArithmeticOperator arithOp, BigInteger leftHandSide,
            RelationalOperator relOp, BigInteger rightHandSide) throws ContradictionException;

    /**
     * Notifies this listener that a {@code primitive} constraint is to be added.
     *
     * @param variable The variable appearing in the constraint.
     * @param arithOp The arithmetic operator applied on the variable.
     * @param leftHandSide The variable on the left-hand side of the constraint.
     * @param relOp The relational operator used to compare the right-hand side with the
     *        left-hand side.
     * @param rightHandSide The value on the right-hand side of the constraint.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    void addPrimitive(String variable, ArithmeticOperator arithOp, String leftHandSide,
            RelationalOperator relOp, BigInteger rightHandSide) throws ContradictionException;

    /**
     * Notifies this listener that a {@code primitive} constraint is to be added.
     *
     * @param variable The variable appearing in the constraint.
     * @param arithOp The arithmetic operator applied on the variable.
     * @param leftHandSide The value on the left-hand side of the constraint.
     * @param relOp The relational operator used to compare the right-hand side with the
     *        left-hand side.
     * @param rightHandSide The variable on the right-hand side of the constraint.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    void addPrimitive(String variable, ArithmeticOperator arithOp, BigInteger leftHandSide,
            RelationalOperator relOp, String rightHandSide) throws ContradictionException;

    /**
     * Notifies this listener that a {@code primitive} constraint is to be added.
     *
     * @param variable The variable appearing in the constraint.
     * @param arithOp The arithmetic operator applied on the variable.
     * @param leftHandSide The variable on the left-hand side of the constraint.
     * @param relOp The relational operator used to compare the right-hand side with the
     *        left-hand side.
     * @param rightHandSide The variable on the right-hand side of the constraint.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    void addPrimitive(String variable, ArithmeticOperator arithOp, String leftHandSide,
            RelationalOperator relOp, String rightHandSide) throws ContradictionException;

    /**
     * Notifies this listener that a {@code primitive} constraint is to be added.
     *
     * @param arithOp The arithmetic operator applied on the variable.
     * @param variable The variable on which the operator is applied.
     * @param rightHandSide The variable on the right-hand side of the constraint.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    void addPrimitive(ArithmeticOperator arithOp, String variable, String rightHandSide)
            throws ContradictionException;

    /**
     * Notifies this listener that a {@code primitive} constraint is to be added.
     *
     * @param variable The variable appearing in the constraint.
     * @param operator The operator defining whether the values are allowed or forbidden.
     * @param values The set of values on which the operator is applied.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    void addPrimitive(String variable, SetBelongingOperator operator, IVec<BigInteger> values)
            throws ContradictionException;

    /**
     * Notifies this listener that a {@code primitive} constraint is to be added.
     *
     * @param variable The variable appearing in the constraint.
     * @param operator The operator defining whether the values are allowed or forbidden.
     * @param min The minimum value of the range on which the operator is applied.
     * @param max The maximum value of the range on which the operator is applied.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    void addPrimitive(String variable, SetBelongingOperator operator, BigInteger min,
            BigInteger max) throws ContradictionException;

    /**
     * Notifies this listener that a {@code minimum} constraint is to be added.
     *
     * @param variables The variables to compute the minimum of.
     * @param operator The relational operator to use to compare the minimum.
     * @param value The value to compare the minimum with.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    void addMinimum(IVec<String> variables, RelationalOperator operator, BigInteger value)
            throws ContradictionException;

    /**
     * Notifies this listener that a {@code minimum} constraint is to be added.
     *
     * @param variables The variables to compute the minimum of.
     * @param operator The relational operator to use to compare the minimum.
     * @param value The variable encoding the value to compare the minimum with.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    void addMinimum(IVec<String> variables, RelationalOperator operator, String value)
            throws ContradictionException;

    /**
     * Notifies this listener that a {@code minimum} constraint is to be added.
     *
     * @param intensionConstraints The intension constraints to compute the minimum of.
     * @param operator The relational operator to use to compare the minimum.
     * @param value The value to compare the minimum with.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    void addMinimumIntension(IVec<IIntensionConstraint> intensionConstraints,
            RelationalOperator operator, BigInteger value) throws ContradictionException;

    /**
     * Notifies this listener that a {@code minimum} constraint is to be added.
     *
     * @param intensionConstraints The intension constraints to compute the minimum of.
     * @param operator The relational operator to use to compare the minimum.
     * @param value The variable encoding the value to compare the minimum with.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    void addMinimumIntension(IVec<IIntensionConstraint> intensionConstraints,
            RelationalOperator operator, String value) throws ContradictionException;

    /**
     * Notifies this listener that a {@code maximum} constraint is to be added.
     *
     * @param variables The variables to compute the maximum of.
     * @param operator The relational operator to use to compare the maximum.
     * @param value The value to compare the maximum with.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    void addMaximum(IVec<String> variables, RelationalOperator operator, BigInteger value)
            throws ContradictionException;

    /**
     * Notifies this listener that a {@code maximum} constraint is to be added.
     *
     * @param variables The variables to compute the maximum of.
     * @param operator The relational operator to use to compare the maximum.
     * @param value The variable encoding the value to compare the maximum with.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    void addMaximum(IVec<String> variables, RelationalOperator operator, String value)
            throws ContradictionException;

    /**
     * Notifies this listener that a {@code maximum} constraint is to be added.
     *
     * @param intensionConstraints The intension constraints to compute the maximum of.
     * @param operator The relational operator to use to compare the maximum.
     * @param value The value to compare the maximum with.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    void addMaximumIntension(IVec<IIntensionConstraint> intensionConstraints,
            RelationalOperator operator, BigInteger value) throws ContradictionException;

    /**
     * Notifies this listener that a {@code maximum} constraint is to be added.
     *
     * @param intensionConstraints The intension constraints to compute the maximum of.
     * @param operator The relational operator to use to compare the maximum.
     * @param value The variable encoding the value to compare the maximum with.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    void addMaximumIntension(IVec<IIntensionConstraint> intensionConstraints,
            RelationalOperator operator, String value) throws ContradictionException;

    /**
     * Notifies this listener that a {@code no-overlap} constraint is to be added.
     *
     * @param variables The variables appearing in the constraint.
     * @param length The length associated to the variables.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    void addNoOverlap(IVec<String> variables, IVec<BigInteger> length)
            throws ContradictionException;

    /**
     * Notifies this listener that a {@code no-overlap} constraint is to be added.
     *
     * @param variables The variables appearing in the constraint.
     * @param length The length associated to the variables.
     * @param zeroIgnored Whether {@code 0}-lengths should be ignored.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    void addNoOverlap(IVec<String> variables, IVec<BigInteger> length, boolean zeroIgnored)
            throws ContradictionException;

    /**
     * Notifies this listener that a {@code no-overlap} constraint is to be added.
     *
     * @param variables The variables appearing in the constraint.
     * @param length The variable for the length of the other variables.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    void addNoOverlapVariableLength(IVec<String> variables, IVec<String> length)
            throws ContradictionException;

    /**
     * Notifies this listener that a {@code no-overlap} constraint is to be added.
     *
     * @param variables The variables appearing in the constraint.
     * @param length The variable for the length of the other variables.
     * @param zeroIgnored Whether {@code 0}-lengths should be ignored.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    void addNoOverlapVariableLength(IVec<String> variables, IVec<String> length,
            boolean zeroIgnored) throws ContradictionException;

    /**
     * Notifies this listener that a {@code no-overlap} constraint is to be added.
     *
     * @param variables The variables appearing in the constraint.
     * @param length The length associated to the variables.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    void addeMultiDimensionalNoOverlap(IVec<IVec<String>> variables,
            IVec<IVec<BigInteger>> length) throws ContradictionException;

    /**
     * Notifies this listener that a {@code no-overlap} constraint is to be added.
     *
     * @param variables The variables appearing in the constraint.
     * @param length The length associated to the variables.
     * @param zeroIgnored Whether {@code 0}-lengths should be ignored.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    void addMultiDimensionalNoOverlap(IVec<IVec<String>> variables,
            IVec<IVec<BigInteger>> length, boolean zeroIgnored) throws ContradictionException;

    /**
     * Notifies this listener that a {@code no-overlap} constraint is to be added.
     *
     * @param variables The variables appearing in the constraint.
     * @param length The variable for the length of the other variables.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    void addMultiDimensionalNoOverlapVariableLength(IVec<IVec<String>> variables,
            IVec<IVec<String>> length) throws ContradictionException;

    /**
     * Notifies this listener that a {@code no-overlap} constraint is to be added.
     *
     * @param variables The variables appearing in the constraint.
     * @param length The variable for the length of the other variables.
     * @param zeroIgnored Whether {@code 0}-lengths should be ignored.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    void addMultiDimensionalNoOverlapVariableLength(IVec<IVec<String>> variables,
            IVec<IVec<String>> length, boolean zeroIgnored) throws ContradictionException;

    /**
     * Notifies this listener that an {@code n-values} constraint is to be added.
     *
     * @param variables The variables appearing in the constraint.
     * @param operator The relational operator used in the constraint.
     * @param nb The number of distinct values to count.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    void addNValues(IVec<String> variables, RelationalOperator operator,
            BigInteger nb) throws ContradictionException;

    /**
     * Notifies this listener that an {@code n-values} constraint is to be added.
     *
     * @param variables The variables appearing in the constraint.
     * @param operator The relational operator used in the constraint.
     * @param nb The number of distinct values to count.
     * @param except The values that should not be counted.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    void addNValuesExcept(IVec<String> variables, RelationalOperator operator,
            BigInteger nb, IVec<BigInteger> except) throws ContradictionException;

    /**
     * Notifies this listener that an {@code n-values} constraint is to be added.
     *
     * @param variables The variables appearing in the constraint.
     * @param operator The relational operator used in the constraint.
     * @param nb The variable counting the number of distinct values.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    void addNValues(IVec<String> variables, RelationalOperator operator,
            String nb) throws ContradictionException;

    /**
     * Notifies this listener that an {@code n-values} constraint is to be added.
     *
     * @param variables The variables appearing in the constraint.
     * @param operator The relational operator used in the constraint.
     * @param nb The variable counting the number of distinct values.
     * @param except The values that should not be counted.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    void addNValuesExcept(IVec<String> variables, RelationalOperator operator,
            String nb, IVec<BigInteger> except) throws ContradictionException;

    /**
     * Notifies this listener that an {@code n-values} constraint is to be added.
     *
     * @param expressions The expressions appearing in the constraint.
     * @param operator The relational operator used in the constraint.
     * @param nb The number of distinct values to count.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    void addNValuesIntension(IVec<IIntensionConstraint> expressions,
            RelationalOperator operator, BigInteger nb) throws ContradictionException;

    /**
     * Notifies this listener that an {@code n-values} constraint is to be added.
     *
     * @param expressions The expressions appearing in the constraint.
     * @param operator The relational operator used in the constraint.
     * @param nb The variable counting the number of distinct values.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    void addNValuesIntension(IVec<IIntensionConstraint> expressions,
            RelationalOperator operator, String nb) throws ContradictionException;

    /**
     * Notifies this listener that an {@code ordered} constraint is to be added.
     *
     * @param variables The variables that should be ordered.
     * @param operator The relational operator defining the order of the variables.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    void addOrdered(IVec<String> variables, RelationalOperator operator)
            throws ContradictionException;

    /**
     * Notifies this listener that an {@code ordered} constraint is to be added.
     *
     * @param variables The variables that should be ordered.
     * @param lengths The lengths that must exist between two consecutive variables.
     * @param operator The relational operator defining the order of the variables.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    void addOrderedWithConstantLength(IVec<String> variables, IVec<BigInteger> lengths,
            RelationalOperator operator) throws ContradictionException;

    /**
     * Notifies this listener that an {@code ordered} constraint is to be added.
     *
     * @param variables The variables that should be ordered.
     * @param lengths The variables encoding the lengths that must exist between two
     *        consecutive variables.
     * @param operator The relational operator defining the order of the variables.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    void addOrderedWithVariableLength(IVec<String> variables, IVec<String> lengths,
            RelationalOperator operator) throws ContradictionException;

    /**
     * Notifies this listener that an {@code all-equal} constraint is to be added.
     *
     * @param variables The variables that should all be equal.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    void addAllEqual(IVec<String> variables) throws ContradictionException;

    /**
     * Notifies this listener that an {@code all-equal} constraint is to be added.
     *
     * @param expressions The expressions that should all be equal.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    void addAllEqualIntension(IVec<IIntensionConstraint> expressions)
            throws ContradictionException;

    /**
     * Notifies this listener that a {@code not-all-equal} constraint is to be added.
     *
     * @param variables The variables that should not be all equal.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    void addNotAllEqual(IVec<String> variables) throws ContradictionException;

    /**
     * Notifies this listener that an {@code lex} constraint is to be added.
     *
     * @param tuples The tuple of variables that should be lexicographically ordered.
     * @param operator The relational operator defining the order of the tuples.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    void addLex(IVec<IVec<String>> tuples, RelationalOperator operator)
            throws ContradictionException;

    /**
     * Notifies this listener that an {@code lex-matrix} constraint is to be added.
     *
     * @param matrix The matrix of variables that should be lexicographically ordered.
     * @param operator The relational operator defining the order in the matrix.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    void addLexMatrix(IVec<IVec<String>> matrix, RelationalOperator operator)
            throws ContradictionException;

    /**
     * Notifies this listener that a {@code sum} constraint is to be added.
     *
     * @param variables The variables appearing in the constraint.
     * @param operator The relational operator used in the constraint.
     * @param value The value of the right-hand side of the constraint.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    void addSum(IVec<String> variables, RelationalOperator operator,
            BigInteger value) throws ContradictionException;

    /**
     * Notifies this listener that a {@code sum} constraint is to be added.
     *
     * @param variables The variables appearing in the constraint.
     * @param coefficients The coefficients of the variables appearing in the constraint.
     * @param operator The relational operator used in the constraint.
     * @param value The value of the right-hand side of the constraint.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    void addSum(IVec<String> variables, IVec<BigInteger> coefficients,
            RelationalOperator operator, BigInteger value) throws ContradictionException;

    /**
     * Notifies this listener that a {@code sum} constraint is to be added.
     *
     * @param variables The variables appearing in the constraint.
     * @param operator The relational operator used in the constraint.
     * @param rightVariable The variable on the right-hand side of the constraint.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    void addSum(IVec<String> variables, RelationalOperator operator,
            String rightVariable) throws ContradictionException;

    /**
     * Notifies this listener that a {@code sum} constraint is to be added.
     *
     * @param variables The variables appearing in the constraint.
     * @param coefficients The coefficients of the variables appearing in the constraint.
     * @param operator The relational operator used in the constraint.
     * @param rightVariable The variable on the right-hand side of the constraint.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    void addSum(IVec<String> variables, IVec<BigInteger> coefficients,
            RelationalOperator operator, String rightVariable) throws ContradictionException;

    /**
     * Notifies this listener that a {@code sum} constraint is to be added.
     *
     * @param intensionConstraints The intension constraints to compute the sum of.
     * @param operator The relational operator used in the constraint.
     * @param value The value of the right-hand side of the constraint.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    void addSumIntension(IVec<IIntensionConstraint> intensionConstraints,
            RelationalOperator operator, BigInteger value) throws ContradictionException;

    /**
     * Notifies this listener that a {@code sum} constraint is to be added.
     *
     * @param intensionConstraints The intension constraints to compute the sum of.
     * @param coefficients The coefficients of the variables appearing in the constraint.
     * @param operator The relational operator used in the constraint.
     * @param value The value of the right-hand side of the constraint.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    void addSumIntension(IVec<IIntensionConstraint> intensionConstraints,
            IVec<BigInteger> coefficients, RelationalOperator operator, BigInteger value)
            throws ContradictionException;

    /**
     * Notifies this listener that a {@code sum} constraint is to be added.
     *
     * @param intensionConstraints The intension constraints to compute the sum of.
     * @param operator The relational operator used in the constraint.
     * @param rightVariable The variable on the right-hand side of the constraint.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    void addSumIntension(IVec<IIntensionConstraint> intensionConstraints,
            RelationalOperator operator, String rightVariable) throws ContradictionException;

    /**
     * Notifies this listener that a {@code sum} constraint is to be added.
     *
     * @param intensionConstraints The intension constraints to compute the sum of.
     * @param coefficients The coefficients of the variables appearing in the constraint.
     * @param operator The relational operator used in the constraint.
     * @param rightVariable The variable on the right-hand side of the constraint.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    void addSumIntension(IVec<IIntensionConstraint> intensionConstraints,
            IVec<BigInteger> coefficients, RelationalOperator operator, String rightVariable)
            throws ContradictionException;

    /**
     * Notifies this listener that a {@code sum} constraint is to be added.
     *
     * @param variables The variables appearing in the constraint.
     * @param coefficients The variables representing the coefficients of the variables
     *        appearing in the constraint.
     * @param operator The relational operator used in the constraint.
     * @param value The value of the right-hand side of the constraint.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    void addSumWithVariableCoefficients(IVec<String> variables, IVec<String> coefficients,
            RelationalOperator operator, BigInteger value) throws ContradictionException;

    /**
     * Notifies this listener that a {@code sum} constraint is to be added.
     *
     * @param variables The variables appearing in the constraint.
     * @param coefficients The variables representing the coefficients of the variables
     *        appearing in the constraint.
     * @param operator The relational operator used in the constraint.
     * @param rightVariable The variable on the right-hand side of the constraint.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    void addSumWithVariableCoefficients(IVec<String> variables, IVec<String> coefficients,
            RelationalOperator operator, String rightVariable) throws ContradictionException;

    /**
     * Notifies this listener that a {@code sum} constraint is to be added.
     *
     * @param intensionConstraints The intension constraints to compute the sum of.
     * @param coefficients The variables representing the coefficients of the variables
     *        appearing in the constraint.
     * @param operator The relational operator used in the constraint.
     * @param value The value of the right-hand side of the constraint.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    void addSumIntensionWithVariableCoefficients(IVec<IIntensionConstraint> intensionConstraints,
            IVec<String> coefficients, RelationalOperator operator, BigInteger value)
            throws ContradictionException;

    /**
     * Notifies this listener that a {@code sum} constraint is to be added.
     *
     * @param intensionConstraints The intension constraints to compute the sum of.
     * @param coefficients The variables representing the coefficients of the variables
     *        appearing in the constraint.
     * @param operator The relational operator used in the constraint.
     * @param rightVariable The variable on the right-hand side of the constraint.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    void addSumIntensionWithVariableCoefficients(IVec<IIntensionConstraint> intensionConstraints,
            IVec<String> coefficients, RelationalOperator operator, String rightVariable)
            throws ContradictionException;

    /**
     * Notifies this listener that an objective function is to be added to minimize the
     * value assigned to a variable.
     *
     * @param variable The variable to minimize the value of.
     */
    void minimizeVariable(String variable);

    /**
     * Notifies this listener that an objective function is to be added to maximize the
     * value assigned to a variable.
     *
     * @param variable The variable to maximize the value of.
     */
    void maximizeVariable(String variable);

    /**
     * Notifies this listener that an objective function is to be added to minimize the
     * value of an expression.
     *
     * @param expression The expression to minimize the value of.
     */
    void minimizeExpression(IIntensionConstraint expression);

    /**
     * Notifies this listener that an objective function is to be added to maximize the
     * value of an expression.
     *
     * @param expression The expression to maximize the value of.
     */
    void maximizeExpression(IIntensionConstraint expression);

    /**
     * Notifies this listener that an objective function is to be added to minimize a sum
     * of variables.
     *
     * @param variables The variables to minimize the sum of.
     */
    void minimizeSum(IVec<String> variables);

    /**
     * Notifies this listener that an objective function is to be added to minimize a sum
     * of variables.
     *
     * @param variables The variables to minimize the sum of.
     * @param coefficients The coefficients of the variables in the sum.
     */
    void minimizeSum(IVec<String> variables, IVec<BigInteger> coefficients);

    /**
     * Notifies this listener that an objective function is to be added to minimize a sum
     * of expressions.
     *
     * @param expressions The expressions to minimize the sum of.
     */
    void minimizeExpressionSum(IVec<IIntensionConstraint> expressions);

    /**
     * Notifies this listener that an objective function is to be added to minimize a sum
     * of expressions.
     *
     * @param expressions The expressions to minimize the sum of.
     * @param coefficients The coefficients of the expressions in the sum.
     */
    void minimizeExpressionSum(IVec<IIntensionConstraint> expressions,
            IVec<BigInteger> coefficients);

    /**
     * Notifies this listener that an objective function is to be added to maximize a sum
     * of variables.
     *
     * @param variables The variables to maximize the sum of.
     */
    void maximizeSum(IVec<String> variables);

    /**
     * Notifies this listener that an objective function is to be added to maximize a sum
     * of variables.
     *
     * @param variables The variables to maximize the sum of.
     * @param coefficients The coefficients of the variables in the sum.
     */
    void maximizeSum(IVec<String> variables, IVec<BigInteger> coefficients);

    /**
     * Notifies this listener that an objective function is to be added to maximize a sum
     * of expressions.
     *
     * @param expressions The expressions to maximize the sum of.
     */
    void maximizeExpressionSum(IVec<IIntensionConstraint> expressions);

    /**
     * Notifies this listener that an objective function is to be added to maximize a sum
     * of expressions.
     *
     * @param expressions The expressions to maximize the sum of.
     * @param coefficients The coefficients of the expressions in the sum.
     */
    void maximizeExpressionSum(IVec<IIntensionConstraint> expressions,
            IVec<BigInteger> coefficients);

    /**
     * Notifies this listener that an objective function is to be added to minimize a
     * product of variables.
     *
     * @param variables The variables to minimize the product of.
     */
    void minimizeProduct(IVec<String> variables);

    /**
     * Notifies this listener that an objective function is to be added to minimize a
     * product of variables.
     *
     * @param variables The variables to minimize the product of.
     * @param coefficients The coefficients of the variables in the product.
     */
    void minimizeProduct(IVec<String> variables, IVec<BigInteger> coefficients);

    /**
     * Notifies this listener that an objective function is to be added to minimize a
     * product of expressions.
     *
     * @param expressions The expressions to minimize the product of.
     */
    void minimizeExpressionProduct(IVec<IIntensionConstraint> expressions);

    /**
     * Notifies this listener that an objective function is to be added to minimize a
     * product of expressions.
     *
     * @param expressions The expressions to minimize the product of.
     * @param coefficients The coefficients of the expressions in the product.
     */
    void minimizeExpressionProduct(IVec<IIntensionConstraint> expressions,
            IVec<BigInteger> coefficients);

    /**
     * Notifies this listener that an objective function is to be added to maximize a
     * product of variables.
     *
     * @param variables The variables to maximize the product of.
     */
    void maximizeProduct(IVec<String> variables);

    /**
     * Notifies this listener that an objective function is to be added to maximize a
     * product of variables.
     *
     * @param variables The variables to maximize the product of.
     * @param coefficients The coefficients of the variables in the product.
     */
    void maximizeProduct(IVec<String> variables, IVec<BigInteger> coefficients);

    /**
     * Notifies this listener that an objective function is to be added to maximize a
     * product of expressions.
     *
     * @param expressions The expressions to maximize the product of.
     */
    void maximizeExpressionProduct(IVec<IIntensionConstraint> expressions);

    /**
     * Notifies this listener that an objective function is to be added to maximize a
     * product of expressions.
     *
     * @param expressions The expressions to maximize the product of.
     * @param coefficients The coefficients of the expressions in the product.
     */
    void maximizeExpressionProduct(IVec<IIntensionConstraint> expressions,
            IVec<BigInteger> coefficients);

    /**
     * Notifies this listener that an objective function is to be added to minimize the
     * minimum of variables.
     *
     * @param variables The variables to minimize the minimum of.
     */
    void minimizeMinimum(IVec<String> variables);

    /**
     * Notifies this listener that an objective function is to be added to minimize the
     * minimum of variables.
     *
     * @param variables The variables to minimize the minimum of.
     * @param coefficients The coefficients of the variables in the minimum.
     */
    void minimizeMinimum(IVec<String> variables, IVec<BigInteger> coefficients);

    /**
     * Notifies this listener that an objective function is to be added to minimize the
     * minimum of expressions.
     *
     * @param expressions The expressions to minimize the minimum of.
     */
    void minimizeExpressionMinimum(IVec<IIntensionConstraint> expressions);

    /**
     * Notifies this listener that an objective function is to be added to minimize the
     * minimum of expressions.
     *
     * @param expressions The expressions to minimize the minimum of.
     * @param coefficients The coefficients of the expressions in the minimum.
     */
    void minimizeExpressionMinimum(IVec<IIntensionConstraint> expressions,
            IVec<BigInteger> coefficients);

    /**
     * Notifies this listener that an objective function is to be added to maximize the
     * minimum of variables.
     *
     * @param variables The variables to maximize the minimum of.
     */
    void maximizeMinimum(IVec<String> variables);

    /**
     * Notifies this listener that an objective function is to be added to maximize the
     * minimum of variables.
     *
     * @param variables The variables to maximize the minimum of.
     * @param coefficients The coefficients of the variables in the minimum.
     */
    void maximizeMinimum(IVec<String> variables, IVec<BigInteger> coefficients);

    /**
     * Notifies this listener that an objective function is to be added to maximize the
     * minimum of expressions.
     *
     * @param expressions The expressions to maximize the minimum of.
     */
    void maximizeExpressionMinimum(IVec<IIntensionConstraint> expressions);

    /**
     * Notifies this listener that an objective function is to be added to maximize the
     * minimum of expressions.
     *
     * @param expressions The expressions to maximize the minimum of.
     * @param coefficients The coefficients of the expressions in the minimum.
     */
    void maximizeExpressionMinimum(IVec<IIntensionConstraint> expressions,
            IVec<BigInteger> coefficients);

    /**
     * Notifies this listener that an objective function is to be added to minimize the
     * maximum of variables.
     *
     * @param variables The variables to minimize the maximum of.
     */
    void minimizeMaximum(IVec<String> variables);

    /**
     * Notifies this listener that an objective function is to be added to minimize the
     * maximum of variables.
     *
     * @param variables The variables to minimize the maximum of.
     * @param coefficients The coefficients of the variables in the maximum.
     */
    void minimizeMaximum(IVec<String> variables, IVec<BigInteger> coefficients);

    /**
     * Notifies this listener that an objective function is to be added to minimize the
     * maximum of expressions.
     *
     * @param expressions The expressions to minimize the maximum of.
     */
    void minimizeExpressionMaximum(IVec<IIntensionConstraint> expressions);

    /**
     * Notifies this listener that an objective function is to be added to minimize the
     * maximum of expressions.
     *
     * @param expressions The expressions to minimize the maximum of.
     * @param coefficients The coefficients of the expressions in the maximum.
     */
    void minimizeExpressionMaximum(IVec<IIntensionConstraint> expressions,
            IVec<BigInteger> coefficients);

    /**
     * Notifies this listener that an objective function is to be added to maximize the
     * maximum of variables.
     *
     * @param variables The variables to maximize the maximum of.
     */
    void maximizeMaximum(IVec<String> variables);

    /**
     * Notifies this listener that an objective function is to be added to maximize the
     * maximum of variables.
     *
     * @param variables The variables to maximize the maximum of.
     * @param coefficients The coefficients of the variables in the maximum.
     */
    void maximizeMaximum(IVec<String> variables, IVec<BigInteger> coefficients);

    /**
     * Notifies this listener that an objective function is to be added to maximize the
     * maximum of expressions.
     *
     * @param expressions The expressions to maximize the maximum of.
     */
    void maximizeExpressionMaximum(IVec<IIntensionConstraint> expressions);

    /**
     * Notifies this listener that an objective function is to be added to maximize the
     * maximum of expressions.
     *
     * @param expressions The expressions to maximize the maximum of.
     * @param coefficients The coefficients of the expressions in the maximum.
     */
    void maximizeExpressionMaximum(IVec<IIntensionConstraint> expressions,
            IVec<BigInteger> coefficients);

    /**
     * Notifies this listener that an objective function is to be added to minimize the
     * number of values assigned to variables.
     *
     * @param variables The variables to minimize the number of values of.
     */
    void minimizeNValues(IVec<String> variables);

    /**
     * Notifies this listener that an objective function is to be added to minimize the
     * number of values assigned to variables.
     *
     * @param variables The variables to minimize the number of values of.
     * @param coefficients The coefficients of the variables.
     */
    void minimizeNValues(IVec<String> variables, IVec<BigInteger> coefficients);

    /**
     * Notifies this listener that an objective function is to be added to minimize the
     * number of values assigned to variables.
     *
     * @param expressions The expressions to minimize the number of values of.
     */
    void minimizeExpressionNValues(IVec<IIntensionConstraint> expressions);

    /**
     * Notifies this listener that an objective function is to be added to minimize the
     * number of values assigned to variables.
     *
     * @param expressions The expressions to minimize the number of values of.
     * @param coefficients The coefficients of the expressions.
     */
    void minimizeExpressionNValues(IVec<IIntensionConstraint> expressions,
            IVec<BigInteger> coefficients);

    /**
     * Notifies this listener that an objective function is to be added to maximize the
     * number of values assigned to variables.
     *
     * @param variables The variables to maximize the number of values of.
     */
    void maximizeNValues(IVec<String> variables);

    /**
     * Notifies this listener that an objective function is to be added to maximize the
     * number of values assigned to variables.
     *
     * @param variables The variables to maximize the number of values of.
     * @param coefficients The coefficients of the variables.
     */
    void maximizeNValues(IVec<String> variables, IVec<BigInteger> coefficients);

    /**
     * Notifies this listener that an objective function is to be added to maximize the
     * number of values assigned to variables.
     *
     * @param expressions The expressions to maximize the number of values of.
     */
    void maximizeExpressionNValues(IVec<IIntensionConstraint> expressions);

    /**
     * Notifies this listener that an objective function is to be added to maximize the
     * number of values assigned to variables.
     *
     * @param expressions The expressions to maximize the number of values of.
     * @param coefficients The coefficients of the expressions.
     */
    void maximizeExpressionNValues(IVec<IIntensionConstraint> expressions,
            IVec<BigInteger> coefficients);

}
