/**
 * The {@code org.sat4j.csp.core} package provides the core classes of the CSP solver
 * based on the Sat4j platform.
 *
 * @author Thibault Falque
 * @author Romain Wallon
 *
 * @version 0.1.0
 */

package org.sat4j.csp.core;
