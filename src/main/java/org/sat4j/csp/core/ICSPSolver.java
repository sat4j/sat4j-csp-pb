/**
 * Sat4j-CSP, a CSP solver based on the Sat4j platform.
 * Copyright (c) 2021-2022 - Exakis Nelite, Univ Artois & CNRS.
 * All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 * If not, see {@link http://www.gnu.org/licenses}.
 */

package org.sat4j.csp.core;

import java.math.BigInteger;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.sat4j.core.ConstrGroup;
import org.sat4j.core.Vec;
import org.sat4j.core.VecInt;
import org.sat4j.csp.constraints.ArithmeticOperator;
import org.sat4j.csp.constraints.BooleanOperator;
import org.sat4j.csp.constraints.RelationalOperator;
import org.sat4j.csp.constraints.SetBelongingOperator;
import org.sat4j.csp.constraints.encoder.alldifferent.IAllDifferentConstraintEncoder;
import org.sat4j.csp.constraints.encoder.cardinality.ICardinalityConstraintEncoder;
import org.sat4j.csp.constraints.encoder.channel.IChannelConstraintEncoder;
import org.sat4j.csp.constraints.encoder.count.ICountConstraintEncoder;
import org.sat4j.csp.constraints.encoder.cumulative.ICumulativeConstraintEncoder;
import org.sat4j.csp.constraints.encoder.element.IElementConstraintEncoder;
import org.sat4j.csp.constraints.encoder.extension.IExtensionConstraintEncoder;
import org.sat4j.csp.constraints.encoder.intension.IIntensionConstraintEncoder;
import org.sat4j.csp.constraints.encoder.intension.IPrimitiveConstraintEncoder;
import org.sat4j.csp.constraints.encoder.minmax.IMinMaxConstraintEncoder;
import org.sat4j.csp.constraints.encoder.multiplication.IMultiplicationConstraintEncoder;
import org.sat4j.csp.constraints.encoder.nooverlap.INoOverlapConstraintEncoder;
import org.sat4j.csp.constraints.encoder.nvalues.INValuesConstraintEncoder;
import org.sat4j.csp.constraints.encoder.order.IOrderedConstraintEncoder;
import org.sat4j.csp.constraints.encoder.sum.ISumConstraintEncoder;
import org.sat4j.csp.constraints.intension.IIntensionConstraint;
import org.sat4j.csp.variables.IVariable;
import org.sat4j.csp.variables.IVariableFactory;
import org.sat4j.pb.IPBSolver;
import org.sat4j.specs.ContradictionException;
import org.sat4j.specs.IConstr;
import org.sat4j.specs.IOptimizationProblem;
import org.sat4j.specs.IVec;
import org.sat4j.specs.IVecInt;
import org.sat4j.specs.TimeoutException;

/**
 * The ICSPSolver defines the interface of a CSP solver based on the Sat4j
 * platform.
 *
 * @author Thibault Falque
 * @author Romain Wallon
 *
 * @version 0.1.0
 */
public interface ICSPSolver extends IPBSolver, IOptimizationProblem {

    /**
     * Creates a new DIMACS variable used in the pseudo-Boolean representation of
     * the problem.
     *
     * @return The DIMACS identifier of the created variable.
     *
     * @see #newVar()
     * @see #newVar(int)
     */
    default int newDimacsVariable() {
        return newDimacsVariables(1);
    }

    /**
     * Creates several new DIMACS variables used in the pseudo-Boolean
     * representation of the problem.
     *
     * @param nb The number of DIMACS variables to create.
     *
     * @return The DIMACS identifier of the last variable in the solver after having
     *         created the variables.
     *
     * @see #newVar()
     * @see #newVar(int)
     */
    int newDimacsVariables(int nb);

    /**
     * Sets the factory to use to create the variables of the encoding.
     *
     * @param variableFactory The factory that will create the variables used in the
     *        encoding.
     */
    void setVariableFactory(IVariableFactory variableFactory);

    /**
     * Gives the factory used to create the variables of the encoding.
     *
     * @return The factory that will create the variables used in the encoding.
     */
    IVariableFactory getVariableFactory();

    /**
     * Creates a new unnamed variable in this solver.
     *
     * @param min The minimum value of the domain of the variable.
     * @param max The maximum value of the domain of the variable.
     *
     * @return The created variable.
     */
    default IVariable newVariable(int min, int max) {
        return newVariable(null, min, max);
    }

    /**
     * Creates a new unnamed variable in this solver.
     *
     * @param min The minimum value of the domain of the variable.
     * @param max The maximum value of the domain of the variable.
     *
     * @return The created variable.
     */
    default IVariable newVariable(BigInteger min, BigInteger max) {
        return newVariable(null, min, max);
    }

    /**
     * Creates a new variable in this solver.
     *
     * @param id The identifier of the variable to create.
     * @param min The minimum value of the domain of the variable.
     * @param max The maximum value of the domain of the variable.
     *
     * @return The created variable.
     */
    default IVariable newVariable(String id, int min, int max) {
        return newVariable(id, BigInteger.valueOf(min), BigInteger.valueOf(max));
    }

    /**
     * Creates a new variable in this solver.
     *
     * @param id The identifier of the variable to create.
     * @param min The minimum value of the domain of the variable.
     * @param max The maximum value of the domain of the variable.
     *
     * @return The created variable.
     */
    IVariable newVariable(String id, BigInteger min, BigInteger max);

    /**
     * Creates a new unnamed variable in this solver.
     *
     * @param values The values of the domain of the variable.
     *
     * @return The created variable.
     */
    default IVariable newVariable(int... values) {
        return newVariable(null, values);
    }

    /**
     * Creates a new unnamed variable in this solver.
     *
     * @param values The values of the domain of the variable.
     *
     * @return The created variable.
     */
    default IVariable newVariable(BigInteger... values) {
        return newVariable(null, values);
    }

    /**
     * Creates a new variable in this solver.
     *
     * @param id The identifier of the variable to create.
     * @param values The values of the domain of the variable.
     *
     * @return The created variable.
     */
    default IVariable newVariable(String id, int... values) {
        return newVariable(id, VecInt.of(values));
    }

    /**
     * Creates a new variable in this solver.
     *
     * @param id The identifier of the variable to create.
     * @param values The values of the domain of the variable.
     *
     * @return The created variable.
     */
    default IVariable newVariable(String id, BigInteger... values) {
        return newVariable(id, Vec.of(values));
    }

    /**
     * Creates a new unnamed variable in this solver.
     *
     * @param values The values of the domain of the variable.
     *
     * @return The created variable.
     */
    default IVariable newVariable(IVecInt values) {
        return newVariable(null, values);
    }

    /**
     * Creates a new unnamed variable in this solver.
     *
     * @param values The values of the domain of the variable.
     *
     * @return The created variable.
     */
    default IVariable newVariable(IVec<BigInteger> values) {
        return newVariable(null, values);
    }

    /**
     * Creates a new variable in this solver.
     *
     * @param id The identifier of the variable to create.
     * @param values The values of the domain of the variable.
     *
     * @return The created variable.
     */
    default IVariable newVariable(String id, IVecInt values) {
        var bigValues = new Vec<BigInteger>(values.size());
        for (var it = values.iterator(); it.hasNext();) {
            bigValues.push(BigInteger.valueOf(it.next()));
        }
        return newVariable(id, bigValues);
    }

    /**
     * Creates a new variable in this solver.
     *
     * @param id The identifier of the variable to create.
     * @param values The values of the domain of the variable.
     *
     * @return The created variable.
     */
    IVariable newVariable(String id, IVec<BigInteger> values);

    /**
     * Gives all the variables known by this solver.
     *
     * @return The variables known by this solver, indexed by their names.
     */
    Map<String, IVariable> getVariables();

    /**
     * Gives the names of all the variables known by this solver.
     *
     * @return The names of the variables known by this solver.
     */
    Collection<String> getVariableNames();

    /**
     * Gives the variable with the given name.
     *
     * @param name The name of the variable to get.
     *
     * @return The variable with the given name.
     */
    IVariable getVariable(String name);

    /**
     * Adds an {@code instantiation} constraint to this solver.
     *
     * @param variable The variable to assign.
     * @param value The value to assign to the variable.
     *
     * @return The internal representation of the constraint.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    default IConstr addInstantiation(String variable, int value) throws ContradictionException {
        return addInstantiation(variable, BigInteger.valueOf(value));
    }

    /**
     * Adds an {@code instantiation} constraint to this solver.
     *
     * @param variable The variable to assign.
     * @param value The value to assign to the variable.
     *
     * @return The internal representation of the constraint.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    IConstr addInstantiation(String variable, BigInteger value) throws ContradictionException;

    /**
     * Adds an {@code instantiation} constraint to this solver.
     *
     * @param variables The variables to assign.
     * @param values The values to assign to the variables.
     *
     * @return The internal representation of the constraint.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    default IConstr addInstantiation(IVec<String> variables, IVecInt values)
            throws ContradictionException {
        var group = new ConstrGroup();
        for (int i = 0; i < variables.size(); i++) {
            var constr = addInstantiation(variables.get(i), values.get(i));
            group.add(constr);
        }
        return group;
    }

    /**
     * Adds an {@code instantiation} constraint to this solver.
     *
     * @param variables The variables to assign.
     * @param values The values to assign to the variables.
     *
     * @return The internal representation of the constraint.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    default IConstr addInstantiation(IVec<String> variables, IVec<BigInteger> values)
            throws ContradictionException {
        var group = new ConstrGroup();
        for (int i = 0; i < variables.size(); i++) {
            var constr = addInstantiation(variables.get(i), values.get(i));
            group.add(constr);
        }
        return group;
    }

    /**
     * Adds a {@code clause} constraint to this solver.
     *
     * @param positive The (Boolean) variables appearing positively in the clause.
     * @param negative The (Boolean) variables appearing negatively in the clause.
     *
     * @return The internal representation of the constraint.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    IConstr addClause(IVec<String> positive, IVec<String> negative) throws ContradictionException;

    /**
     * Adds a pseudo-Boolean constraint to this solver.
     *
     * @param literals The literals of the pseudo-Boolean constraint.
     * @param coefficients The coefficient of the pseudo-Boolean constraint.
     * @param operator The relational operator of the pseudo-Boolean constraint.
     * @param degree The degree of the pseudo-Boolean constraint.
     *
     * @return The internal representation of the constraint.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    IConstr addPseudoBoolean(IVecInt literals, IVec<BigInteger> coefficients,
            RelationalOperator operator, BigInteger degree) throws ContradictionException;

    /**
     * Adds a soft pseudo-Boolean constraint to this solver.
     *
     * @param selector The selector that allows to turn off the constraint.
     *        The satisfaction of this selector entails that of the constraint.
     * @param literals The literals of the pseudo-Boolean constraint.
     * @param coefficients The coefficient of the pseudo-Boolean constraint.
     * @param operator The relational operator of the pseudo-Boolean constraint.
     * @param degree The degree of the pseudo-Boolean constraint.
     *
     * @return The internal representation of the constraint.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    IConstr addSoftPseudoBoolean(int selector, IVecInt literals, IVec<BigInteger> coefficients,
            RelationalOperator operator, BigInteger degree) throws ContradictionException;

    /**
     * Adds a loosely-soft pseudo-Boolean constraint to this solver.
     *
     * @param selector The selector that allows to turn off the constraint.
     *        The satisfaction of this selector entails that of the constraint.
     * @param literals The literals of the pseudo-Boolean constraint.
     * @param coefficients The coefficient of the pseudo-Boolean constraint.
     * @param operator The relational operator of the pseudo-Boolean constraint.
     * @param degree The degree of the pseudo-Boolean constraint.
     *
     * @return The internal representation of the constraint.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    IConstr addLooseSoftPseudoBoolean(int selector, IVecInt literals, IVec<BigInteger> coefficients,
            RelationalOperator operator, BigInteger degree) throws ContradictionException;

    /**
     * Adds a soft pseudo-Boolean {@code at-least} constraint to this solver.
     *
     * @param selector The selector that allows to turn off the constraint.
     *        The satisfaction of this selector entails that of the constraint.
     * @param literals The literals of the pseudo-Boolean constraint.
     * @param degree The degree of the pseudo-Boolean constraint.
     *
     * @return The internal representation of the constraint.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    default IConstr addSoftAtLeast(int selector, IVecInt literals, int degree)
            throws ContradictionException {
        return addSoftAtLeast(selector, literals, BigInteger.valueOf(degree));
    }

    /**
     * Adds a loosely-soft pseudo-Boolean {@code at-least} constraint to this
     * solver.
     *
     * @param selector The selector that allows to turn off the constraint.
     *        The satisfaction of this selector entails that of the constraint.
     * @param literals The literals of the pseudo-Boolean constraint.
     * @param degree The degree of the pseudo-Boolean constraint.
     *
     * @return The internal representation of the constraint.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    default IConstr addLooseSoftAtLeast(int selector, IVecInt literals, int degree)
            throws ContradictionException {
        return addLooseSoftAtLeast(selector, literals, BigInteger.valueOf(degree));
    }

    /**
     * Adds a soft pseudo-Boolean {@code at-least} constraint to this solver.
     *
     * @param selector The selector that allows to turn off the constraint.
     *        The satisfaction of this selector entails that of the constraint.
     * @param literals The literals of the pseudo-Boolean constraint.
     * @param degree The degree of the pseudo-Boolean constraint.
     *
     * @return The internal representation of the constraint.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    default IConstr addSoftAtLeast(int selector, IVecInt literals, BigInteger degree)
            throws ContradictionException {
        return addSoftAtLeast(
                selector, literals, new Vec<>(literals.size(), BigInteger.ONE), degree);
    }

    /**
     * Adds a loosely-soft pseudo-Boolean {@code at-least} constraint to this
     * solver.
     *
     * @param selector The selector that allows to turn off the constraint.
     *        The satisfaction of this selector entails that of the constraint.
     * @param literals The literals of the pseudo-Boolean constraint.
     * @param degree The degree of the pseudo-Boolean constraint.
     *
     * @return The internal representation of the constraint.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    default IConstr addLooseSoftAtLeast(int selector, IVecInt literals, BigInteger degree)
            throws ContradictionException {
        return addLooseSoftAtLeast(
                selector, literals, new Vec<>(literals.size(), BigInteger.ONE), degree);
    }

    /**
     * Adds a soft pseudo-Boolean {@code at-least} constraint to this solver.
     *
     * @param selector The selector that allows to turn off the constraint.
     *        The satisfaction of this selector entails that of the constraint.
     * @param literals The literals of the pseudo-Boolean constraint.
     * @param coefficients The coefficient of the pseudo-Boolean constraint.
     * @param degree The degree of the pseudo-Boolean constraint.
     *
     * @return The internal representation of the constraint.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    IConstr addSoftAtLeast(int selector, IVecInt literals, IVec<BigInteger> coefficients,
            BigInteger degree) throws ContradictionException;

    /**
     * Adds a loosely-soft pseudo-Boolean {@code at-least} constraint to this
     * solver.
     *
     * @param selector The selector that allows to turn off the constraint.
     *        The satisfaction of this selector entails that of the constraint.
     * @param literals The literals of the pseudo-Boolean constraint.
     * @param coefficients The coefficient of the pseudo-Boolean constraint.
     * @param degree The degree of the pseudo-Boolean constraint.
     *
     * @return The internal representation of the constraint.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    IConstr addLooseSoftAtLeast(int selector, IVecInt literals, IVec<BigInteger> coefficients,
            BigInteger degree) throws ContradictionException;

    /**
     * Adds a {@code logical} constraint to this solver.
     *
     * @param operator The Boolean operator to apply on the variables.
     * @param variables The (Boolean) variables on which the operator is applied.
     *
     * @return The internal representation of the constraint.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    IConstr addLogical(BooleanOperator operator, IVec<String> variables)
            throws ContradictionException;

    /**
     * Adds a {@code logical} constraint to this solver.
     *
     * @param variable The (Boolean) variable whose assignment depends on the truth
     *        value of the logical operations.
     * @param equiv Whether {@code variable} must be equivalent to the truth
     *        value of the logical operations.
     * @param operator The Boolean operator to apply on the variables.
     * @param variables The (Boolean) variables on which the operator is applied.
     *
     * @return The internal representation of the constraint.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    IConstr addLogical(String variable, boolean equiv, BooleanOperator operator,
            IVec<String> variables) throws ContradictionException;

    /**
     * Adds a {@code logical} constraint to this solver.
     *
     * @param variable The (Boolean) variable whose assignment depends on the truth value
     *        of the comparison between {@code left} and {@code right}.
     * @param left The variable on the left-hand side of the comparison.
     * @param operator The relational operator used to compare {@code left} and
     *        {@code right}.
     * @param right The value on the right-hand side of the comparison.
     *
     * @return The internal representation of the constraint.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    IConstr addLogical(String variable, String left, RelationalOperator operator, BigInteger right)
            throws ContradictionException;

    /**
     * Adds a {@code logical} constraint to this solver.
     *
     * @param variable The (Boolean) variable whose assignment depends on the truth value
     *        of the comparison between {@code left} and {@code right}.
     * @param left The variable on the left-hand side of the comparison.
     * @param operator The relational operator used to compare {@code left} and
     *        {@code right}.
     * @param right The variable on the right-hand side of the comparison.
     *
     * @return The internal representation of the constraint.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    IConstr addLogical(String variable, String left, RelationalOperator operator, String right)
            throws ContradictionException;

    /**
     * Sets the encoder to use to encode multiplicative operations.
     *
     * @param multiplicationEncoder The encoder for multiplicative operations.
     */
    void setMultiplicationEncoder(IMultiplicationConstraintEncoder multiplicationEncoder);

    /**
     * Gives the encoder to use to encode multiplicative operations.
     *
     * @return The encoder to use to encode multiplicative operations.
     */
    IMultiplicationConstraintEncoder getMultiplicationEncoder();

    /**
     * Sets the encoder to use to encode {@code all-different} constraints.
     *
     * @param allDifferentEncoder The encoder for the {@code all-different}
     *        constraints.
     */
    void setAllDifferentEncoder(IAllDifferentConstraintEncoder allDifferentEncoder);

    /**
     * Gives the encoder to use to encode {@code all-different} constraints.
     *
     * @return The encoder to use to encode {@code all-different} constraints.
     */
    IAllDifferentConstraintEncoder getAllDifferentEncoder();

    /**
     * Adds an {@code all-different} constraint to this solver.
     *
     * @param variables The variables that should all be different.
     *
     * @return The internal representation of the constraint.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    IConstr addAllDifferent(IVec<String> variables) throws ContradictionException;

    /**
     * Adds an {@code all-different} constraint to this solver.
     *
     * @param variables The variables that should all be different.
     * @param except The values not to consider in the constraint.
     *
     * @return The internal representation of the constraint.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    IConstr addAllDifferent(IVec<String> variables, IVec<BigInteger> except)
            throws ContradictionException;

    /**
     * Adds an {@code all-different} constraint to this solver.
     *
     * @param variableMatrix The matrix of variables that should all be different.
     *
     * @return The internal representation of the constraint.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    IConstr addAllDifferentMatrix(IVec<IVec<String>> variableMatrix) throws ContradictionException;

    /**
     * Adds an {@code all-different} constraint to this solver.
     *
     * @param variableMatrix The matrix of variables that should all be different.
     * @param except The values not to consider in the constraint.
     *
     * @return The internal representation of the constraint.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    IConstr addAllDifferentMatrix(IVec<IVec<String>> variableMatrix, IVec<BigInteger> except)
            throws ContradictionException;

    /**
     * Adds an {@code all-different} constraint to this solver.
     *
     * @param variableLists The lists of variables that should all be different.
     *
     * @return The internal representation of the constraint.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    IConstr addAllDifferentList(IVec<IVec<String>> variableLists) throws ContradictionException;

    /**
     * Adds an {@code all-different} constraint to this solver.
     *
     * @param intensionConstraints The intension constraints that should all be
     *        different.
     *
     * @return The internal representation of the constraint.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    IConstr addAllDifferentIntension(IVec<IIntensionConstraint> intensionConstraints)
            throws ContradictionException;

    /**
     * Sets the encoder to use to encode {@code cardinality} constraints.
     *
     * @param cardinalityEncoder The encoder for the {@code cardinality}
     *        constraints.
     */
    void setCardinalityEncoder(ICardinalityConstraintEncoder cardinalityEncoder);

    /**
     * Gives the encoder to use to encode {@code cardinality} constraints.
     *
     * @return The encoder to use to encode {@code cardinality} constraints.
     */
    ICardinalityConstraintEncoder getCardinalityEncoder();

    /**
     * Adds a {@code cardinality} constraint to this solver.
     *
     * @param variables The variables to count the assignments of.
     * @param values The assignable values to count.
     * @param occurs The number of times each value can be assigned.
     * @param closed Whether only the values in {@code value} can be assigned to
     *        the variables.
     *
     * @return The internal representation of the constraint.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    IConstr addCardinalityWithConstantValuesAndConstantCounts(IVec<String> variables,
            IVec<BigInteger> values, IVec<BigInteger> occurs, boolean closed)
            throws ContradictionException;

    /**
     * Adds a {@code cardinality} constraint to this solver.
     *
     * @param variables The variables to count the assignments of.
     * @param values The assignable values to count.
     * @param occursMin The minimum number of times each value can be assigned.
     * @param occursMax The maximum number of times each value can be assigned.
     * @param closed Whether only the values in {@code value} can be assigned to
     *        the variables.
     *
     * @return The internal representation of the constraint.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    IConstr addCardinalityWithConstantValuesAndConstantIntervalCounts(IVec<String> variables,
            IVec<BigInteger> values, IVec<BigInteger> occursMin, IVec<BigInteger> occursMax,
            boolean closed) throws ContradictionException;

    /**
     * Adds a {@code cardinality} constraint to this solver.
     *
     * @param variables The variables to count the assignments of.
     * @param values The assignable values to count.
     * @param occurs The variables encoding the number of times each value can be
     *        assigned.
     * @param closed Whether only the values in {@code value} can be assigned to
     *        the variables.
     *
     * @return The internal representation of the constraint.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    IConstr addCardinalityWithConstantValuesAndVariableCounts(IVec<String> variables,
            IVec<BigInteger> values, IVec<String> occurs, boolean closed)
            throws ContradictionException;

    /**
     * Adds a {@code cardinality} constraint to this solver.
     *
     * @param variables The variables to count the assignments of.
     * @param values The variables encoding the assignable values to count.
     * @param occurs The number of times each value can be assigned.
     * @param closed Whether only the values in {@code value} can be assigned to
     *        the variables.
     *
     * @return The internal representation of the constraint.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    IConstr addCardinalityWithVariableValuesAndConstantCounts(IVec<String> variables,
            IVec<String> values, IVec<BigInteger> occurs, boolean closed)
            throws ContradictionException;

    /**
     * Adds a {@code cardinality} constraint to this solver.
     *
     * @param variables The variables to count the assignments of.
     * @param values The variables encoding the assignable values to count.
     * @param occursMin The minimum number of times each value can be assigned.
     * @param occursMax The maximum number of times each value can be assigned.
     * @param closed Whether only the values in {@code value} can be assigned to
     *        the variables.
     *
     * @return The internal representation of the constraint.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    IConstr addCardinalityWithVariableValuesAndConstantIntervalCounts(IVec<String> variables,
            IVec<String> values, IVec<BigInteger> occursMin, IVec<BigInteger> occursMax,
            boolean closed) throws ContradictionException;

    /**
     * Adds a {@code cardinality} constraint to this solver.
     *
     * @param variables The variables to count the assignments of.
     * @param values The variables encoding the assignable values to count.
     * @param occurs The variables encoding the number of times each value can be
     *        assigned.
     * @param closed Whether only the values in {@code value} can be assigned to
     *        the variables.
     *
     * @return The internal representation of the constraint.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    IConstr addCardinalityWithVariableValuesAndVariableCounts(IVec<String> variables,
            IVec<String> values, IVec<String> occurs, boolean closed) throws ContradictionException;

    /**
     * Sets the encoder to use to encode {@code channel} constraints.
     *
     * @param channelEncoder The encoder for the {@code channel} constraints.
     */
    void setChannelEncoder(IChannelConstraintEncoder channelEncoder);

    /**
     * Gives the encoder to use to encode {@code channel} constraints.
     *
     * @return The encoder to use to encode {@code channel} constraints.
     */
    IChannelConstraintEncoder getChannelEncoder();

    /**
     * Adds a {@code channel} constraint to this solver.
     *
     * @param variables The variables appearing in the constraint.
     * @param startIndex The index at which the constraint starts.
     *
     * @return The internal representation of the constraint.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    IConstr addChannel(IVec<String> variables, int startIndex) throws ContradictionException;

    /**
     * Adds a {@code channel} constraint to this solver.
     *
     * @param variables The variables among which exactly one should be satisfied
     *        starting from the given index.
     * @param startIndex The index at which the constraint starts.
     * @param value The variable containing the index of the satisfied
     *        variable.
     *
     * @return The internal representation of the constraint.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    IConstr addChannel(IVec<String> variables, int startIndex, String value)
            throws ContradictionException;

    /**
     * Adds a {@code channel} constraint to this solver.
     *
     * @param variables The variables appearing in the constraint.
     * @param startIndex The index at which the constraint starts on the first
     *        vector of variables.
     * @param otherVariables The variables with which to channel the variables of
     *        the first vector.
     * @param otherStartIndex The index at which the constraint starts on the second
     *        vector of variables.
     *
     * @return The internal representation of the constraint.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    IConstr addChannel(IVec<String> variables, int startIndex, IVec<String> otherVariables,
            int otherStartIndex) throws ContradictionException;

    /**
     * Sets the encoder to use to encode {@code count} constraints.
     *
     * @param countEncoder The encoder for the {@code count} constraints.
     */
    void setCountEncoder(ICountConstraintEncoder countEncoder);

    /**
     * Gives the encoder to use to encode {@code count} constraints.
     *
     * @return The encoder to use to encode {@code count} constraints.
     */
    ICountConstraintEncoder getCountEncoder();

    /**
     * Adds an {@code at-least} constraint to this solver.
     *
     * @param variables The variables to count the assignments of.
     * @param value The value to count the assignments of.
     * @param count The minimum number of times the value can be assigned among
     *        the variables.
     *
     * @return The internal representation of the constraint.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    IConstr addAtLeast(IVec<String> variables, BigInteger value, BigInteger count)
            throws ContradictionException;

    /**
     * Adds an {@code exactly} constraint to this solver.
     *
     * @param variables The variables to count the assignments of.
     * @param value The value to count the assignments of.
     * @param count The number of times the value can be assigned among the
     *        variables.
     *
     * @return The internal representation of the constraint.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    IConstr addExactly(IVec<String> variables, BigInteger value, BigInteger count)
            throws ContradictionException;

    /**
     * Adds an {@code exactly} constraint to this solver.
     *
     * @param variables The variables to count the assignments of.
     * @param value The value to count the assignments of.
     * @param count The variable encoding the number of times the value can be
     *        assigned among the variables.
     *
     * @return The internal representation of the constraint.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    IConstr addExactly(IVec<String> variables, BigInteger value, String count)
            throws ContradictionException;

    /**
     * Adds an {@code among} constraint to this solver.
     *
     * @param variables The variables to count the assignments of.
     * @param values The values to count the assignments of.
     * @param count The number of times the value can be assigned among the
     *        variables.
     *
     * @return The internal representation of the constraint.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    IConstr addAmong(IVec<String> variables, IVec<BigInteger> values, BigInteger count)
            throws ContradictionException;

    /**
     * Adds an {@code among} constraint to this solver.
     *
     * @param variables The variables to count the assignments of.
     * @param values The values to count the assignments of.
     * @param count The variable encoding the number of times the values can be
     *        assigned among the variables.
     *
     * @return The internal representation of the constraint.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    IConstr addAmong(IVec<String> variables, IVec<BigInteger> values, String count)
            throws ContradictionException;

    /**
     * Adds an {@code at-most} constraint to this solver.
     *
     * @param variables The variables to count the assignments of.
     * @param value The value to count the assignments of.
     * @param count The maximum number of times the value can be assigned among
     *        the variables.
     *
     * @return The internal representation of the constraint.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    IConstr addAtMost(IVec<String> variables, BigInteger value, BigInteger count)
            throws ContradictionException;

    /**
     * Adds a {@code count} constraint to this solver.
     *
     * @param variables The variables to count the assignments of.
     * @param values The values to count the assignments of.
     * @param operator The operator to use to compare the number of assignments to
     *        their expected count.
     * @param count The number of times the value can be assigned among the
     *        variables.
     *
     * @return The internal representation of the constraint.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    IConstr addCountWithConstantValues(IVec<String> variables, IVec<BigInteger> values,
            RelationalOperator operator, BigInteger count) throws ContradictionException;

    /**
     * Adds a {@code count} constraint to this solver.
     *
     * @param variables The variables to count the assignments of.
     * @param values The values to count the assignments of.
     * @param operator The operator to use to compare the number of assignments to
     *        their expected count.
     * @param count The variable encoding the number of times the values can be
     *        assigned among the variables.
     *
     * @return The internal representation of the constraint.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    IConstr addCountWithConstantValues(IVec<String> variables, IVec<BigInteger> values,
            RelationalOperator operator, String count) throws ContradictionException;

    /**
     * Adds a {@code count} constraint to this solver.
     *
     * @param variables The variables to count the assignments of.
     * @param values The variables encoding the values to count the assignments
     *        of.
     * @param operator The operator to use to compare the number of assignments to
     *        their expected count.
     * @param count The number of times the values can be assigned among the
     *        variables.
     *
     * @return The internal representation of the constraint.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    IConstr addCountWithVariableValues(IVec<String> variables, IVec<String> values,
            RelationalOperator operator, BigInteger count) throws ContradictionException;

    /**
     * Adds a {@code count} constraint to this solver.
     *
     * @param variables The variables to count the assignments of.
     * @param values The variables encoding the values to count the assignments
     *        of.
     * @param operator The operator to use to compare the number of assignments to
     *        their expected count.
     * @param count The variable encoding the number of times the values can be
     *        assigned among the variables.
     *
     * @return The internal representation of the constraint.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    IConstr addCountWithVariableValues(IVec<String> variables, IVec<String> values,
            RelationalOperator operator, String count) throws ContradictionException;

    /**
     * Adds a {@code count} constraint to this solver.
     *
     * @param expressions The expressions to count the assignments of.
     * @param values The values to count the assignments of.
     * @param operator The operator to use to compare the number of assignments to
     *        their expected count.
     * @param count The variable encoding the number of times the values can be
     *        assigned among the variables.
     *
     * @return The internal representation of the constraint.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    IConstr addCountIntensionWithConstantValues(IVec<IIntensionConstraint> expressions,
            IVec<BigInteger> values, RelationalOperator operator, BigInteger count)
            throws ContradictionException;

    /**
     * Adds a {@code count} constraint to this solver.
     *
     * @param expressions The expressions to count the assignments of.
     * @param values The values to count the assignments of.
     * @param operator The operator to use to compare the number of assignments to
     *        their expected count.
     * @param count The variable encoding the number of times the values can be
     *        assigned among the variables.
     *
     * @return The internal representation of the constraint.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    IConstr addCountIntensionWithConstantValues(IVec<IIntensionConstraint> expressions,
            IVec<BigInteger> values, RelationalOperator operator, String count)
            throws ContradictionException;

    /**
     * Sets the encoder to use to encode {@code cumulative} constraints.
     *
     * @param cumulativeEncoder The encoder for the {@code cumulative} constraints.
     */
    void setCumulativeEncoder(ICumulativeConstraintEncoder cumulativeEncoder);

    /**
     * Gives the encoder to use to encode {@code cumulative} constraints.
     *
     * @return The encoder to use to encode {@code cumulative} constraints.
     */
    ICumulativeConstraintEncoder getCumulativeEncoder();

    /**
     * Adds a {@code cumulative} constraint to this solver.
     *
     * @param origins The variables encoding the origins of the resources.
     * @param lengths The lengths of the tasks to assign.
     * @param heights The heights of the tasks to assign.
     * @param operator The operator to compare the cumulative use with.
     * @param value The value for the cumulative use.
     *
     * @return The internal representation of the constraint.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    IConstr addCumulativeConstantLengthsConstantHeights(IVec<String> origins,
            IVec<BigInteger> lengths, IVec<BigInteger> heights, RelationalOperator operator,
            BigInteger value) throws ContradictionException;

    /**
     * Adds a {@code cumulative} constraint to this solver.
     *
     * @param origins The variables encoding the origins of the resources.
     * @param lengths The lengths of the tasks to assign.
     * @param ends The variables encoding the ends of the resources.
     * @param heights The heights of the tasks to assign.
     * @param operator The operator to compare the cumulative use with.
     * @param value The value for the cumulative use.
     *
     * @return The internal representation of the constraint.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    IConstr addCumulativeConstantLengthsConstantHeights(IVec<String> origins,
            IVec<BigInteger> lengths, IVec<String> ends, IVec<BigInteger> heights,
            RelationalOperator operator, BigInteger value) throws ContradictionException;

    /**
     * Adds a {@code cumulative} constraint to this solver.
     *
     * @param origins The variables encoding the origins of the resources.
     * @param lengths The lengths of the tasks to assign.
     * @param heights The heights of the tasks to assign.
     * @param operator The operator to compare the cumulative use with.
     * @param value The variable encoding the value for the cumulative use.
     *
     * @return The internal representation of the constraint.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    IConstr addCumulativeConstantLengthsConstantHeights(IVec<String> origins,
            IVec<BigInteger> lengths, IVec<BigInteger> heights, RelationalOperator operator,
            String value) throws ContradictionException;

    /**
     * Adds a {@code cumulative} constraint to this solver.
     *
     * @param origins The variables encoding the origins of the resources.
     * @param lengths The lengths of the tasks to assign.
     * @param ends The variables encoding the ends of the resources.
     * @param heights The heights of the tasks to assign.
     * @param operator The operator to compare the cumulative use with.
     * @param value The variable encoding the value for the cumulative use.
     *
     * @return The internal representation of the constraint.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    IConstr addCumulativeConstantLengthsConstantHeights(IVec<String> origins,
            IVec<BigInteger> lengths, IVec<String> ends, IVec<BigInteger> heights,
            RelationalOperator operator, String value) throws ContradictionException;

    /**
     * Adds a {@code cumulative} constraint to this solver.
     *
     * @param origins The variables encoding the origins of the resources.
     * @param lengths The lengths of the tasks to assign.
     * @param heights The variable encoding the heights of the tasks to assign.
     * @param operator The operator to compare the cumulative use with.
     * @param value The value for the cumulative use.
     *
     * @return The internal representation of the constraint.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    IConstr addCumulativeConstantLengthsVariableHeights(IVec<String> origins,
            IVec<BigInteger> lengths, IVec<String> heights, RelationalOperator operator,
            BigInteger value) throws ContradictionException;

    /**
     * Adds a {@code cumulative} constraint to this solver.
     *
     * @param origins The variables encoding the origins of the resources.
     * @param lengths The lengths of the tasks to assign.
     * @param ends The variables encoding the ends of the resources.
     * @param heights The variable encoding the heights of the tasks to assign.
     * @param operator The operator to compare the cumulative use with.
     * @param value The value for the cumulative use.
     *
     * @return The internal representation of the constraint.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    IConstr addCumulativeConstantLengthsVariableHeights(IVec<String> origins,
            IVec<BigInteger> lengths, IVec<String> ends, IVec<String> heights,
            RelationalOperator operator, BigInteger value) throws ContradictionException;

    /**
     * Adds a {@code cumulative} constraint to this solver.
     *
     * @param origins The variables encoding the origins of the resources.
     * @param lengths The lengths of the tasks to assign.
     * @param heights The variable encoding the heights of the tasks to assign.
     * @param operator The operator to compare the cumulative use with.
     * @param value The variable encoding the value for the cumulative use.
     *
     * @return The internal representation of the constraint.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    IConstr addCumulativeConstantLengthsVariableHeights(IVec<String> origins,
            IVec<BigInteger> lengths, IVec<String> heights, RelationalOperator operator,
            String value) throws ContradictionException;

    /**
     * Adds a {@code cumulative} constraint to this solver.
     *
     * @param origins The variables encoding the origins of the resources.
     * @param lengths The lengths of the tasks to assign.
     * @param ends The variables encoding the ends of the resources.
     * @param heights The variable encoding the heights of the tasks to assign.
     * @param operator The operator to compare the cumulative use with.
     * @param value The variable encoding the value for the cumulative use.
     *
     * @return The internal representation of the constraint.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    IConstr addCumulativeConstantLengthsVariableHeights(IVec<String> origins,
            IVec<BigInteger> lengths, IVec<String> ends, IVec<String> heights,
            RelationalOperator operator, String value) throws ContradictionException;

    /**
     * Adds a {@code cumulative} constraint to this solver.
     *
     * @param origins The variables encoding the origins of the resources.
     * @param lengths The variables encoding the lengths of the tasks to assign.
     * @param heights The heights of the tasks to assign.
     * @param operator The operator to compare the cumulative use with.
     * @param value The value for the cumulative use.
     *
     * @return The internal representation of the constraint.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    IConstr addCumulativeVariableLengthsConstantHeights(IVec<String> origins, IVec<String> lengths,
            IVec<BigInteger> heights, RelationalOperator operator, BigInteger value)
            throws ContradictionException;

    /**
     * Adds a {@code cumulative} constraint to this solver.
     *
     * @param origins The variables encoding the origins of the resources.
     * @param lengths The variables encoding the lengths of the tasks to assign.
     * @param ends The variables encoding the ends of the resources.
     * @param heights The heights of the tasks to assign.
     * @param operator The operator to compare the cumulative use with.
     * @param value The value for the cumulative use.
     *
     * @return The internal representation of the constraint.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    IConstr addCumulativeVariableLengthsConstantHeights(IVec<String> origins, IVec<String> lengths,
            IVec<String> ends, IVec<BigInteger> heights, RelationalOperator operator,
            BigInteger value) throws ContradictionException;

    /**
     * Adds a {@code cumulative} constraint to this solver.
     *
     * @param origins The variables encoding the origins of the resources.
     * @param lengths The variables encoding the lengths of the tasks to assign.
     * @param heights The heights of the tasks to assign.
     * @param operator The operator to compare the cumulative use with.
     * @param value The variable encoding the value for the cumulative use.
     *
     * @return The internal representation of the constraint.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    IConstr addCumulativeVariableLengthsConstantHeights(IVec<String> origins, IVec<String> lengths,
            IVec<BigInteger> heights, RelationalOperator operator, String value)
            throws ContradictionException;

    /**
     * Adds a {@code cumulative} constraint to this solver.
     *
     * @param origins The variables encoding the origins of the resources.
     * @param lengths The variables encoding the lengths of the tasks to assign.
     * @param ends The variables encoding the ends of the resources.
     * @param heights The heights of the tasks to assign.
     * @param operator The operator to compare the cumulative use with.
     * @param value The variable encoding the value for the cumulative use.
     *
     * @return The internal representation of the constraint.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    IConstr addCumulativeVariableLengthsConstantHeights(IVec<String> origins, IVec<String> lengths,
            IVec<String> ends, IVec<BigInteger> heights, RelationalOperator operator, String value)
            throws ContradictionException;

    /**
     * Adds a {@code cumulative} constraint to this solver.
     *
     * @param origins The variables encoding the origins of the resources.
     * @param lengths The variables encoding the lengths of the tasks to assign.
     * @param heights The variables encoding the heights of the tasks to assign.
     * @param operator The operator to compare the cumulative use with.
     * @param value The value for the cumulative use.
     *
     * @return The internal representation of the constraint.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    IConstr addCumulativeVariableLengthsVariableHeights(IVec<String> origins, IVec<String> lengths,
            IVec<String> heights, RelationalOperator operator, BigInteger value)
            throws ContradictionException;

    /**
     * Adds a {@code cumulative} constraint to this solver.
     *
     * @param origins The variables encoding the origins of the resources.
     * @param lengths The variables encoding the lengths of the tasks to assign.
     * @param ends The variables encoding the ends of the resources.
     * @param heights The variables encoding the heights of the tasks to assign.
     * @param operator The operator to compare the cumulative use with.
     * @param value The value for the cumulative use.
     *
     * @return The internal representation of the constraint.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    IConstr addCumulativeVariableLengthsVariableHeights(IVec<String> origins, IVec<String> lengths,
            IVec<String> ends, IVec<String> heights, RelationalOperator operator, BigInteger value)
            throws ContradictionException;

    /**
     * Adds a {@code cumulative} constraint to this solver.
     *
     * @param origins The variables encoding the origins of the resources.
     * @param lengths The variables encoding the lengths of the tasks to assign.
     * @param heights The variables encoding the heights of the tasks to assign.
     * @param operator The operator to compare the cumulative use with.
     * @param value The variable encoding the value for the cumulative use.
     *
     * @return The internal representation of the constraint.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    IConstr addCumulativeVariableLengthsVariableHeights(IVec<String> origins, IVec<String> lengths,
            IVec<String> heights, RelationalOperator operator, String value)
            throws ContradictionException;

    /**
     * Adds a {@code cumulative} constraint to this solver.
     *
     * @param origins The variables encoding the origins of the resources.
     * @param lengths The variables encoding the lengths of the tasks to assign.
     * @param ends The variables encoding the ends of the resources.
     * @param heights The variables encoding the heights of the tasks to assign.
     * @param operator The operator to compare the cumulative use with.
     * @param value The variable encoding the value for the cumulative use.
     *
     * @return The internal representation of the constraint.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    IConstr addCumulativeVariableLengthsVariableHeights(IVec<String> origins, IVec<String> lengths,
            IVec<String> ends, IVec<String> heights, RelationalOperator operator, String value)
            throws ContradictionException;

    /**
     * Sets the encoder to use to encode {@code element} constraints.
     *
     * @param elementEncoder The encoder for the {@code element} constraints.
     */
    void setElementEncoder(IElementConstraintEncoder elementEncoder);

    /**
     * Gives the encoder to use to encode {@code element} constraints.
     *
     * @return The encoder to use to encode {@code element} constraints.
     */
    IElementConstraintEncoder getElementEncoder();

    /**
     * Adds an {@code element} constraint to this solver.
     *
     * @param variables The variables appearing in the constraint.
     * @param value The value that must appear among the variables.
     *
     * @return The internal representation of the constraint.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    IConstr addElement(IVec<String> variables, BigInteger value) throws ContradictionException;

    /**
     * Adds an {@code element} constraint to this solver.
     *
     * @param variables The variables appearing in the constraint.
     * @param value The variable encoding the value that must appear among the
     *        variables.
     *
     * @return The internal representation of the constraint.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    IConstr addElement(IVec<String> variables, String value) throws ContradictionException;

    /**
     * Adds an {@code element} constraint to this solver.
     *
     * @param values The values among which to look for the variable.
     * @param startIndex The index at which to start looking for the variable.
     * @param index The index at which the variable appears in the values.
     * @param value The value to look for.
     *
     * @return The internal representation of the constraint.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    IConstr addElementConstantValues(IVec<BigInteger> values, int startIndex, String index,
            BigInteger value) throws ContradictionException;

    /**
     * Adds an {@code element} constraint to this solver.
     *
     * @param values The values among which to look for the variable.
     * @param startIndex The index at which to start looking for the variable.
     * @param index The index at which the variable appears in the values.
     * @param variable The variable whose value is to be looked for.
     *
     * @return The internal representation of the constraint.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    IConstr addElementConstantValues(IVec<BigInteger> values, int startIndex, String index,
            String variable) throws ContradictionException;

    /**
     * Adds an {@code element} constraint to this solver.
     *
     * @param variables The variables among which to look for the value.
     * @param startIndex The index at which to start looking for the value.
     * @param index The index at which the value appears in the variables.
     * @param value The value to look for.
     *
     * @return The internal representation of the constraint.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    IConstr addElement(IVec<String> variables, int startIndex, String index, BigInteger value)
            throws ContradictionException;

    /**
     * Adds an {@code element} constraint to this solver.
     *
     * @param values The variables representing the values among which to look
     *        for the variable.
     * @param startIndex The index at which to start looking for the variable.
     * @param index The index at which the variable appears in the values.
     * @param variable The variable whose value is to be looked for.
     *
     * @return The internal representation of the constraint.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    IConstr addElement(IVec<String> values, int startIndex, String index, String variable)
            throws ContradictionException;

    /**
     * Adds an {@code element} constraint to this solver.
     *
     * @param matrix The matrix of values among which the value must appear.
     * @param startRowIndex The index of the row starting from which the value must
     *        appear.
     * @param rowIndex The variable encoding the index of the row at which the
     *        value appears.
     * @param startColIndex The index of the column starting from which the value
     *        must appear.
     * @param colIndex The variable encoding the index of the column at which
     *        the value appears.
     * @param value The value to look for inside the matrix.
     *
     * @return The internal representation of the constraint.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    IConstr addElementConstantMatrix(IVec<IVec<BigInteger>> matrix, int startRowIndex,
            String rowIndex, int startColIndex, String colIndex, BigInteger value)
            throws ContradictionException;

    /**
     * Adds an {@code element} constraint to this solver.
     *
     * @param matrix The matrix of values among which the value must appear.
     * @param startRowIndex The index of the row starting from which the value must
     *        appear.
     * @param rowIndex The variable encoding the index of the row at which the
     *        value appears.
     * @param startColIndex The index of the column starting from which the value
     *        must appear.
     * @param colIndex The variable encoding the index of the column at which
     *        the value appears.
     * @param value The variable encoding the value to look for inside the
     *        matrix.
     *
     * @return The internal representation of the constraint.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    IConstr addElementConstantMatrix(IVec<IVec<BigInteger>> matrix, int startRowIndex,
            String rowIndex, int startColIndex, String colIndex, String value)
            throws ContradictionException;

    /**
     * Adds an {@code element} constraint to this solver.
     *
     * @param matrix The matrix of variables among which the value must be
     *        assigned.
     * @param startRowIndex The index of the row starting from which the value must
     *        appear.
     * @param rowIndex The variable encoding the index of the row at which the
     *        value appears.
     * @param startColIndex The index of the column starting from which the value
     *        must appear.
     * @param colIndex The variable encoding the index of the column at which
     *        the value appears.
     * @param value The variable encoding the value to look for inside the
     *        matrix.
     *
     * @return The internal representation of the constraint.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    IConstr addElementMatrix(IVec<IVec<String>> matrix, int startRowIndex, String rowIndex,
            int startColIndex, String colIndex, BigInteger value) throws ContradictionException;

    /**
     * Adds an {@code element} constraint to this solver.
     *
     * @param matrix The matrix of variables among which the value must be
     *        assigned.
     * @param startRowIndex The index of the row starting from which the value must
     *        appear.
     * @param rowIndex The variable encoding the index of the row at which the
     *        value appears.
     * @param startColIndex The index of the column starting from which the value
     *        must appear.
     * @param colIndex The variable encoding the index of the column at which
     *        the value appears.
     * @param value The variable encoding the value to look for inside the
     *        matrix.
     *
     * @return The internal representation of the constraint.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    IConstr addElementMatrix(IVec<IVec<String>> matrix, int startRowIndex, String rowIndex,
            int startColIndex, String colIndex, String value) throws ContradictionException;

    /**
     * Sets the encoder to use to encode {@code extension} constraints.
     *
     * @param extensionEncoder The encoder for the {@code extension} constraints.
     */
    void setExtensionEncoder(IExtensionConstraintEncoder extensionEncoder);

    /**
     * Gives the encoder to use to encode {@code extension} constraints.
     *
     * @return The encoder to use to encode {@code extension} constraints.
     */
    IExtensionConstraintEncoder getExtensionEncoder();

    /**
     * Adds an {@code extension} constraint describing the support of a tuple of
     * variables to this solver.
     *
     * @param variable The variable for which the support is given.
     * @param allowedValues The values allowed for the variable.
     *
     * @return The internal representation of the constraint.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    IConstr addSupport(String variable, IVec<BigInteger> allowedValues)
            throws ContradictionException;

    /**
     * Adds an {@code extension} constraint describing the support of a tuple of
     * variables to this solver.
     *
     * @param variableTuple The tuple of variables for which the support is given.
     * @param allowedValues The values allowed for the tuple variables. Values equal
     *        to {@code null} are interpreted as "any value" (as in
     *        so-called "starred tuples").
     *
     * @return The internal representation of the constraint.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    IConstr addSupport(IVec<String> variableTuple, IVec<IVec<BigInteger>> allowedValues)
            throws ContradictionException;

    /**
     * Adds an {@code extension} constraint describing the conflicts of a tuple of
     * variables to this solver.
     *
     * @param variable The variable for which the conflicts are given.
     * @param forbiddenValues The values forbidden for the variable.
     *
     * @return The internal representation of the constraint.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    IConstr addConflicts(String variable, IVec<BigInteger> forbiddenValues)
            throws ContradictionException;

    /**
     * Adds an {@code extension} constraint describing the conflicts of a tuple of
     * variables to this solver.
     *
     * @param variableTuple The tuple of variables for which the conflicts are
     *        given.
     * @param forbiddenValues The values forbidden for the tuple variables. Values
     *        equal to {@code null} are interpreted as "any value"
     *        (as in so-called "starred tuples").
     *
     * @return The internal representation of the constraint.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    IConstr addConflicts(IVec<String> variableTuple, IVec<IVec<BigInteger>> forbiddenValues)
            throws ContradictionException;

    /**
     * Sets the encoder to use to encode {@code intension} constraints.
     *
     * @param intensionEncoder The encoder for the {@code intension} constraints.
     */
    void setIntensionEncoder(IIntensionConstraintEncoder intensionEncoder);

    /**
     * Gives the encoder to use to encode {@code intension} constraints.
     *
     * @return The encoder to use to encode {@code intension} constraints.
     */
    IIntensionConstraintEncoder getIntensionEncoder();

    /**
     * Adds an {@code intension} constraint to this solver.
     *
     * @param constr The user-friendly representation of the constraint to add.
     *
     * @return The internal representation of the constraint.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    IConstr addIntension(IIntensionConstraint constr) throws ContradictionException;

    /**
     * Sets the encoder to use to encode {@code primitive} constraints.
     *
     * @param primitiveEncoder The encoder for the {@code primitive} constraints.
     */
    void setPrimitiveEncoder(IPrimitiveConstraintEncoder primitiveEncoder);

    /**
     * Gives the encoder to use to encode {@code primitive} constraints.
     *
     * @return The encoder to use to encode {@code primitive} constraints.
     */
    IPrimitiveConstraintEncoder getPrimitiveEncoder();

    /**
     * Adds a {@code primitive} constraint to this solver.
     *
     * @param variable The variable appearing in the constraint.
     * @param operator The operator used in the constraint.
     * @param value The value to compare the variable with.
     *
     * @return The internal representation of the constraint.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    IConstr addPrimitive(String variable, RelationalOperator operator, BigInteger value)
            throws ContradictionException;

    /**
     * Adds a {@code primitive} constraint to this solver.
     *
     * @param variable The variable appearing in the constraint.
     * @param arithOp The arithmetic operator applied on the variable.
     * @param leftHandSide The value on the left-hand side of the constraint.
     * @param relOp The relational operator used to compare the right-hand
     *        side with the left-hand side.
     * @param rightHandSide The value on the right-hand side of the constraint.
     *
     * @return The internal representation of the constraint.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    IConstr addPrimitive(String variable, ArithmeticOperator arithOp, BigInteger leftHandSide,
            RelationalOperator relOp, BigInteger rightHandSide) throws ContradictionException;

    /**
     * Adds a {@code primitive} constraint to this solver.
     *
     * @param variable The variable appearing in the constraint.
     * @param arithOp The arithmetic operator applied on the variable.
     * @param leftHandSide The variable on the left-hand side of the constraint.
     * @param relOp The relational operator used to compare the right-hand
     *        side with the left-hand side.
     * @param rightHandSide The value on the right-hand side of the constraint.
     *
     * @return The internal representation of the constraint.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    IConstr addPrimitive(String variable, ArithmeticOperator arithOp, String leftHandSide,
            RelationalOperator relOp, BigInteger rightHandSide) throws ContradictionException;

    /**
     * Adds a {@code primitive} constraint to this solver.
     *
     * @param variable The variable appearing in the constraint.
     * @param arithOp The arithmetic operator applied on the variable.
     * @param leftHandSide The value on the left-hand side of the constraint.
     * @param relOp The relational operator used to compare the right-hand
     *        side with the left-hand side.
     * @param rightHandSide The variable on the right-hand side of the constraint.
     *
     * @return The internal representation of the constraint.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    IConstr addPrimitive(String variable, ArithmeticOperator arithOp, BigInteger leftHandSide,
            RelationalOperator relOp, String rightHandSide) throws ContradictionException;

    /**
     * Adds a {@code primitive} constraint to this solver.
     *
     * @param variable The variable appearing in the constraint.
     * @param arithOp The arithmetic operator applied on the variable.
     * @param leftHandSide The variable on the left-hand side of the constraint.
     * @param relOp The relational operator used to compare the right-hand
     *        side with the left-hand side.
     * @param rightHandSide The variable on the right-hand side of the constraint.
     *
     * @return The internal representation of the constraint.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    IConstr addPrimitive(String variable, ArithmeticOperator arithOp, String leftHandSide,
            RelationalOperator relOp, String rightHandSide) throws ContradictionException;

    /**
     * Adds a {@code primitive} constraint to this solver.
     *
     * @param arithOp The arithmetic operator applied on the variable.
     * @param variable The variable on which the operator is applied.
     * @param rightHandSide The variable on the right-hand side of the constraint.
     *
     * @return The internal representation of the constraint.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    IConstr addPrimitive(ArithmeticOperator arithOp, String variable, String rightHandSide)
            throws ContradictionException;

    /**
     * Adds a {@code primitive} constraint to this solver.
     *
     * @param variable The variable appearing in the constraint.
     * @param operator The operator defining whether the values are allowed or forbidden.
     * @param values The set of values on which the operator is applied.
     *
     * @return The internal representation of the constraint.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    IConstr addPrimitive(String variable, SetBelongingOperator operator, IVec<BigInteger> values)
            throws ContradictionException;

    /**
     * Adds a {@code primitive} constraint to this solver.
     *
     * @param variable The variable appearing in the constraint.
     * @param operator The operator defining whether the values are allowed or forbidden.
     * @param min The minimum value of the range on which the operator is applied.
     * @param max The maximum value of the range on which the operator is applied.
     *
     * @return The internal representation of the constraint.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    IConstr addPrimitive(String variable, SetBelongingOperator operator, BigInteger min,
            BigInteger max) throws ContradictionException;

    /**
     * Sets the encoder to use to encode {@code minimum} and {@code maximum} constraints.
     *
     * @param minMaxEncoder The encoder for the {@code minimum} and {@code maximum}
     *        constraints.
     */
    void setMinMaxEncoder(IMinMaxConstraintEncoder minMaxEncoder);

    /**
     * Gives the encoder to use to encode {@code minimum} and {@code maximum}
     * constraints.
     *
     * @return The encoder to use to encode {@code minimum} and {@code maximum}
     *         constraints.
     */
    IMinMaxConstraintEncoder getMinMaxEncoder();

    /**
     * Adds a {@code minimum} constraint to this solver.
     *
     * @param variables The variables to compute the minimum of.
     * @param operator The relational operator to use to compare the minimum.
     * @param value The value to compare the minimum with.
     *
     * @return The internal representation of the constraint.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    IConstr addMinimum(IVec<String> variables, RelationalOperator operator, BigInteger value)
            throws ContradictionException;

    /**
     * Adds a {@code minimum} constraint to this solver.
     *
     * @param variables The variables to compute the minimum of.
     * @param operator The relational operator to use to compare the minimum.
     * @param value The variable encoding the value to compare the minimum with.
     *
     * @return The internal representation of the constraint.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    IConstr addMinimum(IVec<String> variables, RelationalOperator operator, String value)
            throws ContradictionException;

    /**
     * Adds a {@code minimum} constraint to this solver.
     *
     * @param intensionConstraints The intension constraints to compute the minimum
     *        of.
     * @param operator The relational operator to use to compare the
     *        minimum.
     * @param value The value to compare the minimum with.
     *
     * @return The internal representation of the constraint.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    IConstr addMinimumIntension(IVec<IIntensionConstraint> intensionConstraints,
            RelationalOperator operator, BigInteger value) throws ContradictionException;

    /**
     * Adds a {@code minimum} constraint to this solver.
     *
     * @param intensionConstraints The intension constraints to compute the minimum
     *        of.
     * @param operator The relational operator to use to compare the
     *        minimum.
     * @param value The variable encoding the value to compare the
     *        minimum with.
     *
     * @return The internal representation of the constraint.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    IConstr addMinimumIntension(IVec<IIntensionConstraint> intensionConstraints,
            RelationalOperator operator, String value) throws ContradictionException;

    /**
     * Adds a {@code maximum} constraint to this solver.
     *
     * @param variables The variables to compute the maximum of.
     * @param operator The relational operator to use to compare the maximum.
     * @param value The value to compare the maximum with.
     *
     * @return The internal representation of the constraint.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    IConstr addMaximum(IVec<String> variables, RelationalOperator operator, BigInteger value)
            throws ContradictionException;

    /**
     * Adds a {@code maximum} constraint to this solver.
     *
     * @param variables The variables to compute the maximum of.
     * @param operator The relational operator to use to compare the maximum.
     * @param value The variable encoding the value to compare the maximum with.
     *
     * @return The internal representation of the constraint.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    IConstr addMaximum(IVec<String> variables, RelationalOperator operator, String value)
            throws ContradictionException;

    /**
     * Adds a {@code maximum} constraint to this solver.
     *
     * @param intensionConstraints The intension constraints to compute the maximum
     *        of.
     * @param operator The relational operator to use to compare the
     *        maximum.
     * @param value The value to compare the maximum with.
     *
     * @return The internal representation of the constraint.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    IConstr addMaximumIntension(IVec<IIntensionConstraint> intensionConstraints,
            RelationalOperator operator, BigInteger value) throws ContradictionException;

    /**
     * Adds a {@code maximum} constraint to this solver.
     *
     * @param intensionConstraints The intension constraints to compute the maximum
     *        of.
     * @param operator The relational operator to use to compare the
     *        maximum.
     * @param value The variable encoding the value to compare the
     *        maximum with.
     *
     * @return The internal representation of the constraint.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    IConstr addMaximumIntension(IVec<IIntensionConstraint> intensionConstraints,
            RelationalOperator operator, String value) throws ContradictionException;

    /**
     * Sets the encoder to use to encode {@code no-overlap} constraints.
     *
     * @param noOverlapEncoder The encoder for the {@code no-overlap} constraints.
     */
    void setNoOverlapEncoder(INoOverlapConstraintEncoder noOverlapEncoder);

    /**
     * Gives the encoder to use to encode {@code no-overlap} constraints.
     *
     * @return The encoder to use to encode {@code no-overlap} constraints.
     */
    INoOverlapConstraintEncoder getNoOverlapEncoder();

    /**
     * Adds a {@code no-overlap} constraint to this solver.
     *
     * @param variables The variables appearing in the constraint.
     * @param length The length associated to the variables.
     *
     * @return The internal representation of the constraint.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    IConstr addNoOverlap(IVec<String> variables, IVec<BigInteger> length)
            throws ContradictionException;

    /**
     * Adds a {@code no-overlap} constraint to this solver.
     *
     * @param variables The variables appearing in the constraint.
     * @param length The length associated to the variables.
     * @param zeroIgnored Whether {@code 0}-lengths should be ignored.
     *
     * @return The internal representation of the constraint.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    IConstr addNoOverlap(IVec<String> variables, IVec<BigInteger> length, boolean zeroIgnored)
            throws ContradictionException;

    /**
     * Adds a {@code no-overlap} constraint to this solver.
     *
     * @param variables The variables appearing in the constraint.
     * @param length The variable for the length of the other variables.
     *
     * @return The internal representation of the constraint.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    IConstr addNoOverlapVariableLength(IVec<String> variables, IVec<String> length)
            throws ContradictionException;

    /**
     * Adds a {@code no-overlap} constraint to this solver.
     *
     * @param variables The variables appearing in the constraint.
     * @param length The variable for the length of the other variables.
     * @param zeroIgnored Whether {@code 0}-lengths should be ignored.
     *
     * @return The internal representation of the constraint.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    IConstr addNoOverlapVariableLength(IVec<String> variables, IVec<String> length,
            boolean zeroIgnored) throws ContradictionException;

    /**
     * Adds a {@code no-overlap} constraint to this solver.
     *
     * @param variables The variables appearing in the constraint.
     * @param length The length associated to the variables.
     *
     * @return The internal representation of the constraint.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    IConstr addMultiDimensionalNoOverlap(IVec<IVec<String>> variables,
            IVec<IVec<BigInteger>> length) throws ContradictionException;

    /**
     * Adds a {@code no-overlap} constraint to this solver.
     *
     * @param variables The variables appearing in the constraint.
     * @param length The length associated to the variables.
     * @param zeroIgnored Whether {@code 0}-lengths should be ignored.
     *
     * @return The internal representation of the constraint.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    IConstr addMultiDimensionalNoOverlap(IVec<IVec<String>> variables,
            IVec<IVec<BigInteger>> length, boolean zeroIgnored) throws ContradictionException;

    /**
     * Adds a {@code no-overlap} constraint to this solver.
     *
     * @param variables The variables appearing in the constraint.
     * @param length The variable for the length of the other variables.
     *
     * @return The internal representation of the constraint.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    IConstr addMultiDimensionalNoOverlapVariableLength(IVec<IVec<String>> variables,
            IVec<IVec<String>> length) throws ContradictionException;

    /**
     * Adds a {@code no-overlap} constraint to this solver.
     *
     * @param variables The variables appearing in the constraint.
     * @param length The variable for the length of the other variables.
     * @param zeroIgnored Whether {@code 0}-lengths should be ignored.
     *
     * @return The internal representation of the constraint.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    IConstr addMultiDimensionalNoOverlapVariableLength(IVec<IVec<String>> variables,
            IVec<IVec<String>> length, boolean zeroIgnored) throws ContradictionException;

    /**
     * Sets the encoder to use to encode {@code n-values} constraints.
     *
     * @param nValuesEncoder The encoder for the {@code n-values} constraints.
     */
    void setNValuesEncoder(INValuesConstraintEncoder nValuesEncoder);

    /**
     * Gives the encoder to use to encode {@code n-values} constraints.
     *
     * @return The encoder to use to encode {@code n-values} constraints.
     */
    INValuesConstraintEncoder getNValuesEncoder();

    /**
     * Adds an {@code n-values} constraint to this solver.
     *
     * @param variables The variables appearing in the constraint.
     * @param operator The relational operator used in the constraint.
     * @param nb The number of distinct values to count.
     *
     * @return The internal representation of the constraint.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    IConstr addNValues(IVec<String> variables, RelationalOperator operator, BigInteger nb)
            throws ContradictionException;

    /**
     * Adds an {@code n-values} constraint to this solver.
     *
     * @param variables The variables appearing in the constraint.
     * @param operator The relational operator used in the constraint.
     * @param nb The number of distinct values to count.
     * @param except The values that should not be counted.
     *
     * @return The internal representation of the constraint.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    IConstr addNValuesExcept(IVec<String> variables, RelationalOperator operator, BigInteger nb,
            IVec<BigInteger> except) throws ContradictionException;

    /**
     * Adds an {@code n-values} constraint to this solver.
     *
     * @param variables The variables appearing in the constraint.
     * @param operator The relational operator used in the constraint.
     * @param nb The variable counting the number of distinct values.
     *
     * @return The internal representation of the constraint.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    IConstr addNValues(IVec<String> variables, RelationalOperator operator, String nb)
            throws ContradictionException;

    /**
     * Adds an {@code n-values} constraint to this solver.
     *
     * @param variables The variables appearing in the constraint.
     * @param operator The relational operator used in the constraint.
     * @param nb The variable counting the number of distinct values.
     * @param except The values that should not be counted.
     *
     * @return The internal representation of the constraint.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    IConstr addNValuesExcept(IVec<String> variables, RelationalOperator operator, String nb,
            IVec<BigInteger> except) throws ContradictionException;

    /**
     * Adds an {@code n-values} constraint to this solver.
     *
     * @param expressions The expressions appearing in the constraint.
     * @param operator The relational operator used in the constraint.
     * @param nb The number of distinct values to count.
     *
     * @return The internal representation of the constraint.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    IConstr addNValuesIntension(IVec<IIntensionConstraint> expressions,
            RelationalOperator operator, BigInteger nb) throws ContradictionException;

    /**
     * Adds an {@code n-values} constraint to this solver.
     *
     * @param expressions The expressions appearing in the constraint.
     * @param operator The relational operator used in the constraint.
     * @param nb The variable counting the number of distinct values.
     *
     * @return The internal representation of the constraint.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    IConstr addNValuesIntension(IVec<IIntensionConstraint> expressions,
            RelationalOperator operator, String nb) throws ContradictionException;

    /**
     * Sets the encoder to use to encode {@code ordered} constraints.
     *
     * @param orderedEncoder The encoder for the {@code ordered} constraints.
     */
    void setOrderedEncoder(IOrderedConstraintEncoder orderedEncoder);

    /**
     * Gives the encoder to use to encode {@code ordered} constraints.
     *
     * @return The encoder to use to encode {@code ordered} constraints.
     */
    IOrderedConstraintEncoder getOrderedEncoder();

    /**
     * Adds an {@code ordered} constraint to this solver.
     *
     * @param variables The variables that should be ordered.
     * @param operator The relational operator defining the order of the variables.
     *
     * @return The internal representation of the constraint.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    IConstr addOrdered(IVec<String> variables, RelationalOperator operator)
            throws ContradictionException;

    /**
     * Adds an {@code ordered} constraint to this solver.
     *
     * @param variables The variables that should be ordered.
     * @param lengths The lengths that must exist between two consecutive
     *        variables.
     * @param operator The relational operator defining the order of the variables.
     *
     * @return The internal representation of the constraint.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    IConstr addOrderedWithConstantLength(IVec<String> variables, IVec<BigInteger> lengths,
            RelationalOperator operator) throws ContradictionException;

    /**
     * Adds an {@code ordered} constraint to this solver.
     *
     * @param variables The variables that should be ordered.
     * @param lengths The variables encoding the lengths that must exist between
     *        two consecutive variables.
     * @param operator The relational operator defining the order of the variables.
     *
     * @return The internal representation of the constraint.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    IConstr addOrderedWithVariableLength(IVec<String> variables, IVec<String> lengths,
            RelationalOperator operator) throws ContradictionException;

    /**
     * Adds an {@code all-equal} constraint to this solver.
     *
     * @param variables The variables that should all be equal.
     *
     * @return The internal representation of the constraint.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    IConstr addAllEqual(IVec<String> variables) throws ContradictionException;

    /**
     * Adds an {@code all-equal} constraint to this solver.
     *
     * @param expressions The expressions that should all be equal.
     *
     * @return The internal representation of the constraint.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    IConstr addAllEqualIntension(IVec<IIntensionConstraint> expressions)
            throws ContradictionException;

    /**
     * Adds a {@code not-all-equal} constraint to this solver.
     *
     * @param variables The variables that should not be all equal.
     *
     * @return The internal representation of the constraint.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    IConstr addNotAllEqual(IVec<String> variables) throws ContradictionException;

    /**
     * Adds an {@code lex} constraint to this solver.
     *
     * @param tuples The tuple of variables that should be lexicographically
     *        ordered.
     * @param operator The relational operator defining the order of the tuples.
     *
     * @return The internal representation of the constraint.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    IConstr addLex(IVec<IVec<String>> tuples, RelationalOperator operator)
            throws ContradictionException;

    /**
     * Adds an {@code lex-matrix} constraint to this solver.
     *
     * @param matrix The matrix of variables that should be lexicographically
     *        ordered.
     * @param operator The relational operator defining the order in the matrix.
     *
     * @return The internal representation of the constraint.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    IConstr addLexMatrix(IVec<IVec<String>> matrix, RelationalOperator operator)
            throws ContradictionException;

    /**
     * Sets the encoder to use to encode {@code sum} constraints.
     *
     * @param sumEncoder The encoder for the {@code sum} constraints.
     */
    void setSumEncoder(ISumConstraintEncoder sumEncoder);

    /**
     * Gives the encoder to use to encode {@code sum} constraints.
     *
     * @return The encoder to use to encode {@code sum} constraints.
     */
    ISumConstraintEncoder getSumEncoder();

    /**
     * Adds a {@code sum} constraint to this solver.
     *
     * @param variables The variables appearing in the constraint.
     * @param operator The relational operator used in the constraint.
     * @param value The value of the right-hand side of the constraint.
     *
     * @return The internal representation of the constraint.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    IConstr addSum(IVec<String> variables, RelationalOperator operator, BigInteger value)
            throws ContradictionException;

    /**
     * Adds a {@code sum} constraint to this solver.
     *
     * @param variables The variables appearing in the constraint.
     * @param coefficients The coefficients of the variables appearing in the
     *        constraint.
     * @param operator The relational operator used in the constraint.
     * @param value The value of the right-hand side of the constraint.
     *
     * @return The internal representation of the constraint.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    IConstr addSum(IVec<String> variables, IVec<BigInteger> coefficients,
            RelationalOperator operator, BigInteger value) throws ContradictionException;

    /**
     * Adds a {@code sum} constraint to this solver.
     *
     * @param variables The variables appearing in the constraint.
     * @param operator The relational operator used in the constraint.
     * @param rightVariable The variable on the right-hand side of the constraint.
     *
     * @return The internal representation of the constraint.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    IConstr addSum(IVec<String> variables, RelationalOperator operator, String rightVariable)
            throws ContradictionException;

    /**
     * Adds a {@code sum} constraint to this solver.
     *
     * @param variables The variables appearing in the constraint.
     * @param coefficients The coefficients of the variables appearing in the
     *        constraint.
     * @param operator The relational operator used in the constraint.
     * @param rightVariable The variable on the right-hand side of the constraint.
     *
     * @return The internal representation of the constraint.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    IConstr addSum(IVec<String> variables, IVec<BigInteger> coefficients,
            RelationalOperator operator, String rightVariable) throws ContradictionException;

    /**
     * Adds a {@code sum} constraint to this solver.
     *
     * @param intensionConstraints The intension constraints to compute the sum of.
     * @param operator The relational operator used in the constraint.
     * @param value The value of the right-hand side of the
     *        constraint.
     *
     * @return The internal representation of the constraint.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    IConstr addSumIntension(IVec<IIntensionConstraint> intensionConstraints,
            RelationalOperator operator, BigInteger value) throws ContradictionException;

    /**
     * Adds a {@code sum} constraint to this solver.
     *
     * @param intensionConstraints The intension constraints to compute the sum of.
     * @param coefficients The coefficients of the variables appearing in
     *        the constraint.
     * @param operator The relational operator used in the constraint.
     * @param value The value of the right-hand side of the
     *        constraint.
     *
     * @return The internal representation of the constraint.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    IConstr addSumIntension(IVec<IIntensionConstraint> intensionConstraints,
            IVec<BigInteger> coefficients, RelationalOperator operator, BigInteger value)
            throws ContradictionException;

    /**
     * Adds a {@code sum} constraint to this solver.
     *
     * @param intensionConstraints The intension constraints to compute the sum of.
     * @param operator The relational operator used in the constraint.
     * @param rightVariable The variable on the right-hand side of the
     *        constraint.
     *
     * @return The internal representation of the constraint.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    IConstr addSumIntension(IVec<IIntensionConstraint> intensionConstraints,
            RelationalOperator operator, String rightVariable) throws ContradictionException;

    /**
     * Adds a {@code sum} constraint to this solver.
     *
     * @param intensionConstraints The intension constraints to compute the sum of.
     * @param coefficients The coefficients of the variables appearing in
     *        the constraint.
     * @param operator The relational operator used in the constraint.
     * @param rightVariable The variable on the right-hand side of the
     *        constraint.
     *
     * @return The internal representation of the constraint.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    IConstr addSumIntension(IVec<IIntensionConstraint> intensionConstraints,
            IVec<BigInteger> coefficients, RelationalOperator operator, String rightVariable)
            throws ContradictionException;

    /**
     * Adds a {@code sum} constraint to this solver.
     *
     * @param variables The variables appearing in the constraint.
     * @param coefficients The variables representing the coefficients of the
     *        variables appearing in the constraint.
     * @param operator The relational operator used in the constraint.
     * @param value The value of the right-hand side of the constraint.
     *
     * @return The internal representation of the constraint.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    IConstr addSumWithVariableCoefficients(IVec<String> variables, IVec<String> coefficients,
            RelationalOperator operator, BigInteger value) throws ContradictionException;

    /**
     * Adds a {@code sum} constraint to this solver.
     *
     * @param variables The variables appearing in the constraint.
     * @param coefficients The variables representing the coefficients of the
     *        variables appearing in the constraint.
     * @param operator The relational operator used in the constraint.
     * @param rightVariable The variable on the right-hand side of the constraint.
     *
     * @return The internal representation of the constraint.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    IConstr addSumWithVariableCoefficients(IVec<String> variables, IVec<String> coefficients,
            RelationalOperator operator, String rightVariable) throws ContradictionException;

    /**
     * Adds a {@code sum} constraint to this solver.
     *
     * @param intensionConstraints The intension constraints to compute the sum of.
     * @param coefficients The variables representing the coefficients of
     *        the variables appearing in the constraint.
     * @param operator The relational operator used in the constraint.
     * @param value The value of the right-hand side of the
     *        constraint.
     *
     * @return The internal representation of the constraint.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    IConstr addSumIntensionWithVariableCoefficients(IVec<IIntensionConstraint> intensionConstraints,
            IVec<String> coefficients, RelationalOperator operator, BigInteger value)
            throws ContradictionException;

    /**
     * Adds a {@code sum} constraint to this solver.
     *
     * @param intensionConstraints The intension constraints to compute the sum of.
     * @param coefficients The variables representing the coefficients of
     *        the variables appearing in the constraint.
     * @param operator The relational operator used in the constraint.
     * @param rightVariable The variable on the right-hand side of the
     *        constraint.
     *
     * @return The internal representation of the constraint.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    IConstr addSumIntensionWithVariableCoefficients(IVec<IIntensionConstraint> intensionConstraints,
            IVec<String> coefficients, RelationalOperator operator, String rightVariable)
            throws ContradictionException;

    /**
     * Adds an objective function to this solver to minimize the value assigned to a
     * variable.
     *
     * @param variable The variable to minimize the value of.
     */
    void minimizeVariable(String variable);

    /**
     * Adds an objective function to this solver to maximize the value assigned to a
     * variable.
     *
     * @param variable The variable to maximize the value of.
     */
    void maximizeVariable(String variable);

    /**
     * Adds an objective function to this solver to minimize the value of an expression.
     *
     * @param expression The expression to minimize the value of.
     */
    void minimizeExpression(IIntensionConstraint expression);

    /**
     * Adds an objective function to this solver to maximize the value of an expression.
     *
     * @param expression The expression to maximize the value of.
     */
    void maximizeExpression(IIntensionConstraint expression);

    /**
     * Adds an objective function to this solver to minimize a sum of variables.
     *
     * @param variables The variables to minimize the sum of.
     */
    void minimizeSum(IVec<String> variables);

    /**
     * Adds an objective function to this solver to minimize a sum of variables.
     *
     * @param variables The variables to minimize the sum of.
     * @param coefficients The coefficients of the variables in the sum.
     */
    void minimizeSum(IVec<String> variables, IVec<BigInteger> coefficients);

    /**
     * Adds an objective function to this solver to minimize a sum of expressions.
     *
     * @param expressions The expressions to minimize the sum of.
     */
    void minimizeExpressionSum(IVec<IIntensionConstraint> expressions);

    /**
     * Adds an objective function to this solver to minimize a sum of expressions.
     *
     * @param expressions The expressions to minimize the sum of.
     * @param coefficients The coefficients of the expressions in the sum.
     */
    void minimizeExpressionSum(IVec<IIntensionConstraint> expressions,
            IVec<BigInteger> coefficients);

    /**
     * Adds an objective function to this solver to maximize a sum of variables.
     *
     * @param variables The variables to maximize the sum of.
     */
    void maximizeSum(IVec<String> variables);

    /**
     * Adds an objective function to this solver to maximize a sum of variables.
     *
     * @param variables The variables to maximize the sum of.
     * @param coefficients The coefficients of the variables in the sum.
     */
    void maximizeSum(IVec<String> variables, IVec<BigInteger> coefficients);

    /**
     * Adds an objective function to this solver to maximize a sum of expressions.
     *
     * @param expressions The expressions to maximize the sum of.
     */
    void maximizeExpressionSum(IVec<IIntensionConstraint> expressions);

    /**
     * Adds an objective function to this solver to maximize a sum of expressions.
     *
     * @param expressions The expressions to maximize the sum of.
     * @param coefficients The coefficients of the expressions in the sum.
     */
    void maximizeExpressionSum(IVec<IIntensionConstraint> expressions,
            IVec<BigInteger> coefficients);

    /**
     * Adds an objective function to this solver to minimize a product of variables.
     *
     * @param variables The variables to minimize the product of.
     */
    void minimizeProduct(IVec<String> variables);

    /**
     * Adds an objective function to this solver to minimize a product of variables.
     *
     * @param variables The variables to minimize the product of.
     * @param coefficients The coefficients of the variables in the product.
     */
    void minimizeProduct(IVec<String> variables, IVec<BigInteger> coefficients);

    /**
     * Adds an objective function to this solver to minimize a product of expressions.
     *
     * @param expressions The expressions to minimize the product of.
     */
    void minimizeExpressionProduct(IVec<IIntensionConstraint> expressions);

    /**
     * Adds an objective function to this solver to minimize a product of expressions.
     *
     * @param expressions The expressions to minimize the product of.
     * @param coefficients The coefficients of the expressions in the product.
     */
    void minimizeExpressionProduct(IVec<IIntensionConstraint> expressions,
            IVec<BigInteger> coefficients);

    /**
     * Adds an objective function to this solver to maximize a product of variables.
     *
     * @param variables The variables to maximize the product of.
     */
    void maximizeProduct(IVec<String> variables);

    /**
     * Adds an objective function to this solver to maximize a product of variables.
     *
     * @param variables The variables to maximize the product of.
     * @param coefficients The coefficients of the variables in the product.
     */
    void maximizeProduct(IVec<String> variables, IVec<BigInteger> coefficients);

    /**
     * Adds an objective function to this solver to maximize a product of expressions.
     *
     * @param expressions The expressions to maximize the product of.
     */
    void maximizeExpressionProduct(IVec<IIntensionConstraint> expressions);

    /**
     * Adds an objective function to this solver to maximize a product of expressions.
     *
     * @param expressions The expressions to maximize the product of.
     * @param coefficients The coefficients of the expressions in the product.
     */
    void maximizeExpressionProduct(IVec<IIntensionConstraint> expressions,
            IVec<BigInteger> coefficients);

    /**
     * Adds an objective function to this solver to minimize the minimum of variables.
     *
     * @param variables The variables to minimize the minimum of.
     */
    void minimizeMinimum(IVec<String> variables);

    /**
     * Adds an objective function to this solver to minimize the minimum of variables.
     *
     * @param variables The variables to minimize the minimum of.
     * @param coefficients The coefficients of the variables in the minimum.
     */
    void minimizeMinimum(IVec<String> variables, IVec<BigInteger> coefficients);

    /**
     * Adds an objective function to this solver to minimize the minimum of expressions.
     *
     * @param expressions The expressions to minimize the minimum of.
     */
    void minimizeExpressionMinimum(IVec<IIntensionConstraint> expressions);

    /**
     * Adds an objective function to this solver to minimize the minimum of expressions.
     *
     * @param expressions The expressions to minimize the minimum of.
     * @param coefficients The coefficients of the expressions in the minimum.
     */
    void minimizeExpressionMinimum(IVec<IIntensionConstraint> expressions,
            IVec<BigInteger> coefficients);

    /**
     * Adds an objective function to this solver to maximize the minimum of variables.
     *
     * @param variables The variables to maximize the minimum of.
     */
    void maximizeMinimum(IVec<String> variables);

    /**
     * Adds an objective function to this solver to maximize the minimum of variables.
     *
     * @param variables The variables to maximize the minimum of.
     * @param coefficients The coefficients of the variables in the minimum.
     */
    void maximizeMinimum(IVec<String> variables, IVec<BigInteger> coefficients);

    /**
     * Adds an objective function to this solver to maximize the minimum of expressions.
     *
     * @param expressions The expressions to maximize the minimum of.
     */
    void maximizeExpressionMinimum(IVec<IIntensionConstraint> expressions);

    /**
     * Adds an objective function to this solver to maximize the minimum of expressions.
     *
     * @param expressions The expressions to maximize the minimum of.
     * @param coefficients The coefficients of the expressions in the minimum.
     */
    void maximizeExpressionMinimum(IVec<IIntensionConstraint> expressions,
            IVec<BigInteger> coefficients);

    /**
     * Adds an objective function to this solver to minimize the maximum of variables.
     *
     * @param variables The variables to minimize the maximum of.
     */
    void minimizeMaximum(IVec<String> variables);

    /**
     * Adds an objective function to this solver to minimize the maximum of variables.
     *
     * @param variables The variables to minimize the maximum of.
     * @param coefficients The coefficients of the variables in the maximum.
     */
    void minimizeMaximum(IVec<String> variables, IVec<BigInteger> coefficients);

    /**
     * Adds an objective function to this solver to minimize the maximum of expressions.
     *
     * @param expressions The expressions to minimize the maximum of.
     */
    void minimizeExpressionMaximum(IVec<IIntensionConstraint> expressions);

    /**
     * Adds an objective function to this solver to minimize the maximum of expressions.
     *
     * @param expressions The expressions to minimize the maximum of.
     * @param coefficients The coefficients of the expressions in the maximum.
     */
    void minimizeExpressionMaximum(IVec<IIntensionConstraint> expressions,
            IVec<BigInteger> coefficients);

    /**
     * Adds an objective function to this solver to maximize the maximum of variables.
     *
     * @param variables The variables to maximize the maximum of.
     */
    void maximizeMaximum(IVec<String> variables);

    /**
     * Adds an objective function to this solver to maximize the maximum of variables.
     *
     * @param variables The variables to maximize the maximum of.
     * @param coefficients The coefficients of the variables in the maximum.
     */
    void maximizeMaximum(IVec<String> variables, IVec<BigInteger> coefficients);

    /**
     * Adds an objective function to this solver to maximize the maximum of expressions.
     *
     * @param expressions The expressions to maximize the maximum of.
     */
    void maximizeExpressionMaximum(IVec<IIntensionConstraint> expressions);

    /**
     * Adds an objective function to this solver to maximize the maximum of expressions.
     *
     * @param expressions The expressions to maximize the maximum of.
     * @param coefficients The coefficients of the expressions in the maximum.
     */
    void maximizeExpressionMaximum(IVec<IIntensionConstraint> expressions,
            IVec<BigInteger> coefficients);

    /**
     * Adds an objective function to this solver to minimize the number of values assigned
     * to variables.
     *
     * @param variables The variables to minimize the number of values of.
     */
    void minimizeNValues(IVec<String> variables);

    /**
     * Adds an objective function to this solver to minimize the number of values assigned
     * to variables.
     *
     * @param variables The variables to minimize the number of values of.
     * @param coefficients The coefficients of the variables.
     */
    void minimizeNValues(IVec<String> variables, IVec<BigInteger> coefficients);

    /**
     * Adds an objective function to this solver to minimize the number of values assigned
     * to variables.
     *
     * @param expressions The expressions to minimize the number of values of.
     */
    void minimizeExpressionNValues(IVec<IIntensionConstraint> expressions);

    /**
     * Adds an objective function to this solver to minimize the number of values assigned
     * to variables.
     *
     * @param expressions The expressions to minimize the number of values of.
     * @param coefficients The coefficients of the expressions.
     */
    void minimizeExpressionNValues(IVec<IIntensionConstraint> expressions,
            IVec<BigInteger> coefficients);

    /**
     * Adds an objective function to this solver to maximize the number of values assigned
     * to variables.
     *
     * @param variables The variables to maximize the number of values of.
     */
    void maximizeNValues(IVec<String> variables);

    /**
     * Adds an objective function to this solver to maximize the number of values assigned
     * to variables.
     *
     * @param variables The variables to maximize the number of values of.
     * @param coefficients The coefficients of the variables.
     */
    void maximizeNValues(IVec<String> variables, IVec<BigInteger> coefficients);

    /**
     * Adds an objective function to this solver to maximize the number of values assigned
     * to variables.
     *
     * @param expressions The expressions to maximize the number of values of.
     */
    void maximizeExpressionNValues(IVec<IIntensionConstraint> expressions);

    /**
     * Adds an objective function to this solver to maximize the number of values assigned
     * to variables.
     *
     * @param expressions The expressions to maximize the number of values of.
     * @param coefficients The coefficients of the expressions.
     */
    void maximizeExpressionNValues(IVec<IIntensionConstraint> expressions,
            IVec<BigInteger> coefficients);

    /**
     * Checks whether the problem is satisfiable under some assumptions.
     *
     * @param assumedVariables The variables for which a value is to be assumed.
     * @param assumedValues The values to assume for the variables.
     *
     * @return Whether the problem is satisfiable.
     *
     * @throws ContradictionException If the problem is (trivially) unsatisfiable.
     * @throws TimeoutException If the problem cannot be solved within the
     *         time limit.
     */
    default boolean isSatisfiable(IVec<String> assumedVariables, IVecInt assumedValues)
            throws ContradictionException, TimeoutException {
        var assumps = new HashMap<String, BigInteger>();
        for (int i = 0; i < assumedVariables.size(); i++) {
            assumps.put(assumedVariables.get(i), BigInteger.valueOf(assumedValues.get(i)));
        }
        return isSatisfiable(assumps);
    }

    /**
     * Checks whether the problem is satisfiable under some assumptions.
     *
     * @param assumedVariables The variables for which a value is to be assumed.
     * @param assumedValues The values to assume for the variables.
     *
     * @return Whether the problem is satisfiable.
     *
     * @throws ContradictionException If the problem is (trivially) unsatisfiable.
     * @throws TimeoutException If the problem cannot be solved within the
     *         time limit.
     */
    default boolean isSatisfiable(IVec<String> assumedVariables, IVec<BigInteger> assumedValues)
            throws ContradictionException, TimeoutException {
        var assumps = new HashMap<String, BigInteger>();
        for (int i = 0; i < assumedVariables.size(); i++) {
            assumps.put(assumedVariables.get(i), assumedValues.get(i));
        }
        return isSatisfiable(assumps);
    }

    /**
     * Checks whether the problem is satisfiable under some assumptions.
     *
     * @param assumps The assignments to assume when solving the problem.
     *
     * @return Whether the problem is satisfiable.
     *
     * @throws ContradictionException If the problem is (trivially) unsatisfiable.
     * @throws TimeoutException If the problem cannot be solved within the
     *         time limit.
     */
    boolean isSatisfiable(Map<String, BigInteger> assumps)
            throws ContradictionException, TimeoutException;

}
