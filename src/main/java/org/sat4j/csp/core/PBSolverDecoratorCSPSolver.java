/**
 * Sat4j-CSP, a CSP solver based on the Sat4j platform.
 * Copyright (c) 2021-2022 - Exakis Nelite, Univ Artois & CNRS.
 * All rightVariables reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 * If not, see {@link http://www.gnu.org/licenses}.
 */

package org.sat4j.csp.core;

import static org.sat4j.csp.constraints.intension.IntensionConstraintFactory.variable;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

import org.sat4j.core.ConstrGroup;
import org.sat4j.core.Vec;
import org.sat4j.core.VecInt;
import org.sat4j.csp.constraints.ArithmeticOperator;
import org.sat4j.csp.constraints.BooleanOperator;
import org.sat4j.csp.constraints.RelationalOperator;
import org.sat4j.csp.constraints.SetBelongingOperator;
import org.sat4j.csp.constraints.encoder.alldifferent.DefaultAllDifferentConstraintEncoder;
import org.sat4j.csp.constraints.encoder.alldifferent.IAllDifferentConstraintEncoder;
import org.sat4j.csp.constraints.encoder.cardinality.DefaultCardinalityConstraintEncoder;
import org.sat4j.csp.constraints.encoder.cardinality.ICardinalityConstraintEncoder;
import org.sat4j.csp.constraints.encoder.channel.DefaultChannelConstraintEncoder;
import org.sat4j.csp.constraints.encoder.channel.IChannelConstraintEncoder;
import org.sat4j.csp.constraints.encoder.count.DefaultCountConstraintEncoder;
import org.sat4j.csp.constraints.encoder.count.ICountConstraintEncoder;
import org.sat4j.csp.constraints.encoder.cumulative.DefaultCumulativeConstraintEncoder;
import org.sat4j.csp.constraints.encoder.cumulative.ICumulativeConstraintEncoder;
import org.sat4j.csp.constraints.encoder.element.DefaultElementConstraintEncoder;
import org.sat4j.csp.constraints.encoder.element.IElementConstraintEncoder;
import org.sat4j.csp.constraints.encoder.extension.DefaultExtensionConstraintEncoder;
import org.sat4j.csp.constraints.encoder.extension.IExtensionConstraintEncoder;
import org.sat4j.csp.constraints.encoder.intension.DefaultIntensionConstraintEncoder;
import org.sat4j.csp.constraints.encoder.intension.DefaultIntensionConstraintVisitor;
import org.sat4j.csp.constraints.encoder.intension.DefaultPrimitiveConstraintEncoder;
import org.sat4j.csp.constraints.encoder.intension.IIntensionConstraintEncoder;
import org.sat4j.csp.constraints.encoder.intension.IPrimitiveConstraintEncoder;
import org.sat4j.csp.constraints.encoder.minmax.DefaultMinMaxConstraintEncoder;
import org.sat4j.csp.constraints.encoder.minmax.IMinMaxConstraintEncoder;
import org.sat4j.csp.constraints.encoder.multiplication.IMultiplicationConstraintEncoder;
import org.sat4j.csp.constraints.encoder.multiplication.NaiveMultiplicationConstraintEncoder;
import org.sat4j.csp.constraints.encoder.nooverlap.DefaultNoOverlapConstraintEncoder;
import org.sat4j.csp.constraints.encoder.nooverlap.INoOverlapConstraintEncoder;
import org.sat4j.csp.constraints.encoder.nvalues.DefaultNValuesConstraintEncoder;
import org.sat4j.csp.constraints.encoder.nvalues.INValuesConstraintEncoder;
import org.sat4j.csp.constraints.encoder.order.DefaultOrderedConstraintEncoder;
import org.sat4j.csp.constraints.encoder.order.IOrderedConstraintEncoder;
import org.sat4j.csp.constraints.encoder.sum.DefaultSumConstraintEncoder;
import org.sat4j.csp.constraints.encoder.sum.ISumConstraintEncoder;
import org.sat4j.csp.constraints.intension.IIntensionConstraint;
import org.sat4j.csp.utils.UncheckedContradictionException;
import org.sat4j.csp.variables.ConstantVariable;
import org.sat4j.csp.variables.IVariable;
import org.sat4j.csp.variables.IVariableFactory;
import org.sat4j.csp.variables.MultipliedVariableDecorator;
import org.sat4j.csp.variables.SmartEncodingVariableFactory;
import org.sat4j.pb.IPBSolver;
import org.sat4j.pb.PseudoOptDecorator;
import org.sat4j.specs.ContradictionException;
import org.sat4j.specs.IConstr;
import org.sat4j.specs.IVec;
import org.sat4j.specs.IVecInt;
import org.sat4j.specs.TimeoutException;

/**
 * The PBSolverDecoratorCSPSolver is an {@link ICSPSolver} that encodes CSP constraints
 * into pseudo-Boolean constraints to solve CSP problems using an {@link IPBSolver}.
 *
 * @author Thibault Falque
 * @author Romain Wallon
 *
 * @version 0.1.0
 */
public class PBSolverDecoratorCSPSolver extends PseudoOptDecorator implements ICSPSolver {

    /**
     * The {@code serialVersionUID} of this {@link Serializable} class.
     */
    private static final long serialVersionUID = 1L;

    /**
     * The factory that will create the variables used in the encodings.
     */
    private transient IVariableFactory variableFactory = new SmartEncodingVariableFactory();

    /**
     * The mapping associating each variable to its name (a.k.a. identifier).
     */
    private transient Map<String, IVariable> mapping = new LinkedHashMap<>();

    /**
     * The encoder for multiplicative operations.
     */
    private transient IMultiplicationConstraintEncoder multiplicationEncoder = new NaiveMultiplicationConstraintEncoder();

    /**
     * The encoder for the {@code all-different} constraints.
     */
    private transient IAllDifferentConstraintEncoder allDifferentEncoder = new DefaultAllDifferentConstraintEncoder();

    /**
     * The encoder for the {@code cardinality} constraints.
     */
    private transient ICardinalityConstraintEncoder cardinalityEncoder = new DefaultCardinalityConstraintEncoder();

    /**
     * The encoder for the {@code channel} constraints.
     */
    private transient IChannelConstraintEncoder channelEncoder = new DefaultChannelConstraintEncoder();

    /**
     * The encoder for the {@code count} constraints.
     */
    private transient ICountConstraintEncoder countEncoder = new DefaultCountConstraintEncoder();

    /**
     * The encoder for the {@code cumulative} constraints.
     */
    private transient ICumulativeConstraintEncoder cumulativeEncoder = new DefaultCumulativeConstraintEncoder();

    /**
     * The encoder for the {@code element} constraints.
     */
    private transient IElementConstraintEncoder elementEncoder = new DefaultElementConstraintEncoder();

    /**
     * The encoder for the {@code extension} constraints.
     */
    private transient IExtensionConstraintEncoder extensionEncoder = new DefaultExtensionConstraintEncoder();

    /**
     * The encoder for the {@code intension} constraints.
     */
    private transient IIntensionConstraintEncoder intensionEncoder = new DefaultIntensionConstraintEncoder();

    /**
     * The encoder for the {@code primitive} constraints.
     */
    private transient IPrimitiveConstraintEncoder primitiveEncoder = new DefaultPrimitiveConstraintEncoder();

    /**
     * The encoder for the {@code minimum} and {@code maximum} constraints.
     */
    private transient IMinMaxConstraintEncoder minMaxEncoder = new DefaultMinMaxConstraintEncoder();

    /**
     * The encoder for the {@code no-overlap} constraints.
     */
    private transient INoOverlapConstraintEncoder noOverlapEncoder = new DefaultNoOverlapConstraintEncoder();

    /**
     * The encoder for the {@code n-values} constraints.
     */
    private transient INValuesConstraintEncoder nValuesEncoder = new DefaultNValuesConstraintEncoder();

    /**
     * The encoder for the {@code ordered} constraints.
     */
    private transient IOrderedConstraintEncoder orderedEncoder = new DefaultOrderedConstraintEncoder();

    /**
     * The encoder for the {@code sum} constraints.
     */
    private transient ISumConstraintEncoder sumEncoder = new DefaultSumConstraintEncoder();

    /**
     * Creates a new PBSolverDecoratorCSPSolver.
     *
     * @param solver The solver that will solve the pseudo-Boolean encoding of the
     *        problem.
     */
    public PBSolverDecoratorCSPSolver(IPBSolver solver) {
        super(solver);
        init();
    }

    /**
     * Initializes this solver.
     */
    private void init() {
        // Initializing the encoders.
        this.variableFactory.setSolver(this);
        this.multiplicationEncoder.setSolver(this);
        this.allDifferentEncoder.setSolver(this);
        this.cardinalityEncoder.setSolver(this);
        this.channelEncoder.setSolver(this);
        this.countEncoder.setSolver(this);
        this.cumulativeEncoder.setSolver(this);
        this.elementEncoder.setSolver(this);
        this.extensionEncoder.setSolver(this);
        this.intensionEncoder.setSolver(this);
        this.primitiveEncoder.setSolver(this);
        this.minMaxEncoder.setSolver(this);
        this.noOverlapEncoder.setSolver(this);
        this.nValuesEncoder.setSolver(this);
        this.orderedEncoder.setSolver(this);
        this.sumEncoder.setSolver(this);

        // Creating the variable for representing constant values.
        try {
            int dimacs = newDimacsVariable();
            addClause(VecInt.of(dimacs));
        } catch (ContradictionException e) {
            throw new UncheckedContradictionException(e);
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.core.ICSPSolver#newDimacsVariables(int)
     */
    @Override
    public int newDimacsVariables(int nb) {
        return newVar(nVars() + nb);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.core.ICSPSolver#setVariableFactory(org.sat4j.csp.variables.
     * IVariableFactory)
     */
    @Override
    public void setVariableFactory(IVariableFactory variableFactory) {
        this.variableFactory = variableFactory;
        this.variableFactory.setSolver(this);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.core.ICSPSolver#getVariableFactory()
     */
    @Override
    public IVariableFactory getVariableFactory() {
        return variableFactory;
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.core.ICSPSolver#newVariable(java.lang.String,
     * java.math.BigInteger, java.math.BigInteger)
     */
    @Override
    public IVariable newVariable(String id, BigInteger min, BigInteger max) {
        var variable = variableFactory.createVariable(min, max);
        if (id != null) {
            mapping.put(id, variable);
        }
        return variable;
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.core.ICSPSolver#newVariable(java.lang.String, java.util.List)
     */
    @Override
    public IVariable newVariable(String id, IVec<BigInteger> values) {
        var variable = variableFactory.createVariable(values);
        if (id != null) {
            mapping.put(id, variable);
        }
        return variable;
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.core.ICSPSolver#getVariables()
     */
    @Override
    public Map<String, IVariable> getVariables() {
        return mapping;
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.core.ICSPSolver#getVariableNames()
     */
    @Override
    public Collection<String> getVariableNames() {
        return mapping.keySet();
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.core.ICSPSolver#getVariable(java.lang.String)
     */
    @Override
    public IVariable getVariable(String name) {
        return mapping.get(name);
    }

    /**
     * Gives the variables with the given names.
     *
     * @param names The names of the variables to get.
     *
     * @return The variables with the given names.
     */
    private IVec<IVariable> getVariables(IVec<String> names) {
        var variables = new Vec<IVariable>(names.size());
        for (var it = names.iterator(); it.hasNext();) {
            variables.push(mapping.get(it.next()));
        }
        return variables;
    }

    /**
     * Gives the variables with the given names.
     *
     * @param names The names of the variables to get.
     *
     * @return The variables with the given names.
     */
    private IVec<IVec<IVariable>> getVariableMatrix(IVec<IVec<String>> names) {
        var variables = new Vec<IVec<IVariable>>(names.size());
        for (var it = names.iterator(); it.hasNext();) {
            variables.push(getVariables(it.next()));
        }
        return variables;
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.core.ICSPSolver#addClause(org.sat4j.specs.IVec,
     * org.sat4j.specs.IVec)
     */
    @Override
    public IConstr addClause(IVec<String> positive, IVec<String> negative)
            throws ContradictionException {
        var clause = new VecInt(positive.size() + negative.size());
        toDimacs(positive, clause);
        toDimacs(negative, clause, true);
        return addClause(clause);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.core.ICSPSolver#addPseudoBoolean(org.sat4j.specs.IVecInt,
     * org.sat4j.specs.IVec, org.sat4j.csp.constraints.RelationalOperator,
     * java.math.BigInteger)
     */
    public IConstr addPseudoBoolean(IVecInt literals, IVec<BigInteger> coefficients,
            RelationalOperator operator, BigInteger degree) throws ContradictionException {
        switch (operator) {
            case LT:
                return addAtMost(literals, coefficients, degree.subtract(BigInteger.ONE));

            case LE:
                return addAtMost(literals, coefficients, degree);

            case EQ:
                return addExactly(literals, coefficients, degree);

            case NEQ:
                var group = new ConstrGroup();
                int atMost = newDimacsVariables(2);
                int atLeast = atMost - 1;
                group.add(addSoftPseudoBoolean(
                        atMost, literals, coefficients, RelationalOperator.LE, degree));
                group.add(addSoftPseudoBoolean(
                        atLeast, literals, coefficients, RelationalOperator.GE, degree));
                group.add(addClause(VecInt.of(-atMost, -atLeast)));
                return group;

            case GE:
                return addAtLeast(literals, coefficients, degree);

            case GT:
                return addAtLeast(literals, coefficients, degree.add(BigInteger.ONE));

            default:
                throw new NullPointerException("Null operator for PB constraint.");
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.core.ICSPSolver#addSoftPseudoBoolean(int,
     * org.sat4j.specs.IVecInt, org.sat4j.specs.IVec,
     * org.sat4j.csp.constraints.RelationalOperator, java.math.BigInteger)
     */
    public IConstr addSoftPseudoBoolean(int selector, IVecInt literals,
            IVec<BigInteger> coefficients, RelationalOperator operator, BigInteger degree)
            throws ContradictionException {
        if ((operator == RelationalOperator.EQ) || (operator == RelationalOperator.NEQ)) {
            // The constraint must be divided into several constraints.
            var group = new ConstrGroup();
            int atMost = newDimacsVariables(2);
            int atLeast = atMost - 1;
            group.add(addSoftPseudoBoolean(
                    atMost, literals, coefficients, RelationalOperator.LE, degree));
            group.add(addSoftPseudoBoolean(
                    atLeast, literals, coefficients, RelationalOperator.GE, degree));

            // Adding the appropriate equivalence with the selector.
            if (operator == RelationalOperator.EQ) {
                group.add(addAtLeast(VecInt.of(-selector, atMost, atLeast), VecInt.of(2, 1, 1), 2));
                group.add(addClause(VecInt.of(selector, -atMost, -atLeast)));
            } else {
                group.add(addAtLeast(VecInt.of(selector, atMost, atLeast), VecInt.of(2, 1, 1), 2));
                group.add(addClause(VecInt.of(-selector, -atMost, -atLeast)));
            }
            return group;
        }

        // Normalizing the constraint.
        var effectiveCoefficients = coefficients;
        var effectiveDegree = degree;

        if (operator == RelationalOperator.LT || operator == RelationalOperator.LE) {
            // The opposite sign is required.
            if (operator == RelationalOperator.LT) {
                // Changing the degree to have an "or-equal" constraint.
                effectiveDegree = degree.subtract(BigInteger.ONE);
            }

            // The coefficients and degrees must all be negated.
            effectiveCoefficients = new Vec<BigInteger>(coefficients.size());
            for (var it = coefficients.iterator(); it.hasNext();) {
                effectiveCoefficients.push(it.next().negate());
            }
            effectiveDegree = effectiveDegree.negate();
        }

        if (operator == RelationalOperator.GT) {
            // Changing the degree to have an "or-equal" constraint.
            effectiveDegree = effectiveDegree.add(BigInteger.ONE);
        }

        // Actually adding the soft (now at-least) constraint.
        return addSoftAtLeast(selector, literals, effectiveCoefficients, effectiveDegree);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.core.ICSPSolver#addLooseSoftPseudoBoolean(int,
     * org.sat4j.specs.IVecInt, org.sat4j.specs.IVec,
     * org.sat4j.csp.constraints.RelationalOperator, java.math.BigInteger)
     */
    @Override
    public IConstr addLooseSoftPseudoBoolean(int selector, IVecInt literals,
            IVec<BigInteger> coefficients, RelationalOperator operator, BigInteger degree)
            throws ContradictionException {
        if ((operator == RelationalOperator.EQ) || (operator == RelationalOperator.NEQ)) {
            // The constraint must be divided into several constraints.
            var group = new ConstrGroup();
            int atMost = newDimacsVariables(2);
            int atLeast = atMost - 1;
            group.add(addLooseSoftPseudoBoolean(
                    atMost, literals, coefficients, RelationalOperator.LE, degree));
            group.add(addLooseSoftPseudoBoolean(
                    atLeast, literals, coefficients, RelationalOperator.GE, degree));

            // Adding the appropriate implication with the selector.
            if (operator == RelationalOperator.EQ) {
                group.add(addAtLeast(VecInt.of(-selector, atMost, atLeast), VecInt.of(2, 1, 1), 2));
            } else {
                group.add(addClause(VecInt.of(-selector, -atMost, -atLeast)));
            }
            return group;
        }

        // Normalizing the constraint.
        var effectiveCoefficients = coefficients;
        var effectiveDegree = degree;

        if (operator == RelationalOperator.LT || operator == RelationalOperator.LE) {
            // The opposite sign is required.
            if (operator == RelationalOperator.LT) {
                // Changing the degree to have an "or-equal" constraint.
                effectiveDegree = degree.subtract(BigInteger.ONE);
            }

            // The coefficients and degrees must all be negated.
            effectiveCoefficients = new Vec<BigInteger>(coefficients.size());
            for (var it = coefficients.iterator(); it.hasNext();) {
                effectiveCoefficients.push(it.next().negate());
            }
            effectiveDegree = effectiveDegree.negate();
        }

        if (operator == RelationalOperator.GT) {
            // Changing the degree to have an "or-equal" constraint.
            effectiveDegree = effectiveDegree.add(BigInteger.ONE);
        }

        // Actually adding the soft (now at-least) constraint.
        return addLooseSoftAtLeast(selector, literals, effectiveCoefficients, effectiveDegree);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.core.ICSPSolver#addSoftAtLeast(int, org.sat4j.specs.IVecInt,
     * org.sat4j.specs.IVec, java.math.BigInteger)
     */
    public IConstr addSoftAtLeast(int selector, IVecInt literals, IVec<BigInteger> coefficients,
            BigInteger degree) throws ContradictionException {
        var group = new ConstrGroup(false);
        var normalizedVariables = new VecInt(literals.size());
        var negatedNormalizedVariables = new VecInt(literals.size());
        var normalizedCoefficients = new Vec<BigInteger>(coefficients.size());
        var normalizedDegree = degree;
        var otherDegree = BigInteger.ONE.subtract(degree);

        // Computing the normalized form of the constraint.
        for (int i = 0; i < literals.size(); i++) {
            int variable = literals.get(i);
            var coefficient = coefficients.get(i);

            if (coefficient.signum() < 0) {
                normalizedVariables.push(-variable);
                negatedNormalizedVariables.push(variable);
                normalizedCoefficients.push(coefficient.negate());
                normalizedDegree = normalizedDegree.subtract(coefficient);

            } else {
                normalizedVariables.push(variable);
                negatedNormalizedVariables.push(-variable);
                normalizedCoefficients.push(coefficient);
                otherDegree = otherDegree.add(coefficient);
            }
        }

        // Adding the implications selector => constraint.
        normalizedVariables.push(-selector);
        normalizedCoefficients.push(normalizedDegree);
        group.add(addAtLeast(normalizedVariables, normalizedCoefficients, normalizedDegree));

        // Adding the implications constraint => selector.
        negatedNormalizedVariables.push(selector);
        normalizedCoefficients.pop();
        normalizedCoefficients.push(otherDegree);
        group.add(addAtLeast(negatedNormalizedVariables, normalizedCoefficients, otherDegree));

        return group;
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.core.ICSPSolver#addLooseSoftAtLeast(int,
     * org.sat4j.specs.IVecInt, org.sat4j.specs.IVec, java.math.BigInteger)
     */
    @Override
    public IConstr addLooseSoftAtLeast(int selector, IVecInt literals,
            IVec<BigInteger> coefficients, BigInteger degree) throws ContradictionException {
        var normalizedVariables = new VecInt(literals.size());
        var normalizedCoefficients = new Vec<BigInteger>(coefficients.size());
        var normalizedDegree = degree;

        // Computing the normalized form of the constraint.
        for (int i = 0; i < literals.size(); i++) {
            int variable = literals.get(i);
            var coefficient = coefficients.get(i);

            if (coefficient.signum() < 0) {
                normalizedVariables.push(-variable);
                normalizedCoefficients.push(coefficient.negate());
                normalizedDegree = normalizedDegree.subtract(coefficient);

            } else {
                normalizedVariables.push(variable);
                normalizedCoefficients.push(coefficient);
            }
        }

        // Adding the implications selector => constraint.
        normalizedVariables.push(-selector);
        normalizedCoefficients.push(normalizedDegree);
        return addAtLeast(normalizedVariables, normalizedCoefficients, normalizedDegree);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.sat4j.csp.core.ICSPSolver#addLogical(org.sat4j.csp.constraints.BooleanOperator,
     * org.sat4j.specs.IVec)
     */
    @Override
    public IConstr addLogical(BooleanOperator operator, IVec<String> variables)
            throws ContradictionException {
        switch (operator) {
            case AND:
                // Adding a native conjunction.
                var conj = new VecInt(variables.size());
                toDimacs(variables, conj);
                return addAtLeast(conj, conj.size());

            case OR:
                // Adding a native disjunction.
                var clause = new VecInt(variables.size());
                toDimacs(variables, clause);
                return addClause(clause);

            case XOR:
                // Adding a native parity constraint.
                var parity = new VecInt(variables.size());
                toDimacs(variables, parity);
                return addParity(parity, true);

            case EQUIV:
                // Adding an intension constraint representing the equivalence.
                var vars = new ArrayList<IIntensionConstraint>(variables.size());
                for (var it = variables.iterator(); it.hasNext();) {
                    vars.add(variable(it.next()));
                }
                return new ConstrGroup();

            case IMPL:
                // Adding a native implication.
                var impl = VecInt.of(-toDimacs(variables.get(0)), toDimacs(variables.get(1)));
                toDimacs(variables, impl);
                return addClause(impl);

            default:
                throw new UnsupportedOperationException(
                        "Cannot use this operator for logical constraint: " + operator);
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.core.ICSPSolver#addLogical(java.lang.String, boolean,
     * org.sat4j.csp.constraints.BooleanOperator, org.sat4j.specs.IVec)
     */
    @Override
    public IConstr addLogical(String variable, boolean equiv, BooleanOperator operator,
            IVec<String> variables) throws ContradictionException {
        // Encoding the literal to encode as equivalent (or not) to the logical
        // constraint.
        var literal = toDimacs(variable);
        if (!equiv) {
            literal = -literal;
        }

        // Encoding the internal variable to encode as equivalent (or not) to the logical
        // constraint.
        var internalVar = getVariable(variable);
        if (!equiv) {
            internalVar = intensionEncoder.encodeNot(internalVar);
        }

        // Adding the appropriate constraint, based on the operator.
        switch (operator) {
            case AND:
                // Adding a native soft conjunction.
                var conj = new VecInt(variables.size());
                toDimacs(variables, conj);
                return addSoftAtLeast(literal, conj, conj.size());

            case OR:
                // Adding a native soft disjunction.
                var clause = new VecInt(variables.size());
                toDimacs(variables, clause);
                return addSoftAtLeast(literal, clause, 1);

            case XOR:
                // Adding an intension constraint representing the parity constraint.
                var addition = intensionEncoder.encodeAddition(getVariables(variables));
                intensionEncoder.encodeModulo(internalVar, addition, ConstantVariable.TWO);
                return new ConstrGroup();

            case EQUIV:
                // Adding an intension constraint representing the equivalence.
                var equivVar = intensionEncoder.encodeEquivalence(getVariables(variables));
                return orderedEncoder.encodeAllEqual(Vec.of(internalVar, equivVar));

            case IMPL:
                // Adding a native soft implication.
                var impl = VecInt.of(-toDimacs(variables.get(0)), toDimacs(variables.get(1)));
                toDimacs(variables, impl);
                return addSoftAtLeast(literal, impl, 1);

            default:
                throw new UnsupportedOperationException(
                        "Cannot use this operator for logical constraint: " + operator);
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.core.ICSPSolver#addLogical(java.lang.String, java.lang.String,
     * org.sat4j.csp.constraints.RelationalOperator, java.math.BigInteger)
     */
    @Override
    public IConstr addLogical(String variable, String left, RelationalOperator operator,
            BigInteger right) throws ContradictionException {
        // We first get the DIMACS representation of the (Boolean) variable.
        var internal = getVariable(variable);
        var dimacs = internal.getVariables().get(0);

        // We then add the equivalence between the comparison and the (DIMACS) variable.
        return sumEncoder.encodeSoftSum(dimacs, Vec.of(getVariable(left)), operator, right);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.core.ICSPSolver#addLogical(java.lang.String, java.lang.String,
     * org.sat4j.csp.constraints.RelationalOperator, java.lang.String)
     */
    @Override
    public IConstr addLogical(String variable, String left, RelationalOperator operator,
            String right) throws ContradictionException {
        // We first get the DIMACS representation of the (Boolean) variable.
        var internal = getVariable(variable);
        var dimacs = internal.getVariables().get(0);

        // We then add the equivalence between the comparison and the (DIMACS) variable.
        return sumEncoder.encodeSoftSum(
                dimacs, Vec.of(getVariable(left)), operator, getVariable(right));
    }

    /**
     * Gives the DIMACS identifier of a Boolean variable.
     *
     * @param id The identifier of the variable to get the DIMACS representation of.
     *
     * @return The DIMACS identifier of the variable.
     *
     * @throws IllegalArgumentException If the variable is not a Boolean variable.
     */
    private int toDimacs(String id) {
        var variable = getVariable(id);
        var allDimacs = variable.getVariables();

        if (allDimacs.size() != 1) {
            throw new IllegalArgumentException("Not a Boolean variable: " + id);
        }

        return allDimacs.get(0);
    }

    /**
     * Gives the DIMACS identifiers of a vector of Boolean variables.
     *
     * @param variables The identifiers of the variables for which to get the DIMACS
     *        representation.
     * @param dest The vector in which to put the DIMACS identifiers of the variables.
     *
     * @return The {@code dest} vector.
     *
     * @throws IllegalArgumentException If one of the variables is not a Boolean variable.
     */
    private IVecInt toDimacs(IVec<String> variables, IVecInt dest) {
        return toDimacs(variables, dest, false);
    }

    /**
     * Gives the DIMACS identifiers of a vector of Boolean variables.
     *
     * @param variables The identifiers of the variables for which to get the DIMACS
     *        representation.
     * @param dest The vector in which to put the DIMACS identifiers of the variables.
     * @param negated Whether the DIMACS identifiers must be negated.
     *
     * @return The {@code dest} vector.
     *
     * @throws IllegalArgumentException If one of the variables is not a Boolean variable.
     */
    private IVecInt toDimacs(IVec<String> variables, IVecInt dest, boolean negated) {
        for (var it = variables.iterator(); it.hasNext();) {
            int dimacs = toDimacs(it.next());
            if (negated) {
                dimacs = -dimacs;
            }
            dest.push(dimacs);
        }
        return dest;
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.core.ICSPSolver#addInstantiation(java.lang.String,
     * java.math.BigInteger)
     */
    @Override
    public IConstr addInstantiation(String variable, BigInteger value)
            throws ContradictionException {
        return addSum(Vec.of(variable), RelationalOperator.EQ, value);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.sat4j.csp.core.ICSPSolver#setMultiplicationEncoder(org.sat4j.csp.constraints.
     * encoder.multiplication.IMultiplicationConstraintEncoder)
     */
    @Override
    public void setMultiplicationEncoder(IMultiplicationConstraintEncoder multiplicationEncoder) {
        this.multiplicationEncoder = multiplicationEncoder;
        this.multiplicationEncoder.setSolver(this);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.core.ICSPSolver#getMultiplicationEncoder()
     */
    @Override
    public IMultiplicationConstraintEncoder getMultiplicationEncoder() {
        return multiplicationEncoder;
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.sat4j.csp.core.ICSPSolver#setAllDifferentEncoder(org.sat4j.csp.constraints.
     * encoder.alldifferent.IAllDifferentConstraintEncoder)
     */
    @Override
    public void setAllDifferentEncoder(IAllDifferentConstraintEncoder allDifferentEncoder) {
        this.allDifferentEncoder = allDifferentEncoder;
        this.allDifferentEncoder.setSolver(this);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.core.ICSPSolver#getAllDifferentEncoder()
     */
    @Override
    public IAllDifferentConstraintEncoder getAllDifferentEncoder() {
        return allDifferentEncoder;
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.core.ICSPSolver#addAllDifferent(org.sat4j.specs.IVec)
     */
    @Override
    public IConstr addAllDifferent(IVec<String> variables) throws ContradictionException {
        return allDifferentEncoder.encodeAllDifferent(getVariables(variables));
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.core.ICSPSolver#addAllDifferent(org.sat4j.specs.IVec,
     * org.sat4j.specs.IVec)
     */
    @Override
    public IConstr addAllDifferent(IVec<String> variables, IVec<BigInteger> except)
            throws ContradictionException {
        return allDifferentEncoder.encodeAllDifferent(getVariables(variables), except);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.core.ICSPSolver#addAllDifferentMatrix(org.sat4j.specs.IVec)
     */
    @Override
    public IConstr addAllDifferentMatrix(IVec<IVec<String>> variableMatrix)
            throws ContradictionException {
        return allDifferentEncoder.encodeAllDifferentMatrix(getVariableMatrix(variableMatrix));
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.core.ICSPSolver#addAllDifferentMatrix(org.sat4j.specs.IVec,
     * org.sat4j.specs.IVec)
     */
    @Override
    public IConstr addAllDifferentMatrix(IVec<IVec<String>> variableMatrix,
            IVec<BigInteger> except) throws ContradictionException {
        return allDifferentEncoder.encodeAllDifferentMatrix(
                getVariableMatrix(variableMatrix), except);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.core.ICSPSolver#addAllDifferentList(org.sat4j.specs.IVec)
     */
    @Override
    public IConstr addAllDifferentList(IVec<IVec<String>> variableLists)
            throws ContradictionException {
        return allDifferentEncoder.encodeAllDifferentList(getVariableMatrix(variableLists));
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.core.ICSPSolver#addAllDifferentIntension(org.sat4j.specs.IVec)
     */
    @Override
    public IConstr addAllDifferentIntension(IVec<IIntensionConstraint> intensionConstraints)
            throws ContradictionException {
        return allDifferentEncoder.encodeAllDifferent(encodeIntension(intensionConstraints));
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.core.ICSPSolver#setCardinalityEncoder(org.sat4j.csp.constraints.
     * encoder.cardinality.ICardinalityConstraintEncoder)
     */
    @Override
    public void setCardinalityEncoder(ICardinalityConstraintEncoder cardinalityEncoder) {
        this.cardinalityEncoder = cardinalityEncoder;
        this.cardinalityEncoder.setSolver(this);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.core.ICSPSolver#getCardinalityEncoder()
     */
    @Override
    public ICardinalityConstraintEncoder getCardinalityEncoder() {
        return cardinalityEncoder;
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.sat4j.csp.core.ICSPSolver#addCardinalityWithConstantValuesAndConstantCounts(
     * org.sat4j.specs.IVec, org.sat4j.specs.IVec, org.sat4j.specs.IVec, boolean)
     */
    @Override
    public IConstr addCardinalityWithConstantValuesAndConstantCounts(IVec<String> variables,
            IVec<BigInteger> values, IVec<BigInteger> occurs, boolean closed)
            throws ContradictionException {
        return cardinalityEncoder.encodeCardinalityWithConstantValuesAndConstantCounts(
                getVariables(variables), values, occurs, closed);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.core.ICSPSolver#
     * addCardinalityWithConstantValuesAndConstantIntervalCounts(org.sat4j.specs.IVec,
     * org.sat4j.specs.IVec, org.sat4j.specs.IVec, org.sat4j.specs.IVec, boolean)
     */
    @Override
    public IConstr addCardinalityWithConstantValuesAndConstantIntervalCounts(
            IVec<String> variables, IVec<BigInteger> values, IVec<BigInteger> occursMin,
            IVec<BigInteger> occursMax, boolean closed) throws ContradictionException {
        return cardinalityEncoder.encodeCardinalityWithConstantValuesAndConstantIntervalCounts(
                getVariables(variables), values, occursMin, occursMax, closed);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.sat4j.csp.core.ICSPSolver#addCardinalityWithConstantValuesAndVariableCounts(
     * org.sat4j.specs.IVec, org.sat4j.specs.IVec, org.sat4j.specs.IVec, boolean)
     */
    @Override
    public IConstr addCardinalityWithConstantValuesAndVariableCounts(IVec<String> variables,
            IVec<BigInteger> values, IVec<String> occurs, boolean closed)
            throws ContradictionException {
        return cardinalityEncoder.encodeCardinalityWithConstantValuesAndVariableCounts(
                getVariables(variables), values, getVariables(occurs), closed);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.sat4j.csp.core.ICSPSolver#addCardinalityWithVariableValuesAndConstantCounts
     * (org.sat4j.specs.IVec, org.sat4j.specs.IVec, org.sat4j.specs.IVec, boolean)
     */
    @Override
    public IConstr addCardinalityWithVariableValuesAndConstantCounts(IVec<String> variables,
            IVec<String> values, IVec<BigInteger> occurs, boolean closed)
            throws ContradictionException {
        return cardinalityEncoder.encodeCardinalityWithVariableValuesAndConstantCounts(
                getVariables(variables), getVariables(values), occurs, closed);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.core.ICSPSolver#
     * addCardinalityWithVariableValuesAndConstantIntervalCounts(org.sat4j.specs.IVec,
     * org.sat4j.specs.IVec, org.sat4j.specs.IVec, org.sat4j.specs.IVec, boolean)
     */
    @Override
    public IConstr addCardinalityWithVariableValuesAndConstantIntervalCounts(
            IVec<String> variables, IVec<String> values, IVec<BigInteger> occursMin,
            IVec<BigInteger> occursMax, boolean closed) throws ContradictionException {
        return cardinalityEncoder.encodeCardinalityWithVariableValuesAndConstantIntervalCounts(
                getVariables(variables), getVariables(values), occursMin, occursMax, closed);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.sat4j.csp.core.ICSPSolver#addCardinalityWithVariableValuesAndVariableCounts
     * (org.sat4j.specs.IVec, org.sat4j.specs.IVec, org.sat4j.specs.IVec, boolean)
     */
    @Override
    public IConstr addCardinalityWithVariableValuesAndVariableCounts(IVec<String> variables,
            IVec<String> values, IVec<String> occurs, boolean closed)
            throws ContradictionException {
        return cardinalityEncoder.encodeCardinalityWithVariableValuesAndVariableCounts(
                getVariables(variables), getVariables(values), getVariables(occurs), closed);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.sat4j.csp.core.ICSPSolver#setChannelEncoder(org.sat4j.csp.constraints.encoder.
     * channel.IChannelConstraintEncoder)
     */
    @Override
    public void setChannelEncoder(IChannelConstraintEncoder channelEncoder) {
        this.channelEncoder = channelEncoder;
        channelEncoder.setSolver(this);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.core.ICSPSolver#getChannelEncoder()
     */
    @Override
    public IChannelConstraintEncoder getChannelEncoder() {
        return channelEncoder;
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.core.ICSPSolver#addChannel(org.sat4j.specs.IVec, int)
     */
    @Override
    public IConstr addChannel(IVec<String> variables, int startIndex)
            throws ContradictionException {
        return channelEncoder.encodeChannel(getVariables(variables), startIndex);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.core.ICSPSolver#addChannel(org.sat4j.specs.IVec, int,
     * java.lang.String)
     */
    @Override
    public IConstr addChannel(IVec<String> variables, int startIndex, String value)
            throws ContradictionException {
        return channelEncoder.encodeChannel(getVariables(variables), startIndex,
                getVariable(value));
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.core.ICSPSolver#addChannel(org.sat4j.specs.IVec, int,
     * org.sat4j.specs.IVec, int)
     */
    @Override
    public IConstr addChannel(IVec<String> variables, int startIndex, IVec<String> otherVariables,
            int otherStartIndex) throws ContradictionException {
        return channelEncoder.encodeChannel(getVariables(variables), startIndex,
                getVariables(otherVariables), otherStartIndex);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.sat4j.csp.core.ICSPSolver#setCountEncoder(org.sat4j.csp.constraints.encoder.
     * count.ICountConstraintEncoder)
     */
    @Override
    public void setCountEncoder(ICountConstraintEncoder countEncoder) {
        this.countEncoder = countEncoder;
        this.countEncoder.setSolver(this);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.core.ICSPSolver#getCountEncoder()
     */
    @Override
    public ICountConstraintEncoder getCountEncoder() {
        return countEncoder;
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.core.ICSPSolver#addAtLeast(org.sat4j.specs.IVec,
     * java.math.BigInteger, java.math.BigInteger)
     */
    @Override
    public IConstr addAtLeast(IVec<String> variables, BigInteger value, BigInteger count)
            throws ContradictionException {
        return countEncoder.encodeAtLeast(getVariables(variables), value, count);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.core.ICSPSolver#addExactly(org.sat4j.specs.IVec,
     * java.math.BigInteger, java.math.BigInteger)
     */
    @Override
    public IConstr addExactly(IVec<String> variables, BigInteger value, BigInteger count)
            throws ContradictionException {
        return countEncoder.encodeExactly(getVariables(variables), value, count);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.core.ICSPSolver#addExactly(org.sat4j.specs.IVec,
     * java.math.BigInteger, java.lang.String)
     */
    @Override
    public IConstr addExactly(IVec<String> variables, BigInteger value, String count)
            throws ContradictionException {
        return countEncoder.encodeExactly(getVariables(variables), value, getVariable(count));
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.core.ICSPSolver#addAmong(org.sat4j.specs.IVec,
     * org.sat4j.specs.IVec, java.math.BigInteger)
     */
    @Override
    public IConstr addAmong(IVec<String> variables, IVec<BigInteger> values, BigInteger count)
            throws ContradictionException {
        return countEncoder.encodeAmong(getVariables(variables), values, count);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.core.ICSPSolver#addAmong(org.sat4j.specs.IVec,
     * org.sat4j.specs.IVec, java.lang.String)
     */
    @Override
    public IConstr addAmong(IVec<String> variables, IVec<BigInteger> values, String count)
            throws ContradictionException {
        return countEncoder.encodeAmong(getVariables(variables), values, getVariable(count));
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.core.ICSPSolver#addAtMost(org.sat4j.specs.IVec,
     * java.math.BigInteger, java.math.BigInteger)
     */
    @Override
    public IConstr addAtMost(IVec<String> variables, BigInteger value, BigInteger count)
            throws ContradictionException {
        return countEncoder.encodeAtMost(getVariables(variables), value, count);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.sat4j.csp.core.ICSPSolver#addCountWithConstantValues(org.sat4j.specs.IVec,
     * org.sat4j.specs.IVec, org.sat4j.csp.constraints.RelationalOperator,
     * java.math.BigInteger)
     */
    @Override
    public IConstr addCountWithConstantValues(IVec<String> variables, IVec<BigInteger> values,
            RelationalOperator operator, BigInteger count) throws ContradictionException {
        return countEncoder.encodeCountWithConstantValues(
                getVariables(variables), values, operator, count);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.sat4j.csp.core.ICSPSolver#addCountWithConstantValues(org.sat4j.specs.IVec,
     * org.sat4j.specs.IVec, org.sat4j.csp.constraints.RelationalOperator,
     * java.lang.String)
     */
    @Override
    public IConstr addCountWithConstantValues(IVec<String> variables, IVec<BigInteger> values,
            RelationalOperator operator, String count) throws ContradictionException {
        return countEncoder.encodeCountWithConstantValues(
                getVariables(variables), values, operator, getVariable(count));
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.sat4j.csp.core.ICSPSolver#addCountWithVariableValues(org.sat4j.specs.IVec,
     * org.sat4j.specs.IVec, org.sat4j.csp.constraints.RelationalOperator,
     * java.math.BigInteger)
     */
    @Override
    public IConstr addCountWithVariableValues(IVec<String> variables, IVec<String> values,
            RelationalOperator operator, BigInteger count) throws ContradictionException {
        return countEncoder.encodeCountWithVariableValues(
                getVariables(variables), getVariables(values), operator, count);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.sat4j.csp.core.ICSPSolver#addCountWithVariableValues(org.sat4j.specs.IVec,
     * org.sat4j.specs.IVec, org.sat4j.csp.constraints.RelationalOperator,
     * java.lang.String)
     */
    @Override
    public IConstr addCountWithVariableValues(IVec<String> variables, IVec<String> values,
            RelationalOperator operator, String count) throws ContradictionException {
        return countEncoder.encodeCountWithVariableValues(
                getVariables(variables), getVariables(values), operator, getVariable(count));
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.sat4j.csp.core.ICSPSolver#addCountIntensionWithConstantValues(org.sat4j.specs.
     * IVec, org.sat4j.specs.IVec, org.sat4j.csp.constraints.RelationalOperator,
     * java.math.BigInteger)
     */
    @Override
    public IConstr addCountIntensionWithConstantValues(IVec<IIntensionConstraint> expressions,
            IVec<BigInteger> values, RelationalOperator operator, BigInteger count)
            throws ContradictionException {
        return countEncoder.encodeCountWithConstantValues(
                encodeIntension(expressions), values, operator, count);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.sat4j.csp.core.ICSPSolver#addCountIntensionWithConstantValues(org.sat4j.specs.
     * IVec, org.sat4j.specs.IVec, org.sat4j.csp.constraints.RelationalOperator,
     * java.lang.String)
     */
    @Override
    public IConstr addCountIntensionWithConstantValues(IVec<IIntensionConstraint> expressions,
            IVec<BigInteger> values, RelationalOperator operator, String count)
            throws ContradictionException {
        return countEncoder.encodeCountWithConstantValues(
                encodeIntension(expressions), values, operator, getVariable(count));
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.core.ICSPSolver#setCumulativeEncoder(org.sat4j.csp.constraints.
     * encoder.cumulative.ICumulativeConstraintEncoder)
     */
    @Override
    public void setCumulativeEncoder(ICumulativeConstraintEncoder cumulativeEncoder) {
        this.cumulativeEncoder = cumulativeEncoder;
        this.cumulativeEncoder.setSolver(this);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.core.ICSPSolver#getCumulativeEncoder()
     */
    @Override
    public ICumulativeConstraintEncoder getCumulativeEncoder() {
        return cumulativeEncoder;
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.sat4j.csp.core.ICSPSolver#addCumulativeConstantLengthsConstantHeights(org.sat4j
     * .specs.IVec, org.sat4j.specs.IVec, org.sat4j.specs.IVec,
     * org.sat4j.csp.constraints.RelationalOperator, java.math.BigInteger)
     */
    @Override
    public IConstr addCumulativeConstantLengthsConstantHeights(IVec<String> origins,
            IVec<BigInteger> lengths, IVec<BigInteger> heights, RelationalOperator operator,
            BigInteger value) throws ContradictionException {
        return cumulativeEncoder.encodeCumulativeConstantLengthsConstantHeights(
                getVariables(origins), lengths, heights, operator, value);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.sat4j.csp.core.ICSPSolver#addCumulativeConstantLengthsConstantHeights(org.sat4j
     * .specs.IVec, org.sat4j.specs.IVec, org.sat4j.specs.IVec, org.sat4j.specs.IVec,
     * org.sat4j.csp.constraints.RelationalOperator, java.math.BigInteger)
     */
    @Override
    public IConstr addCumulativeConstantLengthsConstantHeights(IVec<String> origins,
            IVec<BigInteger> lengths, IVec<String> ends, IVec<BigInteger> heights,
            RelationalOperator operator, BigInteger value) throws ContradictionException {
        return cumulativeEncoder.encodeCumulativeConstantLengthsConstantHeights(
                getVariables(origins), lengths, getVariables(ends), heights, operator, value);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.sat4j.csp.core.ICSPSolver#addCumulativeConstantLengthsConstantHeights(org.sat4j
     * .specs.IVec, org.sat4j.specs.IVec, org.sat4j.specs.IVec,
     * org.sat4j.csp.constraints.RelationalOperator, java.lang.String)
     */
    @Override
    public IConstr addCumulativeConstantLengthsConstantHeights(IVec<String> origins,
            IVec<BigInteger> lengths, IVec<BigInteger> heights, RelationalOperator operator,
            String value) throws ContradictionException {
        return cumulativeEncoder.encodeCumulativeConstantLengthsConstantHeights(
                getVariables(origins), lengths, heights, operator, getVariable(value));
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.sat4j.csp.core.ICSPSolver#addCumulativeConstantLengthsConstantHeights(org.sat4j
     * .specs.IVec, org.sat4j.specs.IVec, org.sat4j.specs.IVec, org.sat4j.specs.IVec,
     * org.sat4j.csp.constraints.RelationalOperator, java.lang.String)
     */
    @Override
    public IConstr addCumulativeConstantLengthsConstantHeights(IVec<String> origins,
            IVec<BigInteger> lengths, IVec<String> ends, IVec<BigInteger> heights,
            RelationalOperator operator, String value) throws ContradictionException {
        return cumulativeEncoder.encodeCumulativeConstantLengthsConstantHeights(
                getVariables(origins), lengths, getVariables(ends), heights, operator,
                getVariable(value));
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.sat4j.csp.core.ICSPSolver#addCumulativeConstantLengthsVariableHeights(org.sat4j
     * .specs.IVec, org.sat4j.specs.IVec, org.sat4j.specs.IVec,
     * org.sat4j.csp.constraints.RelationalOperator, java.math.BigInteger)
     */
    @Override
    public IConstr addCumulativeConstantLengthsVariableHeights(IVec<String> origins,
            IVec<BigInteger> lengths, IVec<String> heights, RelationalOperator operator,
            BigInteger value) throws ContradictionException {
        return cumulativeEncoder.encodeCumulativeConstantLengthsVariableHeights(
                getVariables(origins), lengths, getVariables(heights), operator, value);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.sat4j.csp.core.ICSPSolver#addCumulativeConstantLengthsVariableHeights(org.sat4j
     * .specs.IVec, org.sat4j.specs.IVec, org.sat4j.specs.IVec, org.sat4j.specs.IVec,
     * org.sat4j.csp.constraints.RelationalOperator, java.math.BigInteger)
     */
    @Override
    public IConstr addCumulativeConstantLengthsVariableHeights(IVec<String> origins,
            IVec<BigInteger> lengths, IVec<String> ends, IVec<String> heights,
            RelationalOperator operator, BigInteger value) throws ContradictionException {
        return cumulativeEncoder.encodeCumulativeConstantLengthsVariableHeights(
                getVariables(origins), lengths, getVariables(ends), getVariables(heights), operator,
                value);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.sat4j.csp.core.ICSPSolver#addCumulativeConstantLengthsVariableHeights(org.sat4j
     * .specs.IVec, org.sat4j.specs.IVec, org.sat4j.specs.IVec,
     * org.sat4j.csp.constraints.RelationalOperator, java.lang.String)
     */
    @Override
    public IConstr addCumulativeConstantLengthsVariableHeights(IVec<String> origins,
            IVec<BigInteger> lengths, IVec<String> heights, RelationalOperator operator,
            String value) throws ContradictionException {
        return cumulativeEncoder.encodeCumulativeConstantLengthsVariableHeights(
                getVariables(origins), lengths, getVariables(heights), operator,
                getVariable(value));
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.sat4j.csp.core.ICSPSolver#addCumulativeConstantLengthsVariableHeights(org.sat4j
     * .specs.IVec, org.sat4j.specs.IVec, org.sat4j.specs.IVec, org.sat4j.specs.IVec,
     * org.sat4j.csp.constraints.RelationalOperator, java.lang.String)
     */
    @Override
    public IConstr addCumulativeConstantLengthsVariableHeights(IVec<String> origins,
            IVec<BigInteger> lengths, IVec<String> ends, IVec<String> heights,
            RelationalOperator operator, String value) throws ContradictionException {
        return cumulativeEncoder.encodeCumulativeConstantLengthsVariableHeights(
                getVariables(origins), lengths, getVariables(ends), getVariables(heights), operator,
                getVariable(value));
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.sat4j.csp.core.ICSPSolver#addCumulativeVariableLengthsConstantHeights(org.sat4j
     * .specs.IVec, org.sat4j.specs.IVec, org.sat4j.specs.IVec,
     * org.sat4j.csp.constraints.RelationalOperator, java.math.BigInteger)
     */
    @Override
    public IConstr addCumulativeVariableLengthsConstantHeights(IVec<String> origins,
            IVec<String> lengths, IVec<BigInteger> heights, RelationalOperator operator,
            BigInteger value) throws ContradictionException {
        return cumulativeEncoder.encodeCumulativeVariableLengthsConstantHeights(
                getVariables(origins), getVariables(lengths), heights, operator, value);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.sat4j.csp.core.ICSPSolver#addCumulativeVariableLengthsConstantHeights(org.sat4j
     * .specs.IVec, org.sat4j.specs.IVec, org.sat4j.specs.IVec, org.sat4j.specs.IVec,
     * org.sat4j.csp.constraints.RelationalOperator, java.math.BigInteger)
     */
    @Override
    public IConstr addCumulativeVariableLengthsConstantHeights(IVec<String> origins,
            IVec<String> lengths, IVec<String> ends, IVec<BigInteger> heights,
            RelationalOperator operator, BigInteger value) throws ContradictionException {
        return cumulativeEncoder.encodeCumulativeVariableLengthsConstantHeights(
                getVariables(origins), getVariables(lengths), getVariables(ends), heights, operator,
                value);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.sat4j.csp.core.ICSPSolver#addCumulativeVariableLengthsConstantHeights(org.sat4j
     * .specs.IVec, org.sat4j.specs.IVec, org.sat4j.specs.IVec,
     * org.sat4j.csp.constraints.RelationalOperator, java.lang.String)
     */
    @Override
    public IConstr addCumulativeVariableLengthsConstantHeights(IVec<String> origins,
            IVec<String> lengths, IVec<BigInteger> heights, RelationalOperator operator,
            String value) throws ContradictionException {
        return cumulativeEncoder.encodeCumulativeVariableLengthsConstantHeights(
                getVariables(origins), getVariables(lengths), heights, operator,
                getVariable(value));
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.sat4j.csp.core.ICSPSolver#addCumulativeVariableLengthsConstantHeights(org.sat4j
     * .specs.IVec, org.sat4j.specs.IVec, org.sat4j.specs.IVec, org.sat4j.specs.IVec,
     * org.sat4j.csp.constraints.RelationalOperator, java.lang.String)
     */
    @Override
    public IConstr addCumulativeVariableLengthsConstantHeights(IVec<String> origins,
            IVec<String> lengths, IVec<String> ends, IVec<BigInteger> heights,
            RelationalOperator operator, String value) throws ContradictionException {
        return cumulativeEncoder.encodeCumulativeVariableLengthsConstantHeights(
                getVariables(origins), getVariables(lengths), getVariables(ends), heights, operator,
                getVariable(value));
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.sat4j.csp.core.ICSPSolver#addCumulativeVariableLengthsVariableHeights(org.sat4j
     * .specs.IVec, org.sat4j.specs.IVec, org.sat4j.specs.IVec,
     * org.sat4j.csp.constraints.RelationalOperator, java.math.BigInteger)
     */
    @Override
    public IConstr addCumulativeVariableLengthsVariableHeights(IVec<String> origins,
            IVec<String> lengths, IVec<String> heights, RelationalOperator operator,
            BigInteger value) throws ContradictionException {
        return cumulativeEncoder.encodeCumulativeVariableLengthsVariableHeights(
                getVariables(origins), getVariables(lengths), getVariables(heights), operator,
                value);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.sat4j.csp.core.ICSPSolver#addCumulativeVariableLengthsVariableHeights(org.sat4j
     * .specs.IVec, org.sat4j.specs.IVec, org.sat4j.specs.IVec, org.sat4j.specs.IVec,
     * org.sat4j.csp.constraints.RelationalOperator, java.math.BigInteger)
     */
    @Override
    public IConstr addCumulativeVariableLengthsVariableHeights(IVec<String> origins,
            IVec<String> lengths, IVec<String> ends, IVec<String> heights,
            RelationalOperator operator, BigInteger value) throws ContradictionException {
        return cumulativeEncoder.encodeCumulativeVariableLengthsVariableHeights(
                getVariables(origins), getVariables(lengths), getVariables(ends),
                getVariables(heights), operator, value);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.sat4j.csp.core.ICSPSolver#addCumulativeVariableLengthsVariableHeights(org.sat4j
     * .specs.IVec, org.sat4j.specs.IVec, org.sat4j.specs.IVec,
     * org.sat4j.csp.constraints.RelationalOperator, java.lang.String)
     */
    @Override
    public IConstr addCumulativeVariableLengthsVariableHeights(IVec<String> origins,
            IVec<String> lengths, IVec<String> heights, RelationalOperator operator, String value)
            throws ContradictionException {
        return cumulativeEncoder.encodeCumulativeVariableLengthsVariableHeights(
                getVariables(origins), getVariables(lengths), getVariables(heights), operator,
                getVariable(value));
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.sat4j.csp.core.ICSPSolver#addCumulativeVariableLengthsVariableHeights(org.sat4j
     * .specs.IVec, org.sat4j.specs.IVec, org.sat4j.specs.IVec, org.sat4j.specs.IVec,
     * org.sat4j.csp.constraints.RelationalOperator, java.lang.String)
     */
    @Override
    public IConstr addCumulativeVariableLengthsVariableHeights(IVec<String> origins,
            IVec<String> lengths, IVec<String> ends, IVec<String> heights,
            RelationalOperator operator, String value) throws ContradictionException {
        return cumulativeEncoder.encodeCumulativeVariableLengthsVariableHeights(
                getVariables(origins), getVariables(lengths), getVariables(ends),
                getVariables(heights), operator, getVariable(value));
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.sat4j.csp.core.ICSPSolver#setElementEncoder(org.sat4j.csp.constraints.encoder.
     * element.IElementConstraintEncoder)
     */
    @Override
    public void setElementEncoder(IElementConstraintEncoder elementEncoder) {
        this.elementEncoder = elementEncoder;
        this.elementEncoder.setSolver(this);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.core.ICSPSolver#getElementEncoder()
     */
    @Override
    public IElementConstraintEncoder getElementEncoder() {
        return elementEncoder;
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.core.ICSPSolver#addElement(org.sat4j.specs.IVec,
     * java.math.BigInteger)
     */
    @Override
    public IConstr addElement(IVec<String> variables, BigInteger value)
            throws ContradictionException {
        return elementEncoder.encodeElement(getVariables(variables), value);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.core.ICSPSolver#addElement(org.sat4j.specs.IVec,
     * java.lang.String)
     */
    @Override
    public IConstr addElement(IVec<String> variables, String value) throws ContradictionException {
        return elementEncoder.encodeElement(getVariables(variables), getVariable(value));
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.core.ICSPSolver#addElementConstantValues(org.sat4j.specs.IVec,
     * int, java.lang.String, java.math.BigInteger)
     */
    @Override
    public IConstr addElementConstantValues(IVec<BigInteger> values, int startIndex, String index,
            BigInteger value) throws ContradictionException {
        return elementEncoder.encodeElementConstantValues(
                values, startIndex, getVariable(index), value);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.core.ICSPSolver#addElementConstantValues(org.sat4j.specs.IVec,
     * int, java.lang.String, java.lang.String)
     */
    @Override
    public IConstr addElementConstantValues(IVec<BigInteger> values, int startIndex, String index,
            String variable) throws ContradictionException {
        return elementEncoder.encodeElementConstantValues(
                values, startIndex, getVariable(index), getVariable(variable));
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.core.ICSPSolver#addElement(org.sat4j.specs.IVec, int,
     * java.lang.String, java.math.BigInteger)
     */
    @Override
    public IConstr addElement(IVec<String> variables, int startIndex, String index,
            BigInteger value) throws ContradictionException {
        return elementEncoder.encodeElement(
                getVariables(variables), startIndex, getVariable(index), value);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.core.ICSPSolver#addElement(org.sat4j.specs.IVec, int,
     * java.lang.String, java.lang.String)
     */
    @Override
    public IConstr addElement(IVec<String> values, int startIndex, String index, String variable)
            throws ContradictionException {
        return elementEncoder.encodeElement(
                getVariables(values), startIndex, getVariable(index), getVariable(variable));
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.core.ICSPSolver#addElementConstantMatrix(org.sat4j.specs.IVec,
     * int, java.lang.String, int, java.lang.String, java.math.BigInteger)
     */
    @Override
    public IConstr addElementConstantMatrix(IVec<IVec<BigInteger>> matrix, int startRowIndex,
            String rowIndex, int startColIndex, String colIndex, BigInteger value)
            throws ContradictionException {
        return elementEncoder.encodeElementConstantMatrix(matrix, startRowIndex,
                getVariable(rowIndex), startColIndex, getVariable(colIndex), value);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.sat4j.csp.core.ICSPSolver#addElementConstantMatrix(org.sat4j.specs.IVec,
     * int, java.lang.String, int, java.lang.String, java.lang.String)
     */
    @Override
    public IConstr addElementConstantMatrix(IVec<IVec<BigInteger>> matrix, int startRowIndex,
            String rowIndex, int startColIndex, String colIndex, String value)
            throws ContradictionException {
        return elementEncoder.encodeElementConstantMatrix(matrix, startRowIndex,
                getVariable(rowIndex), startColIndex, getVariable(colIndex), getVariable(value));
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.core.ICSPSolver#addElementMatrix(org.sat4j.specs.IVec, int,
     * java.lang.String, int, java.lang.String, java.math.BigInteger)
     */
    @Override
    public IConstr addElementMatrix(IVec<IVec<String>> matrix, int startRowIndex, String rowIndex,
            int startColIndex, String colIndex, BigInteger value) throws ContradictionException {
        return elementEncoder.encodeElementMatrix(getVariableMatrix(matrix), startRowIndex,
                getVariable(rowIndex), startColIndex, getVariable(colIndex), value);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.core.ICSPSolver#addElementMatrix(org.sat4j.specs.IVec, int,
     * java.lang.String, int, java.lang.String, java.lang.String)
     */
    @Override
    public IConstr addElementMatrix(IVec<IVec<String>> matrix, int startRowIndex, String rowIndex,
            int startColIndex, String colIndex, String value) throws ContradictionException {
        return elementEncoder.encodeElementMatrix(getVariableMatrix(matrix), startRowIndex,
                getVariable(rowIndex), startColIndex, getVariable(colIndex), getVariable(value));
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.sat4j.csp.core.ICSPSolver#setExtensionEncoder(org.sat4j.csp.constraints.encoder
     * .extension.IExtensionConstraintEncoder)
     */
    @Override
    public void setExtensionEncoder(IExtensionConstraintEncoder extensionEncoder) {
        this.extensionEncoder = extensionEncoder;
        this.extensionEncoder.setSolver(this);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.core.ICSPSolver#getExtensionEncoder()
     */
    @Override
    public IExtensionConstraintEncoder getExtensionEncoder() {
        return extensionEncoder;
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.core.ICSPSolver#addSupport(java.lang.String,
     * org.sat4j.specs.IVec)
     */
    @Override
    public IConstr addSupport(String variable, IVec<BigInteger> allowedValues)
            throws ContradictionException {
        return extensionEncoder.encodeSupport(getVariable(variable), allowedValues);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.core.ICSPSolver#addSupport(org.sat4j.specs.IVec,
     * org.sat4j.specs.IVec)
     */
    @Override
    public IConstr addSupport(IVec<String> variableTuple, IVec<IVec<BigInteger>> allowedValues)
            throws ContradictionException {
        return extensionEncoder.encodeSupport(getVariables(variableTuple), allowedValues);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.core.ICSPSolver#addConflicts(java.lang.String,
     * org.sat4j.specs.IVec)
     */
    @Override
    public IConstr addConflicts(String variable, IVec<BigInteger> forbiddenValues)
            throws ContradictionException {
        return extensionEncoder.encodeConflicts(getVariable(variable), forbiddenValues);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.core.ICSPSolver#addConflicts(org.sat4j.specs.IVec,
     * org.sat4j.specs.IVec)
     */
    @Override
    public IConstr addConflicts(IVec<String> variableTuple, IVec<IVec<BigInteger>> forbiddenValues)
            throws ContradictionException {
        return extensionEncoder.encodeConflicts(getVariables(variableTuple), forbiddenValues);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.sat4j.csp.core.ICSPSolver#setIntensionEncoder(org.sat4j.csp.constraints.encoder
     * .intension.IIntensionConstraintEncoder)
     */
    @Override
    public void setIntensionEncoder(IIntensionConstraintEncoder intensionEncoder) {
        this.intensionEncoder = intensionEncoder;
        this.intensionEncoder.setSolver(this);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.core.ICSPSolver#getIntensionEncoder()
     */
    @Override
    public IIntensionConstraintEncoder getIntensionEncoder() {
        return intensionEncoder;
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.sat4j.csp.core.ICSPSolver#addIntension(org.sat4j.csp.constraints.intension.
     * IIntensionConstraint)
     */
    @Override
    public IConstr addIntension(IIntensionConstraint constr) throws ContradictionException {
        var variable = encodeIntension(constr);
        return addClause(VecInt.of(variable.getVariables().get(0)));
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.sat4j.csp.core.ICSPSolver#setPrimitiveEncoder(org.sat4j.csp.constraints.encoder
     * .intension.IPrimitiveConstraintEncoder)
     */
    @Override
    public void setPrimitiveEncoder(IPrimitiveConstraintEncoder primitiveEncoder) {
        this.primitiveEncoder = primitiveEncoder;
        this.primitiveEncoder.setSolver(this);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.core.ICSPSolver#getPrimitiveEncoder()
     */
    @Override
    public IPrimitiveConstraintEncoder getPrimitiveEncoder() {
        return primitiveEncoder;
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.core.ICSPSolver#addPrimitive(java.lang.String,
     * org.sat4j.csp.constraints.RelationalOperator, java.math.BigInteger)
     */
    @Override
    public IConstr addPrimitive(String variable, RelationalOperator operator, BigInteger value)
            throws ContradictionException {
        return primitiveEncoder.encodePrimitive(getVariable(variable), operator, value);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.core.ICSPSolver#addPrimitive(java.lang.String,
     * org.sat4j.csp.constraints.ArithmeticOperator, java.math.BigInteger,
     * org.sat4j.csp.constraints.RelationalOperator, java.math.BigInteger)
     */
    @Override
    public IConstr addPrimitive(String variable, ArithmeticOperator arithOp,
            BigInteger leftHandSide, RelationalOperator relOp, BigInteger rightHandSide)
            throws ContradictionException {
        return primitiveEncoder.encodePrimitive(
                getVariable(variable), arithOp, leftHandSide, relOp, rightHandSide);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.core.ICSPSolver#addPrimitive(java.lang.String,
     * org.sat4j.csp.constraints.ArithmeticOperator, java.lang.String,
     * org.sat4j.csp.constraints.RelationalOperator, java.math.BigInteger)
     */
    @Override
    public IConstr addPrimitive(String variable, ArithmeticOperator arithOp, String leftHandSide,
            RelationalOperator relOp, BigInteger rightHandSide) throws ContradictionException {
        return primitiveEncoder.encodePrimitive(
                getVariable(variable), arithOp, getVariable(leftHandSide), relOp, rightHandSide);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.core.ICSPSolver#addPrimitive(java.lang.String,
     * org.sat4j.csp.constraints.ArithmeticOperator, java.math.BigInteger,
     * org.sat4j.csp.constraints.RelationalOperator, java.lang.String)
     */
    @Override
    public IConstr addPrimitive(String variable, ArithmeticOperator arithOp,
            BigInteger leftHandSide, RelationalOperator relOp, String rightHandSide)
            throws ContradictionException {
        return primitiveEncoder.encodePrimitive(
                getVariable(variable), arithOp, leftHandSide, relOp, getVariable(rightHandSide));
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.core.ICSPSolver#addPrimitive(java.lang.String,
     * org.sat4j.csp.constraints.ArithmeticOperator, java.lang.String,
     * org.sat4j.csp.constraints.RelationalOperator, java.lang.String)
     */
    @Override
    public IConstr addPrimitive(String variable, ArithmeticOperator arithOp, String leftHandSide,
            RelationalOperator relOp, String rightHandSide) throws ContradictionException {
        return primitiveEncoder.encodePrimitive(
                getVariable(variable), arithOp, getVariable(leftHandSide), relOp,
                getVariable(rightHandSide));
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.core.ICSPSolver#addPrimitive(org.sat4j.csp.constraints.
     * ArithmeticOperator, java.lang.String, java.lang.String)
     */
    @Override
    public IConstr addPrimitive(ArithmeticOperator arithOp, String variable, String rightHandSide)
            throws ContradictionException {
        return primitiveEncoder.encodePrimitive(arithOp, getVariable(variable),
                getVariable(rightHandSide));
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.core.ICSPSolver#addPrimitive(java.lang.String,
     * org.sat4j.csp.constraints.SetBelongingOperator, org.sat4j.specs.IVec)
     */
    @Override
    public IConstr addPrimitive(String variable, SetBelongingOperator operator,
            IVec<BigInteger> values) throws ContradictionException {
        return primitiveEncoder.encodePrimitive(getVariable(variable), operator, values);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.core.ICSPSolver#addPrimitive(java.lang.String,
     * org.sat4j.csp.constraints.SetBelongingOperator, java.math.BigInteger,
     * java.math.BigInteger)
     */
    @Override
    public IConstr addPrimitive(String variable, SetBelongingOperator operator, BigInteger min,
            BigInteger max) throws ContradictionException {
        return primitiveEncoder.encodePrimitive(getVariable(variable), operator, min, max);
    }

    /**
     * Encodes an {@code intension} constraint as a variable.
     *
     * @param constr The constraint to encode.
     *
     * @return The variable representing the constraint.
     *
     * @throws ContradictionException If encoding the constraint results in a trivial
     *         inconsistency.
     */
    private IVariable encodeIntension(IIntensionConstraint constr) throws ContradictionException {
        var visitor = new DefaultIntensionConstraintVisitor();
        visitor.setSolver(this);
        constr.accept(visitor);
        return visitor.getEncodingVariable();
    }

    /**
     * Encodes a vector of {@code intension} constraints into variables.
     *
     * @param constrs The constraints to encode.
     *
     * @return The vector of the variables representing the constraints.
     *
     * @throws ContradictionException If encoding the constraints results in a trivial
     *         inconsistency.
     */
    private IVec<IVariable> encodeIntension(IVec<IIntensionConstraint> constrs)
            throws ContradictionException {
        var vec = new Vec<IVariable>(constrs.size());
        for (var it = constrs.iterator(); it.hasNext();) {
            vec.push(encodeIntension(it.next()));
        }
        return vec;
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.sat4j.csp.core.ICSPSolver#setMinMaxEncoder(org.sat4j.csp.constraints.encoder.
     * minmax.IMinMaxConstraintEncoder)
     */
    @Override
    public void setMinMaxEncoder(IMinMaxConstraintEncoder minMaxEncoder) {
        this.minMaxEncoder = minMaxEncoder;
        this.minMaxEncoder.setSolver(this);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.core.ICSPSolver#getMinMaxEncoder()
     */
    @Override
    public IMinMaxConstraintEncoder getMinMaxEncoder() {
        return minMaxEncoder;
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.core.ICSPSolver#addMinimum(org.sat4j.specs.IVec,
     * org.sat4j.csp.constraints.RelationalOperator, java.math.BigInteger)
     */
    @Override
    public IConstr addMinimum(IVec<String> variables, RelationalOperator operator,
            BigInteger value) throws ContradictionException {
        return minMaxEncoder.encodeMinimum(getVariables(variables), operator, value);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.core.ICSPSolver#addMinimum(org.sat4j.specs.IVec,
     * org.sat4j.csp.constraints.RelationalOperator, java.lang.String)
     */
    @Override
    public IConstr addMinimum(IVec<String> variables, RelationalOperator operator, String value)
            throws ContradictionException {
        return minMaxEncoder.encodeMinimum(getVariables(variables), operator, getVariable(value));
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.core.ICSPSolver#addMinimumIntension(org.sat4j.specs.IVec,
     * org.sat4j.csp.constraints.RelationalOperator, java.math.BigInteger)
     */
    @Override
    public IConstr addMinimumIntension(IVec<IIntensionConstraint> intensionConstraints,
            RelationalOperator operator, BigInteger value) throws ContradictionException {
        return minMaxEncoder.encodeMinimum(
                encodeIntension(intensionConstraints), operator, value);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.core.ICSPSolver#addMinimumIntension(org.sat4j.specs.IVec,
     * org.sat4j.csp.constraints.RelationalOperator, java.lang.String)
     */
    @Override
    public IConstr addMinimumIntension(IVec<IIntensionConstraint> intensionConstraints,
            RelationalOperator operator, String value) throws ContradictionException {
        return minMaxEncoder.encodeMinimum(
                encodeIntension(intensionConstraints), operator, getVariable(value));
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.core.ICSPSolver#addMaximum(org.sat4j.specs.IVec,
     * org.sat4j.csp.constraints.RelationalOperator, java.math.BigInteger)
     */
    @Override
    public IConstr addMaximum(IVec<String> variables, RelationalOperator operator,
            BigInteger value) throws ContradictionException {
        return minMaxEncoder.encodeMaximum(getVariables(variables), operator, value);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.core.ICSPSolver#addMaximum(org.sat4j.specs.IVec,
     * org.sat4j.csp.constraints.RelationalOperator, java.lang.String)
     */
    @Override
    public IConstr addMaximum(IVec<String> variables, RelationalOperator operator, String value)
            throws ContradictionException {
        return minMaxEncoder.encodeMaximum(getVariables(variables), operator, getVariable(value));
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.core.ICSPSolver#addMaximumIntension(org.sat4j.specs.IVec,
     * org.sat4j.csp.constraints.RelationalOperator, java.math.BigInteger)
     */
    @Override
    public IConstr addMaximumIntension(IVec<IIntensionConstraint> intensionConstraints,
            RelationalOperator operator, BigInteger value) throws ContradictionException {
        return minMaxEncoder.encodeMaximum(
                encodeIntension(intensionConstraints), operator, value);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.core.ICSPSolver#addMaximumIntension(org.sat4j.specs.IVec,
     * org.sat4j.csp.constraints.RelationalOperator, java.lang.String)
     */
    @Override
    public IConstr addMaximumIntension(IVec<IIntensionConstraint> intensionConstraints,
            RelationalOperator operator, String value) throws ContradictionException {
        return minMaxEncoder.encodeMaximum(
                encodeIntension(intensionConstraints), operator, getVariable(value));
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.sat4j.csp.core.ICSPSolver#setNoOverlapEncoder(org.sat4j.csp.constraints.encoder
     * .nooverlap.INoOverlapConstraintEncoder)
     */
    @Override
    public void setNoOverlapEncoder(INoOverlapConstraintEncoder noOverlapEncoder) {
        this.noOverlapEncoder = noOverlapEncoder;
        this.noOverlapEncoder.setSolver(this);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.core.ICSPSolver#getNoOverlapEncoder()
     */
    @Override
    public INoOverlapConstraintEncoder getNoOverlapEncoder() {
        return noOverlapEncoder;
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.core.ICSPSolver#addNoOverlap(org.sat4j.specs.IVec,
     * org.sat4j.specs.IVec)
     */
    @Override
    public IConstr addNoOverlap(IVec<String> variables, IVec<BigInteger> length)
            throws ContradictionException {
        return noOverlapEncoder.encodeNoOverlap(getVariables(variables), length);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.core.ICSPSolver#addNoOverlap(org.sat4j.specs.IVec,
     * org.sat4j.specs.IVec, boolean)
     */
    @Override
    public IConstr addNoOverlap(IVec<String> variables, IVec<BigInteger> length,
            boolean zeroIgnored) throws ContradictionException {
        return noOverlapEncoder.encodeNoOverlap(getVariables(variables), length, zeroIgnored);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.core.ICSPSolver#addNoOverlapVariableLength(org.sat4j.specs.IVec,
     * org.sat4j.specs.IVec)
     */
    @Override
    public IConstr addNoOverlapVariableLength(IVec<String> variables, IVec<String> length)
            throws ContradictionException {
        return noOverlapEncoder.encodeNoOverlapVariableLength(
                getVariables(variables), getVariables(length));
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.core.ICSPSolver#addNoOverlapVariableLength(org.sat4j.specs.IVec,
     * org.sat4j.specs.IVec, boolean)
     */
    @Override
    public IConstr addNoOverlapVariableLength(IVec<String> variables, IVec<String> length,
            boolean zeroIgnored) throws ContradictionException {
        return noOverlapEncoder.encodeNoOverlapVariableLength(
                getVariables(variables), getVariables(length), zeroIgnored);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.sat4j.csp.core.ICSPSolver#addMultiDimensionalNoOverlap(org.sat4j.specs.IVec,
     * org.sat4j.specs.IVec)
     */
    @Override
    public IConstr addMultiDimensionalNoOverlap(IVec<IVec<String>> variables,
            IVec<IVec<BigInteger>> length) throws ContradictionException {
        return noOverlapEncoder.encodeMultiDimensionalNoOverlap(
                getVariableMatrix(variables), length);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.sat4j.csp.core.ICSPSolver#addMultiDimensionalNoOverlap(org.sat4j.specs.IVec,
     * org.sat4j.specs.IVec, boolean)
     */
    @Override
    public IConstr addMultiDimensionalNoOverlap(IVec<IVec<String>> variables,
            IVec<IVec<BigInteger>> length, boolean zeroIgnored) throws ContradictionException {
        return noOverlapEncoder.encodeMultiDimensionalNoOverlap(
                getVariableMatrix(variables), length, zeroIgnored);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.sat4j.csp.core.ICSPSolver#addMultiDimensionalNoOverlapVariableLength(org.sat4j.
     * specs.IVec, org.sat4j.specs.IVec)
     */
    @Override
    public IConstr addMultiDimensionalNoOverlapVariableLength(IVec<IVec<String>> variables,
            IVec<IVec<String>> length) throws ContradictionException {
        return noOverlapEncoder.encodeMultiDimensionalNoOverlapVariableLength(
                getVariableMatrix(variables), getVariableMatrix(length));
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.sat4j.csp.core.ICSPSolver#addMultiDimensionalNoOverlapVariableLength(org.sat4j.
     * specs.IVec, org.sat4j.specs.IVec, boolean)
     */
    @Override
    public IConstr addMultiDimensionalNoOverlapVariableLength(IVec<IVec<String>> variables,
            IVec<IVec<String>> length, boolean zeroIgnored) throws ContradictionException {
        return noOverlapEncoder.encodeMultiDimensionalNoOverlapVariableLength(
                getVariableMatrix(variables), getVariableMatrix(length), zeroIgnored);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.sat4j.csp.core.ICSPSolver#setNValuesEncoder(org.sat4j.csp.constraints.encoder.
     * nvalues.INValuesConstraintEncoder)
     */
    @Override
    public void setNValuesEncoder(INValuesConstraintEncoder nValuesEncoder) {
        this.nValuesEncoder = nValuesEncoder;
        this.nValuesEncoder.setSolver(this);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.core.ICSPSolver#getNValuesEncoder()
     */
    @Override
    public INValuesConstraintEncoder getNValuesEncoder() {
        return nValuesEncoder;
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.core.ICSPSolver#addNValues(org.sat4j.specs.IVec,
     * org.sat4j.csp.constraints.RelationalOperator, java.math.BigInteger)
     */
    @Override
    public IConstr addNValues(IVec<String> variables, RelationalOperator operator, BigInteger nb)
            throws ContradictionException {
        return nValuesEncoder.encodeNValues(getVariables(variables), operator, nb);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.core.ICSPSolver#addNValuesExcept(org.sat4j.specs.IVec,
     * org.sat4j.csp.constraints.RelationalOperator, java.math.BigInteger,
     * org.sat4j.specs.IVec)
     */
    @Override
    public IConstr addNValuesExcept(IVec<String> variables, RelationalOperator operator,
            BigInteger nb, IVec<BigInteger> except) throws ContradictionException {
        return nValuesEncoder.encodeNValuesExcept(getVariables(variables), operator, nb, except);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.core.ICSPSolver#addNValues(org.sat4j.specs.IVec,
     * org.sat4j.csp.constraints.RelationalOperator, java.lang.String)
     */
    @Override
    public IConstr addNValues(IVec<String> variables, RelationalOperator operator, String nb)
            throws ContradictionException {
        return nValuesEncoder.encodeNValues(getVariables(variables), operator, getVariable(nb));
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.core.ICSPSolver#addNValuesExcept(org.sat4j.specs.IVec,
     * org.sat4j.csp.constraints.RelationalOperator, java.lang.String,
     * org.sat4j.specs.IVec)
     */
    @Override
    public IConstr addNValuesExcept(IVec<String> variables, RelationalOperator operator, String nb,
            IVec<BigInteger> except) throws ContradictionException {
        return nValuesEncoder.encodeNValuesExcept(
                getVariables(variables), operator, getVariable(nb), except);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.core.ICSPSolver#addNValuesIntension(org.sat4j.specs.IVec,
     * org.sat4j.csp.constraints.RelationalOperator, java.math.BigInteger)
     */
    @Override
    public IConstr addNValuesIntension(IVec<IIntensionConstraint> expressions,
            RelationalOperator operator, BigInteger nb) throws ContradictionException {
        return nValuesEncoder.encodeNValues(encodeIntension(expressions), operator, nb);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.core.ICSPSolver#addNValuesIntension(org.sat4j.specs.IVec,
     * org.sat4j.csp.constraints.RelationalOperator, java.lang.String)
     */
    @Override
    public IConstr addNValuesIntension(IVec<IIntensionConstraint> expressions,
            RelationalOperator operator, String nb) throws ContradictionException {
        return nValuesEncoder.encodeNValues(
                encodeIntension(expressions), operator, getVariable(nb));
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.sat4j.csp.core.ICSPSolver#setOrderedEncoder(org.sat4j.csp.constraints.encoder.
     * order.IOrderedConstraintEncoder)
     */
    @Override
    public void setOrderedEncoder(IOrderedConstraintEncoder orderedEncoder) {
        this.orderedEncoder = orderedEncoder;
        this.orderedEncoder.setSolver(this);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.core.ICSPSolver#getOrderedEncoder()
     */
    @Override
    public IOrderedConstraintEncoder getOrderedEncoder() {
        return orderedEncoder;
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.core.ICSPSolver#addOrdered(org.sat4j.specs.IVec,
     * org.sat4j.csp.constraints.RelationalOperator)
     */
    @Override
    public IConstr addOrdered(IVec<String> variables, RelationalOperator operator)
            throws ContradictionException {
        return orderedEncoder.encodeOrdered(getVariables(variables), operator);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.sat4j.csp.core.ICSPSolver#addOrderedWithConstantLength(org.sat4j.specs.IVec,
     * org.sat4j.specs.IVec, org.sat4j.csp.constraints.RelationalOperator)
     */
    @Override
    public IConstr addOrderedWithConstantLength(IVec<String> variables, IVec<BigInteger> lengths,
            RelationalOperator operator) throws ContradictionException {
        return orderedEncoder.encodeOrderedWithConstantLength(
                getVariables(variables), lengths, operator);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.sat4j.csp.core.ICSPSolver#addOrderedWithVariableLength(org.sat4j.specs.IVec,
     * org.sat4j.specs.IVec, org.sat4j.csp.constraints.RelationalOperator)
     */
    @Override
    public IConstr addOrderedWithVariableLength(IVec<String> variables, IVec<String> lengths,
            RelationalOperator operator) throws ContradictionException {
        return orderedEncoder.encodeOrderedWithVariableLength(
                getVariables(variables), getVariables(lengths), operator);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.core.ICSPSolver#addAllEqual(org.sat4j.specs.IVec)
     */
    @Override
    public IConstr addAllEqual(IVec<String> variables) throws ContradictionException {
        return orderedEncoder.encodeAllEqual(getVariables(variables));
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.core.ICSPSolver#addAllEqualIntension(org.sat4j.specs.IVec)
     */
    @Override
    public IConstr addAllEqualIntension(IVec<IIntensionConstraint> expressions)
            throws ContradictionException {
        return orderedEncoder.encodeAllEqual(encodeIntension(expressions));
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.core.ICSPSolver#addNotAllEqual(org.sat4j.specs.IVec)
     */
    @Override
    public IConstr addNotAllEqual(IVec<String> variables) throws ContradictionException {
        return orderedEncoder.encodeNotAllEqual(getVariables(variables));
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.core.ICSPSolver#addLex(org.sat4j.specs.IVec,
     * org.sat4j.csp.constraints.RelationalOperator)
     */
    @Override
    public IConstr addLex(IVec<IVec<String>> tuples, RelationalOperator operator)
            throws ContradictionException {
        return orderedEncoder.encodeLex(getVariableMatrix(tuples), operator);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.core.ICSPSolver#addLexMatrix(org.sat4j.specs.IVec,
     * org.sat4j.csp.constraints.RelationalOperator)
     */
    @Override
    public IConstr addLexMatrix(IVec<IVec<String>> matrix, RelationalOperator operator)
            throws ContradictionException {
        return orderedEncoder.encodeLexMatrix(getVariableMatrix(matrix), operator);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.sat4j.csp.core.ICSPSolver#setSumEncoder(org.sat4j.csp.constraints.encoder.sum.
     * ISumConstraintEncoder)
     */
    @Override
    public void setSumEncoder(ISumConstraintEncoder sumEncoder) {
        this.sumEncoder = sumEncoder;
        this.sumEncoder.setSolver(this);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.core.ICSPSolver#getSumEncoder()
     */
    @Override
    public ISumConstraintEncoder getSumEncoder() {
        return sumEncoder;
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.core.ICSPSolver#addSum(org.sat4j.specs.IVec,
     * org.sat4j.csp.constraints.RelationalOperator, java.math.BigInteger)
     */
    @Override
    public IConstr addSum(IVec<String> variables, RelationalOperator operator, BigInteger value)
            throws ContradictionException {
        return sumEncoder.encodeSum(getVariables(variables), operator, value);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.core.ICSPSolver#addSum(org.sat4j.specs.IVec,
     * org.sat4j.specs.IVec, org.sat4j.csp.constraints.RelationalOperator,
     * java.math.BigInteger)
     */
    @Override
    public IConstr addSum(IVec<String> variables, IVec<BigInteger> coefficients,
            RelationalOperator operator, BigInteger value) throws ContradictionException {
        return sumEncoder.encodeSum(getVariables(variables), coefficients, operator, value);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.core.ICSPSolver#addSum(org.sat4j.specs.IVec,
     * org.sat4j.csp.constraints.RelationalOperator, java.lang.String)
     */
    @Override
    public IConstr addSum(IVec<String> variables, RelationalOperator operator, String rightVariable)
            throws ContradictionException {
        return sumEncoder.encodeSum(
                getVariables(variables), operator, getVariable(rightVariable));
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.core.ICSPSolver#addSum(org.sat4j.specs.IVec,
     * org.sat4j.specs.IVec, org.sat4j.csp.constraints.RelationalOperator,
     * java.lang.String)
     */
    @Override
    public IConstr addSum(IVec<String> variables, IVec<BigInteger> coefficients,
            RelationalOperator operator, String rightVariable) throws ContradictionException {
        return sumEncoder.encodeSum(
                getVariables(variables), coefficients, operator, getVariable(rightVariable));
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.core.ICSPSolver#addSumIntension(org.sat4j.specs.IVec,
     * org.sat4j.csp.constraints.RelationalOperator, java.math.BigInteger)
     */
    @Override
    public IConstr addSumIntension(IVec<IIntensionConstraint> intensionConstraints,
            RelationalOperator operator, BigInteger value) throws ContradictionException {
        return sumEncoder.encodeSum(encodeIntension(intensionConstraints), operator, value);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.core.ICSPSolver#addSumIntension(org.sat4j.specs.IVec,
     * org.sat4j.specs.IVec, org.sat4j.csp.constraints.RelationalOperator,
     * java.math.BigInteger)
     */
    @Override
    public IConstr addSumIntension(IVec<IIntensionConstraint> intensionConstraints,
            IVec<BigInteger> coefficients, RelationalOperator operator, BigInteger value)
            throws ContradictionException {
        return sumEncoder.encodeSum(
                encodeIntension(intensionConstraints), coefficients, operator, value);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.core.ICSPSolver#addSumIntension(org.sat4j.specs.IVec,
     * org.sat4j.csp.constraints.RelationalOperator, java.lang.String)
     */
    @Override
    public IConstr addSumIntension(IVec<IIntensionConstraint> intensionConstraints,
            RelationalOperator operator, String rightVariable) throws ContradictionException {
        return sumEncoder.encodeSum(
                encodeIntension(intensionConstraints), operator, getVariable(rightVariable));
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.core.ICSPSolver#addSumIntension(org.sat4j.specs.IVec,
     * org.sat4j.specs.IVec, org.sat4j.csp.constraints.RelationalOperator,
     * java.lang.String)
     */
    @Override
    public IConstr addSumIntension(IVec<IIntensionConstraint> intensionConstraints,
            IVec<BigInteger> coefficients, RelationalOperator operator, String rightVariable)
            throws ContradictionException {
        return sumEncoder.encodeSum(encodeIntension(intensionConstraints),
                coefficients, operator, getVariable(rightVariable));
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.sat4j.csp.core.ICSPSolver#addSumWithVariableCoefficients(org.sat4j.specs.IVec,
     * org.sat4j.specs.IVec, org.sat4j.csp.constraints.RelationalOperator,
     * java.math.BigInteger)
     */
    @Override
    public IConstr addSumWithVariableCoefficients(IVec<String> variables, IVec<String> coefficients,
            RelationalOperator operator, BigInteger value) throws ContradictionException {
        var multipliedVariables = multiplicationEncoder.encodeVariableMultiplications(
                getVariables(variables), getVariables(coefficients));
        return sumEncoder.encodeSum(multipliedVariables, operator, value);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.sat4j.csp.core.ICSPSolver#addSumWithVariableCoefficients(org.sat4j.specs.IVec,
     * org.sat4j.specs.IVec, org.sat4j.csp.constraints.RelationalOperator,
     * java.lang.String)
     */
    @Override
    public IConstr addSumWithVariableCoefficients(IVec<String> variables, IVec<String> coefficients,
            RelationalOperator operator, String rightVariable) throws ContradictionException {
        var multipliedVariables = multiplicationEncoder.encodeVariableMultiplications(
                getVariables(variables), getVariables(coefficients));
        return sumEncoder.encodeSum(multipliedVariables, operator, getVariable(rightVariable));
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.sat4j.csp.core.ICSPSolver#addSumIntensionWithVariableCoefficients(org.sat4j.
     * specs.IVec, org.sat4j.specs.IVec, org.sat4j.csp.constraints.RelationalOperator,
     * java.math.BigInteger)
     */
    @Override
    public IConstr addSumIntensionWithVariableCoefficients(
            IVec<IIntensionConstraint> intensionConstraints, IVec<String> coefficients,
            RelationalOperator operator, BigInteger value) throws ContradictionException {
        var variables = encodeIntension(intensionConstraints);
        var multipliedVariables = multiplicationEncoder.encodeVariableMultiplications(variables,
                getVariables(coefficients));
        return sumEncoder.encodeSum(multipliedVariables, operator, value);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.sat4j.csp.core.ICSPSolver#addSumIntensionWithVariableCoefficients(org.sat4j.
     * specs.IVec, org.sat4j.specs.IVec, org.sat4j.csp.constraints.RelationalOperator,
     * java.lang.String)
     */
    @Override
    public IConstr addSumIntensionWithVariableCoefficients(
            IVec<IIntensionConstraint> intensionConstraints, IVec<String> coefficients,
            RelationalOperator operator, String rightVariable) throws ContradictionException {
        var variables = encodeIntension(intensionConstraints);
        var multipliedVariables = multiplicationEncoder.encodeVariableMultiplications(variables,
                getVariables(coefficients));
        return sumEncoder.encodeSum(multipliedVariables, operator, getVariable(rightVariable));
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.core.ICSPSolver#minimizeVariable(java.lang.String)
     */
    @Override
    public void minimizeVariable(String variable) {
        minimizeSum(Vec.of(variable));
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.core.ICSPSolver#maximizeVariable(java.lang.String)
     */
    @Override
    public void maximizeVariable(String variable) {
        maximizeSum(Vec.of(variable));
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.core.ICSPSolver#minimizeExpression(org.sat4j.csp.constraints.
     * intension.IIntensionConstraint)
     */
    @Override
    public void minimizeExpression(IIntensionConstraint expression) {
        minimizeExpressionSum(Vec.of(expression));
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.core.ICSPSolver#maximizeExpression(org.sat4j.csp.constraints.
     * intension.IIntensionConstraint)
     */
    @Override
    public void maximizeExpression(IIntensionConstraint expression) {
        maximizeExpressionSum(Vec.of(expression));
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.core.ICSPSolver#minimizeSum(org.sat4j.specs.IVec)
     */
    @Override
    public void minimizeSum(IVec<String> variables) {
        minimizeSum(variables, new Vec<>(variables.size(), BigInteger.ONE));
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.core.ICSPSolver#minimizeSum(org.sat4j.specs.IVec,
     * org.sat4j.specs.IVec)
     */
    @Override
    public void minimizeSum(IVec<String> variables, IVec<BigInteger> coefficients) {
        var objectiveFunction = sumEncoder.encodeAsObjectiveFunction(
                getVariables(variables), coefficients);
        setObjectiveFunction(objectiveFunction);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.core.ICSPSolver#minimizeExpressionSum(org.sat4j.specs.IVec)
     */
    @Override
    public void minimizeExpressionSum(IVec<IIntensionConstraint> expressions) {
        minimizeExpressionSum(expressions, new Vec<>(expressions.size(), BigInteger.ONE));
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.core.ICSPSolver#minimizeExpressionSum(org.sat4j.specs.IVec,
     * org.sat4j.specs.IVec)
     */
    @Override
    public void minimizeExpressionSum(IVec<IIntensionConstraint> expressions,
            IVec<BigInteger> coefficients) {
        try {
            var objectiveFunction = sumEncoder.encodeAsObjectiveFunction(
                    encodeIntension(expressions), coefficients);
            setObjectiveFunction(objectiveFunction);
        } catch (ContradictionException e) {
            throw new UncheckedContradictionException(e);
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.core.ICSPSolver#maximizeSum(org.sat4j.specs.IVec)
     */
    @Override
    public void maximizeSum(IVec<String> variables) {
        maximizeSum(variables, new Vec<>(variables.size(), BigInteger.ONE));
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.core.ICSPSolver#maximizeSum(org.sat4j.specs.IVec,
     * org.sat4j.specs.IVec)
     */
    @Override
    public void maximizeSum(IVec<String> variables, IVec<BigInteger> coefficients) {
        var objectiveFunction = sumEncoder.encodeAsObjectiveFunction(
                getVariables(variables), coefficients);
        objectiveFunction = objectiveFunction.multiply(BigInteger.ONE.negate());
        objectiveFunction.setCorrectionFactor(BigInteger.ONE.negate());
        setObjectiveFunction(objectiveFunction);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.core.ICSPSolver#maximizeExpressionSum(org.sat4j.specs.IVec)
     */
    @Override
    public void maximizeExpressionSum(IVec<IIntensionConstraint> expressions) {
        maximizeExpressionSum(expressions, new Vec<>(expressions.size(), BigInteger.ONE));
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.core.ICSPSolver#maximizeExpressionSum(org.sat4j.specs.IVec,
     * org.sat4j.specs.IVec)
     */
    @Override
    public void maximizeExpressionSum(IVec<IIntensionConstraint> expressions,
            IVec<BigInteger> coefficients) {
        try {
            var objectiveFunction = sumEncoder.encodeAsObjectiveFunction(
                    encodeIntension(expressions), coefficients);
            objectiveFunction = objectiveFunction.multiply(BigInteger.ONE.negate());
            objectiveFunction.setCorrectionFactor(BigInteger.ONE.negate());
            setObjectiveFunction(objectiveFunction);
        } catch (ContradictionException e) {
            throw new UncheckedContradictionException(e);
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.core.ICSPSolver#minimizeProduct(org.sat4j.specs.IVec)
     */
    @Override
    public void minimizeProduct(IVec<String> variables) {
        try {
            var product = multiplicationEncoder.encodeMultiplication(getVariables(variables));
            var objectiveFunction = sumEncoder.encodeAsObjectiveFunction(
                    Vec.of(product), Vec.of(BigInteger.ONE));
            setObjectiveFunction(objectiveFunction);
        } catch (ContradictionException e) {
            throw new UncheckedContradictionException(e);
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.core.ICSPSolver#minimizeProduct(org.sat4j.specs.IVec,
     * org.sat4j.specs.IVec)
     */
    @Override
    public void minimizeProduct(IVec<String> variables, IVec<BigInteger> coefficients) {
        try {
            var multipliedVars = encodeConstantMultiplications(
                    getVariables(variables), coefficients);
            var product = multiplicationEncoder.encodeMultiplication(multipliedVars);
            var objectiveFunction = sumEncoder.encodeAsObjectiveFunction(
                    Vec.of(product), Vec.of(BigInteger.ONE));
            setObjectiveFunction(objectiveFunction);
        } catch (ContradictionException e) {
            throw new UncheckedContradictionException(e);
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.core.ICSPSolver#minimizeExpressionProduct(org.sat4j.specs.IVec)
     */
    @Override
    public void minimizeExpressionProduct(IVec<IIntensionConstraint> expressions) {
        try {
            var variables = encodeIntension(expressions);
            var product = multiplicationEncoder.encodeMultiplication(variables);
            var objectiveFunction = sumEncoder.encodeAsObjectiveFunction(
                    Vec.of(product), Vec.of(BigInteger.ONE));
            setObjectiveFunction(objectiveFunction);
        } catch (ContradictionException e) {
            throw new UncheckedContradictionException(e);
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.core.ICSPSolver#minimizeExpressionProduct(org.sat4j.specs.IVec,
     * org.sat4j.specs.IVec)
     */
    @Override
    public void minimizeExpressionProduct(IVec<IIntensionConstraint> expressions,
            IVec<BigInteger> coefficients) {
        try {
            var variables = encodeIntension(expressions);
            var multipliedVars = encodeConstantMultiplications(variables, coefficients);
            var product = multiplicationEncoder.encodeMultiplication(multipliedVars);
            var objectiveFunction = sumEncoder.encodeAsObjectiveFunction(
                    Vec.of(product), Vec.of(BigInteger.ONE));
            setObjectiveFunction(objectiveFunction);
        } catch (ContradictionException e) {
            throw new UncheckedContradictionException(e);
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.core.ICSPSolver#maximizeProduct(org.sat4j.specs.IVec)
     */
    @Override
    public void maximizeProduct(IVec<String> variables) {
        try {
            var product = multiplicationEncoder.encodeMultiplication(getVariables(variables));
            var objectiveFunction = sumEncoder.encodeAsObjectiveFunction(
                    Vec.of(product), Vec.of(BigInteger.ONE));
            objectiveFunction = objectiveFunction.multiply(BigInteger.ONE.negate());
            objectiveFunction.setCorrectionFactor(BigInteger.ONE.negate());
            setObjectiveFunction(objectiveFunction);
        } catch (ContradictionException e) {
            throw new UncheckedContradictionException(e);
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.core.ICSPSolver#maximizeProduct(org.sat4j.specs.IVec,
     * org.sat4j.specs.IVec)
     */
    @Override
    public void maximizeProduct(IVec<String> variables, IVec<BigInteger> coefficients) {
        try {
            var multipliedVars = encodeConstantMultiplications(
                    getVariables(variables), coefficients);
            var product = multiplicationEncoder.encodeMultiplication(multipliedVars);
            var objectiveFunction = sumEncoder.encodeAsObjectiveFunction(
                    Vec.of(product), Vec.of(BigInteger.ONE));
            objectiveFunction = objectiveFunction.multiply(BigInteger.ONE.negate());
            objectiveFunction.setCorrectionFactor(BigInteger.ONE.negate());
            setObjectiveFunction(objectiveFunction);
        } catch (ContradictionException e) {
            throw new UncheckedContradictionException(e);
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.core.ICSPSolver#maximizeExpressionProduct(org.sat4j.specs.IVec)
     */
    @Override
    public void maximizeExpressionProduct(IVec<IIntensionConstraint> expressions) {
        try {
            var variables = encodeIntension(expressions);
            var product = multiplicationEncoder.encodeMultiplication(variables);
            var objectiveFunction = sumEncoder.encodeAsObjectiveFunction(
                    Vec.of(product), Vec.of(BigInteger.ONE));
            objectiveFunction = objectiveFunction.multiply(BigInteger.ONE.negate());
            objectiveFunction.setCorrectionFactor(BigInteger.ONE.negate());
            setObjectiveFunction(objectiveFunction);
        } catch (ContradictionException e) {
            throw new UncheckedContradictionException(e);
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.core.ICSPSolver#maximizeExpressionProduct(org.sat4j.specs.IVec,
     * org.sat4j.specs.IVec)
     */
    @Override
    public void maximizeExpressionProduct(IVec<IIntensionConstraint> expressions,
            IVec<BigInteger> coefficients) {
        try {
            var variables = encodeIntension(expressions);
            var multipliedVars = encodeConstantMultiplications(variables, coefficients);
            var product = multiplicationEncoder.encodeMultiplication(multipliedVars);
            var objectiveFunction = sumEncoder.encodeAsObjectiveFunction(
                    Vec.of(product), Vec.of(BigInteger.ONE));
            objectiveFunction = objectiveFunction.multiply(BigInteger.ONE.negate());
            objectiveFunction.setCorrectionFactor(BigInteger.ONE.negate());
            setObjectiveFunction(objectiveFunction);
        } catch (ContradictionException e) {
            throw new UncheckedContradictionException(e);
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.core.ICSPSolver#minimizeMinimum(org.sat4j.specs.IVec)
     */
    @Override
    public void minimizeMinimum(IVec<String> variables) {
        try {
            var min = intensionEncoder.encodeMinimum(getVariables(variables));
            var objectiveFunction = sumEncoder.encodeAsObjectiveFunction(
                    Vec.of(min), Vec.of(BigInteger.ONE));
            setObjectiveFunction(objectiveFunction);
        } catch (ContradictionException e) {
            throw new UncheckedContradictionException(e);
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.core.ICSPSolver#minimizeMinimum(org.sat4j.specs.IVec,
     * org.sat4j.specs.IVec)
     */
    @Override
    public void minimizeMinimum(IVec<String> variables, IVec<BigInteger> coefficients) {
        try {
            var multipliedVars = encodeConstantMultiplications(
                    getVariables(variables), coefficients);
            var min = intensionEncoder.encodeMinimum(multipliedVars);
            var objectiveFunction = sumEncoder.encodeAsObjectiveFunction(
                    Vec.of(min), Vec.of(BigInteger.ONE));
            setObjectiveFunction(objectiveFunction);
        } catch (ContradictionException e) {
            throw new UncheckedContradictionException(e);
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.core.ICSPSolver#minimizeExpressionMinimum(org.sat4j.specs.IVec)
     */
    @Override
    public void minimizeExpressionMinimum(IVec<IIntensionConstraint> expressions) {
        try {
            var variables = encodeIntension(expressions);
            var min = intensionEncoder.encodeMinimum(variables);
            var objectiveFunction = sumEncoder.encodeAsObjectiveFunction(
                    Vec.of(min), Vec.of(BigInteger.ONE));
            setObjectiveFunction(objectiveFunction);
        } catch (ContradictionException e) {
            throw new UncheckedContradictionException(e);
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.core.ICSPSolver#minimizeExpressionMinimum(org.sat4j.specs.IVec,
     * org.sat4j.specs.IVec)
     */
    @Override
    public void minimizeExpressionMinimum(IVec<IIntensionConstraint> expressions,
            IVec<BigInteger> coefficients) {
        try {
            var variables = encodeIntension(expressions);
            var multipliedVars = encodeConstantMultiplications(variables, coefficients);
            var min = intensionEncoder.encodeMinimum(multipliedVars);
            var objectiveFunction = sumEncoder.encodeAsObjectiveFunction(
                    Vec.of(min), Vec.of(BigInteger.ONE));
            setObjectiveFunction(objectiveFunction);
        } catch (ContradictionException e) {
            throw new UncheckedContradictionException(e);
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.core.ICSPSolver#maximizeMinimum(org.sat4j.specs.IVec)
     */
    @Override
    public void maximizeMinimum(IVec<String> variables) {
        try {
            var min = intensionEncoder.encodeMinimum(getVariables(variables));
            var objectiveFunction = sumEncoder.encodeAsObjectiveFunction(
                    Vec.of(min), Vec.of(BigInteger.ONE));
            objectiveFunction = objectiveFunction.multiply(BigInteger.ONE.negate());
            objectiveFunction.setCorrectionFactor(BigInteger.ONE.negate());
            setObjectiveFunction(objectiveFunction);
        } catch (ContradictionException e) {
            throw new UncheckedContradictionException(e);
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.core.ICSPSolver#maximizeMinimum(org.sat4j.specs.IVec,
     * org.sat4j.specs.IVec)
     */
    @Override
    public void maximizeMinimum(IVec<String> variables, IVec<BigInteger> coefficients) {
        try {
            var multipliedVars = encodeConstantMultiplications(
                    getVariables(variables), coefficients);
            var min = intensionEncoder.encodeMinimum(multipliedVars);
            var objectiveFunction = sumEncoder.encodeAsObjectiveFunction(
                    Vec.of(min), Vec.of(BigInteger.ONE));
            objectiveFunction = objectiveFunction.multiply(BigInteger.ONE.negate());
            objectiveFunction.setCorrectionFactor(BigInteger.ONE.negate());
            setObjectiveFunction(objectiveFunction);
        } catch (ContradictionException e) {
            throw new UncheckedContradictionException(e);
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.core.ICSPSolver#maximizeExpressionMinimum(org.sat4j.specs.IVec)
     */
    @Override
    public void maximizeExpressionMinimum(IVec<IIntensionConstraint> expressions) {
        try {
            var variables = encodeIntension(expressions);
            var min = intensionEncoder.encodeMinimum(variables);
            var objectiveFunction = sumEncoder.encodeAsObjectiveFunction(
                    Vec.of(min), Vec.of(BigInteger.ONE));
            objectiveFunction = objectiveFunction.multiply(BigInteger.ONE.negate());
            objectiveFunction.setCorrectionFactor(BigInteger.ONE.negate());
            setObjectiveFunction(objectiveFunction);
        } catch (ContradictionException e) {
            throw new UncheckedContradictionException(e);
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.core.ICSPSolver#maximizeExpressionMinimum(org.sat4j.specs.IVec,
     * org.sat4j.specs.IVec)
     */
    @Override
    public void maximizeExpressionMinimum(IVec<IIntensionConstraint> expressions,
            IVec<BigInteger> coefficients) {
        try {
            var variables = encodeIntension(expressions);
            var multipliedVars = encodeConstantMultiplications(variables, coefficients);
            var min = intensionEncoder.encodeMinimum(multipliedVars);
            var objectiveFunction = sumEncoder.encodeAsObjectiveFunction(
                    Vec.of(min), Vec.of(BigInteger.ONE));
            objectiveFunction = objectiveFunction.multiply(BigInteger.ONE.negate());
            objectiveFunction.setCorrectionFactor(BigInteger.ONE.negate());
            setObjectiveFunction(objectiveFunction);
        } catch (ContradictionException e) {
            throw new UncheckedContradictionException(e);
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.core.ICSPSolver#minimizeMaximum(org.sat4j.specs.IVec)
     */
    @Override
    public void minimizeMaximum(IVec<String> variables) {
        try {
            var max = intensionEncoder.encodeMaximum(getVariables(variables));
            var objectiveFunction = sumEncoder.encodeAsObjectiveFunction(
                    Vec.of(max), Vec.of(BigInteger.ONE));
            setObjectiveFunction(objectiveFunction);
        } catch (ContradictionException e) {
            throw new UncheckedContradictionException(e);
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.core.ICSPSolver#minimizeMaximum(org.sat4j.specs.IVec,
     * org.sat4j.specs.IVec)
     */
    @Override
    public void minimizeMaximum(IVec<String> variables, IVec<BigInteger> coefficients) {
        try {
            var multipliedVars = encodeConstantMultiplications(
                    getVariables(variables), coefficients);
            var max = intensionEncoder.encodeMaximum(multipliedVars);
            var objectiveFunction = sumEncoder.encodeAsObjectiveFunction(
                    Vec.of(max), Vec.of(BigInteger.ONE));
            setObjectiveFunction(objectiveFunction);
        } catch (ContradictionException e) {
            throw new UncheckedContradictionException(e);
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.core.ICSPSolver#minimizeExpressionMaximum(org.sat4j.specs.IVec)
     */
    @Override
    public void minimizeExpressionMaximum(IVec<IIntensionConstraint> expressions) {
        try {
            var variables = encodeIntension(expressions);
            var max = intensionEncoder.encodeMaximum(variables);
            var objectiveFunction = sumEncoder.encodeAsObjectiveFunction(
                    Vec.of(max), Vec.of(BigInteger.ONE));
            setObjectiveFunction(objectiveFunction);
        } catch (ContradictionException e) {
            throw new UncheckedContradictionException(e);
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.core.ICSPSolver#minimizeExpressionMaximum(org.sat4j.specs.IVec,
     * org.sat4j.specs.IVec)
     */
    @Override
    public void minimizeExpressionMaximum(IVec<IIntensionConstraint> expressions,
            IVec<BigInteger> coefficients) {
        try {
            var variables = encodeIntension(expressions);
            var multipliedVars = encodeConstantMultiplications(variables, coefficients);
            var max = intensionEncoder.encodeMaximum(multipliedVars);
            var objectiveFunction = sumEncoder.encodeAsObjectiveFunction(
                    Vec.of(max), Vec.of(BigInteger.ONE));
            setObjectiveFunction(objectiveFunction);
        } catch (ContradictionException e) {
            throw new UncheckedContradictionException(e);
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.core.ICSPSolver#maximizeMaximum(org.sat4j.specs.IVec)
     */
    @Override
    public void maximizeMaximum(IVec<String> variables) {
        try {
            var max = intensionEncoder.encodeMaximum(getVariables(variables));
            var objectiveFunction = sumEncoder.encodeAsObjectiveFunction(
                    Vec.of(max), Vec.of(BigInteger.ONE));
            objectiveFunction = objectiveFunction.multiply(BigInteger.ONE.negate());
            objectiveFunction.setCorrectionFactor(BigInteger.ONE.negate());
            setObjectiveFunction(objectiveFunction);
        } catch (ContradictionException e) {
            throw new UncheckedContradictionException(e);
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.core.ICSPSolver#maximizeMaximum(org.sat4j.specs.IVec,
     * org.sat4j.specs.IVec)
     */
    @Override
    public void maximizeMaximum(IVec<String> variables, IVec<BigInteger> coefficients) {
        try {
            var multipliedVars = encodeConstantMultiplications(
                    getVariables(variables), coefficients);
            var max = intensionEncoder.encodeMaximum(multipliedVars);
            var objectiveFunction = sumEncoder.encodeAsObjectiveFunction(
                    Vec.of(max), Vec.of(BigInteger.ONE));
            objectiveFunction = objectiveFunction.multiply(BigInteger.ONE.negate());
            objectiveFunction.setCorrectionFactor(BigInteger.ONE.negate());
            setObjectiveFunction(objectiveFunction);
        } catch (ContradictionException e) {
            throw new UncheckedContradictionException(e);
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.core.ICSPSolver#maximizeExpressionMaximum(org.sat4j.specs.IVec)
     */
    @Override
    public void maximizeExpressionMaximum(IVec<IIntensionConstraint> expressions) {
        try {
            var variables = encodeIntension(expressions);
            var max = intensionEncoder.encodeMaximum(variables);
            var objectiveFunction = sumEncoder.encodeAsObjectiveFunction(
                    Vec.of(max), Vec.of(BigInteger.ONE));
            objectiveFunction = objectiveFunction.multiply(BigInteger.ONE.negate());
            objectiveFunction.setCorrectionFactor(BigInteger.ONE.negate());
            setObjectiveFunction(objectiveFunction);
        } catch (ContradictionException e) {
            throw new UncheckedContradictionException(e);
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.core.ICSPSolver#maximizeExpressionMaximum(org.sat4j.specs.IVec,
     * org.sat4j.specs.IVec)
     */
    @Override
    public void maximizeExpressionMaximum(IVec<IIntensionConstraint> expressions,
            IVec<BigInteger> coefficients) {
        try {
            var variables = encodeIntension(expressions);
            var multipliedVars = encodeConstantMultiplications(variables, coefficients);
            var max = intensionEncoder.encodeMaximum(multipliedVars);
            var objectiveFunction = sumEncoder.encodeAsObjectiveFunction(
                    Vec.of(max), Vec.of(BigInteger.ONE));
            objectiveFunction = objectiveFunction.multiply(BigInteger.ONE.negate());
            objectiveFunction.setCorrectionFactor(BigInteger.ONE.negate());
            setObjectiveFunction(objectiveFunction);
        } catch (ContradictionException e) {
            throw new UncheckedContradictionException(e);
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.core.ICSPSolver#minimizeNValues(org.sat4j.specs.IVec)
     */
    @Override
    public void minimizeNValues(IVec<String> variables) {
        try {
            var nValues = nValuesEncoder.encodeNValues(getVariables(variables));
            var objectiveFunction = sumEncoder.encodeAsObjectiveFunction(
                    Vec.of(nValues), Vec.of(BigInteger.ONE));
            setObjectiveFunction(objectiveFunction);
        } catch (ContradictionException e) {
            throw new UncheckedContradictionException(e);
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.core.ICSPSolver#minimizeNValues(org.sat4j.specs.IVec,
     * org.sat4j.specs.IVec)
     */
    @Override
    public void minimizeNValues(IVec<String> variables, IVec<BigInteger> coefficients) {
        try {
            var multipliedVars = encodeConstantMultiplications(
                    getVariables(variables), coefficients);
            var nValues = nValuesEncoder.encodeNValues(multipliedVars);
            var objectiveFunction = sumEncoder.encodeAsObjectiveFunction(
                    Vec.of(nValues), Vec.of(BigInteger.ONE));
            setObjectiveFunction(objectiveFunction);
        } catch (ContradictionException e) {
            throw new UncheckedContradictionException(e);
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.core.ICSPSolver#minimizeExpressionNValues(org.sat4j.specs.IVec)
     */
    @Override
    public void minimizeExpressionNValues(IVec<IIntensionConstraint> expressions) {
        try {
            var variables = encodeIntension(expressions);
            var nValues = nValuesEncoder.encodeNValues(variables);
            var objectiveFunction = sumEncoder.encodeAsObjectiveFunction(
                    Vec.of(nValues), Vec.of(BigInteger.ONE));
            setObjectiveFunction(objectiveFunction);
        } catch (ContradictionException e) {
            throw new UncheckedContradictionException(e);
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.core.ICSPSolver#minimizeExpressionNValues(org.sat4j.specs.IVec,
     * org.sat4j.specs.IVec)
     */
    @Override
    public void minimizeExpressionNValues(IVec<IIntensionConstraint> expressions,
            IVec<BigInteger> coefficients) {
        try {
            var variables = encodeIntension(expressions);
            var multipliedVars = encodeConstantMultiplications(variables, coefficients);
            var nValues = nValuesEncoder.encodeNValues(multipliedVars);
            var objectiveFunction = sumEncoder.encodeAsObjectiveFunction(
                    Vec.of(nValues), Vec.of(BigInteger.ONE));
            setObjectiveFunction(objectiveFunction);
        } catch (ContradictionException e) {
            throw new UncheckedContradictionException(e);
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.core.ICSPSolver#maximizeNValues(org.sat4j.specs.IVec)
     */
    @Override
    public void maximizeNValues(IVec<String> variables) {
        try {
            var nValues = nValuesEncoder.encodeNValues(getVariables(variables));
            var objectiveFunction = sumEncoder.encodeAsObjectiveFunction(
                    Vec.of(nValues), Vec.of(BigInteger.ONE));
            objectiveFunction = objectiveFunction.multiply(BigInteger.ONE.negate());
            objectiveFunction.setCorrectionFactor(BigInteger.ONE.negate());
            setObjectiveFunction(objectiveFunction);
        } catch (ContradictionException e) {
            throw new UncheckedContradictionException(e);
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.core.ICSPSolver#maximizeNValues(org.sat4j.specs.IVec,
     * org.sat4j.specs.IVec)
     */
    @Override
    public void maximizeNValues(IVec<String> variables, IVec<BigInteger> coefficients) {
        try {
            var multipliedVars = encodeConstantMultiplications(
                    getVariables(variables), coefficients);
            var nValues = nValuesEncoder.encodeNValues(multipliedVars);
            var objectiveFunction = sumEncoder.encodeAsObjectiveFunction(
                    Vec.of(nValues), Vec.of(BigInteger.ONE));
            objectiveFunction = objectiveFunction.multiply(BigInteger.ONE.negate());
            objectiveFunction.setCorrectionFactor(BigInteger.ONE.negate());
            setObjectiveFunction(objectiveFunction);
        } catch (ContradictionException e) {
            throw new UncheckedContradictionException(e);
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.core.ICSPSolver#maximizeExpressionNValues(org.sat4j.specs.IVec)
     */
    @Override
    public void maximizeExpressionNValues(IVec<IIntensionConstraint> expressions) {
        try {
            var variables = encodeIntension(expressions);
            var nValues = nValuesEncoder.encodeNValues(variables);
            var objectiveFunction = sumEncoder.encodeAsObjectiveFunction(
                    Vec.of(nValues), Vec.of(BigInteger.ONE));
            objectiveFunction = objectiveFunction.multiply(BigInteger.ONE.negate());
            objectiveFunction.setCorrectionFactor(BigInteger.ONE.negate());
            setObjectiveFunction(objectiveFunction);
        } catch (ContradictionException e) {
            throw new UncheckedContradictionException(e);
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.core.ICSPSolver#maximizeExpressionNValues(org.sat4j.specs.IVec,
     * org.sat4j.specs.IVec)
     */
    @Override
    public void maximizeExpressionNValues(IVec<IIntensionConstraint> expressions,
            IVec<BigInteger> coefficients) {
        try {
            var variables = encodeIntension(expressions);
            var multipliedVars = encodeConstantMultiplications(variables, coefficients);
            var nValues = nValuesEncoder.encodeNValues(multipliedVars);
            var objectiveFunction = sumEncoder.encodeAsObjectiveFunction(
                    Vec.of(nValues), Vec.of(BigInteger.ONE));
            objectiveFunction = objectiveFunction.multiply(BigInteger.ONE.negate());
            objectiveFunction.setCorrectionFactor(BigInteger.ONE.negate());
            setObjectiveFunction(objectiveFunction);
        } catch (ContradictionException e) {
            throw new UncheckedContradictionException(e);
        }
    }

    /**
     * Encodes the pairwise multiplication between a variable and its coefficient.
     *
     * @param variables The variables to multiply.
     * @param coefficients The coefficients to multiply.
     *
     * @return The vector of multiplied variables.
     */
    private IVec<IVariable> encodeConstantMultiplications(IVec<IVariable> variables,
            IVec<BigInteger> coefficients) {
        var multipliedVariables = new Vec<IVariable>(variables.size());
        for (int i = 0; i < variables.size(); i++) {
            multipliedVariables.push(
                    new MultipliedVariableDecorator(variables.get(i), coefficients.get(i)));
        }
        return multipliedVariables;
    }



    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.core.ICSPSolver#isSatisfiable(java.util.Map)
     */
    @Override
    public boolean isSatisfiable(Map<String, BigInteger> assumps)
            throws ContradictionException, TimeoutException {
        var dimacsAssumps = new VecInt(assumps.size());

        for (var assignment : assumps.entrySet()) {
            var variable = getVariable(assignment.getKey());
            var value = assignment.getValue();
            var literal = variable.getLiteralForValue(value);

            if (literal.isPresent()) {
                dimacsAssumps.push(literal.getAsInt());
            } else {
                throw new ContradictionException(
                        value + " is not in the domain of " + assignment.getKey());
            }
        }

        return isSatisfiable(dimacsAssumps);
    }

}
