/**
 * Sat4j-CSP, a CSP solver based on the Sat4j platform.
 * Copyright (c) 2021-2022 - Exakis Nelite, Univ Artois & CNRS.
 * All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 * If not, see {@link http://www.gnu.org/licenses}.
 */

package org.sat4j.csp.utils;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Iterator;

import org.sat4j.specs.IVec;

/**
 * The ShiftedVec is a decorator for an {@link IVec} containing {@link BigInteger} that
 * allows to shift the values contained in this {@link IVec}.
 *
 * @author Thibault Falque
 * @author Romain Wallon
 *
 * @version 0.1.0
 */
public class ShiftedVec extends AbstractVecDecorator<BigInteger> {

    /**
     * The {@code serialVersionUID} of this {@link Serializable} class.
     */
    private static final long serialVersionUID = 1L;

    /**
     * The shift to apply to the values in the vector.
     */
    private final BigInteger shift;

    /**
     * Creates a new ShiftedVec.
     *
     * @param decorated The instance of {@link IVec} to decorate.
     * @param shift The shift to apply to the values in the vector.
     */
    public ShiftedVec(IVec<BigInteger> decorated, BigInteger shift) {
        super(decorated);
        this.shift = shift;
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.utils.AbstractVecDecorator#growTo(int, java.lang.Object)
     */
    @Override
    public void growTo(int newsize, BigInteger pad) {
        super.growTo(newsize, pad.subtract(shift));
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.utils.AbstractVecDecorator#push(java.lang.Object)
     */
    @Override
    public IVec<BigInteger> push(BigInteger elem) {
        return super.push(elem.subtract(shift));
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.utils.AbstractVecDecorator#unsafePush(java.lang.Object)
     */
    @Override
    public void unsafePush(BigInteger elem) {
        super.unsafePush(elem.subtract(shift));
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.utils.AbstractVecDecorator#insertFirst(java.lang.Object)
     */
    @Override
    public void insertFirst(BigInteger elem) {
        super.insertFirst(elem.subtract(shift));
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.sat4j.csp.utils.AbstractVecDecorator#insertFirstWithShifting(java.lang.Object)
     */
    @Override
    public void insertFirstWithShifting(BigInteger elem) {
        super.insertFirstWithShifting(elem.subtract(shift));
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.utils.AbstractVecDecorator#last()
     */
    @Override
    public BigInteger last() {
        return super.last().add(shift);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.utils.AbstractVecDecorator#get(int)
     */
    @Override
    public BigInteger get(int i) {
        return super.get(i).add(shift);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.utils.AbstractVecDecorator#set(int, java.lang.Object)
     */
    @Override
    public void set(int i, BigInteger o) {
        super.set(i, o.subtract(shift));
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.utils.AbstractVecDecorator#remove(java.lang.Object)
     */
    @Override
    public void remove(BigInteger elem) {
        super.remove(elem.subtract(shift));
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.utils.AbstractVecDecorator#removeFromLast(java.lang.Object)
     */
    @Override
    public void removeFromLast(BigInteger elem) {
        super.removeFromLast(elem.subtract(shift));
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.utils.AbstractVecDecorator#iterator()
     */
    @Override
    public Iterator<BigInteger> iterator() {
        return new Iterator<BigInteger>() {

            /**
             * The decorated iterator
             */
            private Iterator<BigInteger> it = ShiftedVec.super.iterator();

            /*
             * (non-Javadoc)
             *
             * @see java.util.Iterator#hasNext()
             */
            @Override
            public boolean hasNext() {
                return it.hasNext();
            }

            /*
             * (non-Javadoc)
             *
             * @see java.util.Iterator#next()
             */
            @Override
            public BigInteger next() {
                return it.next().add(shift);
            }

        };
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.utils.AbstractVecDecorator#contains(java.lang.Object)
     */
    @Override
    public boolean contains(BigInteger element) {
        return super.contains(element.subtract(shift));
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.utils.AbstractVecDecorator#indexOf(java.lang.Object)
     */
    @Override
    public int indexOf(BigInteger element) {
        return super.indexOf(element.subtract(shift));
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.utils.AbstractVecDecorator#clone()
     */
    @Override
    public IVec<BigInteger> clone() {
        return new ShiftedVec(super.clone(), shift);
    }

}
