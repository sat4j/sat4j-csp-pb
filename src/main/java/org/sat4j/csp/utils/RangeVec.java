/**
 * Sat4j-CSP, a CSP solver based on the Sat4j platform.
 * Copyright (c) 2021-2022 - Exakis Nelite, Univ Artois & CNRS.
 * All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 * If not, see {@link http://www.gnu.org/licenses}.
 */

package org.sat4j.csp.utils;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Comparator;
import java.util.Iterator;
import java.util.NoSuchElementException;

import org.sat4j.specs.IVec;

/**
 * The RangeVec is an implementation of {@link IVec} that contains a range of consecutive
 * {@link BigInteger}.
 * It is immutable by nature.
 *
 * @author Thibault Falque
 * @author Romain Wallon
 *
 * @version 0.1.0
 */
public final class RangeVec implements IVec<BigInteger> {

    /**
     * The {@code serialVersionUID} of this {@link Serializable} class.
     */
    private static final long serialVersionUID = 1L;

    /**
     * The minimum value of the range (inclusive).
     */
    private final BigInteger min;

    /**
     * The maximum value of the range (inclusive).
     */
    private final BigInteger max;

    /**
     * The size of the range, cached to avoid operations on {@link BigInteger}.
     */
    private final int size;

    /**
     * Creates a new RangeVec.
     *
     * @param min The minimum value of the range (inclusive).
     * @param max The maximum value of the range (inclusive).
     */
    private RangeVec(BigInteger min, BigInteger max) {
        this.min = min;
        this.max = max;
        this.size = max.subtract(min).intValue() + 1;
    }

    /**
     * Creates a new RangeVec, starting at {@code 0}.
     *
     * @param max The maximum value of the range (inclusive).
     *
     * @return The created range.
     */
    public static IVec<BigInteger> of(BigInteger max) {
        return new RangeVec(BigInteger.ZERO, max);
    }

    /**
     * Creates a new RangeVec.
     *
     * @param min The minimum value of the range (inclusive).
     * @param max The maximum value of the range (inclusive).
     *
     * @return The created range.
     */
    public static IVec<BigInteger> of(BigInteger min, BigInteger max) {
        return new RangeVec(min, max);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.specs.IVec#size()
     */
    @Override
    public int size() {
        return size;
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.specs.IVec#shrink(int)
     */
    @Override
    public void shrink(int nofelems) {
        throw new UnsupportedOperationException();
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.specs.IVec#shrinkTo(int)
     */
    @Override
    public void shrinkTo(int newsize) {
        throw new UnsupportedOperationException();
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.specs.IVec#pop()
     */
    @Override
    public void pop() {
        throw new UnsupportedOperationException();
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.specs.IVec#growTo(int, java.lang.Object)
     */
    @Override
    public void growTo(int newsize, BigInteger pad) {
        throw new UnsupportedOperationException();
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.specs.IVec#ensure(int)
     */
    @Override
    public void ensure(int nsize) {
        throw new UnsupportedOperationException();
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.specs.IVec#push(java.lang.Object)
     */
    @Override
    public IVec<BigInteger> push(BigInteger elem) {
        throw new UnsupportedOperationException();
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.specs.IVec#unsafePush(java.lang.Object)
     */
    @Override
    public void unsafePush(BigInteger elem) {
        throw new UnsupportedOperationException();
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.specs.IVec#insertFirst(java.lang.Object)
     */
    @Override
    public void insertFirst(BigInteger elem) {
        throw new UnsupportedOperationException();
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.specs.IVec#insertFirstWithShifting(java.lang.Object)
     */
    @Override
    public void insertFirstWithShifting(BigInteger elem) {
        throw new UnsupportedOperationException();
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.specs.IVec#clear()
     */
    @Override
    public void clear() {
        throw new UnsupportedOperationException();
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.specs.IVec#last()
     */
    @Override
    public BigInteger last() {
        return max;
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.specs.IVec#get(int)
     */
    @Override
    public BigInteger get(int i) {
        return min.add(BigInteger.valueOf(i));
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.specs.IVec#set(int, java.lang.Object)
     */
    @Override
    public void set(int i, BigInteger o) {
        throw new UnsupportedOperationException();
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.specs.IVec#remove(java.lang.Object)
     */
    @Override
    public void remove(BigInteger elem) {
        throw new UnsupportedOperationException();
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.specs.IVec#removeFromLast(java.lang.Object)
     */
    @Override
    public void removeFromLast(BigInteger elem) {
        throw new UnsupportedOperationException();
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.specs.IVec#delete(int)
     */
    @Override
    public BigInteger delete(int i) {
        throw new UnsupportedOperationException();
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.specs.IVec#copyTo(org.sat4j.specs.IVec)
     */
    @Override
    public void copyTo(IVec<BigInteger> copy) {
        copy.ensure(copy.size() + size);
        for (int i = 0; i < size; i++) {
            copy.push(get(i));
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.specs.IVec#copyTo(java.lang.Object[])
     */
    @Override
    public <E> void copyTo(E[] dest) {
        for (int i = 0; i < size; i++) {
            @SuppressWarnings("unchecked")
            E element = (E) get(i);
            dest[i] = element;
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.specs.IVec#toArray()
     */
    @Override
    public BigInteger[] toArray() {
        var array = new BigInteger[size];
        for (int i = 0; i < size; i++) {
            array[i] = get(i);
        }
        return array;
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.specs.IVec#moveTo(org.sat4j.specs.IVec)
     */
    @Override
    public void moveTo(IVec<BigInteger> dest) {
        throw new UnsupportedOperationException();
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.specs.IVec#moveTo(int, int)
     */
    @Override
    public void moveTo(int dest, int source) {
        throw new UnsupportedOperationException();
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.specs.IVec#sort(java.util.Comparator)
     */
    @Override
    public void sort(Comparator<BigInteger> comparator) {
        throw new UnsupportedOperationException();
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.specs.IVec#sortUnique(java.util.Comparator)
     */
    @Override
    public void sortUnique(Comparator<BigInteger> comparator) {
        throw new UnsupportedOperationException();
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.specs.IVec#isEmpty()
     */
    @Override
    public boolean isEmpty() {
        return 0 < size;
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.specs.IVec#iterator()
     */
    @Override
    public Iterator<BigInteger> iterator() {
        return new Iterator<BigInteger>() {

            /**
             * The current index in the range.
             */
            private int index = 0;

            /*
             * (non-Javadoc)
             *
             * @see java.util.Iterator#hasNext()
             */
            @Override
            public boolean hasNext() {
                return index < size;
            }

            /*
             * (non-Javadoc)
             *
             * @see java.util.Iterator#next()
             */
            @Override
            public BigInteger next() {
                if (!hasNext()) {
                    throw new NoSuchElementException();
                }

                var next = get(index);
                index++;
                return next;
            }

        };
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.specs.IVec#contains(java.lang.Object)
     */
    @Override
    public boolean contains(BigInteger element) {
        return (min.compareTo(element) <= 0) && (element.compareTo(max) <= 0);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.specs.IVec#indexOf(java.lang.Object)
     */
    @Override
    public int indexOf(BigInteger element) {
        if (contains(element)) {
            return element.subtract(min).intValue();
        }
        return -1;
    }

    /*
     * (non-Javadoc)
     *
     * @see java.lang.Object#clone()
     */
    @Override
    public IVec<BigInteger> clone() {
        // This is not how this should be done, but as a range is immutable, it is still safe.
        return this;
    }

    /*
     * (non-Javadoc)
     *
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        var builder = new StringBuilder();
        for (int i = 0; i < size(); i++) {
            builder.append(get(i));
            if (i < size() - 1) {
                builder.append(',');
            }
        }
        return builder.toString();
    }

}
