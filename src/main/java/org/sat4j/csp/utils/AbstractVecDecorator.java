/**
 * Sat4j-CSP, a CSP solver based on the Sat4j platform.
 * Copyright (c) 2021-2022 - Exakis Nelite, Univ Artois & CNRS.
 * All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 * If not, see {@link http://www.gnu.org/licenses}.
 */

package org.sat4j.csp.utils;

import java.io.Serializable;
import java.util.Comparator;
import java.util.Iterator;

import org.sat4j.specs.IVec;

/**
 * The AbstractVecDecorator is the parent class for the decorators of {@link IVec}.
 *
 * @author Thibault Falque
 * @author Romain Wallon
 *
 * @version 0.1.0
 */
public abstract class AbstractVecDecorator<T> implements IVec<T> {

    /**
     * The {@code serialVersionUID} of this {@link Serializable} class.
     */
    private static final long serialVersionUID = 1L;

    /**
     * The decorated instance of {@link IVec}.
     */
    private final IVec<T> decorated;

    /**
     * Creates a new AbstractVecDecorator.
     *
     * @param decorated The instance of {@link IVec} to decorate.
     */
    protected AbstractVecDecorator(IVec<T> decorated) {
        this.decorated = decorated;
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.specs.IVec#size()
     */
    @Override
    public int size() {
        return decorated.size();
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.specs.IVec#shrink(int)
     */
    @Override
    public void shrink(int nofelems) {
        decorated.shrink(nofelems);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.specs.IVec#shrinkTo(int)
     */
    @Override
    public void shrinkTo(int newsize) {
        decorated.shrinkTo(newsize);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.specs.IVec#pop()
     */
    @Override
    public void pop() {
        decorated.pop();
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.specs.IVec#growTo(int, java.lang.Object)
     */
    @Override
    public void growTo(int newsize, T pad) {
        decorated.growTo(newsize, pad);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.specs.IVec#ensure(int)
     */
    @Override
    public void ensure(int nsize) {
        decorated.ensure(nsize);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.specs.IVec#push(java.lang.Object)
     */
    @Override
    public IVec<T> push(T elem) {
        decorated.push(elem);
        return this;
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.specs.IVec#unsafePush(java.lang.Object)
     */
    @Override
    public void unsafePush(T elem) {
        decorated.unsafePush(elem);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.specs.IVec#insertFirst(java.lang.Object)
     */
    @Override
    public void insertFirst(T elem) {
        decorated.insertFirst(elem);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.specs.IVec#insertFirstWithShifting(java.lang.Object)
     */
    @Override
    public void insertFirstWithShifting(T elem) {
        decorated.insertFirstWithShifting(elem);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.specs.IVec#clear()
     */
    @Override
    public void clear() {
        decorated.clear();
    }

    @Override
    public T last() {
        return decorated.last();
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.specs.IVec#get(int)
     */
    @Override
    public T get(int i) {
        return decorated.get(i);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.specs.IVec#set(int, java.lang.Object)
     */
    @Override
    public void set(int i, T o) {
        decorated.set(i, o);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.specs.IVec#remove(java.lang.Object)
     */
    @Override
    public void remove(T elem) {
        decorated.remove(elem);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.specs.IVec#removeFromLast(java.lang.Object)
     */
    @Override
    public void removeFromLast(T elem) {
        decorated.removeFromLast(elem);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.specs.IVec#delete(int)
     */
    @Override
    public T delete(int i) {
        return decorated.delete(i);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.specs.IVec#copyTo(org.sat4j.specs.IVec)
     */
    @Override
    public void copyTo(IVec<T> copy) {
        copy.ensure(copy.size() + size());
        for (int i = 0; i < size(); i++) {
            copy.push(get(i));
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.specs.IVec#copyTo(java.lang.Object[])
     */
    @Override
    public <E> void copyTo(E[] dest) {
        for (int i = 0; i < size(); i++) {
            @SuppressWarnings("unchecked")
            E element = (E) get(i);
            dest[i] = element;
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.specs.IVec#toArray()
     */
    @Override
    public T[] toArray() {
        @SuppressWarnings("unchecked")
        var array = (T[]) new Object[size()];
        for (int i = 0; i < size(); i++) {
            array[i] = get(i);
        }
        return array;
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.specs.IVec#moveTo(org.sat4j.specs.IVec)
     */
    @Override
    public void moveTo(IVec<T> dest) {
        copyTo(dest);
        clear();
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.specs.IVec#moveTo(int, int)
     */
    @Override
    public void moveTo(int dest, int source) {
        decorated.moveTo(dest, source);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.specs.IVec#sort(java.util.Comparator)
     */
    @Override
    public void sort(Comparator<T> comparator) {
        decorated.sort(comparator);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.specs.IVec#sortUnique(java.util.Comparator)
     */
    @Override
    public void sortUnique(Comparator<T> comparator) {
        decorated.sortUnique(comparator);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.specs.IVec#isEmpty()
     */
    @Override
    public boolean isEmpty() {
        return decorated.isEmpty();
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.specs.IVec#iterator()
     */
    @Override
    public Iterator<T> iterator() {
        return decorated.iterator();
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.specs.IVec#contains(java.lang.Object)
     */
    @Override
    public boolean contains(T element) {
        return decorated.contains(element);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.specs.IVec#indexOf(java.lang.Object)
     */
    @Override
    public int indexOf(T element) {
        return decorated.indexOf(element);
    }

    /*
     * (non-Javadoc)
     *
     * @see java.lang.Object#clone()
     */
    @Override
    public IVec<T> clone() {
        return decorated.clone();
    }

    /*
     * (non-Javadoc)
     *
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        var builder = new StringBuilder();
        for (int i = 0; i < size(); i++) {
            builder.append(get(i));
            if (i < size() - 1) {
                builder.append(',');
            }
        }
        return builder.toString();
    }

}
