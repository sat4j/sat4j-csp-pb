/**
 * Sat4j-CSP, a CSP solver based on the Sat4j platform.
 * Copyright (c) 2021-2022 - Exakis Nelite, Univ Artois & CNRS.
 * All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 * If not, see {@link http://www.gnu.org/licenses}.
 */

package org.sat4j.csp.utils;

import java.io.PrintWriter;
import java.math.BigInteger;

import org.sat4j.core.Vec;
import org.sat4j.pb.OPBStringSolver;
import org.sat4j.specs.ContradictionException;
import org.sat4j.specs.FakeConstr;
import org.sat4j.specs.IConstr;
import org.sat4j.specs.IVec;
import org.sat4j.specs.IVecInt;

/**
 * The PBEncodingOutputSolver
 *
 * @author Thibault Falque
 * @author Romain Wallon
 *
 * @version 0.1.0
 */
public class PBEncodingOutputSolver extends OPBStringSolver {

    /**
     * The {@code serialVersionUID of this Serializable object.
     */
    private static final long serialVersionUID = 1L;

    /**
     * The number of variables that have been read.
     */
    private int nbVariables;

    /**
     * The number of constraints that have been read.
     */
    private IVec<String> constraints = new Vec<>();

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.pb.OPBStringSolver#newVar(int)
     */
    @Override
    public int newVar(int howmany) {
        this.nbVariables = howmany;
        return howmany;
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.tools.DimacsStringSolver#nVars()
     */
    @Override
    public int nVars() {
        return nbVariables;
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.pb.OPBStringSolver#addClause(org.sat4j.specs.IVecInt)
     */
    @Override
    public IConstr addClause(IVecInt literals) throws ContradictionException {
        return addAtLeast(literals, 1);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.pb.OPBStringSolver#addAtLeast(org.sat4j.specs.IVecInt, int)
     */
    @Override
    public IConstr addAtLeast(IVecInt literals, int degree) throws ContradictionException {
        var out = new StringBuilder();
        int finalDegree = degree;

        for (var it = literals.iterator(); it.hasNext();) {
            int lit = it.next();

            if (lit > 0) {
                out.append("+1 x" + lit + " ");

            } else {
                out.append("-1 x" + -lit + " ");
                finalDegree--;
            }
        }

        out.append(">= " + finalDegree + " ;\n");
        constraints.push(out.toString());

        return FakeConstr.instance();
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.pb.OPBStringSolver#addAtLeast(org.sat4j.specs.IVecInt,
     * org.sat4j.specs.IVecInt, int)
     */
    @Override
    public IConstr addAtLeast(IVecInt literals, IVecInt coeffs, int degree)
            throws ContradictionException {
        var out = new StringBuilder();
        int finalDegree = degree;

        for (int i = 0; i < literals.size(); i++) {
            int lit = literals.get(i);
            int coeff = coeffs.get(i);

            if (lit > 0) {
                out.append(coeff + " x" + lit + " ");

            } else {
                out.append(-coeff + " x" + -lit + " ");
                finalDegree -= coeff;
            }
        }

        out.append(">= " + finalDegree + " ;\n");
        constraints.push(out.toString());

        return FakeConstr.instance();
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.pb.OPBStringSolver#addAtLeast(org.sat4j.specs.IVecInt,
     * org.sat4j.specs.IVec, java.math.BigInteger)
     */
    @Override
    public IConstr addAtLeast(IVecInt literals, IVec<BigInteger> coeffs, BigInteger degree)
            throws ContradictionException {
        var out = new StringBuilder();
        var finalDegree = degree;

        for (int i = 0; i < literals.size(); i++) {
            int lit = literals.get(i);
            var coeff = coeffs.get(i);

            if (lit > 0) {
                out.append(coeff + " x" + lit + " ");

            } else {
                out.append(coeff.negate() + " x" + -lit + " ");
                finalDegree = finalDegree.subtract(coeff);
            }
        }

        out.append(">= " + finalDegree + " ;\n");
        constraints.push(out.toString());

        return FakeConstr.instance();
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.pb.OPBStringSolver#addAtMost(org.sat4j.specs.IVecInt, int)
     */
    @Override
    public IConstr addAtMost(IVecInt literals, int degree) throws ContradictionException {
        var out = new StringBuilder();
        int finalDegree = -degree;

        for (var it = literals.iterator(); it.hasNext();) {
            int lit = it.next();

            if (lit > 0) {
                out.append("-1 x" + lit + " ");

            } else {
                out.append("+1 x" + -lit + " ");
                finalDegree++;
            }
        }

        out.append(">= " + finalDegree + " ;\n");
        constraints.push(out.toString());

        return FakeConstr.instance();
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.pb.OPBStringSolver#addAtMost(org.sat4j.specs.IVecInt,
     * org.sat4j.specs.IVecInt, int)
     */
    @Override
    public IConstr addAtMost(IVecInt literals, IVecInt coeffs, int degree)
            throws ContradictionException {
        var out = new StringBuilder();
        var finalDegree = -degree;

        for (int i = 0; i < literals.size(); i++) {
            int lit = literals.get(i);
            var coeff = coeffs.get(i);

            if (lit > 0) {
                out.append(-coeff + " x" + lit + " ");

            } else {
                out.append(coeff + " x" + -lit + " ");
                finalDegree += coeff;
            }
        }

        out.append(">= " + finalDegree + " ;\n");
        constraints.push(out.toString());

        return FakeConstr.instance();
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.pb.OPBStringSolver#addAtMost(org.sat4j.specs.IVecInt,
     * org.sat4j.specs.IVec, java.math.BigInteger)
     */
    @Override
    public IConstr addAtMost(IVecInt literals, IVec<BigInteger> coeffs, BigInteger degree)
            throws ContradictionException {
        var out = new StringBuilder();
        var finalDegree = degree.negate();

        for (int i = 0; i < literals.size(); i++) {
            int lit = literals.get(i);
            var coeff = coeffs.get(i);

            if (lit > 0) {
                out.append(coeff.negate() + " x" + lit + " ");

            } else {
                out.append(coeff + " x" + -lit + " ");
                finalDegree = finalDegree.add(coeff);
            }
        }

        out.append(">= " + finalDegree + " ;\n");
        constraints.push(out.toString());

        return FakeConstr.instance();
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.pb.OPBStringSolver#addExactly(org.sat4j.specs.IVecInt, int)
     */
    @Override
    public IConstr addExactly(IVecInt literals, int weight) throws ContradictionException {
        var out = new StringBuilder();
        int finalDegree = weight;

        for (var it = literals.iterator(); it.hasNext();) {
            int lit = it.next();

            if (lit > 0) {
                out.append("+1 x" + lit + " ");

            } else {
                out.append("-1 x" + -lit + " ");
                finalDegree--;
            }
        }

        out.append("= " + finalDegree + " ;\n");
        constraints.push(out.toString());

        return FakeConstr.instance();
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.pb.OPBStringSolver#addExactly(org.sat4j.specs.IVecInt,
     * org.sat4j.specs.IVecInt, int)
     */
    @Override
    public IConstr addExactly(IVecInt literals, IVecInt coeffs, int weight)
            throws ContradictionException {
        var out = new StringBuilder();
        int finalDegree = weight;

        for (int i = 0; i < literals.size(); i++) {
            int lit = literals.get(i);
            int coeff = coeffs.get(i);

            if (lit > 0) {
                out.append(coeff + " x" + lit + " ");

            } else {
                out.append(-coeff + " x" + -lit + " ");
                finalDegree -= coeff;
            }
        }

        out.append("= " + finalDegree + " ;\n");
        constraints.push(out.toString());

        return FakeConstr.instance();
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.pb.OPBStringSolver#addExactly(org.sat4j.specs.IVecInt,
     * org.sat4j.specs.IVec, java.math.BigInteger)
     */
    @Override
    public IConstr addExactly(IVecInt literals, IVec<BigInteger> coeffs, BigInteger weight)
            throws ContradictionException {
        var out = new StringBuilder();
        var finalDegree = weight;

        for (int i = 0; i < literals.size(); i++) {
            int lit = literals.get(i);
            var coeff = coeffs.get(i);

            if (lit > 0) {
                out.append(coeff + " x" + lit + " ");

            } else {
                out.append(coeff.negate() + " x" + -lit + " ");
                finalDegree = finalDegree.subtract(coeff);
            }
        }

        out.append("= " + finalDegree + " ;\n");
        constraints.push(out.toString());

        return FakeConstr.instance();
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.tools.DimacsStringSolver#printInfos(java.io.PrintWriter)
     */
    @Override
    public void printInfos(PrintWriter out) {
        out.print("* #variable= " + nbVariables);
        out.println(" #constraint= " + constraints.size());

        var obj = getObjectiveFunction();
        if ((obj != null) && (!obj.getCoeffs().isEmpty())) {
            out.println("min: " + obj + ";");
        }

        for (var it = constraints.iterator(); it.hasNext();) {
            out.print(it.next());
        }
    }

}
