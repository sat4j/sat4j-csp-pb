/**
 * The {@code org.sat4j.csp.utils} package provides utility classes for Sat4j-CSP.
 *
 * @author Thibault Falque
 * @author Romain Wallon
 *
 * @version 0.1.0
 */

package org.sat4j.csp.utils;
