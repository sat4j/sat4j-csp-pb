/**
 * Sat4j-CSP, a CSP solver based on the Sat4j platform.
 * Copyright (c) 2021-2022 - Exakis Nelite, Univ Artois & CNRS.
 * All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 * If not, see {@link http://www.gnu.org/licenses}.
 */

package org.sat4j.csp.utils;

import java.io.Serializable;

import org.sat4j.specs.ContradictionException;

/**
 * The UncheckedContradictionException allows to throw a {@link ContradictionException}
 * (or to simulate such an exception) while being a {@link RuntimeException}, so as to
 * avoid having to declare it as being thrown (especially, when this is incompatible
 * with the implementation of existing interfaces).
 *
 * @author Thibault Falque
 * @author Romain Wallon
 *
 * @version 0.1.0
 */
public final class UncheckedContradictionException extends RuntimeException {

    /**
     * The {@code serialVersionUID} of this {@link Serializable} class.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Creates a new UncheckedContradictionException.
     *
     * @param cause The cause of the exception.
     */
    public UncheckedContradictionException(ContradictionException cause) {
        super(cause.getMessage(), cause);
    }

    /**
     * Creates a new UncheckedContradictionException.
     *
     * @param message The message of the exception.
     */
    public UncheckedContradictionException(String message) {
        super(message);
    }

    /**
     * Creates a new UncheckedContradictionException.
     *
     * @param message The message of the exception.
     * @param cause The cause of the exception.
     */
    public UncheckedContradictionException(String message, ContradictionException cause) {
        super(message, cause);
    }

}
