/**
 * Sat4j-CSP, a CSP solver based on the Sat4j platform.
 * Copyright (c) 2021-2022 - Exakis Nelite, Univ Artois & CNRS.
 * All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 * If not, see {@link http://www.gnu.org/licenses}.
 */

package org.sat4j.csp.utils;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Comparator;
import java.util.Iterator;

import org.sat4j.specs.IVec;

/**
 * The MultipliedVec is a decorator for an {@link IVec} containing {@link BigInteger} that
 * allows to multiply the values contained in this {@link IVec}.
 *
 * @author Thibault Falque
 * @author Romain Wallon
 *
 * @version 0.1.0
 */
public class MultipliedVec extends AbstractVecDecorator<BigInteger> {

    /**
     * The {@code serialVersionUID} of this {@link Serializable} class.
     */
    private static final long serialVersionUID = 1L;

    /**
     * The coefficient by which to multiply the values in the vector.
     */
    private final BigInteger coefficient;

    /**
     * Creates a new MultipliedVec.
     *
     * @param decorated The instance of {@link IVec} to decorate.
     * @param coefficient The coefficient by which to multiply the values in the vector.
     */
    public MultipliedVec(IVec<BigInteger> decorated, BigInteger coefficient) {
        super(decorated);
        this.coefficient = coefficient;
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.utils.AbstractVecDecorator#growTo(int, java.lang.Object)
     */
    @Override
    public void growTo(int newsize, BigInteger pad) {
        super.growTo(newsize, pad.divide(coefficient));
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.utils.AbstractVecDecorator#push(java.lang.Object)
     */
    @Override
    public IVec<BigInteger> push(BigInteger elem) {
        return super.push(elem.divide(coefficient));
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.utils.AbstractVecDecorator#unsafePush(java.lang.Object)
     */
    @Override
    public void unsafePush(BigInteger elem) {
        super.unsafePush(elem.divide(coefficient));
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.utils.AbstractVecDecorator#insertFirst(java.lang.Object)
     */
    @Override
    public void insertFirst(BigInteger elem) {
        super.insertFirst(elem.divide(coefficient));
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.sat4j.csp.utils.AbstractVecDecorator#insertFirstWithShifting(java.lang.Object)
     */
    @Override
    public void insertFirstWithShifting(BigInteger elem) {
        super.insertFirstWithShifting(elem.divide(coefficient));
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.utils.AbstractVecDecorator#last()
     */
    @Override
    public BigInteger last() {
        return super.last().multiply(coefficient);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.utils.AbstractVecDecorator#get(int)
     */
    @Override
    public BigInteger get(int i) {
        return super.get(i).multiply(coefficient);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.utils.AbstractVecDecorator#set(int, java.lang.Object)
     */
    @Override
    public void set(int i, BigInteger o) {
        super.set(i, o.divide(coefficient));
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.utils.AbstractVecDecorator#remove(java.lang.Object)
     */
    @Override
    public void remove(BigInteger elem) {
        super.remove(elem.divide(coefficient));
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.utils.AbstractVecDecorator#removeFromLast(java.lang.Object)
     */
    @Override
    public void removeFromLast(BigInteger elem) {
        super.removeFromLast(elem.divide(coefficient));
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.specs.IVec#sort(java.util.Comparator)
     */
    @Override
    public void sort(Comparator<BigInteger> comparator) {
        if (coefficient.signum() >= 0) {
            super.sort(comparator);

        } else {
            super.sort((a, b) -> comparator.compare(b, a));
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.specs.IVec#sortUnique(java.util.Comparator)
     */
    @Override
    public void sortUnique(Comparator<BigInteger> comparator) {
        if (coefficient.signum() >= 0) {
            super.sortUnique(comparator);

        } else {
            super.sortUnique((a, b) -> comparator.compare(b, a));
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.utils.AbstractVecDecorator#iterator()
     */
    @Override
    public Iterator<BigInteger> iterator() {
        return new Iterator<BigInteger>() {

            /**
             * The decorated iterator
             */
            private Iterator<BigInteger> it = MultipliedVec.super.iterator();

            /*
             * (non-Javadoc)
             *
             * @see java.util.Iterator#hasNext()
             */
            @Override
            public boolean hasNext() {
                return it.hasNext();
            }

            /*
             * (non-Javadoc)
             *
             * @see java.util.Iterator#next()
             */
            @Override
            public BigInteger next() {
                return it.next().multiply(coefficient);
            }

        };
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.utils.AbstractVecDecorator#contains(java.lang.Object)
     */
    @Override
    public boolean contains(BigInteger element) {
        return super.contains(element.divide(coefficient));
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.utils.AbstractVecDecorator#indexOf(java.lang.Object)
     */
    @Override
    public int indexOf(BigInteger element) {
        return super.indexOf(element.divide(coefficient));
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.utils.AbstractVecDecorator#clone()
     */
    @Override
    public IVec<BigInteger> clone() {
        return new MultipliedVec(super.clone(), coefficient);
    }

}
