/**
 * Sat4j-CSP, a CSP solver based on the Sat4j platform.
 * Copyright (c) 2021-2022 - Exakis Nelite, Univ Artois & CNRS.
 * All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 * If not, see {@link http://www.gnu.org/licenses}.
 */

package org.sat4j.csp;

import java.io.PrintWriter;
import java.io.Serializable;
import java.util.List;

import org.sat4j.AbstractLauncher;
import org.sat4j.ExitCode;
import org.sat4j.ILauncherMode;
import org.sat4j.csp.constraints.encoder.intension.IPrimitiveConstraintEncoder;
import org.sat4j.csp.core.ICSPSolver;
import org.sat4j.csp.reader.XCSP3Reader;
import org.sat4j.csp.utils.UncheckedContradictionException;
import org.sat4j.csp.variables.IVariableFactory;
import org.sat4j.csp.variables.ShiftedVariableFactoryDecorator;
import org.sat4j.reader.Reader;
import org.sat4j.specs.ILogAble;
import org.sat4j.specs.IProblem;
import org.sat4j.specs.ISolver;
import org.sat4j.specs.TimeoutException;

import fr.cril.cli.ClassParser;
import fr.cril.cli.CliArgsParser;
import fr.cril.cli.CliOptionDefinitionException;
import fr.cril.cli.CliUsageException;
import fr.cril.cli.annotations.Args;
import fr.cril.cli.annotations.Description;
import fr.cril.cli.annotations.LongName;
import fr.cril.cli.annotations.Params;
import fr.cril.cli.annotations.ShortName;

/**
 * The LauncherCSP is a launcher that allows to launch Sat4j-CSP from the command line.
 *
 * @author Thibault Falque
 * @author Romain Wallon
 *
 * @version 0.1.0
 */
@Params("1..1")
public class LauncherCSP extends AbstractLauncher implements ILogAble {

    /**
     * The {@code serialVersionUID} of this {@link Serializable} class.
     */
    private static final long serialVersionUID = 1L;

    /**
     * The flag to decide whether to display the help of Sat4j-CSP.
     */
    @ShortName("h")
    @LongName("help")
    @Description("Displays the help of Sat4j-CSP.")
    @Args(0)
    private boolean help;

    /**
     * The option specifying the name of the solver to use to solve the
     * (PB encoding of the) CSP problem.
     */
    @ShortName("s")
    @LongName("solver")
    @Description("The name of the solver to use to solve the (PB encoding of the) CSP problem.")
    @Args(value = 1, names = "solver-name")
    private String solverName = "Default";

    /**
     * The option specifying the name of the encoding to use to encode CSP
     * variables into Boolean variables.
     */
    @ShortName("e")
    @LongName("variable-encoding")
    @Description("The name of the encoding to use to encode CSP variables into Boolean variables.")
    @Args(value = 1, names = "encoding-name")
    private String variableEncoding = "SmartEncoding";

    /**
     * The option specifying the name of the encoding to use to encode
     * primitive constraints.
     */
    @ShortName("p")
    @LongName("primitive-encoding")
    @Description("The name of the encoding to use to encode primitive constraints.")
    @Args(value = 1, names = "encoding-name")
    private String primitiveEncoding = "Default";

    /**
     * The remaining parameters of the command line.
     * It should contain exactly one parameter: the path of the input problem.
     */
    private List<String> parameters;

    /**
     * The class parser used to read the command line arguments of Sat4j-CSP.
     */
    private final ClassParser<LauncherCSP> classParser = new ClassParser<>(LauncherCSP.class);

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.AbstractLauncher#run(java.lang.String[])
     */
    @Override
    public void run(String[] args) {
        setLauncherMode(ILauncherMode.OPTIMIZATION);
        parseCliArguments(args);
        super.run(args);
    }

    /**
     * Parses the command line arguments given to Sat4j-CSP.
     *
     * @param args The command line arguments.
     */
    private void parseCliArguments(String[] args) {
        var argsParser = new CliArgsParser<>(classParser);

        try {
            argsParser.parse(this, args);
            parameters = argsParser.getParameters();

            if (help) {
                // If the help is asked, Sat4j-CSP must NOT be launched.
                // So, the usage is displayed and the program exits.
                usage();
                System.exit(0);
            }

        } catch (CliUsageException | CliOptionDefinitionException e) {
            usage();
            throw new IllegalArgumentException(e);
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.AbstractLauncher#usage()
     */
    @Override
    public void usage() {
        System.err.println("sat4j-csp.sh [options] <xcsp3-instance>");
        classParser.printOptionUsage(new PrintWriter(System.err));
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.AbstractLauncher#configureSolver(java.lang.String[])
     */
    @Override
    protected ISolver configureSolver(String[] args) {
        var solver = SolverFactory.instance().createSolverByName(solverName);
        solver.setVariableFactory(getVariableFactory());
        solver.setPrimitiveEncoder(getPrimitiveEncoder());
        return solver;
    }

    /**
     * Gives the variable factory to use to encode CSP variables, based on the
     * value given to {@link #variableEncoding}.
     *
     * @return The variable factory to use to encode CSP variables.
     */
    private IVariableFactory getVariableFactory() {
        return new ShiftedVariableFactoryDecorator(
                instantiate("org.sat4j.csp.variables", variableEncoding, "VariableFactory"));
    }

    /**
     * Gives the constraint encoder to use to encode {@code primitive} constraints, based
     * on the value given to {@link #primitiveEncoding}.
     *
     * @return The constraint encoder to use to encode {@code primitive} constraints.
     */
    private IPrimitiveConstraintEncoder getPrimitiveEncoder() {
        return instantiate("org.sat4j.csp.constraints.encoder.intension",
                primitiveEncoding, "PrimitiveConstraintEncoder");
    }

    /**
     * Reflectively instantiate an object.
     *
     * @param <T> The type of the object to instantiate.
     *
     * @param packageName The name of the package containing the class of the object.
     * @param classId The identifier of the class of the object.
     * @param suffix The suffix of the class of the object.
     *
     * @return The instantiated object.
     *
     * @throws IllegalArgumentException If the class of the object could not be found.
     */
    private static <T> T instantiate(String packageName, String classId, String suffix) {
        var className = packageName + "." + classId + suffix;

        try {
            var classObject = Class.forName(className);
            @SuppressWarnings("unchecked")
            var instance = (T) classObject.getDeclaredConstructor().newInstance();
            return instance;

        } catch (ReflectiveOperationException e) {
            throw new IllegalArgumentException("Unknown class: " + className);
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.AbstractLauncher#getInstanceName(java.lang.String[])
     */
    @Override
    protected String getInstanceName(String[] args) {
        if (parameters.size() == 1) {
            return parameters.get(0);
        }
        throw new IllegalArgumentException("No XCSP3 input instance given!");
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.AbstractLauncher#createReader(org.sat4j.specs.ISolver,
     * java.lang.String)
     */
    @Override
    protected Reader createReader(ISolver theSolver, String problemname) {
        return new XCSP3Reader((ICSPSolver) theSolver);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.AbstractLauncher#solve(org.sat4j.specs.IProblem)
     */
    @Override
    protected void solve(IProblem problem) throws TimeoutException {
        addHook();
        super.solve(problem);
    }

    /**
     * Deals with an unsupported input instance.
     *
     * @param message The message describing why the input is unsupported.
     */
    protected void unsupported(String message) {
        if (message != null) {
            System.out.println("c Problem contains unsupported feature(s):");
            System.out.println("c " + message);
        }
        System.out.println("s UNSUPPORTED");
    }

    /**
     * Executes Sat4j-CSP from the command line.
     *
     * @param args The command line arguments.
     */
    public static void main(String[] args) {
        var launcher = new LauncherCSP();

        try {
            launcher.run(args);
            System.exit(launcher.getExitCode().value());

        } catch (UncheckedContradictionException e) {
            // A trivial inconsistency has been detected.
            System.out.println("s UNSATISFIABLE");
            System.exit(ExitCode.UNSATISFIABLE.value());

        } catch (UnsupportedOperationException e) {
            // The problem contains unsupported features.
            launcher.unsupported(e.getMessage());
            System.exit(ExitCode.UNKNOWN.value());

        } catch (Exception e) {
            // Another error occurred.
            System.out.println("c An unexpected error has occured:");
            System.out.println("c " + e.getMessage());
            System.exit(ExitCode.UNKNOWN.value());
        }
    }

}
