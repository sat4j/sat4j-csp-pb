/**
 * Sat4j-CSP, a CSP solver based on the Sat4j platform.
 * Copyright (c) 2021-2022 - Exakis Nelite, Univ Artois & CNRS.
 * All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 * If not, see {@link http://www.gnu.org/licenses}.
 */

package org.sat4j.csp;

import java.io.Serializable;

import org.sat4j.core.ASolverFactory;
import org.sat4j.csp.core.ICSPSolver;
import org.sat4j.csp.core.PBSolverDecoratorCSPSolver;
import org.sat4j.csp.utils.PBEncodingOutputSolver;
import org.sat4j.minisat.learning.NoLearningButHeuristics;
import org.sat4j.minisat.learning.NoLearningNoHeuristics;
import org.sat4j.pb.core.PBSolver;

/**
 * The SolverFactory provides an easy way of instantiating CSP solvers based
 * on the Sat4j platform.
 *
 * @author Thibault Falque
 * @author Romain Wallon
 *
 * @version 0.1.0
 */
public class SolverFactory extends ASolverFactory<ICSPSolver> {

    /**
     * The {@code serialVersionUID} of this {@link Serializable} class.
     */
    private static final long serialVersionUID = 1L;

    /**
     * The single instance of this class.
     */
    private static final SolverFactory INSTANCE = new SolverFactory();

    /**
     * Disables external instantiation.
     */
    private SolverFactory() {
        // Nothing to do: Singleton Design Pattern.
    }

    /**
     * Gives the single instance of SolverFactory.
     *
     * @return The single instance of this class.
     */
    public static SolverFactory instance() {
        return INSTANCE;
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.core.ASolverFactory#createSolverByName(java.lang.String)
     */
    @Override
    public ICSPSolver createSolverByName(String solvername) {
        // Looking for a CSP solver with the given name.
        var solver = super.createSolverByName(solvername);

        // If there is no such CSP solver, we try to decorate an existing PB solver.
        if (solver == null) {
            var pbSolver = org.sat4j.pb.SolverFactory.instance().createSolverByName(solvername);
            return new PBSolverDecoratorCSPSolver(pbSolver);
        }

        // Returning the CSP solver.
        return solver;
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.core.ASolverFactory#defaultSolver()
     */
    @Override
    public ICSPSolver defaultSolver() {
        return newDefault();
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.core.ASolverFactory#lightSolver()
     */
    @Override
    public ICSPSolver lightSolver() {
        return newDefault();
    }

    /**
     * Creates the default CSP solver.
     * It is a solver {@link org.sat4j.pb.SolverFactory#newRoundingSatPOS2020WL()}
     * decorated as a CSP solver.
     *
     * @return The created solver.
     */
    public static ICSPSolver newDefault() {
        var solver = org.sat4j.pb.SolverFactory.newRoundingSatPOS2020WL();
        return new PBSolverDecoratorCSPSolver(solver);
    }

    /**
     * Creates a solver {@link org.sat4j.pb.SolverFactory#newBoth()}
     * decorated as a CSP solver.
     *
     * @return The created solver.
     */
    public static ICSPSolver newBoth() {
        var solver = org.sat4j.pb.SolverFactory.newBoth();
        return new PBSolverDecoratorCSPSolver(solver);
    }

    /**
     * Creates a solver {@link org.sat4j.pb.SolverFactory#newBothPOS2020()}
     * decorated as a CSP solver.
     *
     * @return The created solver.
     */
    public static ICSPSolver newBothPOS2020() {
        var solver = org.sat4j.pb.SolverFactory.newBothPOS2020();
        return new PBSolverDecoratorCSPSolver(solver);
    }

    /**
     * Creates a solver {@link org.sat4j.pb.SolverFactory#newCuttingPlanes()}
     * decorated as a CSP solver.
     *
     * @return The created solver.
     */
    public static ICSPSolver newCuttingPlanes() {
        var solver = org.sat4j.pb.SolverFactory.newCuttingPlanes();
        return new PBSolverDecoratorCSPSolver(solver);
    }

    /**
     * Creates a solver {@link org.sat4j.pb.SolverFactory#newCuttingPlanesPOS2020()}
     * decorated as a CSP solver.
     *
     * @return The created solver.
     */
    public static ICSPSolver newCuttingPlanesPOS2020() {
        var solver = org.sat4j.pb.SolverFactory.newCuttingPlanesPOS2020();
        return new PBSolverDecoratorCSPSolver(solver);
    }

    /**
     * Creates a solver {@link org.sat4j.pb.SolverFactory#newRoundingSat()}
     * decorated as a CSP solver.
     *
     * @return The created solver.
     */
    public static ICSPSolver newRoundingSat() {
        var solver = org.sat4j.pb.SolverFactory.newRoundingSat();
        return new PBSolverDecoratorCSPSolver(solver);
    }

    /**
     * Creates a solver {@link org.sat4j.pb.SolverFactory#newRoundingSatPOS2020()}
     * decorated as a CSP solver.
     *
     * @return The created solver.
     */
    public static ICSPSolver newRoundingSatPOS2020() {
        var solver = org.sat4j.pb.SolverFactory.newRoundingSatPOS2020();
        return new PBSolverDecoratorCSPSolver(solver);
    }

    /**
     * Creates a solver {@link org.sat4j.pb.SolverFactory#newPartialRoundingSat()}
     * decorated as a CSP solver.
     *
     * @return The created solver.
     */
    public static ICSPSolver newPartialRoundingSat() {
        var solver = org.sat4j.pb.SolverFactory.newPartialRoundingSat();
        return new PBSolverDecoratorCSPSolver(solver);
    }

    /**
     * Creates a solver {@link org.sat4j.pb.SolverFactory#newPartialRoundingSatPOS2020()}
     * decorated as a CSP solver.
     *
     * @return The created solver.
     */
    public static ICSPSolver newPartialRoundingSatPOS2020() {
        var solver = org.sat4j.pb.SolverFactory.newPartialRoundingSatPOS2020();
        return new PBSolverDecoratorCSPSolver(solver);
    }

    /**
     * Creates a solver {@link org.sat4j.pb.SolverFactory#newResolution()}
     * decorated as a CSP solver.
     *
     * @return The created solver.
     */
    public static ICSPSolver newResolution() {
        var solver = org.sat4j.pb.SolverFactory.newResolution();
        return new PBSolverDecoratorCSPSolver(solver);
    }

    /**
     * Creates a {@link #newDefault()} solver that uses a {@link NoLearningButHeuristics}
     * learning strategy.
     * This solver does not learn any constraint, but still uses VSIDS as branching
     * heuristics.
     *
     * This solver is not expected to perform well, and is only made available for
     * experimental purposes.
     *
     * @return The created solver.
     */
    public static PBSolverDecoratorCSPSolver newDefaultNoLearningButHeuristics() {
        var solver = org.sat4j.pb.SolverFactory.newRoundingSatPOS2020WL();
        ((PBSolver) solver).setLearningStrategy(new NoLearningButHeuristics<>());
        return new PBSolverDecoratorCSPSolver(solver);
    }

    /**
     * Creates a {@link #newDefault()} solver that uses a {@link NoLearningNoHeuristics}
     * learning strategy.
     * This solver does not learn any constraint, and neither uses VSIDS as branching
     * heuristics.
     *
     * This solver is not expected to perform well, and is only made available for
     * experimental purposes.
     *
     * @return The created solver.
     */
    public static PBSolverDecoratorCSPSolver newDefaultNoLearningNoHeuristics() {
        var solver = org.sat4j.pb.SolverFactory.newRoundingSatPOS2020WL();
        ((PBSolver) solver).setLearningStrategy(new NoLearningNoHeuristics<>());
        return new PBSolverDecoratorCSPSolver(solver);
    }

    /**
     * Creates a solver that only outputs the pseudo-Boolean encoding of the CSP problem.
     * This solver cannot be used to actually solve input instances.
     *
     * @return The created solver.
     */
    public static ICSPSolver newEncoderOnly() {
        return new PBSolverDecoratorCSPSolver(new PBEncodingOutputSolver());
    }

}
