/**
 * Sat4j-CSP, a CSP solver based on the Sat4j platform.
 * Copyright (c) 2021-2022 - Exakis Nelite, Univ Artois & CNRS.
 * All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 * If not, see {@link http://www.gnu.org/licenses}.
 */

package org.sat4j.csp.constraints.intension;

import org.sat4j.csp.constraints.encoder.intension.IIntensionConstraintVisitor;
import org.sat4j.specs.ContradictionException;

/**
 * The IIntensionConstraint defines an interface for representing the abstract syntax tree
 * of an intension constraint, to allow its encoding using pseudo-Boolean constraints
 * and/or clauses.
 *
 * @author Thibault Falque
 * @author Romain Wallon
 *
 * @version 0.1.0
 */
public interface IIntensionConstraint {

    /**
     * Accepts an {@link IIntensionConstraintVisitor} to encode this constraint into
     * pseudo-Boolean constraints.
     *
     * @param visitor The visitor to accept.
     *
     * @throws ContradictionException If visiting (and encoding) this constraint results
     *         in a trivial inconsistency.
     */
    void accept(IIntensionConstraintVisitor visitor) throws ContradictionException;

}
