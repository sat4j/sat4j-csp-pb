/**
 * Sat4j-CSP, a CSP solver based on the Sat4j platform.
 * Copyright (c) 2021-2022 - Exakis Nelite, Univ Artois & CNRS.
 * All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 * If not, see {@link http://www.gnu.org/licenses}.
 */

package org.sat4j.csp.constraints.intension;

import java.util.List;

import org.sat4j.csp.constraints.Operator;
import org.sat4j.csp.constraints.encoder.intension.IIntensionConstraintVisitor;
import org.sat4j.specs.ContradictionException;

/**
 * The NaryIntensionConstraint is an {@link IIntensionConstraint} that applies an
 * operator on several intension constraints.
 *
 * @author Thibault Falque
 * @author Romain Wallon
 *
 * @version 0.1.0
 */
public class NaryIntensionConstraint extends OperatorIntensionConstraint {

    /**
     * The intension constraints on which an operator is applied.
     */
    private List<? extends IIntensionConstraint> children;

    /**
     * Creates a new NaryIntensionConstraint.
     *
     * @param operator The operator applied by the constraint.
     * @param children The intension constraints on which the operator is applied.
     */
    public NaryIntensionConstraint(Operator operator,
            List<? extends IIntensionConstraint> children) {
        super(operator);
        this.children = children;
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.constraints.intension.IIntensionConstraint#accept(org.sat4j.csp.
     * constraints.encoder.intension.IIntensionConstraintVisitor)
     */
    @Override
    public void accept(IIntensionConstraintVisitor visitor) throws ContradictionException {
        for (var child : children) {
            child.accept(visitor);
        }
        visitor.visit(this);
    }

    /**
     * Gives the arity of this constraints.
     *
     * @return The number of intension constraints on which the operator is applied.
     */
    public int getArity() {
        return children.size();
    }

}
