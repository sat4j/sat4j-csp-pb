/**
 * sat4j-csp
 * Copyright (c) 2023 - Romain Wallon.
 * All rights reserved.
 *
 * This library is free softwarethrows ContradictionException; you can redistribute it
 * and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundationthrows ContradictionException;
 * either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTYthrows ContradictionException; without even the implied
 * warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 * If not, see {@link http://www.gnu.org/licenses}.
 */

package org.sat4j.csp.constraints.encoder.knapsack;

import java.math.BigInteger;

import org.sat4j.csp.constraints.RelationalOperator;
import org.sat4j.csp.constraints.SetBelongingOperator;
import org.sat4j.csp.constraints.encoder.IConstraintEncoder;
import org.sat4j.csp.variables.IVariable;
import org.sat4j.specs.ContradictionException;
import org.sat4j.specs.IVec;

/**
 * The IKnapsackEncoder
 *
 * @author Romain Wallon
 *
 * @version 0.1.0
 */
public interface IKnapsackEncoder extends IConstraintEncoder {

    /**
     * Adds a {@code knapsack} constraint to this solver.
     *
     * @param variables The variables appearing in the constraint.
     * @param weights The weights of the elements to put in the knapsack.
     * @param wOperator The operator for comparing the weight of the knapsack.
     * @param wValue The total weight of the knapsack.
     * @param profits The profits of the elements to put in the knapsack.
     * @param pOperator The operator for comparing the profit of the knapsack.
     * @param pValue The total profit of the knapsack.
     *
     * @throws ContradictionException If adding the constraint results in a
     *         trivial inconsistency.
     */
    void encodeKnapsack(IVec<IVariable> variables, IVec<BigInteger> weights,
            RelationalOperator wOperator, BigInteger wValue, IVec<BigInteger> profits,
            RelationalOperator pOperator, BigInteger pValue) throws ContradictionException;

    /**
     * Adds a {@code knapsack} constraint to this solver.
     *
     * @param variables The variables appearing in the constraint.
     * @param weights The weights of the elements to put in the knapsack.
     * @param wOperator The operator for comparing the weight of the knapsack.
     * @param wValue The total weight of the knapsack.
     * @param profits The profits of the elements to put in the knapsack.
     * @param pOperator The operator for comparing the profit of the knapsack.
     * @param pVariable The variable encoding the total profit of the knapsack.
     *
     * @throws ContradictionException If adding the constraint results in a
     *         trivial inconsistency.
     */
    void encodeKnapsack(IVec<IVariable> variables, IVec<BigInteger> weights,
            RelationalOperator wOperator, BigInteger wValue, IVec<BigInteger> profits,
            RelationalOperator pOperator, IVariable pVariable) throws ContradictionException;

    /**
     * Adds a {@code knapsack} constraint to this solver.
     *
     * @param variables The variables appearing in the constraint.
     * @param weights The weights of the elements to put in the knapsack.
     * @param wOperator The operator for comparing the weight of the knapsack.
     * @param wValue The total weight of the knapsack.
     * @param profits The profits of the elements to put in the knapsack.
     * @param pOperator The operator for comparing the profit of the knapsack.
     * @param pMin The minimum profit of the knapsack.
     * @param pMax The maximum profit of the knapsack.
     *
     * @throws ContradictionException If adding the constraint results in a
     *         trivial inconsistency.
     */
    void encodeKnapsack(IVec<IVariable> variables, IVec<BigInteger> weights,
            RelationalOperator wOperator, BigInteger wValue, IVec<BigInteger> profits,
            SetBelongingOperator pOperator, BigInteger pMin, BigInteger pMax)
            throws ContradictionException;

    /**
     * Adds a {@code knapsack} constraint to this solver.
     *
     * @param variables The variables appearing in the constraint.
     * @param weights The weights of the elements to put in the knapsack.
     * @param wOperator The operator for comparing the weight of the knapsack.
     * @param wValue The total weight of the knapsack.
     * @param profits The profits of the elements to put in the knapsack.
     * @param pOperator The operator for comparing the profit of the knapsack.
     * @param pSet The allowed total profits of the knapsack.
     *
     * @throws ContradictionException If adding the constraint results in a
     *         trivial inconsistency.
     */
    void encodeKnapsack(IVec<IVariable> variables, IVec<BigInteger> weights,
            RelationalOperator wOperator, BigInteger wValue, IVec<BigInteger> profits,
            SetBelongingOperator pOperator, IVec<BigInteger> pSet) throws ContradictionException;

    /**
     * Adds a {@code knapsack} constraint to this solver.
     *
     * @param variables The variables appearing in the constraint.
     * @param weights The weights of the elements to put in the knapsack.
     * @param wOperator The operator for comparing the weight of the knapsack.
     * @param wVariable The variable encoding the total weight of the knapsack.
     * @param profits The profits of the elements to put in the knapsack.
     * @param pOperator The operator for comparing the profit of the knapsack.
     * @param pValue The variable encoding the total profit of the knapsack.
     *
     * @throws ContradictionException If adding the constraint results in a
     *         trivial inconsistency.
     */
    void encodeKnapsack(IVec<IVariable> variables, IVec<BigInteger> weights,
            RelationalOperator wOperator, IVariable wVariable, IVec<BigInteger> profits,
            RelationalOperator pOperator, BigInteger pValue) throws ContradictionException;

    /**
     * Adds a {@code knapsack} constraint to this solver.
     *
     * @param variables The variables appearing in the constraint.
     * @param weights The weights of the elements to put in the knapsack.
     * @param wOperator The operator for comparing the weight of the knapsack.
     * @param wVariable The variable encoding the total weight of the knapsack.
     * @param profits The profits of the elements to put in the knapsack.
     * @param pOperator The operator for comparing the profit of the knapsack.
     * @param pVariable The variable encoding the total profit of the knapsack.
     *
     * @throws ContradictionException If adding the constraint results in a
     *         trivial inconsistency.
     */
    void encodeKnapsack(IVec<IVariable> variables, IVec<BigInteger> weights,
            RelationalOperator wOperator, IVariable wVariable, IVec<BigInteger> profits,
            RelationalOperator pOperator, IVariable pVariable) throws ContradictionException;

    /**
     * Adds a {@code knapsack} constraint to this solver.
     *
     * @param variables The variables appearing in the constraint.
     * @param weights The weights of the elements to put in the knapsack.
     * @param wOperator The operator for comparing the weight of the knapsack.
     * @param wVariable The variable encoding the total weight of the knapsack.
     * @param profits The profits of the elements to put in the knapsack.
     * @param pOperator The operator for comparing the profit of the knapsack.
     * @param pMin The minimum profit of the knapsack.
     * @param pMax The maximum profit of the knapsack.
     *
     * @throws ContradictionException If adding the constraint results in a
     *         trivial inconsistency.
     */
    void encodeKnapsack(IVec<IVariable> variables, IVec<BigInteger> weights,
            RelationalOperator wOperator, IVariable wVariable, IVec<BigInteger> profits,
            SetBelongingOperator pOperator, BigInteger pMin, BigInteger pMax)
            throws ContradictionException;

    /**
     * Adds a {@code knapsack} constraint to this solver.
     *
     * @param variables The variables appearing in the constraint.
     * @param weights The weights of the elements to put in the knapsack.
     * @param wOperator The operator for comparing the weight of the knapsack.
     * @param wVariable The variable encoding the total weight of the knapsack.
     * @param profits The profits of the elements to put in the knapsack.
     * @param pOperator The operator for comparing the profit of the knapsack.
     * @param pSet The allowed total profits of the knapsack.
     *
     * @throws ContradictionException If adding the constraint results in a
     *         trivial inconsistency.
     */
    void encodeKnapsack(IVec<IVariable> variables, IVec<BigInteger> weights,
            RelationalOperator wOperator, IVariable wVariable, IVec<BigInteger> profits,
            SetBelongingOperator pOperator, IVec<BigInteger> pSet) throws ContradictionException;

    /**
     * Adds a {@code knapsack} constraint to this solver.
     *
     * @param variables The variables appearing in the constraint.
     * @param weights The weights of the elements to put in the knapsack.
     * @param wOperator The operator for comparing the weight of the knapsack.
     * @param wMin The minimum weight of the knapsack.
     * @param wMax The maximum weight of the knapsack.
     * @param profits The profits of the elements to put in the knapsack.
     * @param pOperator The operator for comparing the profit of the knapsack.
     * @param pValue The total profit of the knapsack.
     *
     * @throws ContradictionException If adding the constraint results in a
     *         trivial inconsistency.
     */
    void encodeKnapsack(IVec<IVariable> variables, IVec<BigInteger> weights,
            SetBelongingOperator wOperator, BigInteger wMin, BigInteger wMax,
            IVec<BigInteger> profits, RelationalOperator pOperator, BigInteger pValue)
            throws ContradictionException;

    /**
     * Adds a {@code knapsack} constraint to this solver.
     *
     * @param variables The variables appearing in the constraint.
     * @param weights The weights of the elements to put in the knapsack.
     * @param wOperator The operator for comparing the weight of the knapsack.
     * @param wMin The minimum weight of the knapsack.
     * @param wMax The maximum weight of the knapsack.
     * @param profits The profits of the elements to put in the knapsack.
     * @param pOperator The operator for comparing the profit of the knapsack.
     * @param pVariable The variable encoding the total profit of the knapsack.
     *
     * @throws ContradictionException If adding the constraint results in a
     *         trivial inconsistency.
     */
    void encodeKnapsack(IVec<IVariable> variables, IVec<BigInteger> weights,
            SetBelongingOperator wOperator, BigInteger wMin, BigInteger wMax,
            IVec<BigInteger> profits, RelationalOperator pOperator, IVariable pVariable)
            throws ContradictionException;

    /**
     * Adds a {@code knapsack} constraint to this solver.
     *
     * @param variables The variables appearing in the constraint.
     * @param weights The weights of the elements to put in the knapsack.
     * @param wOperator The operator for comparing the weight of the knapsack.
     * @param wMin The minimum weight of the knapsack.
     * @param wMax The maximum weight of the knapsack.
     * @param profits The profits of the elements to put in the knapsack.
     * @param pOperator The operator for comparing the profit of the knapsack.
     * @param pMin The minimum profit of the knapsack.
     * @param pMax The maximum profit of the knapsack.
     *
     * @throws ContradictionException If adding the constraint results in a
     *         trivial inconsistency.
     */
    void encodeKnapsack(IVec<IVariable> variables, IVec<BigInteger> weights,
            SetBelongingOperator wOperator, BigInteger wMin, BigInteger wMax,
            IVec<BigInteger> profits, SetBelongingOperator pOperator, BigInteger pMin,
            BigInteger pMax) throws ContradictionException;

    /**
     * Adds a {@code knapsack} constraint to this solver.
     *
     * @param variables The variables appearing in the constraint.
     * @param weights The weights of the elements to put in the knapsack.
     * @param wOperator The operator for comparing the weight of the knapsack.
     * @param wMin The minimum weight of the knapsack.
     * @param wMax The maximum weight of the knapsack.
     * @param profits The profits of the elements to put in the knapsack.
     * @param pOperator The operator for comparing the profit of the knapsack.
     * @param pSet The allowed total profits of the knapsack.
     *
     * @throws ContradictionException If adding the constraint results in a
     *         trivial inconsistency.
     */
    void encodeKnapsack(IVec<IVariable> variables, IVec<BigInteger> weights,
            SetBelongingOperator wOperator, BigInteger wMin, BigInteger wMax,
            IVec<BigInteger> profits, SetBelongingOperator pOperator,
            IVec<BigInteger> pSet) throws ContradictionException;

    /**
     * Adds a {@code knapsack} constraint to this solver.
     *
     * @param variables The variables appearing in the constraint.
     * @param weights The weights of the elements to put in the knapsack.
     * @param wOperator The operator for comparing the weight of the knapsack.
     * @param wSet The allowed total weights of the knapsack.
     * @param profits The profits of the elements to put in the knapsack.
     * @param pOperator The operator for comparing the profit of the knapsack.
     * @param pValue The variable encoding the total profit of the knapsack.
     *
     * @throws ContradictionException If adding the constraint results in a
     *         trivial inconsistency.
     */
    void encodeKnapsack(IVec<IVariable> variables, IVec<BigInteger> weights,
            SetBelongingOperator wOperator, IVec<BigInteger> wSet, IVec<BigInteger> profits,
            RelationalOperator pOperator, BigInteger pValue) throws ContradictionException;

    /**
     * Adds a {@code knapsack} constraint to this solver.
     *
     * @param variables The variables appearing in the constraint.
     * @param weights The weights of the elements to put in the knapsack.
     * @param wOperator The operator for comparing the weight of the knapsack.
     * @param wSet The allowed total weights of the knapsack.
     * @param profits The profits of the elements to put in the knapsack.
     * @param pOperator The operator for comparing the profit of the knapsack.
     * @param pVariable The variable encoding the total profit of the knapsack.
     *
     * @throws ContradictionException If adding the constraint results in a
     *         trivial inconsistency.
     */
    void encodeKnapsack(IVec<IVariable> variables, IVec<BigInteger> weights,
            SetBelongingOperator wOperator, IVec<BigInteger> wSet, IVec<BigInteger> profits,
            RelationalOperator pOperator, IVariable pVariable) throws ContradictionException;

    /**
     * Adds a {@code knapsack} constraint to this solver.
     *
     * @param variables The variables appearing in the constraint.
     * @param weights The weights of the elements to put in the knapsack.
     * @param wOperator The operator for comparing the weight of the knapsack.
     * @param wSet The allowed total weights of the knapsack.
     * @param profits The profits of the elements to put in the knapsack.
     * @param pOperator The operator for comparing the profit of the knapsack.
     * @param pMin The minimum profit of the knapsack.
     * @param pMax The maximum profit of the knapsack.
     *
     * @throws ContradictionException If adding the constraint results in a
     *         trivial inconsistency.
     */
    void encodeKnapsack(IVec<IVariable> variables, IVec<BigInteger> weights,
            SetBelongingOperator wOperator, IVec<BigInteger> wSet, IVec<BigInteger> profits,
            SetBelongingOperator pOperator, BigInteger pMin, BigInteger pMax)
            throws ContradictionException;

    /**
     * Adds a {@code knapsack} constraint to this solver.
     *
     * @param variables The variables appearing in the constraint.
     * @param weights The weights of the elements to put in the knapsack.
     * @param wOperator The operator for comparing the weight of the knapsack.
     * @param wSet The allowed total weights of the knapsack.
     * @param profits The profits of the elements to put in the knapsack.
     * @param pOperator The operator for comparing the profit of the knapsack.
     * @param pSet The allowed total profits of the knapsack.
     *
     * @throws ContradictionException If adding the constraint results in a
     *         trivial inconsistency.
     */
    void encodeKnapsack(IVec<IVariable> variables, IVec<BigInteger> weights,
            SetBelongingOperator wOperator, IVec<BigInteger> wSet, IVec<BigInteger> profits,
            SetBelongingOperator pOperator, IVec<BigInteger> pSet) throws ContradictionException;
}
