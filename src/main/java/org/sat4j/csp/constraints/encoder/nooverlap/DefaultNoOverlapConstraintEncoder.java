/**
 * Sat4j-CSP, a CSP solver based on the Sat4j platform.
 * Copyright (c) 2021-2022 - Exakis Nelite, Univ Artois & CNRS.
 * All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 * If not, see {@link http://www.gnu.org/licenses}.
 */

package org.sat4j.csp.constraints.encoder.nooverlap;

import java.math.BigInteger;

import org.sat4j.core.ConstrGroup;
import org.sat4j.core.Vec;
import org.sat4j.core.VecInt;
import org.sat4j.csp.constraints.RelationalOperator;
import org.sat4j.csp.constraints.encoder.AbstractConstraintEncoder;
import org.sat4j.csp.variables.ConstantVariable;
import org.sat4j.csp.variables.IVariable;
import org.sat4j.specs.ContradictionException;
import org.sat4j.specs.IConstr;
import org.sat4j.specs.IVec;

/**
 * The DefaultNoOverlapConstraintEncoder provides a base encoding for {@code no-overlap}
 * constraints using pseudo-Boolean constraints and/or clauses.
 *
 * @author Thibault Falque
 * @author Romain Wallon
 *
 * @version 0.1.0
 */
public class DefaultNoOverlapConstraintEncoder extends AbstractConstraintEncoder
        implements INoOverlapConstraintEncoder {

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.constraints.encoder.nooverlap.INoOverlapConstraintEncoder#
     * encodeNoOverlap(org.sat4j.specs.IVec, org.sat4j.specs.IVec, boolean)
     */
    @Override
    public IConstr encodeNoOverlap(IVec<IVariable> variables, IVec<BigInteger> length,
            boolean zeroIgnored) throws ContradictionException {
        return encodeNoOverlapVariableLength(
                variables, toConstantVariables(length, zeroIgnored), zeroIgnored);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.constraints.encoder.nooverlap.INoOverlapConstraintEncoder#
     * encodeNoOverlapVariableLength(org.sat4j.specs.IVec, org.sat4j.specs.IVec, boolean)
     */
    @Override
    public IConstr encodeNoOverlapVariableLength(IVec<IVariable> variables, IVec<IVariable> length,
            boolean zeroIgnored) throws ContradictionException {
        var sumEncoder = solver.getSumEncoder();

        var group = new ConstrGroup();
        for (int i = 0; i < variables.size(); i++) {
            var x1 = variables.get(i);
            var l1 = length.get(i);

            for (int j = i + 1; j < variables.size(); j++) {
                var x2 = variables.get(j);
                var l2 = length.get(j);

                int s1 = solver.newDimacsVariables(2);
                int s2 = s1 - 1;

                group.add(sumEncoder.encodeSoftSum(s1, Vec.of(x1, l1), RelationalOperator.LE, x2));
                group.add(sumEncoder.encodeSoftSum(s2, Vec.of(x2, l2), RelationalOperator.LE, x1));
                group.add(solver.addClause(VecInt.of(s1, s2)));
            }

            if (!zeroIgnored) {
                group.add(solver.getOrderedEncoder().encodeOrdered(
                        l1, RelationalOperator.NEQ, ConstantVariable.ZERO));
            }
        }

        return group;
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.constraints.encoder.nooverlap.INoOverlapConstraintEncoder#
     * encodeMultiDimensionalNoOverlap(org.sat4j.specs.IVec, org.sat4j.specs.IVec,
     * boolean)
     */
    @Override
    public IConstr encodeMultiDimensionalNoOverlap(IVec<IVec<IVariable>> variables,
            IVec<IVec<BigInteger>> length,
            boolean zeroIgnored) throws ContradictionException {
        var lengthVariables = new Vec<IVec<IVariable>>(length.size());
        for (var it = length.iterator(); it.hasNext();) {
            lengthVariables.push(toConstantVariables(it.next(), zeroIgnored));
        }

        return encodeMultiDimensionalNoOverlapVariableLength(
                variables, lengthVariables, zeroIgnored);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.constraints.encoder.nooverlap.INoOverlapConstraintEncoder#
     * encodeMultiDimensionalNoOverlapVariableLength(org.sat4j.specs.IVec,
     * org.sat4j.specs.IVec, boolean)
     */
    @Override
    public IConstr encodeMultiDimensionalNoOverlapVariableLength(IVec<IVec<IVariable>> variables,
            IVec<IVec<IVariable>> length, boolean zeroIgnored) throws ContradictionException {
        var sumEncoder = solver.getSumEncoder();
        int n = variables.get(0).size();
        var selectors = new VecInt(n);

        var group = new ConstrGroup();
        for (int i = 0; i < variables.size(); i++) {
            var x1 = variables.get(i);
            var l1 = length.get(i);

            for (int j = i + 1; j < variables.size(); j++) {
                var x2 = variables.get(j);
                var l2 = length.get(j);

                int dimacs = solver.newDimacsVariables(3 * n);

                for (int k = 0; k < n; k++, dimacs -= 3) {
                    int s1 = dimacs;
                    int s2 = dimacs - 1;
                    int s3 = dimacs - 2;

                    group.add(sumEncoder.encodeSoftSum(
                            s1, Vec.of(x1.get(k), l1.get(k)), RelationalOperator.LE, x2.get(k)));
                    group.add(sumEncoder.encodeSoftSum(
                            s2, Vec.of(x2.get(k), l2.get(k)), RelationalOperator.LE, x1.get(k)));
                    group.add(solver.addSoftAtLeast(s3, VecInt.of(s1, s2), 1));

                    selectors.push(s3);
                }

                group.add(solver.addClause(selectors));
                selectors.clear();
            }

            if (!zeroIgnored) {
                for (var it = l1.iterator(); it.hasNext();) {
                    group.add(solver.getOrderedEncoder().encodeOrdered(
                            it.next(), RelationalOperator.NEQ, ConstantVariable.ZERO));
                }
            }
        }

        return group;
    }

    /**
     * Translates a vector of constant lengths into a vector of (constant variables).
     *
     * @param length The length associated to translate.
     * @param zeroIgnored Whether {@code 0}-lengths should be ignored.
     *
     * @return The vector of (constant) variables.
     *
     * @throws ContradictionException If {@code 0} are not ignored, and one of the length
     *         is equal to 0.
     */
    private static IVec<IVariable> toConstantVariables(IVec<BigInteger> length, boolean zeroIgnored)
            throws ContradictionException {
        var lengthVariable = new Vec<IVariable>(length.size());

        for (var it = length.iterator(); it.hasNext();) {
            var value = it.next();
            if ((!zeroIgnored) && (value.signum() == 0)) {
                throw new ContradictionException("Unpackable task of length 0.");
            }
            lengthVariable.push(ConstantVariable.of(value));
        }

        return lengthVariable;
    }

}
