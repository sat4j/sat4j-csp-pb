/**
 * Sat4j-CSP, a CSP solver based on the Sat4j platform.
 * Copyright (c) 2021-2022 - Exakis Nelite, Univ Artois & CNRS.
 * All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 * If not, see {@link http://www.gnu.org/licenses}.
 */

package org.sat4j.csp.constraints.encoder.intension;

import static java.math.BigInteger.ONE;
import static java.math.BigInteger.ZERO;

import java.math.BigInteger;

import org.sat4j.core.Vec;
import org.sat4j.core.VecInt;
import org.sat4j.csp.constraints.RelationalOperator;
import org.sat4j.csp.constraints.encoder.AbstractConstraintEncoder;
import org.sat4j.csp.variables.BooleanVariable;
import org.sat4j.csp.variables.ConstantVariable;
import org.sat4j.csp.variables.IDomain;
import org.sat4j.csp.variables.IVariable;
import org.sat4j.csp.variables.RangeDomain;
import org.sat4j.csp.variables.ShiftedVariableDecorator;
import org.sat4j.specs.ContradictionException;
import org.sat4j.specs.IVec;

/**
 * The DefaultIntensionConstraintEncoder provides a base encoding for {@code intension}
 * constraints using pseudo-Boolean constraints and/or clauses.
 *
 * @author Thibault Falque
 * @author Romain Wallon
 *
 * @version 0.1.0
 */
public class DefaultIntensionConstraintEncoder extends AbstractConstraintEncoder
        implements IIntensionConstraintEncoder {

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.constraints.encoder.intension.IIntensionConstraintEncoder#
     * encodeOpposite(org.sat4j.csp.variables.IVariable)
     */
    @Override
    public IVariable encodeOpposite(IVariable variable) throws ContradictionException {
        return variable.negate();
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.constraints.encoder.intension.IIntensionConstraintEncoder#
     * encodeAbsoluteValue(org.sat4j.csp.variables.IVariable)
     */
    @Override
    public IVariable encodeAbsoluteValue(IVariable variable) throws ContradictionException {
        var domain = variable.getDomain();
        var min = domain.min();
        var max = domain.max();

        if (domain.contains(ZERO)) {
            // There may be both negative and positive values in the domain.
            max = min.abs().max(max.abs());
            min = ZERO;

        } else if (min.signum() < 0) {
            // All values are negative : switching to positive values.
            var tmp = min.abs();
            min = max.abs();
            max = tmp;
        }

        var abs = solver.getVariableFactory().createVariable(min, max);
        return encodeAbsoluteValue(abs, variable);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.constraints.encoder.intension.IIntensionConstraintEncoder#
     * encodeAbsoluteValue(org.sat4j.csp.variables.IVariable,
     * org.sat4j.csp.variables.IVariable)
     */
    @Override
    public IVariable encodeAbsoluteValue(IVariable abs, IVariable variable)
            throws ContradictionException {
        IVariable isPositive = encodeGreaterOrEqual(variable, ConstantVariable.ZERO);
        return encodeIfThenElse(abs, isPositive, variable, variable.negate());
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.constraints.encoder.intension.IIntensionConstraintEncoder#
     * encodeAddition(org.sat4j.specs.IVec)
     */
    @Override
    public IVariable encodeAddition(IVec<IVariable> variables) throws ContradictionException {
        var additionDomain = IDomain.additionOf(getDomainsOf(variables));
        var addition = additionDomain.newVariableWithThisDomain(solver.getVariableFactory());
        solver.getSumEncoder().encodeSum(variables, RelationalOperator.EQ, addition);
        return addition;
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.constraints.encoder.intension.IIntensionConstraintEncoder#
     * encodeSubtraction(org.sat4j.csp.variables.IVariable,
     * org.sat4j.csp.variables.IVariable)
     */
    @Override
    public IVariable encodeSubtraction(IVariable left, IVariable right)
            throws ContradictionException {
        return encodeAddition(left, right.negate());
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.constraints.encoder.intension.IIntensionConstraintEncoder#
     * encodeMultiplication(org.sat4j.specs.IVec)
     */
    @Override
    public IVariable encodeMultiplication(IVec<IVariable> variables) throws ContradictionException {
        return solver.getMultiplicationEncoder().encodeMultiplication(variables);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.constraints.encoder.intension.IIntensionConstraintEncoder#
     * encodeDivision(org.sat4j.csp.variables.IVariable,
     * org.sat4j.csp.variables.IVariable)
     */
    @Override
    public IVariable encodeDivision(IVariable left, IVariable right) throws ContradictionException {
        // Creating the variable holding the quotient.
        var domainDiv = computeQuotientDomain(left, right);
        var div = domainDiv.newVariableWithThisDomain(solver.getVariableFactory());

        // Actually encoding the division.
        return encodeDivision(div, left, right);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.constraints.encoder.intension.IIntensionConstraintEncoder#
     * encodeDivision(org.sat4j.csp.variables.IVariable,
     * org.sat4j.csp.variables.IVariable, org.sat4j.csp.variables.IVariable)
     */
    @Override
    public IVariable encodeDivision(IVariable div, IVariable left, IVariable right)
            throws ContradictionException {
        // Creating the variable holding the remainder.
        var domainMod = computeRemainderDomain(right);
        var mod = domainMod.newVariableWithThisDomain(solver.getVariableFactory());

        // Actually encoding the division.
        encodeDivision(div, mod, left, right);
        return div;
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.constraints.encoder.intension.IIntensionConstraintEncoder#
     * encodeModulo(org.sat4j.csp.variables.IVariable, org.sat4j.csp.variables.IVariable)
     */
    @Override
    public IVariable encodeModulo(IVariable left, IVariable right) throws ContradictionException {
        // Creating the variable holding the remainder.
        var domainMod = computeRemainderDomain(right);
        var mod = domainMod.newVariableWithThisDomain(solver.getVariableFactory());

        // Actually encoding the modulo.
        return encodeModulo(mod, left, right);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.constraints.encoder.intension.IIntensionConstraintEncoder#
     * encodeModulo(org.sat4j.csp.variables.IVariable, org.sat4j.csp.variables.IVariable,
     * org.sat4j.csp.variables.IVariable)
     */
    @Override
    public IVariable encodeModulo(IVariable mod, IVariable left, IVariable right)
            throws ContradictionException {
        // Creating the variable holding the quotient.
        var domainDiv = computeQuotientDomain(left, right);
        var div = domainDiv.newVariableWithThisDomain(solver.getVariableFactory());

        // Actually encoding the modulo.
        encodeDivision(div, mod, left, right);
        return mod;
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.constraints.encoder.intension.IIntensionConstraintEncoder#
     * encodeDivision(org.sat4j.csp.variables.IVariable,
     * org.sat4j.csp.variables.IVariable, org.sat4j.csp.variables.IVariable,
     * org.sat4j.csp.variables.IVariable)
     */
    @Override
    public IVariable encodeDivision(IVariable div, IVariable mod, IVariable left, IVariable right)
            throws ContradictionException {
        // Encoding the equality of the Euclidean division.
        solver.getSumEncoder().encodeSum(
                Vec.of(encodeMultiplication(right, div), mod), RelationalOperator.EQ, left);

        // The divisor cannot be 0.
        solver.getOrderedEncoder().encodeOrdered(right, RelationalOperator.NEQ,
                ConstantVariable.ZERO);

        // The modulo must be positive and less than the divisor.
        solver.getOrderedEncoder().encodeOrdered(ConstantVariable.ZERO, RelationalOperator.LE, mod);
        solver.getOrderedEncoder().encodeOrdered(mod, RelationalOperator.LT, right);

        return div;
    }

    /**
     * Computes the domain of a variable representing the quotient
     * of an Euclidean division.
     *
     * @param dividend The dividend of the Euclidean division.
     * @param divisor The divisor of the Euclidean division.
     *
     * @return The domain of the quotient of the division.
     */
    private static IDomain computeQuotientDomain(IVariable dividend, IVariable divisor) {
        // Extracting the bounds of the variables.
        var minDividend = dividend.getDomain().min();
        var maxDividend = dividend.getDomain().max();
        var minDivisor = divisor.getDomain().min();
        var maxDivisor = divisor.getDomain().max();

        // If an extremum of the divisor is 0, we must exclude it.
        if (minDivisor.signum() == 0) {
            minDivisor = ONE;
        }
        if (maxDivisor.signum() == 0) {
            maxDivisor = ONE.negate();
        }

        // Then, we compute the extrema of the quotient.
        var minQuotient = minDividend.divide(maxDivisor);
        var maxQuotient = maxDividend.divide(minDivisor);

        // If one of the divisions above involved opposite signs,
        // the order of the extrema may not have been preserved.
        // We thus need to take this into account while building the domain.
        if (minQuotient.compareTo(maxQuotient) < 0) {
            return RangeDomain.of(minQuotient, maxQuotient);
        }
        return RangeDomain.of(maxQuotient, minQuotient);
    }

    /**
     * Computes the domain of a variable representing the remainder
     * of an Euclidean division.
     *
     * @param divisor The divisor of the Euclidean division.
     *
     * @return The domain of the remainder of the division.
     */
    private static IDomain computeRemainderDomain(IVariable divisor) {
        // Extracting the bounds of the variable.
        var absMinDivisor = divisor.getDomain().min();
        var absMaxDivisor = divisor.getDomain().max();

        // When taking the absolute value, the order may have change.
        // We thus need to take this into account while building the domain.
        if (absMinDivisor.compareTo(absMaxDivisor) < 0) {
            return RangeDomain.of(ZERO, absMaxDivisor.subtract(ONE));
        }
        return RangeDomain.of(ZERO, absMinDivisor.subtract(ONE));
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.constraints.encoder.intension.IIntensionConstraintEncoder#
     * encodeSquare(org.sat4j.csp.variables.IVariable)
     */
    @Override
    public IVariable encodeSquare(IVariable variable) throws ContradictionException {
        return encodeMultiplication(variable, variable);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.sat4j.csp.constraints.encoder.intension.IIntensionConstraintEncoder#encodePower
     * (org.sat4j.csp.variables.IVariable, org.sat4j.csp.variables.IVariable)
     */
    @Override
    public IVariable encodePower(IVariable variable, IVariable exponent)
            throws ContradictionException {
        throw new UnsupportedOperationException();
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.sat4j.csp.constraints.encoder.intension.IIntensionConstraintEncoder#encodePower
     * (org.sat4j.csp.variables.IVariable, org.sat4j.csp.variables.IVariable,
     * org.sat4j.csp.variables.IVariable)
     */
    @Override
    public IVariable encodePower(IVariable pow, IVariable variable, IVariable exponent)
            throws ContradictionException {
        throw new UnsupportedOperationException();
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.constraints.encoder.intension.IIntensionConstraintEncoder#
     * encodeMinimum(org.sat4j.specs.IVec)
     */
    @Override
    public IVariable encodeMinimum(IVec<IVariable> variables) throws ContradictionException {
        var unionDomain = IDomain.unionOf(getDomainsOf(variables));
        var min = unionDomain.newVariableWithThisDomain(solver.getVariableFactory());
        return encodeMinimum(min, variables);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.constraints.encoder.intension.IIntensionConstraintEncoder#
     * encodeMinimum(org.sat4j.csp.variables.IVariable, org.sat4j.specs.IVec)
     */
    @Override
    public IVariable encodeMinimum(IVariable min, IVec<IVariable> variables)
            throws ContradictionException {
        var atLeastOneVariableIsEqual = new VecInt(variables.size());

        for (var it = variables.iterator(); it.hasNext();) {
            var variable = it.next();

            // The minimum is ALWAYS at most equal to each variable.
            // We thus need a hard constraint for that.
            solver.getOrderedEncoder().encodeOrdered(min, RelationalOperator.LE, variable);

            // The minimum is equal to at least one of the variables.
            // We thus need a soft constraint for that.
            var isEqual = encodeEqual(variable, min);
            atLeastOneVariableIsEqual.push(isEqual.getVariables().get(0));
        }

        // We add a clause encoding that at least one variable must be equal to the
        // minimum.
        solver.addClause(atLeastOneVariableIsEqual);
        return min;
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.constraints.encoder.intension.IIntensionConstraintEncoder#
     * encodeMaximum(org.sat4j.specs.IVec)
     */
    @Override
    public IVariable encodeMaximum(IVec<IVariable> variables) throws ContradictionException {
        var unionDomain = IDomain.unionOf(getDomainsOf(variables));
        var max = unionDomain.newVariableWithThisDomain(solver.getVariableFactory());
        return encodeMaximum(max, variables);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.constraints.encoder.intension.IIntensionConstraintEncoder#
     * encodeMaximum(org.sat4j.csp.variables.IVariable, org.sat4j.specs.IVec)
     */
    @Override
    public IVariable encodeMaximum(IVariable max, IVec<IVariable> variables)
            throws ContradictionException {
        var atLeastOneVariableIsEqual = new VecInt(variables.size());

        for (var it = variables.iterator(); it.hasNext();) {
            var variable = it.next();

            // The maximum is ALWAYS at least equal to each variable.
            // We thus need a hard constraint for that.
            solver.getOrderedEncoder().encodeOrdered(max, RelationalOperator.GE, variable);

            // The maximum is equal to at least one of the variables.
            // We thus need a soft constraint for that.
            var isEqual = encodeEqual(variable, max);
            atLeastOneVariableIsEqual.push(isEqual.getVariables().get(0));
        }

        // We add a clause encoding that at least one variable must be equal to the
        // maximum.
        solver.addClause(atLeastOneVariableIsEqual);
        return max;
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.constraints.encoder.intension.IIntensionConstraintEncoder#
     * encodeDistance(org.sat4j.csp.variables.IVariable,
     * org.sat4j.csp.variables.IVariable)
     */
    @Override
    public IVariable encodeDistance(IVariable left, IVariable right) throws ContradictionException {
        return encodeAbsoluteValue(encodeSubtraction(left, right));
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.constraints.encoder.intension.IIntensionConstraintEncoder#
     * encodeDistance(org.sat4j.csp.variables.IVariable,
     * org.sat4j.csp.variables.IVariable, org.sat4j.csp.variables.IVariable)
     */
    @Override
    public IVariable encodeDistance(IVariable dist, IVariable left, IVariable right)
            throws ContradictionException {
        return encodeAbsoluteValue(dist, encodeSubtraction(left, right));
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.constraints.encoder.intension.IIntensionConstraintEncoder#
     * encodeLessThan(org.sat4j.csp.variables.IVariable,
     * org.sat4j.csp.variables.IVariable)
     */
    @Override
    public IVariable encodeLessThan(IVariable left, IVariable right) throws ContradictionException {
        int v = solver.newDimacsVariable();
        solver.getSumEncoder().encodeSoftSum(v, Vec.of(left), RelationalOperator.LT, right);
        return BooleanVariable.of(v);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.constraints.encoder.intension.IIntensionConstraintEncoder#
     * encodeLessOrEqual(org.sat4j.csp.variables.IVariable,
     * org.sat4j.csp.variables.IVariable)
     */
    @Override
    public IVariable encodeLessOrEqual(IVariable left, IVariable right)
            throws ContradictionException {
        int v = solver.newDimacsVariable();
        solver.getSumEncoder().encodeSoftSum(v, Vec.of(left), RelationalOperator.LE, right);
        return BooleanVariable.of(v);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.sat4j.csp.constraints.encoder.intension.IIntensionConstraintEncoder#encodeEqual
     * (org.sat4j.specs.IVec)
     */
    @Override
    public IVariable encodeEqual(IVec<IVariable> variables) throws ContradictionException {
        var selectors = new Vec<IVariable>(variables.size());
        for (int i = 1; i < variables.size(); i++) {
            selectors.push(encodeGreaterOrEqual(variables.get(i - 1), variables.get(i)));
        }
        selectors.push(encodeGreaterOrEqual(variables.get(variables.size() - 1), variables.get(0)));
        return encodeConjunction(selectors);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.constraints.encoder.intension.IIntensionConstraintEncoder#
     * encodeDifferent(org.sat4j.csp.variables.IVariable,
     * org.sat4j.csp.variables.IVariable)
     */
    @Override
    public IVariable encodeDifferent(IVariable left, IVariable right)
            throws ContradictionException {
        return encodeDisjunction(encodeLessThan(left, right), encodeGreaterThan(left, right));
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.constraints.encoder.intension.IIntensionConstraintEncoder#
     * encodeGreaterOrEqual(org.sat4j.csp.variables.IVariable,
     * org.sat4j.csp.variables.IVariable)
     */
    @Override
    public IVariable encodeGreaterOrEqual(IVariable left, IVariable right)
            throws ContradictionException {
        int v = solver.newDimacsVariable();
        solver.getSumEncoder().encodeSoftSum(v, Vec.of(left), RelationalOperator.GE, right);
        return BooleanVariable.of(v);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.constraints.encoder.intension.IIntensionConstraintEncoder#
     * encodeGreaterThan(org.sat4j.csp.variables.IVariable,
     * org.sat4j.csp.variables.IVariable)
     */
    @Override
    public IVariable encodeGreaterThan(IVariable left, IVariable right)
            throws ContradictionException {
        int v = solver.newDimacsVariable();
        solver.getSumEncoder().encodeSoftSum(v, Vec.of(left), RelationalOperator.GT, right);
        return BooleanVariable.of(v);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.sat4j.csp.constraints.encoder.intension.IIntensionConstraintEncoder#encodeNot(
     * org.sat4j.csp.variables.IVariable)
     */
    @Override
    public IVariable encodeNot(IVariable variable) throws ContradictionException {
        return new ShiftedVariableDecorator(variable.negate(), ONE);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.constraints.encoder.intension.IIntensionConstraintEncoder#
     * encodeConjunction(org.sat4j.specs.IVec)
     */
    @Override
    public IVariable encodeConjunction(IVec<IVariable> variables) throws ContradictionException {
        return encodePseudoBoolean(variables, BigInteger.valueOf(variables.size()));
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.constraints.encoder.intension.IIntensionConstraintEncoder#
     * encodeDisjunction(org.sat4j.specs.IVec)
     */
    @Override
    public IVariable encodeDisjunction(IVec<IVariable> variables) throws ContradictionException {
        return encodePseudoBoolean(variables, ONE);
    }

    /**
     * Encodes an {@code intension} constraint that can be represented as a native
     * pseudo-Boolean constraint.
     *
     * @param variables The (Boolean) variables of the constraint to add.
     * @param initialDegree The initial degree of the constraint to add.
     *
     * @return The variable representing the reification of the encoded constraint.
     *
     * @throws ContradictionException If encoding the constraint results in a trivial
     *         inconsistency.
     */
    private IVariable encodePseudoBoolean(IVec<IVariable> variables, BigInteger initialDegree)
            throws ContradictionException {
        var vars = new VecInt(variables.size());
        var coeffs = new Vec<BigInteger>(variables.size());
        var degree = initialDegree;

        // Adding the variables and coefficients of the constraint.
        for (var it = variables.iterator(); it.hasNext();) {
            var variable = it.next();
            variable.getVariables().copyTo(vars);
            variable.getCoefficients().copyTo(coeffs);
            degree = degree.subtract(variable.getShift());
        }

        // Adding the constraint as a (reified) soft constraint.
        int v = solver.newDimacsVariable();
        solver.addSoftPseudoBoolean(v, vars, coeffs, RelationalOperator.GE, degree);
        return BooleanVariable.of(v);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.constraints.encoder.intension.IIntensionConstraintEncoder#
     * encodeExclusiveDisjunction(org.sat4j.specs.IVec)
     */
    @Override
    public IVariable encodeExclusiveDisjunction(IVec<IVariable> variables)
            throws ContradictionException {
        return encodeModulo(encodeAddition(variables), ConstantVariable.TWO);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.constraints.encoder.intension.IIntensionConstraintEncoder#
     * encodeEquivalence(org.sat4j.specs.IVec)
     */
    @Override
    public IVariable encodeEquivalence(IVec<IVariable> variables) throws ContradictionException {
        var selectors = new Vec<IVariable>(variables.size());
        for (int i = 1; i < variables.size(); i++) {
            selectors.push(encodeImplication(variables.get(i - 1), variables.get(i)));
        }
        selectors.push(encodeImplication(variables.get(variables.size() - 1), variables.get(0)));
        return encodeConjunction(selectors);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.constraints.encoder.intension.IIntensionConstraintEncoder#
     * encodeImplication(org.sat4j.csp.variables.IVariable,
     * org.sat4j.csp.variables.IVariable)
     */
    @Override
    public IVariable encodeImplication(IVariable left, IVariable right)
            throws ContradictionException {
        return encodeDisjunction(encodeNot(left), right);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.constraints.encoder.intension.IIntensionConstraintEncoder#
     * encodeIfThenElse(org.sat4j.csp.variables.IVariable,
     * org.sat4j.csp.variables.IVariable, org.sat4j.csp.variables.IVariable)
     */
    @Override
    public IVariable encodeIfThenElse(IVariable condition, IVariable ifTrue, IVariable ifFalse)
            throws ContradictionException {
        var domain = ifTrue.getDomain().union(ifFalse.getDomain());
        var ite = domain.newVariableWithThisDomain(solver.getVariableFactory());
        return encodeIfThenElse(ite, condition, ifTrue, ifFalse);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.constraints.encoder.intension.IIntensionConstraintEncoder#
     * encodeIfThenElse(org.sat4j.csp.variables.IVariable,
     * org.sat4j.csp.variables.IVariable, org.sat4j.csp.variables.IVariable,
     * org.sat4j.csp.variables.IVariable)
     */
    @Override
    public IVariable encodeIfThenElse(IVariable ite, IVariable condition, IVariable ifTrue,
            IVariable ifFalse) throws ContradictionException {
        // Encoding the case when the condition is true.
        var eqIfTrue = encodeEqual(ite, ifTrue);
        var implIfTrue = encodeImplication(condition, eqIfTrue);

        // Encoding the case when the condition is false.
        var eqIfFalse = encodeEqual(ite, ifFalse);
        var implIfFalse = encodeImplication(encodeNot(condition), eqIfFalse);

        // Both implications are enforced.
        solver.addClause(implIfTrue.getVariables());
        solver.addClause(implIfFalse.getVariables());

        // The output variable is that encoding the value of the alternative.
        return ite;
    }

}
