/**
 * Sat4j-CSP, a CSP solver based on the Sat4j platform.
 * Copyright (c) 2021-2022 - Exakis Nelite, Univ Artois & CNRS.
 * All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 * If not, see {@link http://www.gnu.org/licenses}.
 */

package org.sat4j.csp.constraints.encoder.cardinality;

import java.math.BigInteger;

import org.sat4j.csp.constraints.encoder.IConstraintEncoder;
import org.sat4j.csp.variables.IVariable;
import org.sat4j.specs.ContradictionException;
import org.sat4j.specs.IConstr;
import org.sat4j.specs.IVec;

/**
 * The ICardinalityConstraintEncoder defines a strategy for encoding {@code cardinality}
 * constraints using pseudo-Boolean constraints and/or clauses.
 *
 * @author Thibault Falque
 * @author Romain Wallon
 *
 * @version 0.1.0
 */
public interface ICardinalityConstraintEncoder extends IConstraintEncoder {

    /**
     * Encodes a {@code cardinality} constraint using pseudo-Boolean constraints and/or
     * clauses.
     *
     * @param variables The variables to count the assignments of.
     * @param values The assignable values to count.
     * @param occurs The number of times each value can be assigned.
     * @param closed Whether only the values in {@code value} can be assigned to the
     *        variables.
     *
     * @return The created constraint(s), as internally represented by Sat4j.
     *
     * @throws ContradictionException If encoding the constraint results in a trivial
     *         inconsistency.
     */
    IConstr encodeCardinalityWithConstantValuesAndConstantCounts(IVec<IVariable> variables,
            IVec<BigInteger> values, IVec<BigInteger> occurs, boolean closed)
            throws ContradictionException;

    /**
     * Encodes a {@code cardinality} constraint using pseudo-Boolean constraints and/or
     * clauses.
     *
     * @param variables The variables to count the assignments of.
     * @param values The assignable values to count.
     * @param occursMin The minimum number of times each value can be assigned.
     * @param occursMax The maximum number of times each value can be assigned.
     * @param closed Whether only the values in {@code value} can be assigned to the
     *        variables.
     *
     * @return The created constraint(s), as internally represented by Sat4j.
     *
     * @throws ContradictionException If encoding the constraint results in a trivial
     *         inconsistency.
     */
    IConstr encodeCardinalityWithConstantValuesAndConstantIntervalCounts(IVec<IVariable> variables,
            IVec<BigInteger> values, IVec<BigInteger> occursMin, IVec<BigInteger> occursMax,
            boolean closed) throws ContradictionException;

    /**
     * Encodes a {@code cardinality} constraint using pseudo-Boolean constraints and/or
     * clauses.
     *
     * @param variables The variables to count the assignments of.
     * @param values The assignable values to count.
     * @param occurs The variables encoding the number of times each value can be
     *        assigned.
     * @param closed Whether only the values in {@code value} can be assigned to the
     *        variables.
     *
     * @return The created constraint(s), as internally represented by Sat4j.
     *
     * @throws ContradictionException If encoding the constraint results in a trivial
     *         inconsistency.
     */
    IConstr encodeCardinalityWithConstantValuesAndVariableCounts(IVec<IVariable> variables,
            IVec<BigInteger> values, IVec<IVariable> occurs, boolean closed)
            throws ContradictionException;

    /**
     * Encodes a {@code cardinality} constraint using pseudo-Boolean constraints and/or
     * clauses.
     *
     * @param variables The variables to count the assignments of.
     * @param values The variables encoding the assignable values to count.
     * @param occurs The number of times each value can be assigned.
     * @param closed Whether only the values in {@code value} can be assigned to the
     *        variables.
     *
     * @return The created constraint(s), as internally represented by Sat4j.
     *
     * @throws ContradictionException If encoding the constraint results in a trivial
     *         inconsistency.
     */
    IConstr encodeCardinalityWithVariableValuesAndConstantCounts(IVec<IVariable> variables,
            IVec<IVariable> values, IVec<BigInteger> occurs, boolean closed)
            throws ContradictionException;

    /**
     * Encodes a {@code cardinality} constraint using pseudo-Boolean constraints and/or
     * clauses.
     *
     * @param variables The variables to count the assignments of.
     * @param values The variables encoding the assignable values to count.
     * @param occursMin The minimum number of times each value can be assigned.
     * @param occursMax The maximum number of times each value can be assigned.
     * @param closed Whether only the values in {@code value} can be assigned to the
     *        variables.
     *
     * @return The created constraint(s), as internally represented by Sat4j.
     *
     * @throws ContradictionException If encoding the constraint results in a trivial
     *         inconsistency.
     */
    IConstr encodeCardinalityWithVariableValuesAndConstantIntervalCounts(IVec<IVariable> variables,
            IVec<IVariable> values, IVec<BigInteger> occursMin, IVec<BigInteger> occursMax,
            boolean closed) throws ContradictionException;

    /**
     * Encodes a {@code cardinality} constraint using pseudo-Boolean constraints and/or
     * clauses.
     *
     * @param variables The variables to count the assignments of.
     * @param values The variables encoding the assignable values to count.
     * @param occurs The variables encoding the number of times each value can be
     *        assigned.
     * @param closed Whether only the values in {@code value} can be assigned to the
     *        variables.
     *
     * @return The created constraint(s), as internally represented by Sat4j.
     *
     * @throws ContradictionException If encoding the constraint results in a trivial
     *         inconsistency.
     */
    IConstr encodeCardinalityWithVariableValuesAndVariableCounts(IVec<IVariable> variables,
            IVec<IVariable> values, IVec<IVariable> occurs, boolean closed)
            throws ContradictionException;

}
