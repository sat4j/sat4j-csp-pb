/**
 * Sat4j-CSP, a CSP solver based on the Sat4j platform.
 * Copyright (c) 2021-2022 - Exakis Nelite, Univ Artois & CNRS.
 * All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 * If not, see {@link http://www.gnu.org/licenses}.
 */

package org.sat4j.csp.constraints.encoder.intension;

import java.math.BigInteger;

import org.sat4j.core.ConstrGroup;
import org.sat4j.core.Vec;
import org.sat4j.core.VecInt;
import org.sat4j.csp.constraints.ArithmeticOperator;
import org.sat4j.csp.constraints.RelationalOperator;
import org.sat4j.csp.constraints.SetBelongingOperator;
import org.sat4j.csp.constraints.encoder.AbstractConstraintEncoder;
import org.sat4j.csp.variables.ConstantVariable;
import org.sat4j.csp.variables.IVariable;
import org.sat4j.specs.ContradictionException;
import org.sat4j.specs.FakeConstr;
import org.sat4j.specs.IConstr;
import org.sat4j.specs.IVec;

/**
 * The DefaultPrimitiveConstraintEncoder provides a base encoding for {@code primitive}
 * constraints using pseudo-Boolean constraints and/or clauses.
 *
 * @author Thibault Falque
 * @author Romain Wallon
 *
 * @version 0.1.0
 */
public class DefaultPrimitiveConstraintEncoder extends AbstractConstraintEncoder
        implements IPrimitiveConstraintEncoder {

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.constraints.encoder.intension.IPrimitiveConstraintEncoder#
     * encodePrimitive(org.sat4j.csp.variables.IVariable,
     * org.sat4j.csp.constraints.RelationalOperator, java.math.BigInteger)
     */
    @Override
    public IConstr encodePrimitive(IVariable variable, RelationalOperator operator,
            BigInteger value) throws ContradictionException {
        var sumEncoder = solver.getSumEncoder();
        return sumEncoder.encodeSum(Vec.of(variable), operator, value);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.constraints.encoder.intension.IPrimitiveConstraintEncoder#
     * encodePrimitive(org.sat4j.csp.variables.IVariable,
     * org.sat4j.csp.constraints.ArithmeticOperator, java.math.BigInteger,
     * org.sat4j.csp.constraints.RelationalOperator, java.math.BigInteger)
     */
    @Override
    public IConstr encodePrimitive(IVariable variable, ArithmeticOperator arithOp,
            BigInteger leftHandSide, RelationalOperator relOp, BigInteger rightHandSide)
            throws ContradictionException {
        return encodePrimitive(variable, arithOp, ConstantVariable.of(leftHandSide), relOp,
                ConstantVariable.of(rightHandSide));
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.constraints.encoder.intension.IPrimitiveConstraintEncoder#
     * encodePrimitive(org.sat4j.csp.variables.IVariable,
     * org.sat4j.csp.constraints.ArithmeticOperator, org.sat4j.csp.variables.IVariable,
     * org.sat4j.csp.constraints.RelationalOperator, java.math.BigInteger)
     */
    @Override
    public IConstr encodePrimitive(IVariable variable, ArithmeticOperator arithOp,
            IVariable leftHandSide, RelationalOperator relOp, BigInteger rightHandSide)
            throws ContradictionException {
        return encodePrimitive(variable, arithOp, leftHandSide, relOp,
                ConstantVariable.of(rightHandSide));
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.constraints.encoder.intension.IPrimitiveConstraintEncoder#
     * encodePrimitive(org.sat4j.csp.variables.IVariable,
     * org.sat4j.csp.constraints.ArithmeticOperator, java.math.BigInteger,
     * org.sat4j.csp.constraints.RelationalOperator, org.sat4j.csp.variables.IVariable)
     */
    @Override
    public IConstr encodePrimitive(IVariable variable, ArithmeticOperator arithOp,
            BigInteger leftHandSide, RelationalOperator relOp, IVariable rightHandSide)
            throws ContradictionException {
        return encodePrimitive(variable, arithOp, ConstantVariable.of(leftHandSide), relOp,
                rightHandSide);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.constraints.encoder.intension.IPrimitiveConstraintEncoder#
     * encodePrimitive(org.sat4j.csp.variables.IVariable,
     * org.sat4j.csp.constraints.ArithmeticOperator, org.sat4j.csp.variables.IVariable,
     * org.sat4j.csp.constraints.RelationalOperator, org.sat4j.csp.variables.IVariable)
     */
    @Override
    public IConstr encodePrimitive(IVariable variable, ArithmeticOperator arithOp,
            IVariable leftHandSide, RelationalOperator relOp, IVariable rightHandSide)
            throws ContradictionException {
        var intensionEncoder = solver.getIntensionEncoder();
        var orderedEncoder = solver.getOrderedEncoder();
        var sumEncoder = solver.getSumEncoder();

        switch (arithOp) {
            case ADD:
                return sumEncoder.encodeSum(Vec.of(variable, leftHandSide), relOp, rightHandSide);

            case SUB:
                return sumEncoder.encodeSum(
                        Vec.of(variable, leftHandSide.negate()), relOp, rightHandSide);

            case MULT:
                var multiplicationEncoder = solver.getMultiplicationEncoder();
                var multipliedVariable = multiplicationEncoder.encodeMultiplication(variable,
                        leftHandSide);
                return sumEncoder.encodeSum(Vec.of(multipliedVariable), relOp, rightHandSide);

            case DIV:
                if (relOp == RelationalOperator.EQ) {
                    intensionEncoder.encodeDivision(rightHandSide, variable, leftHandSide);
                    return FakeConstr.instance();
                }

                var div = intensionEncoder.encodeDivision(variable, leftHandSide);
                return orderedEncoder.encodeOrdered(div, relOp, rightHandSide);

            case MOD:
                if (relOp == RelationalOperator.EQ) {
                    intensionEncoder.encodeModulo(rightHandSide, variable, leftHandSide);
                    return FakeConstr.instance();
                }

                var mod = intensionEncoder.encodeModulo(variable, leftHandSide);
                return orderedEncoder.encodeOrdered(mod, relOp, rightHandSide);

            case POW:
                if (relOp == RelationalOperator.EQ) {
                    intensionEncoder.encodePower(rightHandSide, variable, leftHandSide);
                    return FakeConstr.instance();
                }

                var pow = intensionEncoder.encodePower(variable, leftHandSide);
                return orderedEncoder.encodeOrdered(pow, relOp, rightHandSide);

            case DIST:
                if (relOp == RelationalOperator.EQ) {
                    intensionEncoder.encodeDistance(rightHandSide, variable, leftHandSide);
                    return FakeConstr.instance();
                }

                var dist = intensionEncoder.encodeDistance(variable, leftHandSide);
                return orderedEncoder.encodeOrdered(dist, relOp, rightHandSide);

            default:
                throw new UnsupportedOperationException("Unknown primitive operator: " + arithOp);
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.constraints.encoder.intension.IPrimitiveConstraintEncoder#
     * encodePrimitive(org.sat4j.csp.constraints.ArithmeticOperator,
     * org.sat4j.csp.variables.IVariable, org.sat4j.csp.variables.IVariable)
     */
    @Override
    public IConstr encodePrimitive(ArithmeticOperator arithOp, IVariable variable,
            IVariable rightHandSide) throws ContradictionException {
        var sumEncoder = solver.getSumEncoder();
        var intensionEncoder = solver.getIntensionEncoder();

        if (arithOp == ArithmeticOperator.ABS) {
            intensionEncoder.encodeAbsoluteValue(rightHandSide, variable);
            return FakeConstr.instance();
        }

        if (arithOp == ArithmeticOperator.NEG) {
            return sumEncoder.encodeSum(Vec.of(variable, rightHandSide),
                    Vec.of(BigInteger.ONE, BigInteger.ONE.negate()),
                    RelationalOperator.EQ, BigInteger.ZERO);
        }

        if (arithOp == ArithmeticOperator.SQR) {
            var multiplicationEncoder = solver.getMultiplicationEncoder();
            var multipliedVariable = multiplicationEncoder.encodeMultiplication(variable, variable);
            return sumEncoder.encodeSum(
                    Vec.of(multipliedVariable), RelationalOperator.EQ, rightHandSide);
        }

        throw new UnsupportedOperationException("Unknown unary operator: " + arithOp);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.constraints.encoder.intension.IPrimitiveConstraintEncoder#
     * encodePrimitive(org.sat4j.csp.variables.IVariable,
     * org.sat4j.csp.constraints.SetBelongingOperator, org.sat4j.specs.IVec)
     */
    @Override
    public IConstr encodePrimitive(IVariable variable, SetBelongingOperator operator,
            IVec<BigInteger> values) throws ContradictionException {
        var extensionEncoder = solver.getExtensionEncoder();

        if (operator == SetBelongingOperator.IN) {
            // The given values are allowed.
            return extensionEncoder.encodeSupport(variable, values);
        }

        // The given values are forbidden.
        return extensionEncoder.encodeConflicts(variable, values);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.constraints.encoder.intension.IPrimitiveConstraintEncoder#
     * encodePrimitive(org.sat4j.csp.variables.IVariable,
     * org.sat4j.csp.constraints.SetBelongingOperator, java.math.BigInteger,
     * java.math.BigInteger)
     */
    @Override
    public IConstr encodePrimitive(IVariable variable, SetBelongingOperator operator,
            BigInteger min, BigInteger max) throws ContradictionException {
        var sumEncoder = solver.getSumEncoder();

        if (operator == SetBelongingOperator.IN) {
            // The given values are allowed.
            var group = new ConstrGroup();
            group.add(sumEncoder.encodeSum(Vec.of(variable), RelationalOperator.GE, min));
            group.add(sumEncoder.encodeSum(Vec.of(variable), RelationalOperator.LE, max));
            return group;
        }

        // The given values are forbidden.
        var group = new ConstrGroup();
        var lesser = solver.newDimacsVariable();
        group.add(sumEncoder.encodeSoftSum(lesser, Vec.of(variable), RelationalOperator.LT, min));
        var greater = solver.newDimacsVariable();
        group.add(sumEncoder.encodeSoftSum(greater, Vec.of(variable), RelationalOperator.GT, max));
        group.add(solver.addClause(VecInt.of(lesser, greater)));
        return group;
    }

}
