/**
 * Sat4j-CSP, a CSP solver based on the Sat4j platform.
 * Copyright (c) 2021-2022 - Exakis Nelite, Univ Artois & CNRS.
 * All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 * If not, see {@link http://www.gnu.org/licenses}.
 */

package org.sat4j.csp.constraints.encoder;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.OptionalInt;

import org.sat4j.csp.core.ICSPSolver;
import org.sat4j.csp.utils.UncheckedContradictionException;
import org.sat4j.csp.variables.IDomain;
import org.sat4j.csp.variables.IVariable;
import org.sat4j.specs.ContradictionException;
import org.sat4j.specs.IVec;

/**
 * The AbstractConstraintEncoder provides a base class for all implementations of
 * {@link IConstraintEncoder}.
 *
 * @author Thibault Falque
 * @author Romain Wallon
 *
 * @version 0.1.0
 */
public abstract class AbstractConstraintEncoder implements IConstraintEncoder {

    /**
     * The solver in which to add the encoding of the constraints.
     */
    protected ICSPSolver solver;

    /*
     * (non-Javadoc)
     *
     * @see
     * org.sat4j.csp.constraints.encoder.IConstraintEncoder#setSolver(org.sat4j.csp.core.
     * ICSPSolver)
     */
    @Override
    public void setSolver(ICSPSolver solver) {
        this.solver = solver;
    }

    /**
     * Encodes the assignment of a value to a variable as a single literal.
     *
     * @param variable The variable to assign.
     * @param value The value to assign.
     *
     * @return The literal encoding the assignment.
     */
    protected final OptionalInt assignmentAsLiteral(IVariable variable, BigInteger value) {
        return variable.getLiteralForValue(value);
    }

    /**
     * Encodes the assignment of a value to a variable as a single literal.
     *
     * @param variable The variable to assign.
     * @param value The variable representing the value to assign.
     *
     * @return The literal encoding the assignment.
     */
    protected final OptionalInt assignmentAsLiteral(IVariable variable, IVariable value) {
        try {
            var literal = solver.getIntensionEncoder().encodeEqual(value, variable);
            return OptionalInt.of(literal.getVariables().get(0));
        } catch (ContradictionException e) {
            throw new UncheckedContradictionException(e);
        }
    }

    /**
     * Computes the list of all the domains of the given variables.
     *
     * @param variables The variables to get the domains of.
     *
     * @return The list of the domains.
     */
    protected static List<IDomain> getDomainsOf(IVec<IVariable> variables) {
        var allDomains = new ArrayList<IDomain>(variables.size());
        for (var it = variables.iterator(); it.hasNext();) {
            allDomains.add(it.next().getDomain());
        }
        return allDomains;
    }

}
