/**
 * Sat4j-CSP, a CSP solver based on the Sat4j platform.
 * Copyright (c) 2021-2022 - Exakis Nelite, Univ Artois & CNRS.
 * All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 * If not, see {@link http://www.gnu.org/licenses}.
 */

package org.sat4j.csp.constraints.encoder.cumulative;

import java.math.BigInteger;

import org.sat4j.core.ConstrGroup;
import org.sat4j.core.Vec;
import org.sat4j.core.VecInt;
import org.sat4j.csp.constraints.RelationalOperator;
import org.sat4j.csp.constraints.encoder.AbstractConstraintEncoder;
import org.sat4j.csp.variables.BooleanVariable;
import org.sat4j.csp.variables.ConstantVariable;
import org.sat4j.csp.variables.IDomain;
import org.sat4j.csp.variables.IVariable;
import org.sat4j.specs.ContradictionException;
import org.sat4j.specs.IConstr;
import org.sat4j.specs.IVec;
import org.sat4j.specs.IVecInt;

/**
 * The DefaultCumulativeConstraintEncoder provides a base encoding for {@code cumulative}
 * constraints using pseudo-Boolean constraints and/or clauses.
 *
 * @author Thibault Falque
 * @author Romain Wallon
 *
 * @version 0.1.0
 */
public class DefaultCumulativeConstraintEncoder extends AbstractConstraintEncoder
        implements ICumulativeConstraintEncoder {
    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.constraints.encoder.cumulative.ICumulativeConstraintEncoder#
     * encodeCumulativeConstantLengthsConstantHeights(org.sat4j.specs.IVec,
     * org.sat4j.specs.IVec, org.sat4j.specs.IVec,
     * org.sat4j.csp.constraints.RelationalOperator, java.math.BigInteger)
     */
    @Override
    public IConstr encodeCumulativeConstantLengthsConstantHeights(IVec<IVariable> origins,
            IVec<BigInteger> lengths, IVec<BigInteger> heights, RelationalOperator operator,
            BigInteger value) throws ContradictionException {

        IDomain unionDomain = IDomain.unionOf(getDomainsOf(origins));
        ConstrGroup group = new ConstrGroup();
        for (var v : unionDomain) {
            IVecInt literals = new VecInt();
            for (int i = 0; i < origins.size(); i++) {
                var o = origins.get(i);
                int s1 = solver.newDimacsVariable();
                group.add(solver.getSumEncoder().encodeSoftSum(s1, Vec.of(o), RelationalOperator.LE,
                        v));
                int s2 = solver.newDimacsVariable();
                group.add(solver.getSumEncoder().encodeSoftSum(s2, Vec.of(o),
                        Vec.of(BigInteger.ONE.negate()), RelationalOperator.LT,
                        lengths.get(i).subtract(v)));
                int s3 = solver.newDimacsVariable();
                group.add(solver.addSoftAtLeast(s3, VecInt.of(s1, s2), 2));
                literals.push(s3);
            }
            group.add(solver.addPseudoBoolean(literals, heights, operator, value));
        }
        return group;
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.constraints.encoder.cumulative.ICumulativeConstraintEncoder#
     * encodeCumulativeConstantLengthsConstantHeights(org.sat4j.specs.IVec,
     * org.sat4j.specs.IVec, org.sat4j.specs.IVec, org.sat4j.specs.IVec,
     * org.sat4j.csp.constraints.RelationalOperator, java.math.BigInteger)
     */
    @Override
    public IConstr encodeCumulativeConstantLengthsConstantHeights(IVec<IVariable> origins,
            IVec<BigInteger> lengths, IVec<IVariable> ends, IVec<BigInteger> heights,
            RelationalOperator operator, BigInteger value) throws ContradictionException {
        ConstrGroup group = new ConstrGroup();
        group.add(encodeCumulativeConstantLengthsConstantHeights(origins, lengths, heights,
                operator, value));
        for (int i = 0; i < origins.size(); i++) {
            group.add(solver.getSumEncoder().encodeSum(Vec.of(ends.get(i), origins.get(i)),
                    Vec.of(BigInteger.ONE, BigInteger.ONE.negate()), RelationalOperator.EQ,
                    lengths.get(i)));
        }
        return group;
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.constraints.encoder.cumulative.ICumulativeConstraintEncoder#
     * encodeCumulativeConstantLengthsConstantHeights(org.sat4j.specs.IVec,
     * org.sat4j.specs.IVec, org.sat4j.specs.IVec,
     * org.sat4j.csp.constraints.RelationalOperator, org.sat4j.csp.variables.IVariable)
     */
    @Override
    public IConstr encodeCumulativeConstantLengthsConstantHeights(IVec<IVariable> origins,
            IVec<BigInteger> lengths, IVec<BigInteger> heights, RelationalOperator operator,
            IVariable value) throws ContradictionException {
        IDomain unionDomain = IDomain.unionOf(getDomainsOf(origins));
        ConstrGroup group = new ConstrGroup();
        for (var v : unionDomain) {
            IVecInt literals = new VecInt();
            IVec<BigInteger> coeffs = new Vec<>();
            for (int i = 0; i < origins.size(); i++) {
                var o = origins.get(i);
                int s1 = solver.newDimacsVariable();
                group.add(solver.getSumEncoder().encodeSoftSum(s1, Vec.of(o), RelationalOperator.LE,
                        v));
                int s2 = solver.newDimacsVariable();
                group.add(solver.getSumEncoder().encodeSoftSum(s2, Vec.of(o),
                        Vec.of(BigInteger.ONE.negate()), RelationalOperator.LT,
                        lengths.get(i).subtract(v)));
                int s3 = solver.newDimacsVariable();
                group.add(solver.addSoftAtLeast(s3, VecInt.of(s1, s2), 2));
                literals.push(s3);
                coeffs.push(heights.get(i));
            }
            for (int i = 0; i < value.nVars(); i++) {
                literals.push(value.getVariables().get(i));
                coeffs.push(value.getCoefficients().get(i).negate());
            }
            group.add(solver.addPseudoBoolean(literals, coeffs, operator, BigInteger.ZERO));
        }
        return group;
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.constraints.encoder.cumulative.ICumulativeConstraintEncoder#
     * encodeCumulativeConstantLengthsConstantHeights(org.sat4j.specs.IVec,
     * org.sat4j.specs.IVec, org.sat4j.specs.IVec, org.sat4j.specs.IVec,
     * org.sat4j.csp.constraints.RelationalOperator, org.sat4j.csp.variables.IVariable)
     */
    @Override
    public IConstr encodeCumulativeConstantLengthsConstantHeights(IVec<IVariable> origins,
            IVec<BigInteger> lengths, IVec<IVariable> ends, IVec<BigInteger> heights,
            RelationalOperator operator, IVariable value) throws ContradictionException {
        ConstrGroup group = new ConstrGroup();
        group.add(encodeCumulativeConstantLengthsConstantHeights(origins, lengths, heights,
                operator, value));
        for (int i = 0; i < origins.size(); i++) {
            group.add(solver.getSumEncoder().encodeSum(Vec.of(ends.get(i), origins.get(i)),
                    Vec.of(BigInteger.ONE, BigInteger.ONE.negate()), RelationalOperator.EQ,
                    lengths.get(i)));
        }
        return group;
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.constraints.encoder.cumulative.ICumulativeConstraintEncoder#
     * encodeCumulativeConstantLengthsVariableHeights(org.sat4j.specs.IVec,
     * org.sat4j.specs.IVec, org.sat4j.specs.IVec,
     * org.sat4j.csp.constraints.RelationalOperator, java.math.BigInteger)
     */
    @Override
    public IConstr encodeCumulativeConstantLengthsVariableHeights(IVec<IVariable> origins,
            IVec<BigInteger> lengths, IVec<IVariable> heights, RelationalOperator operator,
            BigInteger value) throws ContradictionException {
        IDomain unionDomain = IDomain.unionOf(getDomainsOf(origins));
        ConstrGroup group = new ConstrGroup();
        for (var v : unionDomain) {
            IVec<IVariable> literals = new Vec<>();
            for (int i = 0; i < origins.size(); i++) {
                var o = origins.get(i);
                int s1 = solver.newDimacsVariable();
                group.add(solver.getSumEncoder().encodeSoftSum(s1, Vec.of(o), RelationalOperator.LE,
                        v));
                int s2 = solver.newDimacsVariable();
                group.add(solver.getSumEncoder().encodeSoftSum(s2, Vec.of(o),
                        Vec.of(BigInteger.ONE.negate()), RelationalOperator.LT,
                        lengths.get(i).subtract(v)));
                int s3 = solver.newDimacsVariable();
                group.add(solver.addSoftAtLeast(s3, VecInt.of(s1, s2), 2));
                literals.push(BooleanVariable.of(s3));
            }
            group.add(solver.getSumEncoder().encodeSum(
                    solver.getMultiplicationEncoder().encodeVariableMultiplications(literals,
                            heights),
                    operator, value));
        }
        return group;
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.constraints.encoder.cumulative.ICumulativeConstraintEncoder#
     * encodeCumulativeConstantLengthsVariableHeights(org.sat4j.specs.IVec,
     * org.sat4j.specs.IVec, org.sat4j.specs.IVec, org.sat4j.specs.IVec,
     * org.sat4j.csp.constraints.RelationalOperator, java.math.BigInteger)
     */
    @Override
    public IConstr encodeCumulativeConstantLengthsVariableHeights(IVec<IVariable> origins,
            IVec<BigInteger> lengths, IVec<IVariable> ends, IVec<IVariable> heights,
            RelationalOperator operator, BigInteger value) throws ContradictionException {
        ConstrGroup group = new ConstrGroup();
        group.add(encodeCumulativeConstantLengthsVariableHeights(origins, lengths, heights,
                operator, value));
        for (int i = 0; i < origins.size(); i++) {
            group.add(solver.getSumEncoder().encodeSum(Vec.of(ends.get(i), origins.get(i)),
                    Vec.of(BigInteger.ONE, BigInteger.ONE.negate()), RelationalOperator.EQ,
                    lengths.get(i)));
        }
        return group;
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.constraints.encoder.cumulative.ICumulativeConstraintEncoder#
     * encodeCumulativeConstantLengthsVariableHeights(org.sat4j.specs.IVec,
     * org.sat4j.specs.IVec, org.sat4j.specs.IVec,
     * org.sat4j.csp.constraints.RelationalOperator, org.sat4j.csp.variables.IVariable)
     */
    @Override
    public IConstr encodeCumulativeConstantLengthsVariableHeights(IVec<IVariable> origins,
            IVec<BigInteger> lengths, IVec<IVariable> heights, RelationalOperator operator,
            IVariable value) throws ContradictionException {
        IDomain unionDomain = IDomain.unionOf(getDomainsOf(origins));
        ConstrGroup group = new ConstrGroup();
        for (var v : unionDomain) {
            IVec<IVariable> literals = new Vec<>();
            IVec<IVariable> coeffs = new Vec<>();
            for (int i = 0; i < origins.size(); i++) {
                var o = origins.get(i);
                int s1 = solver.newDimacsVariable();
                group.add(solver.getSumEncoder().encodeSoftSum(s1, Vec.of(o), RelationalOperator.LE,
                        v));
                int s2 = solver.newDimacsVariable();
                group.add(solver.getSumEncoder().encodeSoftSum(s2, Vec.of(o),
                        Vec.of(BigInteger.ONE.negate()), RelationalOperator.LT,
                        lengths.get(i).subtract(v)));
                int s3 = solver.newDimacsVariable();
                group.add(solver.addSoftAtLeast(s3, VecInt.of(s1, s2), 2));
                literals.push(BooleanVariable.of(s3));
                coeffs.push(heights.get(i));
            }
            for (int i = 0; i < value.nVars(); i++) {
                literals.push(BooleanVariable.of(value.getVariables().get(i)));
                coeffs.push(ConstantVariable.of(value.getCoefficients().get(i).negate()));
            }
            group.add(solver.getSumEncoder().encodeSum(
                    solver.getMultiplicationEncoder().encodeVariableMultiplications(literals,
                            heights),
                    operator, value));
        }
        return group;
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.constraints.encoder.cumulative.ICumulativeConstraintEncoder#
     * encodeCumulativeConstantLengthsVariableHeights(org.sat4j.specs.IVec,
     * org.sat4j.specs.IVec, org.sat4j.specs.IVec, org.sat4j.specs.IVec,
     * org.sat4j.csp.constraints.RelationalOperator, org.sat4j.csp.variables.IVariable)
     */
    @Override
    public IConstr encodeCumulativeConstantLengthsVariableHeights(IVec<IVariable> origins,
            IVec<BigInteger> lengths, IVec<IVariable> ends, IVec<IVariable> heights,
            RelationalOperator operator, IVariable value) throws ContradictionException {
        ConstrGroup group = new ConstrGroup();
        group.add(encodeCumulativeConstantLengthsVariableHeights(origins, lengths, heights,
                operator, value));
        for (int i = 0; i < origins.size(); i++) {
            group.add(solver.getSumEncoder().encodeSum(Vec.of(ends.get(i), origins.get(i)),
                    Vec.of(BigInteger.ONE, BigInteger.ONE.negate()), RelationalOperator.EQ,
                    lengths.get(i)));
        }
        return group;
    }
    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.constraints.encoder.cumulative.ICumulativeConstraintEncoder#
     * encodeCumulativeVariableLengthsConstantHeights(org.sat4j.specs.IVec,
     * org.sat4j.specs.IVec, org.sat4j.specs.IVec,
     * org.sat4j.csp.constraints.RelationalOperator, java.math.BigInteger)
     */
    @Override
    public IConstr encodeCumulativeVariableLengthsConstantHeights(IVec<IVariable> origins,
            IVec<IVariable> lengths, IVec<BigInteger> heights, RelationalOperator operator,
            BigInteger value) throws ContradictionException {

        IDomain unionDomain = IDomain.unionOf(getDomainsOf(origins));
        ConstrGroup group = new ConstrGroup();
        for (var v : unionDomain) {
            IVecInt literals = new VecInt();
            for (int i = 0; i < origins.size(); i++) {
                var o = origins.get(i);
                int s1 = solver.newDimacsVariable();
                group.add(solver.getSumEncoder().encodeSoftSum(s1, Vec.of(o), RelationalOperator.LE,
                        v));
                int s2 = solver.newDimacsVariable();
                group.add(solver.getSumEncoder().encodeSoftSum(s2, Vec.of(o,lengths.get(i)),
                        Vec.of(BigInteger.ONE,BigInteger.ONE), RelationalOperator.GT,
                        v));
                int s3 = solver.newDimacsVariable();
                group.add(solver.addSoftAtLeast(s3, VecInt.of(s1, s2), 2));
                literals.push(s3);
            }
            group.add(solver.addPseudoBoolean(literals, heights, operator, value));
        }
        return group;
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.constraints.encoder.cumulative.ICumulativeConstraintEncoder#
     * encodeCumulativeVariableLengthsConstantHeights(org.sat4j.specs.IVec,
     * org.sat4j.specs.IVec, org.sat4j.specs.IVec, org.sat4j.specs.IVec,
     * org.sat4j.csp.constraints.RelationalOperator, java.math.BigInteger)
     */
    @Override
    public IConstr encodeCumulativeVariableLengthsConstantHeights(IVec<IVariable> origins,
            IVec<IVariable> lengths, IVec<IVariable> ends, IVec<BigInteger> heights,
            RelationalOperator operator, BigInteger value) throws ContradictionException {
        ConstrGroup group = new ConstrGroup();
        group.add(encodeCumulativeVariableLengthsConstantHeights(origins, lengths, heights,
                operator, value));
        for (int i = 0; i < origins.size(); i++) {
            group.add(solver.getSumEncoder().encodeSum(Vec.of(ends.get(i), origins.get(i)),
                    Vec.of(BigInteger.ONE, BigInteger.ONE.negate()), RelationalOperator.EQ,
                    lengths.get(i)));
        }
        return group;
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.constraints.encoder.cumulative.ICumulativeConstraintEncoder#
     * encodeCumulativeVariableLengthsConstantHeights(org.sat4j.specs.IVec,
     * org.sat4j.specs.IVec, org.sat4j.specs.IVec,
     * org.sat4j.csp.constraints.RelationalOperator, org.sat4j.csp.variables.IVariable)
     */
    @Override
    public IConstr encodeCumulativeVariableLengthsConstantHeights(IVec<IVariable> origins,
            IVec<IVariable> lengths, IVec<BigInteger> heights, RelationalOperator operator,
            IVariable value) throws ContradictionException {
        IDomain unionDomain = IDomain.unionOf(getDomainsOf(origins));
        ConstrGroup group = new ConstrGroup();
        for (var v : unionDomain) {
            IVecInt literals = new VecInt();
            IVec<BigInteger> coeffs = new Vec<>();
            for (int i = 0; i < origins.size(); i++) {
                var o = origins.get(i);
                int s1 = solver.newDimacsVariable();
                group.add(solver.getSumEncoder().encodeSoftSum(s1, Vec.of(o), RelationalOperator.LE,
                        v));
                int s2 = solver.newDimacsVariable();
                group.add(solver.getSumEncoder().encodeSoftSum(s2, Vec.of(o,lengths.get(i)),
                        Vec.of(BigInteger.ONE,BigInteger.ONE), RelationalOperator.GT,
                        v));
                int s3 = solver.newDimacsVariable();
                group.add(solver.addSoftAtLeast(s3, VecInt.of(s1, s2), 2));
                literals.push(s3);
                coeffs.push(heights.get(i));
            }
            for (int i = 0; i < value.nVars(); i++) {
                literals.push(value.getVariables().get(i));
                coeffs.push(value.getCoefficients().get(i).negate());
            }
            group.add(solver.addPseudoBoolean(literals, coeffs, operator, BigInteger.ZERO));
        }
        return group;
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.constraints.encoder.cumulative.ICumulativeConstraintEncoder#
     * encodeCumulativeVariableLengthsConstantHeights(org.sat4j.specs.IVec,
     * org.sat4j.specs.IVec, org.sat4j.specs.IVec, org.sat4j.specs.IVec,
     * org.sat4j.csp.constraints.RelationalOperator, org.sat4j.csp.variables.IVariable)
     */
    @Override
    public IConstr encodeCumulativeVariableLengthsConstantHeights(IVec<IVariable> origins,
            IVec<IVariable> lengths, IVec<IVariable> ends, IVec<BigInteger> heights,
            RelationalOperator operator, IVariable value) throws ContradictionException {
        ConstrGroup group = new ConstrGroup();
        group.add(encodeCumulativeVariableLengthsConstantHeights(origins, lengths, heights,
                operator, value));
        for (int i = 0; i < origins.size(); i++) {
            group.add(solver.getSumEncoder().encodeSum(Vec.of(ends.get(i), origins.get(i)),
                    Vec.of(BigInteger.ONE, BigInteger.ONE.negate()), RelationalOperator.EQ,
                    lengths.get(i)));
        }
        return group;
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.constraints.encoder.cumulative.ICumulativeConstraintEncoder#
     * encodeCumulativeVariableLengthsVariableHeights(org.sat4j.specs.IVec,
     * org.sat4j.specs.IVec, org.sat4j.specs.IVec,
     * org.sat4j.csp.constraints.RelationalOperator, java.math.BigInteger)
     */
    @Override
    public IConstr encodeCumulativeVariableLengthsVariableHeights(IVec<IVariable> origins,
            IVec<IVariable> lengths, IVec<IVariable> heights, RelationalOperator operator,
            BigInteger value) throws ContradictionException {
        IDomain unionDomain = IDomain.unionOf(getDomainsOf(origins));
        ConstrGroup group = new ConstrGroup();
        for (var v : unionDomain) {
            IVec<IVariable> literals = new Vec<>();
            for (int i = 0; i < origins.size(); i++) {
                var o = origins.get(i);
                int s1 = solver.newDimacsVariable();
                group.add(solver.getSumEncoder().encodeSoftSum(s1, Vec.of(o), RelationalOperator.LE,
                        v));
                int s2 = solver.newDimacsVariable();
                group.add(solver.getSumEncoder().encodeSoftSum(s2, Vec.of(o,lengths.get(i)),
                        Vec.of(BigInteger.ONE,BigInteger.ONE), RelationalOperator.GT,
                        v));
                int s3 = solver.newDimacsVariable();
                group.add(solver.addSoftAtLeast(s3, VecInt.of(s1, s2), 2));
                literals.push(BooleanVariable.of(s3));
            }
            group.add(solver.getSumEncoder().encodeSum(
                    solver.getMultiplicationEncoder().encodeVariableMultiplications(literals,
                            heights),
                    operator, value));
        }
        return group;
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.constraints.encoder.cumulative.ICumulativeConstraintEncoder#
     * encodeCumulativeVariableLengthsVariableHeights(org.sat4j.specs.IVec,
     * org.sat4j.specs.IVec, org.sat4j.specs.IVec, org.sat4j.specs.IVec,
     * org.sat4j.csp.constraints.RelationalOperator, java.math.BigInteger)
     */
    @Override
    public IConstr encodeCumulativeVariableLengthsVariableHeights(IVec<IVariable> origins,
            IVec<IVariable> lengths, IVec<IVariable> ends, IVec<IVariable> heights,
            RelationalOperator operator, BigInteger value) throws ContradictionException {
        ConstrGroup group = new ConstrGroup();
        group.add(encodeCumulativeVariableLengthsVariableHeights(origins, lengths, heights,
                operator, value));
        for (int i = 0; i < origins.size(); i++) {
            group.add(solver.getSumEncoder().encodeSum(Vec.of(ends.get(i), origins.get(i)),
                    Vec.of(BigInteger.ONE, BigInteger.ONE.negate()), RelationalOperator.EQ,
                    lengths.get(i)));
        }
        return group;
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.constraints.encoder.cumulative.ICumulativeConstraintEncoder#
     * encodeCumulativeVariableLengthsVariableHeights(org.sat4j.specs.IVec,
     * org.sat4j.specs.IVec, org.sat4j.specs.IVec,
     * org.sat4j.csp.constraints.RelationalOperator, org.sat4j.csp.variables.IVariable)
     */
    @Override
    public IConstr encodeCumulativeVariableLengthsVariableHeights(IVec<IVariable> origins,
            IVec<IVariable> lengths, IVec<IVariable> heights, RelationalOperator operator,
            IVariable value) throws ContradictionException {
        IDomain unionDomain = IDomain.unionOf(getDomainsOf(origins));
        ConstrGroup group = new ConstrGroup();
        for (var v : unionDomain) {
            IVec<IVariable> literals = new Vec<>();
            IVec<IVariable> coeffs = new Vec<>();
            for (int i = 0; i < origins.size(); i++) {
                var o = origins.get(i);
                int s1 = solver.newDimacsVariable();
                group.add(solver.getSumEncoder().encodeSoftSum(s1, Vec.of(o), RelationalOperator.LE,
                        v));
                int s2 = solver.newDimacsVariable();
                group.add(solver.getSumEncoder().encodeSoftSum(s2, Vec.of(o,lengths.get(i)),
                        Vec.of(BigInteger.ONE,BigInteger.ONE), RelationalOperator.GT,
                        v));
                int s3 = solver.newDimacsVariable();
                group.add(solver.addSoftAtLeast(s3, VecInt.of(s1, s2), 2));
                literals.push(BooleanVariable.of(s3));
                coeffs.push(heights.get(i));
            }
            for (int i = 0; i < value.nVars(); i++) {
                literals.push(BooleanVariable.of(value.getVariables().get(i)));
                coeffs.push(ConstantVariable.of(value.getCoefficients().get(i).negate()));
            }
            group.add(solver.getSumEncoder().encodeSum(
                    solver.getMultiplicationEncoder().encodeVariableMultiplications(literals,
                            heights),
                    operator, value));
        }
        return group;
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.constraints.encoder.cumulative.ICumulativeConstraintEncoder#
     * encodeCumulativeVariableLengthsVariableHeights(org.sat4j.specs.IVec,
     * org.sat4j.specs.IVec, org.sat4j.specs.IVec, org.sat4j.specs.IVec,
     * org.sat4j.csp.constraints.RelationalOperator, org.sat4j.csp.variables.IVariable)
     */
    @Override
    public IConstr encodeCumulativeVariableLengthsVariableHeights(IVec<IVariable> origins,
            IVec<IVariable> lengths, IVec<IVariable> ends, IVec<IVariable> heights,
            RelationalOperator operator, IVariable value) throws ContradictionException {
        ConstrGroup group = new ConstrGroup();
        group.add(encodeCumulativeVariableLengthsVariableHeights(origins, lengths, heights,
                operator, value));
        for (int i = 0; i < origins.size(); i++) {
            group.add(solver.getSumEncoder().encodeSum(Vec.of(ends.get(i), origins.get(i)),
                    Vec.of(BigInteger.ONE, BigInteger.ONE.negate()), RelationalOperator.EQ,
                    lengths.get(i)));
        }
        return group;
    }
}
