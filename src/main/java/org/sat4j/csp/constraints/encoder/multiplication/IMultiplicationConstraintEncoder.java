/**
 * Sat4j-CSP, a CSP solver based on the Sat4j platform.
 * Copyright (c) 2021-2022 - Exakis Nelite, Univ Artois & CNRS.
 * All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 * If not, see {@link http://www.gnu.org/licenses}.
 */

package org.sat4j.csp.constraints.encoder.multiplication;

import org.sat4j.core.Vec;
import org.sat4j.core.VecInt;
import org.sat4j.csp.constraints.encoder.IConstraintEncoder;
import org.sat4j.csp.variables.IVariable;
import org.sat4j.specs.ContradictionException;
import org.sat4j.specs.IVec;
import org.sat4j.specs.IVecInt;

/**
 * The IMultiplicationConstraintEncoder defines a strategy for encoding multiplicative
 * operations using pseudo-Boolean constraints and/or clauses.
 *
 * @author Thibault Falque
 * @author Romain Wallon
 *
 * @version 0.1.0
 */
public interface IMultiplicationConstraintEncoder extends IConstraintEncoder {

    /**
     * Encodes a multiplication of literals using pseudo-Boolean constraints and/or
     * clauses.
     *
     * @param literals The literals to encode the multiplication of.
     *
     * @return The literal representing the multiplication of the others.
     *
     * @throws ContradictionException If encoding the multiplication results in a trivial
     *         inconsistency.
     *
     * @implSpec The default implementation invokes {@link #encodeMultiplication(IVecInt)}
     *           with a vector containing the given literals.
     *
     * @see #encodeMultiplication(IVecInt)
     */
    default int encodeMultiplication(int... literals) throws ContradictionException {
        return encodeMultiplication(VecInt.of(literals));
    }

    /**
     * Encodes a multiplication of literals using pseudo-Boolean constraints and/or
     * clauses.
     *
     * @param literals The literals to encode the multiplication of.
     *
     * @return The literal representing the multiplication of the others.
     *
     * @throws ContradictionException If encoding the multiplication results in a trivial
     *         inconsistency.
     */
    int encodeMultiplication(IVecInt literals) throws ContradictionException;

    /**
     * Encodes a multiplication of two variables using pseudo-Boolean constraints and/or
     * clauses.
     *
     * @param left The left variable in the multiplication operation.
     * @param right The right variable in the multiplication operation.
     *
     * @return The variable representing the multiplication of the others.
     *
     * @throws ContradictionException If encoding the multiplication results in a trivial
     *         inconsistency.
     */
    IVariable encodeMultiplication(IVariable left, IVariable right) throws ContradictionException;

    /**
     * Encodes a multiplication of several variables using pseudo-Boolean constraints
     * and/or clauses.
     *
     * @param variables The variables in the multiplication operation.
     *
     * @return The variable representing the multiplication of the others.
     *
     * @throws ContradictionException If encoding the multiplication results in a trivial
     *         inconsistency.
     *
     * @implSpec The default implementation invokes {@link #encodeMultiplication(IVec)}
     *           with a vector containing the given variables.
     *
     * @see #encodeMultiplication(IVec)
     */
    default IVariable encodeMultiplication(IVariable... variables) throws ContradictionException {
        return encodeMultiplication(Vec.of(variables));
    }

    /**
     * Encodes a multiplication of several variables using pseudo-Boolean constraints
     * and/or clauses.
     *
     * @param variables The variables in the multiplication operation.
     *
     * @return The variable representing the multiplication of the others.
     *
     * @throws ContradictionException If encoding the multiplication results in a trivial
     *         inconsistency.
     */
    IVariable encodeMultiplication(IVec<IVariable> variables) throws ContradictionException;
    
    /**
     * Encodes the pairwise multiplication between a variable and its coefficient.
     *
     * @param variables The variables to multiply.
     * @param coefficients The coefficients to multiply.
     *
     * @return The vector of multiplied variables.
     *
     * @throws ContradictionException If encoding the multiplication results in a trivial
     *         inconsistency.
     */
    default IVec<IVariable> encodeVariableMultiplications(IVec<IVariable> variables,
            IVec<IVariable> coefficients) throws ContradictionException {
        var multipliedVariables = new Vec<IVariable>(variables.size());
        for (int i = 0; i < variables.size(); i++) {
            multipliedVariables.push(encodeMultiplication(variables.get(i),
                    coefficients.get(i)));
        }
        return multipliedVariables;
    }

}
