/**
 * Sat4j-CSP, a CSP solver based on the Sat4j platform.
 * Copyright (c) 2021-2022 - Exakis Nelite, Univ Artois & CNRS.
 * All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 * If not, see {@link http://www.gnu.org/licenses}.
 */

package org.sat4j.csp.constraints.encoder.channel;

import org.sat4j.core.ConstrGroup;
import org.sat4j.core.VecInt;
import org.sat4j.csp.constraints.encoder.AbstractConstraintEncoder;
import org.sat4j.csp.variables.ConstantVariable;
import org.sat4j.csp.variables.IVariable;
import org.sat4j.specs.ContradictionException;
import org.sat4j.specs.IConstr;
import org.sat4j.specs.IVec;

/**
 * The DefaultChannelConstraintEncoder provides a base encoding for {@code channel}
 * constraints using pseudo-Boolean constraints and/or clauses.
 *
 * @author Thibault Falque
 * @author Romain Wallon
 *
 * @version 0.1.0
 */
public class DefaultChannelConstraintEncoder extends AbstractConstraintEncoder
        implements IChannelConstraintEncoder {

    /*
     * (non-Javadoc)
     *
     * @see
     * org.sat4j.csp.constraints.encoder.channel.IChannelConstraintEncoder#encodeChannel(
     * org.sat4j.specs.IVec, int)
     */
    @Override
    public IConstr encodeChannel(IVec<IVariable> variables, int startIndex)
            throws ContradictionException {
        return encodeChannel(variables, startIndex, variables, startIndex);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.sat4j.csp.constraints.encoder.channel.IChannelConstraintEncoder#encodeChannel(
     * org.sat4j.specs.IVec, int, org.sat4j.csp.variables.IVariable)
     */
    @Override
    public IConstr encodeChannel(IVec<IVariable> variables, int startIndex, IVariable value)
            throws ContradictionException {
        var group = new ConstrGroup();
        var clause = new VecInt(variables.size() - startIndex);
        var cube = new VecInt(variables.size() - startIndex);
        var encoder = solver.getIntensionEncoder();

        for (int i = startIndex; i < variables.size(); i++) {
            var xi = variables.get(i);
            var vEqualsI = encoder.encodeEqual(value, ConstantVariable.of(i));
            var equiv = encoder.encodeEquivalence(xi, vEqualsI);
            cube.push(equiv.getVariables().get(0));
            clause.push(xi.getVariables().get(0));
        }

        group.add(solver.addAtLeast(cube, cube.size()));
        group.add(solver.addClause(clause));
        return group;
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.sat4j.csp.constraints.encoder.channel.IChannelConstraintEncoder#encodeChannel(
     * org.sat4j.specs.IVec, int, org.sat4j.specs.IVec, int)
     */
    @Override
    public IConstr encodeChannel(IVec<IVariable> variables, int startIndex,
            IVec<IVariable> otherVariables, int otherStartIndex) throws ContradictionException {
        var group = new ConstrGroup();
        var encoder = solver.getIntensionEncoder();

        for (int i = startIndex; i < variables.size(); i++) {
            for (int j = otherStartIndex; j < otherVariables.size(); j++) {
                var xiEqualsJ = encoder.encodeEqual(variables.get(i), ConstantVariable.of(j));
                int xi = xiEqualsJ.getVariables().get(0);

                var yjEqualsI = encoder.encodeEqual(otherVariables.get(j),
                        ConstantVariable.of(i));
                int yj = yjEqualsI.getVariables().get(0);

                group.add(solver.addClause(VecInt.of(-xi, yj)));
                if (variables.size() == otherVariables.size()) {
                    group.add(solver.addClause(VecInt.of(-yj, xi)));
                }
            }
        }

        return group;
    }

}
