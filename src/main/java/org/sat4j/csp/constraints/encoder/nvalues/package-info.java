/**
 * The {@code org.sat4j.csp.constraints.encoder.nvalues} package provides encodings
 * for {@code n-values} constraints.
 *
 * @author Thibault Falque
 * @author Romain Wallon
 *
 * @version 0.1.0
 */

package org.sat4j.csp.constraints.encoder.nvalues;
