/**
 * Sat4j-CSP, a CSP solver based on the Sat4j platform.
 * Copyright (c) 2021-2022 - Exakis Nelite, Univ Artois & CNRS.
 * All rightVariables reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 * If not, see {@link http://www.gnu.org/licenses}.
 */

package org.sat4j.csp.constraints.encoder.sum;

import java.math.BigInteger;

import org.sat4j.core.ConstrGroup;
import org.sat4j.core.Vec;
import org.sat4j.core.VecInt;
import org.sat4j.csp.constraints.RelationalOperator;
import org.sat4j.csp.constraints.SetBelongingOperator;
import org.sat4j.csp.constraints.encoder.AbstractConstraintEncoder;
import org.sat4j.csp.variables.IVariable;
import org.sat4j.pb.ObjectiveFunction;
import org.sat4j.specs.ContradictionException;
import org.sat4j.specs.IConstr;
import org.sat4j.specs.IVec;
import org.sat4j.specs.IVecInt;

/**
 * The DefaultSumConstraintEncoder provides a base encoding of {@code sum} constraints
 * using pseudo-Boolean constraints and/or clauses.
 *
 * @author Thibault Falque
 * @author Romain Wallon
 *
 * @version 0.1.0
 */
public class DefaultSumConstraintEncoder extends AbstractConstraintEncoder
        implements ISumConstraintEncoder {

    /*
     * (non-Javadoc)
     *
     * @see
     * org.sat4j.csp.constraints.encoder.sum.ISumConstraintEncoder#encodeSum(org.sat4j.
     * specs.IVec, org.sat4j.specs.IVec, org.sat4j.csp.constraints.RelationalOperator,
     * java.math.BigInteger)
     */
    @Override
    public IConstr encodeSum(IVec<IVariable> variables, IVec<BigInteger> coefficients,
            RelationalOperator operator, BigInteger value) throws ContradictionException {
        var literals = new VecInt();
        var coeffs = new Vec<BigInteger>();
        var degree = encodeIntoPseudoBoolean(variables, coefficients, literals, coeffs);
        return solver.addPseudoBoolean(literals, coeffs, operator, degree.add(value));
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.constraints.encoder.sum.ISumConstraintEncoder#encodeSoftSum(int,
     * org.sat4j.specs.IVec, org.sat4j.specs.IVec,
     * org.sat4j.csp.constraints.RelationalOperator, java.math.BigInteger)
     */
    @Override
    public IConstr encodeSoftSum(int selector, IVec<IVariable> variables,
            IVec<BigInteger> coefficients, RelationalOperator operator, BigInteger value)
            throws ContradictionException {
        var literals = new VecInt();
        var coeffs = new Vec<BigInteger>();
        var degree = encodeIntoPseudoBoolean(variables, coefficients, literals, coeffs);
        return solver.addSoftPseudoBoolean(selector, literals, coeffs, operator, degree.add(value));
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.sat4j.csp.constraints.encoder.sum.ISumConstraintEncoder#encodeSum(org.sat4j.
     * specs.IVec, org.sat4j.specs.IVec, org.sat4j.csp.constraints.RelationalOperator,
     * org.sat4j.csp.variables.IVariable)
     */
    @Override
    public IConstr encodeSum(IVec<IVariable> variables, IVec<BigInteger> coefficients,
            RelationalOperator operator, IVariable rightVariable) throws ContradictionException {
        // Computing the original left-hand side.
        var literals = new VecInt();
        var coeffs = new Vec<BigInteger>();
        var degree = encodeIntoPseudoBoolean(variables, coefficients, literals, coeffs);

        // Moving the right-hand side to the left-hand side.
        for (int i = 0; i < rightVariable.nVars(); i++) {
            literals.push(rightVariable.getVariables().get(i));
            coeffs.push(rightVariable.getCoefficients().get(i).negate());
        }
        degree = degree.add(rightVariable.getShift());

        return solver.addPseudoBoolean(literals, coeffs, operator, degree);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.constraints.encoder.sum.ISumConstraintEncoder#encodeSoftSum(int,
     * org.sat4j.specs.IVec, org.sat4j.specs.IVec,
     * org.sat4j.csp.constraints.RelationalOperator, org.sat4j.csp.variables.IVariable)
     */
    @Override
    public IConstr encodeSoftSum(int selector, IVec<IVariable> variables,
            IVec<BigInteger> coefficients, RelationalOperator operator, IVariable rightVariable)
            throws ContradictionException {
        // Computing the original left-hand side.
        var literals = new VecInt();
        var coeffs = new Vec<BigInteger>();
        var degree = encodeIntoPseudoBoolean(variables, coefficients, literals, coeffs);

        // Moving the right-hand side to the left-hand side.
        for (int i = 0; i < rightVariable.nVars(); i++) {
            literals.push(rightVariable.getVariables().get(i));
            coeffs.push(rightVariable.getCoefficients().get(i).negate());
        }
        degree = degree.add(rightVariable.getShift());

        return solver.addSoftPseudoBoolean(selector, literals, coeffs, operator, degree);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.sat4j.csp.constraints.encoder.sum.ISumConstraintEncoder#encodeObjectiveFunction
     * (org.sat4j.specs.IVec, org.sat4j.specs.IVec)
     */
    @Override
    public ObjectiveFunction encodeAsObjectiveFunction(IVec<IVariable> variables,
            IVec<BigInteger> coefficients) {
        var literals = new VecInt();
        var coeffs = new Vec<BigInteger>();
        var offset = encodeIntoPseudoBoolean(variables, coefficients, literals, coeffs);
        var objectiveFunction = new ObjectiveFunction(literals, coeffs);
        objectiveFunction.setCorrectionOffset(offset.negate());
        return objectiveFunction;
    }

    @Override
    public IConstr encodeSum(IVec<IVariable> variables, IVec<BigInteger> coefficients,
            SetBelongingOperator operator, IVec<BigInteger> set) throws ContradictionException {
        if (operator == SetBelongingOperator.IN) {
            IVariable v = solver.newVariable(set);
            return encodeSum(variables, coefficients, RelationalOperator.EQ, v);
        } else {
            var group = new ConstrGroup();
            for (var it = set.iterator(); it.hasNext();) {
                group.add(encodeSum(variables, coefficients, RelationalOperator.NEQ, it.next()));
            }
            return group;
        }

    }

    @Override
    public IConstr encodeSum(IVec<IVariable> variables, IVec<BigInteger> coefficients,
            SetBelongingOperator operator, BigInteger min, BigInteger max)
            throws ContradictionException {
        var group = new ConstrGroup();
        if (operator == SetBelongingOperator.IN) {
            group.add(encodeSum(variables, coefficients, RelationalOperator.LE, max));
            group.add(encodeSum(variables, coefficients, RelationalOperator.GE, min));
        } else {
            int s = solver.newDimacsVariable();
            group.add(encodeSoftSum(s, variables, coefficients, RelationalOperator.GT, max));
            int s2 = solver.newDimacsVariable();
            group.add(encodeSoftSum(s2, variables, coefficients, RelationalOperator.LT, min));
            group.add(solver.addClause(VecInt.of(s, s2)));
        }
        
        return group;

    }

    /**
     * Encodes the left-hand side of a {@code sum} constraint from the variables of this
     * constraint, and moves all constants to the right-hand side, to produce a
     * pseudo-Boolean constraint.
     *
     * @param variables The variables appearing in the constraint.
     * @param coefficients The coefficients of the variables appearing in the constraint.
     * @param vecVariables The vector of the variables of the pseudo-Boolean constraint to
     *        compute (to be filled by this method).
     * @param vecCoefficients The vector of the coefficients of the pseudo-Boolean
     *        constraint to compute (to be filled by this method).
     *
     * @return The initial value of the right-hand side of the pseudo-Boolean constraint
     *         to compute.
     */
    private BigInteger encodeIntoPseudoBoolean(IVec<IVariable> variables,
            IVec<BigInteger> coefficients, IVecInt vecVariables, IVec<BigInteger> vecCoefficients) {
        var degree = BigInteger.ZERO;
        for (int i = 0; i < variables.size(); i++) {
            var variable = variables.get(i);
            var coefficient = coefficients.get(i);

            for (int j = 0; j < variable.nVars(); j++) {
                vecVariables.push(variable.getVariables().get(j));
                vecCoefficients.push(coefficient.multiply(variable.getCoefficients().get(j)));
            }

            degree = degree.subtract(coefficient.multiply(variable.getShift()));
        }

        return degree;
    }

}
