/**
 * The {@code org.sat4j.csp.constraints.encoder.count} package provides encodings for
 * {@code count}-based constraints.
 *
 * @author Thibault Falque
 * @author Romain Wallon
 *
 * @version 0.1.0
 */

package org.sat4j.csp.constraints.encoder.count;
