/**
 * Sat4j-CSP, a CSP solver based on the Sat4j platform.
 * Copyright (c) 2021-2022 - Exakis Nelite, Univ Artois & CNRS.
 * All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 * If not, see {@link http://www.gnu.org/licenses}.
 */

package org.sat4j.csp.constraints.encoder.count;

import java.math.BigInteger;

import org.sat4j.core.Vec;
import org.sat4j.csp.constraints.RelationalOperator;
import org.sat4j.csp.constraints.encoder.IConstraintEncoder;
import org.sat4j.csp.variables.IVariable;
import org.sat4j.specs.ContradictionException;
import org.sat4j.specs.IConstr;
import org.sat4j.specs.IVec;

/**
 * The ICountConstraintEncoder defines a strategy for encoding {@code count}
 * constraints using pseudo-Boolean constraints and/or clauses.
 *
 * @author Thibault Falque
 * @author Romain Wallon
 *
 * @version 0.1.0
 */
public interface ICountConstraintEncoder extends IConstraintEncoder {

    /**
     * Encodes an {@code at-least} constraint using pseudo-Boolean constraints and/or
     * clauses.
     *
     * @param variables The variables to count the assignments of.
     * @param value The value to count the assignments of.
     * @param count The minimum number of times the value can be assigned among the
     *        variables.
     *
     * @return The created constraint(s), as internally represented by Sat4j.
     *
     * @throws ContradictionException If encoding the constraint results in a trivial
     *         inconsistency.
     *
     * @implSpec The default implementation invokes
     *           {@link #encodeCountWithConstantValues(IVec, IVec, RelationalOperator, BigInteger)}
     *           with {@link RelationalOperator#GE} as operator.
     *
     * @see #encodeCountWithConstantValues(IVec, IVec, RelationalOperator, BigInteger)
     */
    default IConstr encodeAtLeast(IVec<IVariable> variables, BigInteger value, BigInteger count)
            throws ContradictionException {
        return encodeCountWithConstantValues(variables, Vec.of(value), RelationalOperator.GE, count);
    }

    /**
     * Encodes an {@code exactly} constraint using pseudo-Boolean constraints and/or
     * clauses.
     *
     * @param variables The variables to count the assignments of.
     * @param value The value to count the assignments of.
     * @param count The number of times the value can be assigned among the variables.
     *
     * @return The created constraint(s), as internally represented by Sat4j.
     *
     * @throws ContradictionException If encoding the constraint results in a trivial
     *         inconsistency.
     *
     * @implSpec The default implementation invokes
     *           {@link #encodeCountWithConstantValues(IVec, IVec, RelationalOperator, BigInteger)}
     *           with {@link RelationalOperator#EQ} as operator.
     *
     * @see #encodeCountWithConstantValues(IVec, IVec, RelationalOperator, BigInteger)
     */
    default IConstr encodeExactly(IVec<IVariable> variables, BigInteger value, BigInteger count)
            throws ContradictionException {
        return encodeCountWithConstantValues(variables, Vec.of(value), RelationalOperator.EQ, count);
    }

    /**
     * Encodes an {@code exactly} constraint using pseudo-Boolean constraints and/or
     * clauses.
     *
     * @param variables The variables to count the assignments of.
     * @param value The value to count the assignments of.
     * @param count The variable encoding the number of times the value can be assigned
     *        among the variables.
     *
     * @return The created constraint(s), as internally represented by Sat4j.
     *
     * @throws ContradictionException If encoding the constraint results in a trivial
     *         inconsistency.
     *
     * @implSpec The default implementation invokes
     *           {@link #encodeCountWithConstantValues(IVec, IVec, RelationalOperator, IVariable)}
     *           with {@link RelationalOperator#EQ} as operator.
     *
     * @see #encodeCountWithConstantValues(IVec, IVec, RelationalOperator, IVariable)
     */
    default IConstr encodeExactly(IVec<IVariable> variables, BigInteger value, IVariable count)
            throws ContradictionException {
        return encodeCountWithConstantValues(variables, Vec.of(value), RelationalOperator.EQ, count);
    }

    /**
     * Encodes an {@code among} constraint using pseudo-Boolean constraints and/or
     * clauses.
     *
     * @param variables The variables to count the assignments of.
     * @param values The values to count the assignments of.
     * @param count The number of times the value can be assigned among the variables.
     *
     * @return The created constraint(s), as internally represented by Sat4j.
     *
     * @throws ContradictionException If encoding the constraint results in a trivial
     *         inconsistency.
     *
     * @implSpec The default implementation invokes
     *           {@link #encodeCountWithConstantValues(IVec, IVec, RelationalOperator, BigInteger)}
     *           with {@link RelationalOperator#EQ} as operator.
     *
     * @see #encodeCountWithConstantValues(IVec, IVec, RelationalOperator, BigInteger)
     */
    default IConstr encodeAmong(IVec<IVariable> variables, IVec<BigInteger> values,
            BigInteger count) throws ContradictionException {
        return encodeCountWithConstantValues(variables, values, RelationalOperator.EQ, count);
    }

    /**
     * Encodes an {@code among} constraint using pseudo-Boolean constraints and/or
     * clauses.
     *
     * @param variables The variables to count the assignments of.
     * @param values The values to count the assignments of.
     * @param count The variable encoding the number of times the values can be assigned
     *        among the variables.
     *
     * @return The created constraint(s), as internally represented by Sat4j.
     *
     * @throws ContradictionException If encoding the constraint results in a trivial
     *         inconsistency.
     *
     * @implSpec The default implementation invokes
     *           {@link #encodeCountWithConstantValues(IVec, IVec, RelationalOperator, IVariable)}
     *           with {@link RelationalOperator#EQ} as operator.
     *
     * @see #encodeCountWithConstantValues(IVec, IVec, RelationalOperator, IVariable)
     */
    default IConstr encodeAmong(IVec<IVariable> variables, IVec<BigInteger> values, IVariable count)
            throws ContradictionException {
        return encodeCountWithConstantValues(variables, values, RelationalOperator.EQ, count);
    }

    /**
     * Encodes an {@code at-most} constraint using pseudo-Boolean constraints and/or
     * clauses.
     *
     * @param variables The variables to count the assignments of.
     * @param value The value to count the assignments of.
     * @param count The maximum number of times the value can be assigned among the
     *        variables.
     *
     * @return The created constraint(s), as internally represented by Sat4j.
     *
     * @throws ContradictionException If encoding the constraint results in a trivial
     *         inconsistency.
     *
     * @implSpec The default implementation invokes
     *           {@link #encodeCountWithConstantValues(IVec, IVec, RelationalOperator, BigInteger)}
     *           with {@link RelationalOperator#LE} as operator.
     *
     * @see #encodeCountWithConstantValues(IVec, IVec, RelationalOperator, BigInteger)
     */
    default IConstr encodeAtMost(IVec<IVariable> variables, BigInteger value, BigInteger count)
            throws ContradictionException {
        return encodeCountWithConstantValues(variables, Vec.of(value), RelationalOperator.LE, count);
    }

    /**
     * Encodes a {@code count} constraint using pseudo-Boolean constraints and/or clauses.
     *
     * @param variables The variables to count the assignments of.
     * @param values The values to count the assignments of.
     * @param operator The operator to use to compare the number of assignments to their
     *        expected count.
     * @param count The number of times the values can be assigned among the variables.
     *
     * @return The created constraint(s), as internally represented by Sat4j.
     *
     * @throws ContradictionException If encoding the constraint results in a trivial
     *         inconsistency.
     */
    IConstr encodeCountWithConstantValues(IVec<IVariable> variables, IVec<BigInteger> values,
            RelationalOperator operator, BigInteger count) throws ContradictionException;

    /**
     * Encodes a {@code count} constraint using pseudo-Boolean constraints and/or clauses.
     *
     * @param variables The variables to count the assignments of.
     * @param values The values to count the assignments of.
     * @param operator The operator to use to compare the number of assignments to their
     *        expected count.
     * @param count The variable encoding the number of times the values can be assigned
     *        among the variables.
     *
     * @return The created constraint(s), as internally represented by Sat4j.
     *
     * @throws ContradictionException If encoding the constraint results in a trivial
     *         inconsistency.
     */
    IConstr encodeCountWithConstantValues(IVec<IVariable> variables, IVec<BigInteger> values,
            RelationalOperator operator, IVariable count) throws ContradictionException;

    /**
     * Encodes a {@code count} constraint using pseudo-Boolean constraints and/or clauses.
     *
     * @param variables The variables to count the assignments of.
     * @param values The variables encoding the values to count the assignments of.
     * @param operator The operator to use to compare the number of assignments to their
     *        expected count.
     * @param count The number of times the values can be assigned among the variables.
     *
     * @return The created constraint(s), as internally represented by Sat4j.
     *
     * @throws ContradictionException If encoding the constraint results in a trivial
     *         inconsistency.
     */
    IConstr encodeCountWithVariableValues(IVec<IVariable> variables, IVec<IVariable> values,
            RelationalOperator operator, BigInteger count) throws ContradictionException;

    /**
     * Encodes a {@code count} constraint using pseudo-Boolean constraints and/or clauses.
     *
     * @param variables The variables to count the assignments of.
     * @param values The variables encoding the values to count the assignments of.
     * @param operator The operator to use to compare the number of assignments to their
     *        expected count.
     * @param count The variable encoding the number of times the values can be assigned
     *        among the variables.
     *
     * @return The created constraint(s), as internally represented by Sat4j.
     *
     * @throws ContradictionException If encoding the constraint results in a trivial
     *         inconsistency.
     */
    IConstr encodeCountWithVariableValues(IVec<IVariable> variables, IVec<IVariable> values,
            RelationalOperator operator, IVariable count) throws ContradictionException;

}
