/**
 * Sat4j-CSP, a CSP solver based on the Sat4j platform.
 * Copyright (c) 2021-2022 - Exakis Nelite, Univ Artois & CNRS.
 * All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 * If not, see {@link http://www.gnu.org/licenses}.
 */

package org.sat4j.csp.constraints.encoder.intension;

import java.math.BigInteger;

import org.sat4j.core.ConstrGroup;
import org.sat4j.core.VecInt;
import org.sat4j.csp.constraints.ArithmeticOperator;
import org.sat4j.csp.constraints.RelationalOperator;
import org.sat4j.csp.constraints.SetBelongingOperator;
import org.sat4j.csp.constraints.encoder.AbstractConstraintEncoderDecorator;
import org.sat4j.csp.variables.IVariable;
import org.sat4j.csp.variables.MultipliedVariableDecorator;
import org.sat4j.csp.variables.ShiftedVariableDecorator;
import org.sat4j.specs.ContradictionException;
import org.sat4j.specs.IConstr;
import org.sat4j.specs.IVec;

/**
 * The OrderEncodingPrimitiveConstraintEncoder is an {@link IPrimitiveConstraintEncoder}
 * that leverages the order-encoding of variables to better encode {@code primitive}
 * constraints.
 *
 * @author Thibault Falque
 * @author Romain Wallon
 *
 * @version 0.1.0
 */
public class OrderEncodingPrimitiveConstraintEncoder
        extends AbstractConstraintEncoderDecorator<IPrimitiveConstraintEncoder>
        implements IPrimitiveConstraintEncoder {

    /**
     * Creates a new OrderEncodingPrimitiveConstraintEncoder.
     */
    public OrderEncodingPrimitiveConstraintEncoder() {
        super(new DefaultPrimitiveConstraintEncoder());
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.constraints.encoder.intension.IPrimitiveConstraintEncoder#
     * encodePrimitive(org.sat4j.csp.variables.IVariable,
     * org.sat4j.csp.constraints.RelationalOperator, java.math.BigInteger)
     */
    @Override
    public IConstr encodePrimitive(IVariable variable, RelationalOperator operator,
            BigInteger value) throws ContradictionException {
        if (operator == RelationalOperator.EQ) {
            var literal = variable.getLiteralForValue(value);
            return solver.addClause(VecInt.of(literal.orElseThrow(
                    () -> new ContradictionException(value + " not in domain"))));
        }

        if (operator == RelationalOperator.NEQ) {
            var literal = variable.getLiteralForValue(value);
            if (literal.isPresent()) {
                return solver.addClause(VecInt.of(-literal.getAsInt()));
            }
        }

        if ((operator == RelationalOperator.GE) || (operator == RelationalOperator.GT)) {
            var actual = (operator == RelationalOperator.GE) ? value : value.add(BigInteger.ONE);
            var literal = variable.getLiteralForValueAtLeast(actual);
            return solver.addClause(VecInt.of(literal));
        }

        var forbidden = (operator == RelationalOperator.LT) ? value.add(BigInteger.ONE) : value;
        var literal = variable.getLiteralForValueAtLeast(forbidden);
        return solver.addClause(VecInt.of(-literal));
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.constraints.encoder.intension.IPrimitiveConstraintEncoder#
     * encodePrimitive(org.sat4j.csp.variables.IVariable,
     * org.sat4j.csp.constraints.ArithmeticOperator, java.math.BigInteger,
     * org.sat4j.csp.constraints.RelationalOperator, java.math.BigInteger)
     */
    @Override
    public IConstr encodePrimitive(IVariable variable, ArithmeticOperator arithOp,
            BigInteger leftHandSide, RelationalOperator relOp, BigInteger rightHandSide)
            throws ContradictionException {
        switch (arithOp) {
            case ADD:
                return encodePrimitive(variable, relOp, rightHandSide.subtract(leftHandSide));

            case SUB:
                return encodePrimitive(variable, relOp, rightHandSide.add(leftHandSide));

            case MULT:
                if (leftHandSide.signum() >= 0) {
                    return encodePrimitive(variable, relOp, rightHandSide.divide(leftHandSide));

                } else {
                    return encodePrimitive(variable, relOp.reverse(),
                            rightHandSide.divide(leftHandSide));
                }

            case DIV:
                if (leftHandSide.signum() >= 0) {
                    return encodePrimitive(variable, relOp, rightHandSide.multiply(leftHandSide));

                } else {
                    return encodePrimitive(
                            variable, relOp.reverse(), rightHandSide.multiply(leftHandSide));
                }

            default:
                return decorated.encodePrimitive(
                        variable, arithOp, leftHandSide, relOp, rightHandSide);
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.constraints.encoder.intension.IPrimitiveConstraintEncoder#
     * encodePrimitive(org.sat4j.csp.variables.IVariable,
     * org.sat4j.csp.constraints.ArithmeticOperator, org.sat4j.csp.variables.IVariable,
     * org.sat4j.csp.constraints.RelationalOperator, java.math.BigInteger)
     */
    @Override
    public IConstr encodePrimitive(IVariable variable, ArithmeticOperator arithOp,
            IVariable leftHandSide, RelationalOperator relOp, BigInteger rightHandSide)
            throws ContradictionException {
        switch (arithOp) {
            case ADD:
                return encodePrimitive(
                        new ShiftedVariableDecorator(variable, rightHandSide.negate()),
                        relOp, leftHandSide.negate());

            case SUB:
                return encodePrimitive(
                        new ShiftedVariableDecorator(variable, rightHandSide.negate()),
                        relOp, leftHandSide);

            default:
                return decorated.encodePrimitive(
                        variable, arithOp, leftHandSide, relOp, rightHandSide);
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.constraints.encoder.intension.IPrimitiveConstraintEncoder#
     * encodePrimitive(org.sat4j.csp.variables.IVariable,
     * org.sat4j.csp.constraints.ArithmeticOperator, java.math.BigInteger,
     * org.sat4j.csp.constraints.RelationalOperator, org.sat4j.csp.variables.IVariable)
     */
    @Override
    public IConstr encodePrimitive(IVariable variable, ArithmeticOperator arithOp,
            BigInteger leftHandSide, RelationalOperator relOp, IVariable rightHandSide)
            throws ContradictionException {
        switch (arithOp) {
            case ADD:
                return encodePrimitive(
                        new ShiftedVariableDecorator(variable, leftHandSide),
                        relOp, rightHandSide);

            case SUB:
                return encodePrimitive(
                        new ShiftedVariableDecorator(variable, leftHandSide.negate()),
                        relOp, rightHandSide);

            case MULT:
                return encodePrimitive(
                        new MultipliedVariableDecorator(variable, leftHandSide),
                        relOp, rightHandSide);

            default:
                return decorated.encodePrimitive(
                        variable, arithOp, leftHandSide, relOp, rightHandSide);
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.constraints.encoder.intension.IPrimitiveConstraintEncoder#
     * encodePrimitive(org.sat4j.csp.variables.IVariable,
     * org.sat4j.csp.constraints.ArithmeticOperator, org.sat4j.csp.variables.IVariable,
     * org.sat4j.csp.constraints.RelationalOperator, org.sat4j.csp.variables.IVariable)
     */
    @Override
    public IConstr encodePrimitive(IVariable variable, ArithmeticOperator arithOp,
            IVariable leftHandSide, RelationalOperator relOp, IVariable rightHandSide)
            throws ContradictionException {
        return decorated.encodePrimitive(variable, arithOp, leftHandSide, relOp, rightHandSide);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.constraints.encoder.intension.IPrimitiveConstraintEncoder#
     * encodePrimitive(org.sat4j.csp.constraints.ArithmeticOperator,
     * org.sat4j.csp.variables.IVariable, org.sat4j.csp.variables.IVariable)
     */
    @Override
    public IConstr encodePrimitive(ArithmeticOperator arithOp, IVariable variable,
            IVariable rightHandSide) throws ContradictionException {
        if (arithOp == ArithmeticOperator.NEG) {
            return encodePrimitive(variable.negate(), RelationalOperator.EQ, rightHandSide);
        }

        return decorated.encodePrimitive(arithOp, variable, rightHandSide);
    }

    /**
     * Encodes a {@code primitive} constraint between two variables.
     *
     * @param leftHandSide The variable on the left-hand side.
     * @param operator The operator used in the constraint.
     * @param rightHandSide The variable on the right-hand side.
     *
     * @return The created constraint(s), as internally represented by Sat4j.
     *
     * @throws ContradictionException If encoding the constraint results in a trivial
     *         inconsistency.
     */
    private IConstr encodePrimitive(IVariable leftHandSide, RelationalOperator operator,
            IVariable rightHandSide) throws ContradictionException {
        switch (operator) {
            case LT:
                return encodeAtLeast(rightHandSide,
                        new ShiftedVariableDecorator(leftHandSide, BigInteger.ONE));

            case LE:
                return encodeAtLeast(rightHandSide, leftHandSide);

            case EQ:
                var group = new ConstrGroup();
                group.add(encodeAtLeast(rightHandSide, leftHandSide));
                group.add(encodeAtLeast(leftHandSide, rightHandSide));
                return group;

            case GE:
                return encodeAtLeast(leftHandSide, rightHandSide);

            case GT:
                return encodeAtLeast(leftHandSide,
                        new ShiftedVariableDecorator(rightHandSide, BigInteger.ONE));

            default:
                throw new UnsupportedOperationException();
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.constraints.encoder.intension.IPrimitiveConstraintEncoder#
     * encodePrimitive(org.sat4j.csp.variables.IVariable,
     * org.sat4j.csp.constraints.SetBelongingOperator, org.sat4j.specs.IVec)
     */
    @Override
    public IConstr encodePrimitive(IVariable variable, SetBelongingOperator operator,
            IVec<BigInteger> values) throws ContradictionException {
        // We cannot do better than the default implementation.
        return decorated.encodePrimitive(variable, operator, values);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.constraints.encoder.intension.IPrimitiveConstraintEncoder#
     * encodePrimitive(org.sat4j.csp.variables.IVariable,
     * org.sat4j.csp.constraints.SetBelongingOperator, java.math.BigInteger,
     * java.math.BigInteger)
     */
    @Override
    public IConstr encodePrimitive(IVariable variable, SetBelongingOperator operator,
            BigInteger min, BigInteger max) throws ContradictionException {
        // Retrieving the literals associated to the given bounds.
        var atLeastMin = variable.getLiteralForValueAtLeast(min);
        var atLeastMax = variable.getLiteralForValueAtLeast(max.add(BigInteger.ONE));

        if (operator == SetBelongingOperator.IN) {
            // The given values are allowed.
            var group = new ConstrGroup();
            group.add(solver.addClause(VecInt.of(atLeastMin)));
            group.add(solver.addClause(VecInt.of(-atLeastMax)));
            return group;
        }

        // The given values are forbidden.
        return solver.addClause(VecInt.of(-atLeastMin, atLeastMax));
    }

    /**
     * Encodes an {@code at-least} constraint between two variables.
     *
     * @param leftHandSide The variable on the left-hand side.
     * @param rightHandSide The variable on the right-hand side.
     *
     * @return The created constraint(s), as internally represented by Sat4j.
     *
     * @throws ContradictionException If encoding the constraint results in a trivial
     *         inconsistency.
     */
    private IConstr encodeAtLeast(IVariable leftHandSide, IVariable rightHandSide)
            throws ContradictionException {
        var group = new ConstrGroup();
        for (var value : leftHandSide.getDomain().union(rightHandSide.getDomain())) {
            var lhs = leftHandSide.getLiteralForValueAtLeast(value);
            var rhs = rightHandSide.getLiteralForValueAtLeast(value);
            solver.addClause(VecInt.of(-lhs, rhs));
        }
        return group;
    }

}
