/**
 * sat4j-csp
 * Copyright (c) 2023 - Romain Wallon.
 * All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 * If not, see {@link http://www.gnu.org/licenses}.
 */

package org.sat4j.csp.constraints.encoder.binpacking;

import java.math.BigInteger;

import org.sat4j.core.ConstrGroup;
import org.sat4j.core.Vec;
import org.sat4j.csp.constraints.RelationalOperator;
import org.sat4j.csp.constraints.SetBelongingOperator;
import org.sat4j.csp.constraints.encoder.AbstractConstraintEncoder;
import org.sat4j.csp.variables.BooleanVariable;
import org.sat4j.csp.variables.IDomain;
import org.sat4j.csp.variables.IVariable;
import org.sat4j.specs.ContradictionException;
import org.sat4j.specs.IConstr;
import org.sat4j.specs.IVec;

/**
 * The DefaultBinPackingEncoder
 *
 * @author Romain Wallon
 *
 * @version 0.1.0
 */
public class DefaultBinPackingEncoder extends AbstractConstraintEncoder
        implements IBinPackingEncoder {

    /*
     * (non-Javadoc)
     *
     * @see
     * org.sat4j.csp.constraints.encoder.binpacking.IBinPackingEncoder#encodeBinPacking(
     * org.sat4j.specs.IVec, org.sat4j.specs.IVec,
     * org.sat4j.csp.constraints.RelationalOperator, java.math.BigInteger)
     */
    @Override
    public IConstr encodeBinPacking(IVec<IVariable> variables, IVec<BigInteger> sizes,
            RelationalOperator operator, BigInteger value) throws ContradictionException {

        IDomain domain = IDomain.unionOf(getDomainsOf(variables));
        var sumEncoder = solver.getSumEncoder();
        var group = new ConstrGroup();
        for (var v : domain) {
            IVec<BigInteger> coeffs = new Vec<>();
            IVec<IVariable> vars = new Vec<>();
            for (int i = 0; i < sizes.size(); i++) {
                var l = variables.get(i).getLiteralForValue(v);
                if (l.isPresent()) {
                    vars.push(BooleanVariable.of(l.getAsInt()));
                    coeffs.push(sizes.get(i));
                }
            }
            group.add(sumEncoder.encodeSum(vars, coeffs, operator, value));
        }
        return group;
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.sat4j.csp.constraints.encoder.binpacking.IBinPackingEncoder#encodeBinPacking(
     * org.sat4j.specs.IVec, org.sat4j.specs.IVec,
     * org.sat4j.csp.constraints.RelationalOperator, org.sat4j.csp.variables.IVariable)
     */
    @Override
    public IConstr encodeBinPacking(IVec<IVariable> variables, IVec<BigInteger> sizes,
            RelationalOperator operator, IVariable variable) throws ContradictionException {
        IDomain domain = IDomain.unionOf(getDomainsOf(variables));
        var sumEncoder = solver.getSumEncoder();
        var group = new ConstrGroup();
        for (var v : domain) {
            IVec<BigInteger> coeffs = new Vec<>();
            IVec<IVariable> vars = new Vec<>();
            for (int i = 0; i < sizes.size(); i++) {
                var l = variables.get(i).getLiteralForValue(v);
                if (l.isPresent()) {
                    vars.push(BooleanVariable.of(l.getAsInt()));
                    coeffs.push(sizes.get(i));
                }
            }
            group.add(sumEncoder.encodeSum(vars, coeffs, operator, variable));
        }
        return group;
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.sat4j.csp.constraints.encoder.binpacking.IBinPackingEncoder#encodeBinPacking(
     * org.sat4j.specs.IVec, org.sat4j.specs.IVec,
     * org.sat4j.csp.constraints.SetBelongingOperator, java.math.BigInteger,
     * java.math.BigInteger)
     */
    @Override
    public IConstr encodeBinPacking(IVec<IVariable> variables, IVec<BigInteger> sizes,
            SetBelongingOperator operator, BigInteger min, BigInteger max)
            throws ContradictionException {
        IDomain domain = IDomain.unionOf(getDomainsOf(variables));
        var sumEncoder = solver.getSumEncoder();
        var group = new ConstrGroup();
        for (var v : domain) {
            IVec<BigInteger> coeffs = new Vec<>();
            IVec<IVariable> vars = new Vec<>();
            for (int i = 0; i < sizes.size(); i++) {
                var l = variables.get(i).getLiteralForValue(v);
                if (l.isPresent()) {
                    vars.push(BooleanVariable.of(l.getAsInt()));
                    coeffs.push(sizes.get(i));
                }
            }
            group.add(sumEncoder.encodeSum(vars, coeffs, operator, min,max));
        }
        return group;
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.sat4j.csp.constraints.encoder.binpacking.IBinPackingEncoder#encodeBinPacking(
     * org.sat4j.specs.IVec, org.sat4j.specs.IVec,
     * org.sat4j.csp.constraints.SetBelongingOperator, org.sat4j.specs.IVec)
     */
    @Override
    public IConstr encodeBinPacking(IVec<IVariable> variables, IVec<BigInteger> sizes,
            SetBelongingOperator operator, IVec<BigInteger> set) throws ContradictionException {
        IDomain domain = IDomain.unionOf(getDomainsOf(variables));
        var sumEncoder = solver.getSumEncoder();
        var group = new ConstrGroup();
        for (var v : domain) {
            IVec<BigInteger> coeffs = new Vec<>();
            IVec<IVariable> vars = new Vec<>();
            for (int i = 0; i < sizes.size(); i++) {
                var l = variables.get(i).getLiteralForValue(v);
                if (l.isPresent()) {
                    vars.push(BooleanVariable.of(l.getAsInt()));
                    coeffs.push(sizes.get(i));
                }
            }
            group.add(sumEncoder.encodeSum(vars, coeffs, operator, set));
        }
        return group;
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.constraints.encoder.binpacking.IBinPackingEncoder#
     * encodeBinPackingWithConstantCapacities(org.sat4j.specs.IVec, org.sat4j.specs.IVec,
     * org.sat4j.specs.IVec, boolean)
     */
    @Override
    public IConstr encodeBinPackingWithConstantCapacities(IVec<IVariable> variables,
            IVec<BigInteger> sizes, IVec<BigInteger> capacities, boolean loads)
            throws ContradictionException {
        IDomain domain = IDomain.unionOf(getDomainsOf(variables));
        var sumEncoder = solver.getSumEncoder();
        var group = new ConstrGroup();
        int b = 0;
        RelationalOperator operator = loads ? RelationalOperator.EQ:RelationalOperator.LE;
        for (var v : domain) {
            IVec<BigInteger> coeffs = new Vec<>();
            IVec<IVariable> vars = new Vec<>();
            for (int i = 0; i < sizes.size(); i++) {
                var l = variables.get(i).getLiteralForValue(v);
                if (l.isPresent()) {
                    vars.push(BooleanVariable.of(l.getAsInt()));
                    coeffs.push(sizes.get(i));
                }
            }
            group.add(sumEncoder.encodeSum(vars, coeffs, operator, capacities.get(b)));
            b++;
        }
        return group;
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.constraints.encoder.binpacking.IBinPackingEncoder#
     * encodeBinPackingWithVariableCapacities(org.sat4j.specs.IVec, org.sat4j.specs.IVec,
     * org.sat4j.specs.IVec, boolean)
     */
    @Override
    public IConstr encodeBinPackingWithVariableCapacities(IVec<IVariable> variables,
            IVec<BigInteger> sizes, IVec<IVariable> capacities, boolean loads)
            throws ContradictionException {
        IDomain domain = IDomain.unionOf(getDomainsOf(variables));
        var sumEncoder = solver.getSumEncoder();
        var group = new ConstrGroup();
        int b = 0;
        RelationalOperator operator = loads ? RelationalOperator.EQ:RelationalOperator.LE;
        for (var v : domain) {
            IVec<BigInteger> coeffs = new Vec<>();
            IVec<IVariable> vars = new Vec<>();
            for (int i = 0; i < sizes.size(); i++) {
                var l = variables.get(i).getLiteralForValue(v);
                if (l.isPresent()) {
                    vars.push(BooleanVariable.of(l.getAsInt()));
                    coeffs.push(sizes.get(i));
                }
            }
            group.add(sumEncoder.encodeSum(vars, coeffs, operator, capacities.get(b)));
            b++;
        }
        return group;
    }

}
