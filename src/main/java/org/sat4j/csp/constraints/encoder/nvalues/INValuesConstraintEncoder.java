/**
 * Sat4j-CSP, a CSP solver based on the Sat4j platform.
 * Copyright (c) 2021-2022 - Exakis Nelite, Univ Artois & CNRS.
 * All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 * If not, see {@link http://www.gnu.org/licenses}.
 */

package org.sat4j.csp.constraints.encoder.nvalues;

import java.math.BigInteger;

import org.sat4j.core.Vec;
import org.sat4j.csp.constraints.RelationalOperator;
import org.sat4j.csp.constraints.encoder.IConstraintEncoder;
import org.sat4j.csp.variables.IVariable;
import org.sat4j.specs.ContradictionException;
import org.sat4j.specs.IConstr;
import org.sat4j.specs.IVec;

/**
 * The INValuesConstraintEncoder defines a strategy for encoding {@code n-values}
 * constraints using pseudo-Boolean constraints and/or clauses.
 *
 * @author Thibault Falque
 * @author Romain Wallon
 *
 * @version 0.1.0
 */
public interface INValuesConstraintEncoder extends IConstraintEncoder {

    /**
     * Encodes an {@code n-values} constraint using pseudo-Boolean constraints and/or
     * clauses.
     *
     * @param variables The variables appearing in the constraint.
     * @param operator The relational operator used in the constraint.
     * @param nb The number of distinct values to count.
     *
     * @return The created constraint(s), as internally represented by Sat4j.
     *
     * @throws ContradictionException If encoding the constraint results in a trivial
     *         inconsistency.
     *
     * @implSpec The default implementation uses an empty {@code except} vector to invoke
     *           {@link #encodeNValuesExcept(IVec, RelationalOperator, BigInteger, IVec)}.
     *
     * @see #encodeNValuesExcept(IVec, RelationalOperator, BigInteger, IVec)
     */
    default IConstr encodeNValues(IVec<IVariable> variables, RelationalOperator operator,
            BigInteger nb) throws ContradictionException {
        return encodeNValuesExcept(variables, operator, nb, Vec.of());
    }

    /**
     * Encodes an {@code n-values} constraint using pseudo-Boolean constraints and/or
     * clauses.
     *
     * @param variables The variables appearing in the constraint.
     * @param operator The relational operator used in the constraint.
     * @param nb The number of distinct values to count.
     * @param except The values that should not be counted.
     *
     * @return The created constraint(s), as internally represented by Sat4j.
     *
     * @throws ContradictionException If encoding the constraint results in a trivial
     *         inconsistency.
     */
    IConstr encodeNValuesExcept(IVec<IVariable> variables, RelationalOperator operator,
            BigInteger nb, IVec<BigInteger> except) throws ContradictionException;

    /**
     * Encodes an {@code n-values} constraint using pseudo-Boolean constraints and/or
     * clauses.
     *
     * @param variables The variables appearing in the constraint.
     * @param operator The relational operator used in the constraint.
     * @param nb The variable counting the number of distinct values.
     *
     * @return The created constraint(s), as internally represented by Sat4j.
     *
     * @throws ContradictionException If encoding the constraint results in a trivial
     *         inconsistency.
     *
     * @implSpec The default implementation uses an empty {@code except} vector to invoke
     *           {@link #encodeNValuesExcept(IVec, RelationalOperator, IVariable, IVec)}.
     *
     * @see #encodeNValuesExcept(IVec, RelationalOperator, IVariable, IVec)
     */
    default IConstr encodeNValues(IVec<IVariable> variables, RelationalOperator operator,
            IVariable nb) throws ContradictionException {
        return encodeNValuesExcept(variables, operator, nb, Vec.of());
    }

    /**
     * Encodes an {@code n-values} constraint using pseudo-Boolean constraints and/or
     * clauses.
     *
     * @param variables The variables appearing in the constraint.
     *
     * @return The variable counting the number of values assigned to the given variables.
     *
     * @throws ContradictionException If encoding the constraint results in a trivial
     *         inconsistency.
     */
    IVariable encodeNValues(IVec<IVariable> variables) throws ContradictionException;

    /**
     * Encodes an {@code n-values} constraint using pseudo-Boolean constraints and/or
     * clauses.
     *
     * @param variables The variables appearing in the constraint.
     * @param operator The relational operator used in the constraint.
     * @param nb The variable counting the number of distinct values.
     * @param except The values that should not be counted.
     *
     * @return The created constraint(s), as internally represented by Sat4j.
     *
     * @throws ContradictionException If encoding the constraint results in a trivial
     *         inconsistency.
     */
    IConstr encodeNValuesExcept(IVec<IVariable> variables, RelationalOperator operator,
            IVariable nb, IVec<BigInteger> except) throws ContradictionException;

    /**
     * Encodes an {@code n-values} constraint using pseudo-Boolean constraints and/or
     * clauses.
     *
     * @param variables The variables appearing in the constraint.
     * @param except The values that should not be counted.
     *
     * @return The variable counting the number of values assigned to the given variables.
     *
     * @throws ContradictionException If encoding the constraint results in a trivial
     *         inconsistency.
     */
    IVariable encodeNValuesExcept(IVec<IVariable> variables, IVec<BigInteger> except)
            throws ContradictionException;

}
