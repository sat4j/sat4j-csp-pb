/**
 * sat4j-csp
 * Copyright (c) 2023 - Romain Wallon.
 * All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 * If not, see {@link http://www.gnu.org/licenses}.
 */

package org.sat4j.csp.constraints.encoder.knapsack;

import java.math.BigInteger;

import org.sat4j.csp.constraints.RelationalOperator;
import org.sat4j.csp.constraints.SetBelongingOperator;
import org.sat4j.csp.constraints.encoder.AbstractConstraintEncoder;
import org.sat4j.csp.variables.IVariable;
import org.sat4j.specs.ContradictionException;
import org.sat4j.specs.IVec;


/**
 * The DefaultKnapsackEncoder
 *
 * @author Romain Wallon
 *
 * @version 0.1.0
 */
public class DefaultKnapsackEncoder extends AbstractConstraintEncoder implements IKnapsackEncoder {

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.constraints.encoder.knapsack.IKnapsackEncoder#encodeKnapsack(org.sat4j.specs.IVec, org.sat4j.specs.IVec, org.sat4j.csp.constraints.RelationalOperator, java.math.BigInteger, org.sat4j.specs.IVec, org.sat4j.csp.constraints.RelationalOperator, java.math.BigInteger)
     */
    @Override
    public void encodeKnapsack(IVec<IVariable> variables, IVec<BigInteger> weights,
            RelationalOperator wOperator, BigInteger wValue, IVec<BigInteger> profits,
            RelationalOperator pOperator, BigInteger pValue) throws ContradictionException {
        
        solver.getSumEncoder().encodeSum(variables, profits, pOperator, pValue);
        solver.getSumEncoder().encodeSum(variables, weights, wOperator, wValue);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.constraints.encoder.knapsack.IKnapsackEncoder#encodeKnapsack(org.sat4j.specs.IVec, org.sat4j.specs.IVec, org.sat4j.csp.constraints.RelationalOperator, java.math.BigInteger, org.sat4j.specs.IVec, org.sat4j.csp.constraints.RelationalOperator, java.lang.IVariable)
     */
    @Override
    public void encodeKnapsack(IVec<IVariable> variables, IVec<BigInteger> weights,
            RelationalOperator wOperator, BigInteger wValue, IVec<BigInteger> profits,
            RelationalOperator pOperator, IVariable pVariable) throws ContradictionException {
        solver.getSumEncoder().encodeSum(variables, profits, pOperator, pVariable);
        solver.getSumEncoder().encodeSum(variables, weights, wOperator, wValue);

    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.constraints.encoder.knapsack.IKnapsackEncoder#encodeKnapsack(org.sat4j.specs.IVec, org.sat4j.specs.IVec, org.sat4j.csp.constraints.RelationalOperator, java.math.BigInteger, org.sat4j.specs.IVec, org.sat4j.csp.constraints.SetBelongingOperator, java.math.BigInteger, java.math.BigInteger)
     */
    @Override
    public void encodeKnapsack(IVec<IVariable> variables, IVec<BigInteger> weights,
            RelationalOperator wOperator, BigInteger wValue, IVec<BigInteger> profits,
            SetBelongingOperator pOperator, BigInteger pMin, BigInteger pMax) throws ContradictionException {
        solver.getSumEncoder().encodeSum(variables, profits, pOperator, pMin,pMax);
        solver.getSumEncoder().encodeSum(variables, weights, wOperator, wValue);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.constraints.encoder.knapsack.IKnapsackEncoder#encodeKnapsack(org.sat4j.specs.IVec, org.sat4j.specs.IVec, org.sat4j.csp.constraints.RelationalOperator, java.math.BigInteger, org.sat4j.specs.IVec, org.sat4j.csp.constraints.SetBelongingOperator, org.sat4j.specs.IVec)
     */
    @Override
    public void encodeKnapsack(IVec<IVariable> variables, IVec<BigInteger> weights,
            RelationalOperator wOperator, BigInteger wValue, IVec<BigInteger> profits,
            SetBelongingOperator pOperator, IVec<BigInteger> pSet) throws ContradictionException {
        solver.getSumEncoder().encodeSum(variables, profits, pOperator, pSet);
        solver.getSumEncoder().encodeSum(variables, weights, wOperator, wValue);

    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.constraints.encoder.knapsack.IKnapsackEncoder#encodeKnapsack(org.sat4j.specs.IVec, org.sat4j.specs.IVec, org.sat4j.csp.constraints.RelationalOperator, java.lang.IVariable, org.sat4j.specs.IVec, org.sat4j.csp.constraints.RelationalOperator, java.math.BigInteger)
     */
    @Override
    public void encodeKnapsack(IVec<IVariable> variables, IVec<BigInteger> weights,
            RelationalOperator wOperator, IVariable wVariable, IVec<BigInteger> profits,
            RelationalOperator pOperator, BigInteger pValue) throws ContradictionException {
        solver.getSumEncoder().encodeSum(variables, profits, pOperator, pValue);
        solver.getSumEncoder().encodeSum(variables, weights, wOperator, wVariable);

    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.constraints.encoder.knapsack.IKnapsackEncoder#encodeKnapsack(org.sat4j.specs.IVec, org.sat4j.specs.IVec, org.sat4j.csp.constraints.RelationalOperator, java.lang.IVariable, org.sat4j.specs.IVec, org.sat4j.csp.constraints.RelationalOperator, java.lang.IVariable)
     */
    @Override
    public void encodeKnapsack(IVec<IVariable> variables, IVec<BigInteger> weights,
            RelationalOperator wOperator, IVariable wVariable, IVec<BigInteger> profits,
            RelationalOperator pOperator, IVariable pVariable) throws ContradictionException {
        solver.getSumEncoder().encodeSum(variables, profits, pOperator, pVariable);
        solver.getSumEncoder().encodeSum(variables, weights, wOperator, wVariable);

    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.constraints.encoder.knapsack.IKnapsackEncoder#encodeKnapsack(org.sat4j.specs.IVec, org.sat4j.specs.IVec, org.sat4j.csp.constraints.RelationalOperator, java.lang.IVariable, org.sat4j.specs.IVec, org.sat4j.csp.constraints.SetBelongingOperator, java.math.BigInteger, java.math.BigInteger)
     */
    @Override
    public void encodeKnapsack(IVec<IVariable> variables, IVec<BigInteger> weights,
            RelationalOperator wOperator, IVariable wVariable, IVec<BigInteger> profits,
            SetBelongingOperator pOperator, BigInteger pMin, BigInteger pMax) throws ContradictionException {
        solver.getSumEncoder().encodeSum(variables, profits, pOperator, pMin,pMax);
        solver.getSumEncoder().encodeSum(variables, weights, wOperator, wVariable);

    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.constraints.encoder.knapsack.IKnapsackEncoder#encodeKnapsack(org.sat4j.specs.IVec, org.sat4j.specs.IVec, org.sat4j.csp.constraints.RelationalOperator, java.lang.IVariable, org.sat4j.specs.IVec, org.sat4j.csp.constraints.SetBelongingOperator, org.sat4j.specs.IVec)
     */
    @Override
    public void encodeKnapsack(IVec<IVariable> variables, IVec<BigInteger> weights,
            RelationalOperator wOperator, IVariable wVariable, IVec<BigInteger> profits,
            SetBelongingOperator pOperator, IVec<BigInteger> pSet) throws ContradictionException {
        solver.getSumEncoder().encodeSum(variables, profits, pOperator, pSet);
        solver.getSumEncoder().encodeSum(variables, weights, wOperator, wVariable);

    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.constraints.encoder.knapsack.IKnapsackEncoder#encodeKnapsack(org.sat4j.specs.IVec, org.sat4j.specs.IVec, org.sat4j.csp.constraints.SetBelongingOperator, java.math.BigInteger, java.math.BigInteger, org.sat4j.specs.IVec, org.sat4j.csp.constraints.RelationalOperator, java.math.BigInteger)
     */
    @Override
    public void encodeKnapsack(IVec<IVariable> variables, IVec<BigInteger> weights,
            SetBelongingOperator wOperator, BigInteger wMin, BigInteger wMax,
            IVec<BigInteger> profits, RelationalOperator pOperator, BigInteger pValue) throws ContradictionException {
        solver.getSumEncoder().encodeSum(variables, profits, pOperator, pValue);
        solver.getSumEncoder().encodeSum(variables, weights, wOperator, wMin, wMax);

    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.constraints.encoder.knapsack.IKnapsackEncoder#encodeKnapsack(org.sat4j.specs.IVec, org.sat4j.specs.IVec, org.sat4j.csp.constraints.SetBelongingOperator, java.math.BigInteger, java.math.BigInteger, org.sat4j.specs.IVec, org.sat4j.csp.constraints.RelationalOperator, java.lang.IVariable)
     */
    @Override
    public void encodeKnapsack(IVec<IVariable> variables, IVec<BigInteger> weights,
            SetBelongingOperator wOperator, BigInteger wMin, BigInteger wMax,
            IVec<BigInteger> profits, RelationalOperator pOperator, IVariable pVariable) throws ContradictionException {
        solver.getSumEncoder().encodeSum(variables, profits, pOperator, pVariable);
        solver.getSumEncoder().encodeSum(variables, weights, wOperator, wMin, wMax);

    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.constraints.encoder.knapsack.IKnapsackEncoder#encodeKnapsack(org.sat4j.specs.IVec, org.sat4j.specs.IVec, org.sat4j.csp.constraints.SetBelongingOperator, java.math.BigInteger, java.math.BigInteger, org.sat4j.specs.IVec, org.sat4j.csp.constraints.SetBelongingOperator, java.math.BigInteger, java.math.BigInteger)
     */
    @Override
    public void encodeKnapsack(IVec<IVariable> variables, IVec<BigInteger> weights,
            SetBelongingOperator wOperator, BigInteger wMin, BigInteger wMax,
            IVec<BigInteger> profits, SetBelongingOperator pOperator, BigInteger pMin,
            BigInteger pMax) throws ContradictionException {
        solver.getSumEncoder().encodeSum(variables, profits, pOperator, pMin,pMax);
        solver.getSumEncoder().encodeSum(variables, weights, wOperator, wMin, wMax);

    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.constraints.encoder.knapsack.IKnapsackEncoder#encodeKnapsack(org.sat4j.specs.IVec, org.sat4j.specs.IVec, org.sat4j.csp.constraints.SetBelongingOperator, java.math.BigInteger, java.math.BigInteger, org.sat4j.specs.IVec, org.sat4j.csp.constraints.SetBelongingOperator, org.sat4j.specs.IVec)
     */
    @Override
    public void encodeKnapsack(IVec<IVariable> variables, IVec<BigInteger> weights,
            SetBelongingOperator wOperator, BigInteger wMin, BigInteger wMax,
            IVec<BigInteger> profits, SetBelongingOperator pOperator, IVec<BigInteger> pSet) throws ContradictionException {
        solver.getSumEncoder().encodeSum(variables, profits, pOperator, pSet);
        solver.getSumEncoder().encodeSum(variables, weights, wOperator, wMin, wMax);

    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.constraints.encoder.knapsack.IKnapsackEncoder#encodeKnapsack(org.sat4j.specs.IVec, org.sat4j.specs.IVec, org.sat4j.csp.constraints.SetBelongingOperator, org.sat4j.specs.IVec, org.sat4j.specs.IVec, org.sat4j.csp.constraints.RelationalOperator, java.math.BigInteger)
     */
    @Override
    public void encodeKnapsack(IVec<IVariable> variables, IVec<BigInteger> weights,
            SetBelongingOperator wOperator, IVec<BigInteger> wSet, IVec<BigInteger> profits,
            RelationalOperator pOperator, BigInteger pValue) throws ContradictionException {
        solver.getSumEncoder().encodeSum(variables, profits, pOperator, pValue);
        solver.getSumEncoder().encodeSum(variables, weights, wOperator, wSet);

    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.constraints.encoder.knapsack.IKnapsackEncoder#encodeKnapsack(org.sat4j.specs.IVec, org.sat4j.specs.IVec, org.sat4j.csp.constraints.SetBelongingOperator, org.sat4j.specs.IVec, org.sat4j.specs.IVec, org.sat4j.csp.constraints.RelationalOperator, java.lang.IVariable)
     */
    @Override
    public void encodeKnapsack(IVec<IVariable> variables, IVec<BigInteger> weights,
            SetBelongingOperator wOperator, IVec<BigInteger> wSet, IVec<BigInteger> profits,
            RelationalOperator pOperator, IVariable pVariable) throws ContradictionException {
        solver.getSumEncoder().encodeSum(variables, profits, pOperator, pVariable);
        solver.getSumEncoder().encodeSum(variables, weights, wOperator, wSet);

    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.constraints.encoder.knapsack.IKnapsackEncoder#encodeKnapsack(org.sat4j.specs.IVec, org.sat4j.specs.IVec, org.sat4j.csp.constraints.SetBelongingOperator, org.sat4j.specs.IVec, org.sat4j.specs.IVec, org.sat4j.csp.constraints.SetBelongingOperator, java.math.BigInteger, java.math.BigInteger)
     */
    @Override
    public void encodeKnapsack(IVec<IVariable> variables, IVec<BigInteger> weights,
            SetBelongingOperator wOperator, IVec<BigInteger> wSet, IVec<BigInteger> profits,
            SetBelongingOperator pOperator, BigInteger pMin, BigInteger pMax) throws ContradictionException {
        solver.getSumEncoder().encodeSum(variables, profits, pOperator, pMin,pMax);
        solver.getSumEncoder().encodeSum(variables, weights, wOperator, wSet);

    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.constraints.encoder.knapsack.IKnapsackEncoder#encodeKnapsack(org.sat4j.specs.IVec, org.sat4j.specs.IVec, org.sat4j.csp.constraints.SetBelongingOperator, org.sat4j.specs.IVec, org.sat4j.specs.IVec, org.sat4j.csp.constraints.SetBelongingOperator, org.sat4j.specs.IVec)
     */
    @Override
    public void encodeKnapsack(IVec<IVariable> variables, IVec<BigInteger> weights,
            SetBelongingOperator wOperator, IVec<BigInteger> wSet, IVec<BigInteger> profits,
            SetBelongingOperator pOperator, IVec<BigInteger> pSet) throws ContradictionException {
        solver.getSumEncoder().encodeSum(variables, profits, pOperator, pSet);
        solver.getSumEncoder().encodeSum(variables, weights, wOperator, wSet);

    }

}

