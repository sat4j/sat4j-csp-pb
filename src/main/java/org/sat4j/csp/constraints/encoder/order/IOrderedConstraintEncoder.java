/**
 * Sat4j-CSP, a CSP solver based on the Sat4j platform.
 * Copyright (c) 2021-2022 - Exakis Nelite, Univ Artois & CNRS.
 * All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 * If not, see {@link http://www.gnu.org/licenses}.
 */

package org.sat4j.csp.constraints.encoder.order;

import java.math.BigInteger;

import org.sat4j.csp.constraints.RelationalOperator;
import org.sat4j.csp.constraints.encoder.IConstraintEncoder;
import org.sat4j.csp.variables.IVariable;
import org.sat4j.specs.ContradictionException;
import org.sat4j.specs.IConstr;
import org.sat4j.specs.IVec;

/**
 * The IOrderedConstraintEncoder defines a strategy for encoding {@code ordered}
 * constraints using pseudo-Boolean constraints and/or clauses.
 *
 * @author Thibault Falque
 * @author Romain Wallon
 *
 * @version 0.1.0
 */
public interface IOrderedConstraintEncoder extends IConstraintEncoder {

    /**
     * Encodes an {@code ordered} constraint using pseudo-Boolean constraints and/or
     * clauses.
     * This method makes easier the encoding of {@code ordered} constraints over two
     * variables only.
     *
     * @param left The left variable to order.
     * @param operator The relational operator defining the order of the variables.
     * @param right The right variable to order.
     *
     * @return The created constraint(s), as internally represented by Sat4j.
     *
     * @throws ContradictionException If encoding the constraint results in a trivial
     *         inconsistency.
     */
    IConstr encodeOrdered(IVariable left, RelationalOperator operator, IVariable right)
            throws ContradictionException;

    /**
     * Encodes a soft {@code ordered} constraint using pseudo-Boolean constraints
     * and/or clauses.
     *
     * @param selector The selector that allows to turn off the constraint.
     * @param left The left variable to order.
     * @param operator The relational operator defining the order of the variables.
     * @param right The right variable to order.
     *
     * @return The created constraint(s), as internally represented by Sat4j.
     *
     * @throws ContradictionException If encoding the constraint results in a trivial
     *         inconsistency.
     */
    IConstr encodeSoftOrdered(int selector, IVariable left, RelationalOperator operator,
            IVariable right) throws ContradictionException;

    /**
     * Encodes an {@code ordered} constraint using pseudo-Boolean constraints and/or
     * clauses.
     *
     * @param variables The variables that should be ordered.
     * @param operator The relational operator defining the order of the variables.
     *
     * @return The created constraint(s), as internally represented by Sat4j.
     *
     * @throws ContradictionException If encoding the constraint results in a trivial
     *         inconsistency.
     */
    IConstr encodeOrdered(IVec<IVariable> variables, RelationalOperator operator)
            throws ContradictionException;

    /**
     * Encodes an {@code ordered} constraint using pseudo-Boolean constraints and/or
     * clauses.
     *
     * @param variables The variables that should be ordered.
     * @param lengths The lengths that must exist between two consecutive variables.
     * @param operator The relational operator defining the order of the variables.
     *
     * @return The created constraint(s), as internally represented by Sat4j.
     *
     * @throws ContradictionException If encoding the constraint results in a trivial
     *         inconsistency.
     */
    IConstr encodeOrderedWithConstantLength(IVec<IVariable> variables, IVec<BigInteger> lengths,
            RelationalOperator operator) throws ContradictionException;

    /**
     * Encodes an {@code ordered} constraint using pseudo-Boolean constraints and/or
     * clauses.
     *
     * @param variables The variables that should be ordered.
     * @param lengths The variables encoding the lengths that must exist between two
     *        consecutive variables.
     * @param operator The relational operator defining the order of the variables.
     *
     * @return The created constraint(s), as internally represented by Sat4j.
     *
     * @throws ContradictionException If encoding the constraint results in a trivial
     *         inconsistency.
     */
    IConstr encodeOrderedWithVariableLength(IVec<IVariable> variables, IVec<IVariable> lengths,
            RelationalOperator operator) throws ContradictionException;

    /**
     * Encodes an {@code all-equal} constraint using pseudo-Boolean constraints and/or
     * clauses.
     *
     * @param variables The variables that should all be equal.
     *
     * @return The created constraint(s), as internally represented by Sat4j.
     *
     * @throws ContradictionException If encoding the constraint results in a trivial
     *         inconsistency.
     *
     * @implSpec The default implementation invokes {@link #encodeOrdered(IVec, RelationalOperator)}
     *           with {@link RelationalOperator#EQ} as operator.
     *
     * @see #encodeOrdered(IVec, RelationalOperator)
     */
    default IConstr encodeAllEqual(IVec<IVariable> variables) throws ContradictionException {
        return encodeOrdered(variables, RelationalOperator.EQ);
    }

    /**
     * Encodes a {@code not-all-equal} constraint using pseudo-Boolean constraints and/or
     * clauses.
     *
     * @param variables The variables that should not be all equal.
     *
     * @return The created constraint(s), as internally represented by Sat4j.
     *
     * @throws ContradictionException If encoding the constraint results in a trivial
     *         inconsistency.
     */
    IConstr encodeNotAllEqual(IVec<IVariable> variables) throws ContradictionException ;

    /**
     * Encodes a {@code lex} constraint using pseudo-Boolean constraints and/or clauses.
     *
     * @param tuples The tuple of variables that should be lexicographically ordered.
     * @param operator The relational operator defining the order of the tuples.
     *
     * @return The created constraint(s), as internally represented by Sat4j.
     *
     * @throws ContradictionException If encoding the constraint results in a trivial
     *         inconsistency.
     */
    IConstr encodeLex(IVec<IVec<IVariable>> tuples, RelationalOperator operator)
            throws ContradictionException;

    /**
     * Encodes a {@code lex-matrix} constraint using pseudo-Boolean constraints and/or
     * clauses.
     *
     * @param matrix The matrix of variables that should be lexicographically ordered.
     * @param operator The relational operator defining the order in the matrix.
     *
     * @return The created constraint(s), as internally represented by Sat4j.
     *
     * @throws ContradictionException If encoding the constraint results in a trivial
     *         inconsistency.
     */
    IConstr encodeLexMatrix(IVec<IVec<IVariable>> matrix, RelationalOperator operator)
            throws ContradictionException;

}
