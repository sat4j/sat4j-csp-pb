/**
 * Sat4j-CSP, a CSP solver based on the Sat4j platform.
 * Copyright (c) 2021-2022 - Exakis Nelite, Univ Artois & CNRS.
 * All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 * If not, see {@link http://www.gnu.org/licenses}.
 */

package org.sat4j.csp.constraints.encoder.nooverlap;

import java.math.BigInteger;

import org.sat4j.csp.constraints.encoder.IConstraintEncoder;
import org.sat4j.csp.variables.IVariable;
import org.sat4j.specs.ContradictionException;
import org.sat4j.specs.IConstr;
import org.sat4j.specs.IVec;

/**
 * The INoOverlapConstraintEncoder defines a strategy for encoding {@code no-overlap}
 * constraints using pseudo-Boolean constraints and/or clauses.
 *
 * @author Thibault Falque
 * @author Romain Wallon
 *
 * @version 0.1.0
 */
public interface INoOverlapConstraintEncoder extends IConstraintEncoder {

    /**
     * Encodes a {@code no-overlap} constraint using pseudo-Boolean constraints and/or
     * clauses.
     *
     * @param variables The variables appearing in the constraint.
     * @param length The length associated to the variables.
     *
     * @return The created constraint(s), as internally represented by Sat4j.
     *
     * @throws ContradictionException If encoding the constraint results in a trivial
     *         inconsistency.
     *
     * @implSpec The default implementation invokes
     *           {@link #encodeNoOverlap(IVec, IVec, boolean)} with {@code zeroIgnored}
     *           set to {@code true}.
     *
     * @see #encodeNoOverlap(IVec, IVec, boolean)
     */
    default IConstr encodeNoOverlap(IVec<IVariable> variables, IVec<BigInteger> length)
            throws ContradictionException {
        return encodeNoOverlap(variables, length, true);
    }

    /**
     * Encodes a {@code no-overlap} constraint using pseudo-Boolean constraints and/or
     * clauses.
     *
     * @param variables The variables appearing in the constraint.
     * @param length The length associated to the variables.
     * @param zeroIgnored Whether {@code 0}-lengths should be ignored.
     *
     * @return The created constraint(s), as internally represented by Sat4j.
     *
     * @throws ContradictionException If encoding the constraint results in a trivial
     *         inconsistency.
     */
    IConstr encodeNoOverlap(IVec<IVariable> variables, IVec<BigInteger> length, boolean zeroIgnored)
            throws ContradictionException;

    /**
     * Encodes a {@code no-overlap} constraint using pseudo-Boolean constraints and/or
     * clauses.
     *
     * @param variables The variables appearing in the constraint.
     * @param length The variable for the length of the other variables.
     *
     * @return The created constraint(s), as internally represented by Sat4j.
     *
     * @throws ContradictionException If encoding the constraint results in a trivial
     *         inconsistency.
     *
     * @implSpec The default implementation invokes
     *           {@link #encodeNoOverlapVariableLength(IVec, IVec, boolean)} with
     *           {@code zeroIgnored} set to {@code true}.
     *
     * @see #encodeNoOverlapVariableLength(IVec, IVec, boolean)
     */
    default IConstr encodeNoOverlapVariableLength(IVec<IVariable> variables, IVec<IVariable> length)
            throws ContradictionException {
        return encodeNoOverlapVariableLength(variables, length, true);
    }

    /**
     * Encodes a {@code no-overlap} constraint using pseudo-Boolean constraints and/or
     * clauses.
     *
     * @param variables The variables appearing in the constraint.
     * @param length The variable for the length of the other variables.
     * @param zeroIgnored Whether {@code 0}-lengths should be ignored.
     *
     * @return The created constraint(s), as internally represented by Sat4j.
     *
     * @throws ContradictionException If encoding the constraint results in a trivial
     *         inconsistency.
     */
    IConstr encodeNoOverlapVariableLength(IVec<IVariable> variables, IVec<IVariable> length,
            boolean zeroIgnored) throws ContradictionException;

    /**
     * Encodes a multidimensional {@code no-overlap} constraint using pseudo-Boolean
     * constraints and/or clauses.
     *
     * @param variables The variables appearing in the constraint.
     * @param length The length associated to the variables.
     *
     * @return The created constraint(s), as internally represented by Sat4j.
     *
     * @throws ContradictionException If encoding the constraint results in a trivial
     *         inconsistency.
     *
     * @implSpec The default implementation invokes
     *           {@link #encodeMultiDimensionalNoOverlap(IVec, IVec, boolean)} with
     *           {@code zeroIgnored} set to {@code true}.
     *
     * @see #encodeMultiDimensionalNoOverlap(IVec, IVec, boolean)
     */
    default IConstr encodeMultiDimensionalNoOverlap(IVec<IVec<IVariable>> variables,
            IVec<IVec<BigInteger>> length) throws ContradictionException {
        return encodeMultiDimensionalNoOverlap(variables, length, true);
    }

    /**
     * Encodes a multidimensional {@code no-overlap} constraint using pseudo-Boolean
     * constraints and/or clauses.
     *
     * @param variables The variables appearing in the constraint.
     * @param length The length associated to the variables.
     * @param zeroIgnored Whether {@code 0}-lengths should be ignored.
     *
     * @return The created constraint(s), as internally represented by Sat4j.
     *
     * @throws ContradictionException If encoding the constraint results in a trivial
     *         inconsistency.
     */
    IConstr encodeMultiDimensionalNoOverlap(IVec<IVec<IVariable>> variables,
            IVec<IVec<BigInteger>> length, boolean zeroIgnored) throws ContradictionException;

    /**
     * Encodes a multidimensional {@code no-overlap} constraint using pseudo-Boolean
     * constraints and/or clauses.
     *
     * @param variables The variables appearing in the constraint.
     * @param length The variable for the length of the other variables.
     *
     * @return The created constraint(s), as internally represented by Sat4j.
     *
     * @throws ContradictionException If encoding the constraint results in a trivial
     *         inconsistency.
     *
     * @implSpec The default implementation invokes
     *           {@link #encodeMultiDimensionalNoOverlapVariableLength(IVec, IVec, boolean)}
     *           with {@code zeroIgnored} set to {@code true}.
     *
     * @see #encodeMultiDimensionalNoOverlapVariableLength(IVec, IVec, boolean)
     */
    default IConstr encodeMultiDimensionalNoOverlapVariableLength(IVec<IVec<IVariable>> variables,
            IVec<IVec<IVariable>> length) throws ContradictionException {
        return encodeMultiDimensionalNoOverlapVariableLength(variables, length, true);
    }

    /**
     * Encodes a multidimensional {@code no-overlap} constraint using pseudo-Boolean
     * constraints and/or clauses.
     *
     * @param variables The variables appearing in the constraint.
     * @param length The variable for the length of the other variables.
     * @param zeroIgnored Whether {@code 0}-lengths should be ignored.
     *
     * @return The created constraint(s), as internally represented by Sat4j.
     *
     * @throws ContradictionException If encoding the constraint results in a trivial
     *         inconsistency.
     */
    IConstr encodeMultiDimensionalNoOverlapVariableLength(IVec<IVec<IVariable>> variables,
            IVec<IVec<IVariable>> length, boolean zeroIgnored) throws ContradictionException;

}
