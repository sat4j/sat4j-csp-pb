/**
 * The {@code org.sat4j.csp.constraints.encoder.channel} package provides encodings
 * for {@code channel} constraints.
 *
 * @author Thibault Falque
 * @author Romain Wallon
 *
 * @version 0.1.0
 */

package org.sat4j.csp.constraints.encoder.channel;
