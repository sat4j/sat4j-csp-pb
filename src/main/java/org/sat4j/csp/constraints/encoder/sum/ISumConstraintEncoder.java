/**
 * Sat4j-CSP, a CSP solver based on the Sat4j platform.
 * Copyright (c) 2021-2022 - Exakis Nelite, Univ Artois & CNRS.
 * All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 * If not, see {@link http://www.gnu.org/licenses}.
 */

package org.sat4j.csp.constraints.encoder.sum;

import java.math.BigInteger;

import org.sat4j.core.Vec;
import org.sat4j.csp.constraints.RelationalOperator;
import org.sat4j.csp.constraints.SetBelongingOperator;
import org.sat4j.csp.constraints.encoder.IConstraintEncoder;
import org.sat4j.csp.variables.IVariable;
import org.sat4j.pb.ObjectiveFunction;
import org.sat4j.specs.ContradictionException;
import org.sat4j.specs.IConstr;
import org.sat4j.specs.IVec;

/**
 * The ISumConstraintEncoder defines a strategy for encoding {@code sum} constraints using
 * pseudo-Boolean constraints and/or clauses.
 *
 * @author Thibault Falque
 * @author Romain Wallon
 *
 * @version 0.1.0
 */
public interface ISumConstraintEncoder extends IConstraintEncoder {

    /**
     * Encodes a {@code sum} constraint using pseudo-Boolean constraints and/or clauses.
     *
     * @param variables The variables appearing in the constraint.
     * @param operator The relational operator used in the constraint.
     * @param value The value of the right-hand side of the constraint.
     *
     * @return The created constraint(s), as internally represented by Sat4j.
     *
     * @throws ContradictionException If encoding the constraint results in a trivial
     *         inconsistency.
     *
     * @implSpec The default implementation invokes
     *           {@link #encodeSum(IVec, IVec, RelationalOperator, BigInteger)} with a
     *           vector of {@link BigInteger#ONE}s for the coefficients.
     *
     * @see #encodeSum(IVec, IVec, RelationalOperator, BigInteger)
     */
    default IConstr encodeSum(IVec<IVariable> variables, RelationalOperator operator,
            BigInteger value) throws ContradictionException {
        return encodeSum(variables, new Vec<>(variables.size(), BigInteger.ONE), operator, value);
    }

    /**
     * Encodes a soft {@code sum} constraint using pseudo-Boolean constraints and/or
     * clauses.
     *
     * @param selector The selector that allows to turn off the constraint.
     * @param variables The variables appearing in the constraint.
     * @param operator The relational operator used in the constraint.
     * @param value The value of the right-hand side of the constraint.
     *
     * @return The created constraint(s), as internally represented by Sat4j.
     *
     * @throws ContradictionException If encoding the constraint results in a trivial
     *         inconsistency.
     *
     * @implSpec The default implementation invokes
     *           {@link #encodeSoftSum(int, IVec, IVec, RelationalOperator, BigInteger)}
     *           with a vector of {@link BigInteger#ONE}s for the coefficients.
     *
     * @see #encodeSoftSum(int, IVec, IVec, RelationalOperator, BigInteger)
     */
    default IConstr encodeSoftSum(int selector, IVec<IVariable> variables,
            RelationalOperator operator, BigInteger value) throws ContradictionException {
        return encodeSoftSum(selector, variables, new Vec<>(variables.size(), BigInteger.ONE),
                operator, value);
    }

    /**
     * Encodes a {@code sum} constraint using pseudo-Boolean constraints and/or clauses.
     *
     * @param variables The variables appearing in the constraint.
     * @param coefficients The coefficients of the variables appearing in the constraint.
     * @param operator The relational operator used in the constraint.
     * @param value The value of the right-hand side of the constraint.
     *
     * @return The created constraint(s), as internally represented by Sat4j.
     *
     * @throws ContradictionException If encoding the constraint results in a trivial
     *         inconsistency.
     */
    IConstr encodeSum(IVec<IVariable> variables, IVec<BigInteger> coefficients,
            RelationalOperator operator, BigInteger value) throws ContradictionException;

    /**
     * Encodes a soft {@code sum} constraint using pseudo-Boolean constraints and/or
     * clauses.
     *
     * @param selector The selector that allows to turn off the constraint.
     * @param variables The variables appearing in the constraint.
     * @param coefficients The coefficients of the variables appearing in the constraint.
     * @param operator The relational operator used in the constraint.
     * @param value The value of the right-hand side of the constraint.
     *
     * @return The created constraint(s), as internally represented by Sat4j.
     *
     * @throws ContradictionException If encoding the constraint results in a trivial
     *         inconsistency.
     */
    IConstr encodeSoftSum(int selector, IVec<IVariable> variables, IVec<BigInteger> coefficients,
            RelationalOperator operator, BigInteger value) throws ContradictionException;

    /**
     * Encodes a {@code sum} constraint using pseudo-Boolean constraints and/or clauses.
     *
     * @param variables The variables appearing in the constraint.
     * @param operator The relational operator used in the constraint.
     * @param rightVariable The variable on the right-hand side of the constraint.
     *
     * @return The created constraint(s), as internally represented by Sat4j.
     *
     * @throws ContradictionException If encoding the constraint results in a trivial
     *         inconsistency.
     *
     * @implSpec The default implementation invokes
     *           {@link #encodeSum(IVec, IVec, RelationalOperator, IVariable)} with a
     *           vector of {@link BigInteger#ONE}s for the coefficients.
     *
     * @see #encodeSum(IVec, IVec, RelationalOperator, IVariable)
     */
    default IConstr encodeSum(IVec<IVariable> variables, RelationalOperator operator,
            IVariable rightVariable) throws ContradictionException {
        return encodeSum(variables, new Vec<>(variables.size(), BigInteger.ONE), operator,
                rightVariable);
    }

    /**
     * Encodes a soft {@code sum} constraint using pseudo-Boolean constraints and/or
     * clauses.
     *
     * @param selector The selector that allows to turn off the constraint.
     * @param variables The variables appearing in the constraint.
     * @param operator The relational operator used in the constraint.
     * @param rightVariable The variable on the right-hand side of the constraint.
     *
     * @return The created constraint(s), as internally represented by Sat4j.
     *
     * @throws ContradictionException If encoding the constraint results in a trivial
     *         inconsistency.
     *
     * @implSpec The default implementation invokes
     *           {@link #encodeSoftSum(int, IVec, IVec, RelationalOperator, IVariable)}
     *           with a vector of {@link BigInteger#ONE}s for the coefficients.
     *
     * @see #encodeSoftSum(int, IVec, IVec, RelationalOperator, IVariable)
     */
    default IConstr encodeSoftSum(int selector, IVec<IVariable> variables,
            RelationalOperator operator, IVariable rightVariable) throws ContradictionException {
        return encodeSoftSum(selector, variables, new Vec<>(variables.size(), BigInteger.ONE),
                operator, rightVariable);
    }

    /**
     * Encodes a {@code sum} constraint using pseudo-Boolean constraints and/or clauses.
     *
     * @param variables The variables appearing in the constraint.
     * @param coefficients The coefficients of the variables appearing in the constraint.
     * @param operator The relational operator used in the constraint.
     * @param rightVariable The variable on the right-hand side of the constraint.
     *
     * @return The created constraint(s), as internally represented by Sat4j.
     *
     * @throws ContradictionException If encoding the constraint results in a trivial
     *         inconsistency.
     */
    IConstr encodeSum(IVec<IVariable> variables, IVec<BigInteger> coefficients,
            RelationalOperator operator, IVariable rightVariable) throws ContradictionException;

    /**
     * Encodes a soft {@code sum} constraint using pseudo-Boolean constraints and/or
     * clauses.
     *
     * @param selector The selector that allows to turn off the constraint.
     * @param variables The variables appearing in the constraint.
     * @param coefficients The coefficients of the variables appearing in the constraint.
     * @param operator The relational operator used in the constraint.
     * @param rightVariable The variable on the right-hand side of the constraint.
     *
     * @return The created constraint(s), as internally represented by Sat4j.
     *
     * @throws ContradictionException If encoding the constraint results in a trivial
     *         inconsistency.
     */
    IConstr encodeSoftSum(int selector, IVec<IVariable> variables, IVec<BigInteger> coefficients,
            RelationalOperator operator, IVariable rightVariable) throws ContradictionException;

    /**
     * Encodes sum of variables as an objective function.
     *
     * @param variables The variables appearing in the objective function.
     * @param coefficients The coefficients of the variables appearing in the objective
     *        function.
     *
     * @return The created objective function.
     */
    ObjectiveFunction encodeAsObjectiveFunction(IVec<IVariable> variables,
            IVec<BigInteger> coefficients);

    IConstr encodeSum(IVec<IVariable> variables, IVec<BigInteger> coefficients,
            SetBelongingOperator operator, IVec<BigInteger> set) throws ContradictionException;

    IConstr encodeSum(IVec<IVariable> variables, IVec<BigInteger> coefficients,
            SetBelongingOperator operator, BigInteger min, BigInteger max) throws ContradictionException ;
}
