/**
 * Sat4j-CSP, a CSP solver based on the Sat4j platform.
 * Copyright (c) 2021-2022 - Exakis Nelite, Univ Artois & CNRS.
 * All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 * If not, see {@link http://www.gnu.org/licenses}.
 */

package org.sat4j.csp.constraints.encoder.intension;

import java.util.Map;

import org.sat4j.core.Vec;
import org.sat4j.csp.constraints.ArithmeticOperator;
import org.sat4j.csp.constraints.BooleanOperator;
import org.sat4j.csp.constraints.Operator;
import org.sat4j.csp.constraints.RelationalOperator;
import org.sat4j.csp.constraints.encoder.AbstractConstraintEncoder;
import org.sat4j.csp.constraints.intension.BinaryIntensionConstraint;
import org.sat4j.csp.constraints.intension.ConstantIntensionConstraint;
import org.sat4j.csp.constraints.intension.IfThenElseIntensionConstraint;
import org.sat4j.csp.constraints.intension.NaryIntensionConstraint;
import org.sat4j.csp.constraints.intension.UnaryIntensionConstraint;
import org.sat4j.csp.constraints.intension.VariableIntensionConstraint;
import org.sat4j.csp.variables.ConstantVariable;
import org.sat4j.csp.variables.IVariable;
import org.sat4j.specs.ContradictionException;
import org.sat4j.specs.IVec;

/**
 * The DefaultIntensionConstraintVisitor provides a default implementation for
 * {@link IIntensionConstraintVisitor}, which relies on the {@link IIntensionConstraintEncoder}
 * used by the associated solver.
 *
 * @author Thibault Falque
 * @author Romain Wallon
 *
 * @version 0.1.0
 */
public class DefaultIntensionConstraintVisitor extends AbstractConstraintEncoder
        implements IIntensionConstraintVisitor {

    /**
     * The encoders for unary operators.
     */
    private Map<Operator, UnaryEncoder> unaryEncoders = Map.of(
            ArithmeticOperator.NEG, v -> solver.getIntensionEncoder().encodeOpposite(v),
            ArithmeticOperator.ABS, v -> solver.getIntensionEncoder().encodeAbsoluteValue(v),
            ArithmeticOperator.SQR, v -> solver.getIntensionEncoder().encodeSquare(v),
            BooleanOperator.NOT, v -> solver.getIntensionEncoder().encodeNot(v));

    /**
     * The encoders for binary operators.
     */
    private Map<Operator, BinaryEncoder> binaryEncoders = Map.ofEntries(
            Map.entry(ArithmeticOperator.SUB, (l, r) -> solver.getIntensionEncoder().encodeSubtraction(l, r)),
            Map.entry(ArithmeticOperator.DIV, (l, r) -> solver.getIntensionEncoder().encodeDivision(l, r)),
            Map.entry(ArithmeticOperator.MOD, (l, r) -> solver.getIntensionEncoder().encodeModulo(l, r)),
            Map.entry(ArithmeticOperator.POW, (l, r) -> solver.getIntensionEncoder().encodePower(l, r)),
            Map.entry(ArithmeticOperator.DIST, (l, r) -> solver.getIntensionEncoder().encodeDistance(l, r)),
            Map.entry(BooleanOperator.IMPL, (l, r) -> solver.getIntensionEncoder().encodeImplication(l, r)),
            Map.entry(RelationalOperator.LT, (l, r) -> solver.getIntensionEncoder().encodeLessThan(l, r)),
            Map.entry(RelationalOperator.LE, (l, r) -> solver.getIntensionEncoder().encodeLessOrEqual(l, r)),
            Map.entry(RelationalOperator.NEQ, (l, r) -> solver.getIntensionEncoder().encodeDifferent(l, r)),
            Map.entry(RelationalOperator.GE, (l, r) -> solver.getIntensionEncoder().encodeGreaterOrEqual(l, r)),
            Map.entry(RelationalOperator.GT, (l, r) -> solver.getIntensionEncoder().encodeGreaterThan(l, r)));

    /**
     * The encoders for n-ary operators.
     */
    private Map<Operator, NaryEncoder> nAryEncoders = Map.of(
            ArithmeticOperator.ADD, vars -> solver.getIntensionEncoder().encodeAddition(vars),
            ArithmeticOperator.MULT, vars -> solver.getIntensionEncoder().encodeMultiplication(vars),
            ArithmeticOperator.MIN, vars -> solver.getIntensionEncoder().encodeMinimum(vars),
            ArithmeticOperator.MAX, vars -> solver.getIntensionEncoder().encodeMaximum(vars),
            BooleanOperator.AND, vars -> solver.getIntensionEncoder().encodeConjunction(vars),
            BooleanOperator.OR, vars -> solver.getIntensionEncoder().encodeDisjunction(vars),
            BooleanOperator.XOR, vars -> solver.getIntensionEncoder().encodeExclusiveDisjunction(vars),
            BooleanOperator.EQUIV, vars -> solver.getIntensionEncoder().encodeEquivalence(vars),
            RelationalOperator.EQ, vars -> solver.getIntensionEncoder().encodeEqual(vars));

    /**
     * The stack of the variables that have been encoded so far.
     */
    private IVec<IVariable> stack;

    /**
     * Creates a new DefaultIntensionConstraintVisitor.
     */
    public DefaultIntensionConstraintVisitor() {
        this.stack = new Vec<>();
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.sat4j.csp.constraints.encoder.intension.IIntensionConstraintVisitor#visit(org.
     * sat4j.csp.constraints.intension.UnaryIntensionConstraint)
     */
    @Override
    public void visit(UnaryIntensionConstraint constr) throws ContradictionException {
        var variable = stack.last();
        stack.pop();

        var operator = constr.getOperator();
        var encoder = unaryEncoders.get(operator);

        if (encoder == null) {
            throw new UnsupportedOperationException("Unknown unary operator: " + operator);
        }

        var encoded = encoder.encode(variable);
        stack.push(encoded);

    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.sat4j.csp.constraints.encoder.intension.IIntensionConstraintVisitor#visit(org.
     * sat4j.csp.constraints.intension.BinaryIntensionConstraint)
     */
    @Override
    public void visit(BinaryIntensionConstraint constr) throws ContradictionException {
        var right = stack.last();
        stack.pop();
        var left = stack.last();
        stack.pop();

        var operator = constr.getOperator();
        var encoder = binaryEncoders.get(operator);

        if (encoder == null) {
            throw new UnsupportedOperationException("Unknown binary operator: " + operator);
        }

        var encoded = encoder.encode(left, right);
        stack.push(encoded);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.sat4j.csp.constraints.encoder.intension.IIntensionConstraintVisitor#visit(org.
     * sat4j.csp.constraints.intension.NaryIntensionConstraint)
     */
    @Override
    public void visit(NaryIntensionConstraint constr) throws ContradictionException {
        var arity = constr.getArity();
        var children = new Vec<IVariable>(arity);
        for (int i = 0; i < arity; i++) {
            children.push(stack.last());
            stack.pop();
        }

        var operator = constr.getOperator();
        var encoder = nAryEncoders.get(operator);

        if (encoder == null) {
            throw new UnsupportedOperationException("Unknown n-ary operator: " + operator);
        }

        var encoded = encoder.encode(children);
        stack.push(encoded);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.sat4j.csp.constraints.encoder.intension.IIntensionConstraintVisitor#visit(org.
     * sat4j.csp.constraints.intension.IfThenElseIntensionConstraint)
     */
    @Override
    public void visit(IfThenElseIntensionConstraint ifThenElse) throws ContradictionException {
        var ifFalse = stack.last();
        stack.pop();
        var ifTrue = stack.last();
        stack.pop();
        var condition = stack.last();
        stack.pop();

        var encoded = solver.getIntensionEncoder().encodeIfThenElse(condition, ifTrue, ifFalse);
        stack.push(encoded);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.sat4j.csp.constraints.encoder.intension.IIntensionConstraintVisitor#visit(org.
     * sat4j.csp.constraints.intension.VariableIntensionConstraint)
     */
    @Override
    public void visit(VariableIntensionConstraint variable) throws ContradictionException {
        var id = variable.getIdentifier();
        var v = solver.getVariable(id);
        stack.push(v);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.sat4j.csp.constraints.encoder.intension.IIntensionConstraintVisitor#visit(org.
     * sat4j.csp.constraints.intension.ConstantIntensionConstraint)
     */
    @Override
    public void visit(ConstantIntensionConstraint constant) throws ContradictionException {
        var value = constant.getValue();
        stack.push(ConstantVariable.of(value));
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.constraints.encoder.intension.IIntensionConstraintVisitor#
     * getEncodingVariable()
     */
    @Override
    public IVariable getEncodingVariable() {
        if (stack.size() != 1) {
            throw new IllegalStateException("Unexpected number of variables on stack: " + stack.size());
        }
        return stack.last();
    }

}
