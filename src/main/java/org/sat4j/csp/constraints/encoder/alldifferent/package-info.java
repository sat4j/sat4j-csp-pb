/**
 * The {@code org.sat4j.csp.constraints.encoder.alldifferent} package provides encodings
 * for {@code all-different} constraints.
 *
 * @author Thibault Falque
 * @author Romain Wallon
 *
 * @version 0.1.0
 */

package org.sat4j.csp.constraints.encoder.alldifferent;
