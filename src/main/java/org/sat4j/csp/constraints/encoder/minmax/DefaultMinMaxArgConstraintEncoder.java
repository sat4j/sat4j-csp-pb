/**
 * Sat4j-CSP, a CSP solver based on the Sat4j platform.
 * Copyright (c) 2021-2022 - Exakis Nelite, Univ Artois & CNRS.
 * All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 * If not, see {@link http://www.gnu.org/licenses}.
 */

package org.sat4j.csp.constraints.encoder.minmax;

import java.math.BigInteger;

import org.sat4j.csp.constraints.RelationalOperator;
import org.sat4j.csp.constraints.encoder.AbstractConstraintEncoder;
import org.sat4j.csp.constraints.encoder.intension.IIntensionConstraintEncoder;
import org.sat4j.csp.variables.ConstantVariable;
import org.sat4j.csp.variables.IVariable;
import org.sat4j.specs.ContradictionException;
import org.sat4j.specs.IConstr;
import org.sat4j.specs.IVec;

/**
 * The DefaultMinMaxConstraintEncoder provides a base encoding for {@code minimum} and
 * {@code maximum} constraints using pseudo-Boolean constraints and/or clauses.
 *
 * @author Thibault Falque
 * @author Romain Wallon
 *
 * @version 0.1.0
 */
public class DefaultMinMaxArgConstraintEncoder extends AbstractConstraintEncoder
        implements IMinMaxArgConstraintEncoder {

    /*
     * (non-Javadoc)
     *
     * @see
     * org.sat4j.csp.constraints.encoder.minmax.IMinMaxConstraintEncoder#encodeMinimum(org
     * .sat4j.specs.IVec, org.sat4j.csp.constraints.RelationalOperator,
     * java.math.BigInteger)
     */
    @Override
    public IConstr encodeMinimumArg(IVec<IVariable> variables, RelationalOperator operator,
            BigInteger value) throws ContradictionException {
        return encodeMinimumArg(variables, operator, ConstantVariable.of(value));
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.sat4j.csp.constraints.encoder.minmax.IMinMaxConstraintEncoder#encodeMinimum(org
     * .sat4j.specs.IVec, org.sat4j.csp.constraints.RelationalOperator,
     * org.sat4j.csp.variables.IVariable)
     */
    @Override
    public IConstr encodeMinimumArg(IVec<IVariable> variables, RelationalOperator operator,
            IVariable value) throws ContradictionException {
        var exactMin = solver.getIntensionEncoder().encodeMinimum(variables);
        var index = encodeIndex(variables, exactMin);
        return solver.getOrderedEncoder().encodeOrdered(index, operator, value);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.sat4j.csp.constraints.encoder.minmax.IMinMaxConstraintEncoder#encodeMaximum(org
     * .sat4j.specs.IVec, org.sat4j.csp.constraints.RelationalOperator,
     * java.math.BigInteger)
     */
    @Override
    public IConstr encodeMaximumArg(IVec<IVariable> variables, RelationalOperator operator,
            BigInteger value) throws ContradictionException {
        return encodeMaximumArg(variables, operator, ConstantVariable.of(value));
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.sat4j.csp.constraints.encoder.minmax.IMinMaxConstraintEncoder#encodeMaximum(org
     * .sat4j.specs.IVec, org.sat4j.csp.constraints.RelationalOperator,
     * org.sat4j.csp.variables.IVariable)
     */
    @Override
    public IConstr encodeMaximumArg(IVec<IVariable> variables, RelationalOperator operator,
            IVariable value) throws ContradictionException {
        var exactMax = solver.getIntensionEncoder().encodeMaximum(variables);
        var index = encodeIndex(variables, exactMax);
        return solver.getOrderedEncoder().encodeOrdered(index, operator, value);
    }
    
    private IVariable encodeIndex(IVec<IVariable> variables, IVariable value) throws ContradictionException {
        var index = solver.newVariable(0, variables.size());
        IIntensionConstraintEncoder intension = solver.getIntensionEncoder();
        for (int i = 0; i <variables.size(); i++) {
            intension.encodeImplication(
                    intension.encodeEqual(index, ConstantVariable.of(i)),
                    intension.encodeEqual(variables.get(i), value));
        }
        return index;
    }

}
