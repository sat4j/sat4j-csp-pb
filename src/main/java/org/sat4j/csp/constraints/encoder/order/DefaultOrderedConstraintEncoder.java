/**
 * Sat4j-CSP, a CSP solver based on the Sat4j platform.
 * Copyright (c) 2021-2022 - Exakis Nelite, Univ Artois & CNRS.
 * All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 * If not, see {@link http://www.gnu.org/licenses}.
 */

package org.sat4j.csp.constraints.encoder.order;

import static java.math.BigInteger.ONE;
import static java.math.BigInteger.ZERO;

import java.math.BigInteger;

import org.sat4j.core.ConstrGroup;
import org.sat4j.core.Vec;
import org.sat4j.core.VecInt;
import org.sat4j.csp.constraints.RelationalOperator;
import org.sat4j.csp.constraints.encoder.AbstractConstraintEncoder;
import org.sat4j.csp.variables.IVariable;
import org.sat4j.specs.ContradictionException;
import org.sat4j.specs.IConstr;
import org.sat4j.specs.IVec;

/**
 * The DefaultOrderedConstraintEncoder provides a base encoding for {@code ordered}
 * constraints using pseudo-Boolean constraints and/or clauses.
 *
 * @author Thibault Falque
 * @author Romain Wallon
 *
 * @version 0.1.0
 */
public class DefaultOrderedConstraintEncoder extends AbstractConstraintEncoder
        implements IOrderedConstraintEncoder {

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.constraints.encoder.order.IOrderedConstraintEncoder#
     * encodeOrdered(org .sat4j.csp.variables.IVariable,
     * org.sat4j.csp.constraints.RelationalOperator,
     * org.sat4j.csp.variables.IVariable)
     */
    @Override
    public IConstr encodeOrdered(IVariable left, RelationalOperator operator, IVariable right)
            throws ContradictionException {
        return solver.getSumEncoder().encodeSum(Vec.of(left), operator, right);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.constraints.encoder.order.IOrderedConstraintEncoder#
     * encodeSoftOrdered (int, org.sat4j.csp.variables.IVariable,
     * org.sat4j.csp.constraints.RelationalOperator,
     * org.sat4j.csp.variables.IVariable)
     */
    @Override
    public IConstr encodeSoftOrdered(int selector, IVariable left, RelationalOperator operator,
            IVariable right) throws ContradictionException {
        return solver.getSumEncoder().encodeSoftSum(selector, Vec.of(left), operator, right);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.constraints.encoder.order.IOrderedConstraintEncoder#
     * encodeOrdered(org .sat4j.specs.IVec,
     * org.sat4j.csp.constraints.RelationalOperator)
     */
    @Override
    public IConstr encodeOrdered(IVec<IVariable> variables, RelationalOperator operator)
            throws ContradictionException {
        var group = new ConstrGroup();
        for (int i = 1; i < variables.size(); i++) {
            group.add(encodeOrdered(variables.get(i - 1), operator, variables.get(i)));
        }
        return group;
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.constraints.encoder.order.IOrderedConstraintEncoder#
     * encodeOrderedWithConstantLength(org.sat4j.specs.IVec, org.sat4j.specs.IVec,
     * org.sat4j.csp.constraints.RelationalOperator)
     */
    @Override
    public IConstr encodeOrderedWithConstantLength(IVec<IVariable> variables,
            IVec<BigInteger> lengths, RelationalOperator operator) throws ContradictionException {
        // The array of coefficients is always the same, so we keep it for all variables.
        var coefficients = Vec.of(ONE, ONE.negate());

        // Encoding the comparison between all variables.
        var group = new ConstrGroup();
        for (int i = 0; i < variables.size() - 1; i++) {
            var first = variables.get(i);
            var second = variables.get(i + 1);
            var length = lengths.get(i);
            var constr = solver.getSumEncoder().encodeSum(
                    Vec.of(first, second), coefficients, operator, length.negate());
            group.add(constr);
        }

        // Returning the group of all the added constraints.
        return group;
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.constraints.encoder.order.IOrderedConstraintEncoder#
     * encodeOrderedWithVariableLength(org.sat4j.specs.IVec, org.sat4j.specs.IVec,
     * org.sat4j.csp.constraints.RelationalOperator)
     */
    @Override
    public IConstr encodeOrderedWithVariableLength(IVec<IVariable> variables,
            IVec<IVariable> lengths, RelationalOperator operator) throws ContradictionException {
        // The coefficients are always the same, so we keep them for all variables.
        var coefficients = Vec.of(ONE, ONE, ONE.negate());

        // Encoding the comparison between all variables.
        var group = new ConstrGroup();
        for (int i = 0; i < variables.size() - 1; i++) {
            var first = variables.get(i);
            var second = variables.get(i + 1);
            var length = lengths.get(i);
            var constr = solver.getSumEncoder().encodeSum(
                    Vec.of(first, length, second), coefficients, operator, ZERO);
            group.add(constr);
        }

        // Returning the group of all the added constraints.
        return group;
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.constraints.encoder.order.IOrderedConstraintEncoder#
     * encodeNotAllEqual (org.sat4j.specs.IVec)
     */
    @Override
    public IConstr encodeNotAllEqual(IVec<IVariable> variables) throws ContradictionException {
        var encoder = solver.getIntensionEncoder();
        var clause = new VecInt(variables.size() - 1);

        for (int i = 1; i < variables.size(); i++) {
            var equal = encoder.encodeEqual(variables.get(i - 1), variables.get(i));
            var notEqual = -equal.getVariables().get(0);
            clause.push(notEqual);
        }

        return solver.addClause(clause);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.sat4j.csp.constraints.encoder.order.IOrderedConstraintEncoder#encodeLex(
     * org. sat4j.specs.IVec, org.sat4j.csp.constraints.RelationalOperator)
     */
    @Override
    public IConstr encodeLex(IVec<IVec<IVariable>> tuples, RelationalOperator operator)
            throws ContradictionException {
        var group = new ConstrGroup();
        for (int i = 1; i < tuples.size(); i++) {
            group.add(encodeLex(tuples.get(i - 1), operator, tuples.get(i)));
        }
        return group;
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.constraints.encoder.order.IOrderedConstraintEncoder#
     * encodeLexMatrix( org.sat4j.specs.IVec,
     * org.sat4j.csp.constraints.RelationalOperator)
     */
    @Override
    public IConstr encodeLexMatrix(IVec<IVec<IVariable>> matrix, RelationalOperator operator)
            throws ContradictionException {
        var group = new ConstrGroup();

        // Encoding the constraints over the rows.
        group.add(encodeLex(matrix, operator));

        // Encoding the constraints over the columns.
        var c1 = new Vec<IVariable>(matrix.size());
        var c2 = new Vec<IVariable>(matrix.size());
        for (int i = 1; i < matrix.get(0).size(); i++) {
            for (int j = 0; j < matrix.size(); j++) {
                c1.push(matrix.get(j).get(i - 1));
                c2.push(matrix.get(j).get(i));
            }

            group.add(encodeLex(c1, operator, c2));
            c1.clear();
            c2.clear();
        }
        return group;
    }

    /**
     * Encodes a {@code lex} constraint using pseudo-Boolean constraints and/or
     * clauses.
     *
     * @param t1 The first tuple of variables to order.
     * @param operator The relational operator defining the order of the tuples.
     * @param t2 The second tuple of variables to order.
     *
     * @return The created constraint(s), as internally represented by Sat4j.
     *
     * @throws ContradictionException If encoding the constraint results in a
     *         trivial inconsistency.
     */
    private IConstr encodeLex(IVec<IVariable> t1, RelationalOperator operator, IVec<IVariable> t2)
            throws ContradictionException {
        // First, we check the operator: only LE, LT, GT and GE are allowed.
        if ((operator == RelationalOperator.EQ) || (operator == RelationalOperator.NEQ)) {
            throw new UnsupportedOperationException(
                    operator + " is not supported for lex constraints");
        }

        // We use the intension encoder to encode what a lex constraint means.
        var intensionEncoder = solver.getIntensionEncoder();

        // Adding the constraint for each two consecutive indices of variables.
        var group = new ConstrGroup();
        var notAllEqual = new VecInt(t1.size());
        var implication = new VecInt(t1.size());
        for (int i = 0; i < t1.size(); i++) {
            // Retrieving the variables.
            var v1 = t1.get(i);
            var v2 = t2.get(i);

            // Encoding the order on v1 and v2.
            IVariable orderVariable;
            if ((operator == RelationalOperator.LE) || (operator == RelationalOperator.LT)) {
                orderVariable = intensionEncoder.encodeLessOrEqual(v1, v2);

            } else {
                orderVariable = intensionEncoder.encodeGreaterOrEqual(v1, v2);
            }

            // In case of equality of all previous variables, the order of v1 and v2 is enforced.
            notAllEqual.copyTo(implication);
            implication.push(orderVariable.getVariables().get(0));
            group.add(solver.addClause(implication));
            implication.clear();

            // Dealing with equal variables for the next index.
            var equality = intensionEncoder.encodeEqual(v1, v2);
            notAllEqual.push(-equality.getVariables().get(0));
        }

        // When the order is strict, at least two variables from the pairs must be different.
        if ((operator == RelationalOperator.LT) || (operator == RelationalOperator.GT)) {
            group.add(solver.addClause(notAllEqual));
        }

        return group;
    }

}
