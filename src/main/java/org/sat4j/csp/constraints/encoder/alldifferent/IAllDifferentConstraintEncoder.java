/**
 * Sat4j-CSP, a CSP solver based on the Sat4j platform.
 * Copyright (c) 2021-2022 - Exakis Nelite, Univ Artois & CNRS.
 * All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 * If not, see {@link http://www.gnu.org/licenses}.
 */

package org.sat4j.csp.constraints.encoder.alldifferent;

import java.math.BigInteger;

import org.sat4j.core.Vec;
import org.sat4j.csp.constraints.encoder.IConstraintEncoder;
import org.sat4j.csp.variables.IVariable;
import org.sat4j.specs.ContradictionException;
import org.sat4j.specs.IConstr;
import org.sat4j.specs.IVec;

/**
 * The IAllDifferentConstraintEncoder defines a strategy for encoding
 * {@code all-different} constraints using pseudo-Boolean constraints and/or clauses.
 *
 * @author Thibault Falque
 * @author Romain Wallon
 *
 * @version 0.1.0
 */
public interface IAllDifferentConstraintEncoder extends IConstraintEncoder {

    /**
     * Encodes an {@code all-different} constraint using pseudo-Boolean constraints and/or
     * clauses.
     *
     * @param variables The variables that should all be different.
     *
     * @return The created constraint(s), as internally represented by Sat4j.
     *
     * @throws ContradictionException If encoding the constraint results in a trivial
     *         inconsistency.
     *
     * @implSpec The default implementation uses an empty {@code except} vector to invoke
     *           {@link #encodeAllDifferent(IVec, IVec)}.
     *
     * @see #encodeAllDifferent(IVec, IVec)
     */
    default IConstr encodeAllDifferent(IVec<IVariable> variables) throws ContradictionException {
        return encodeAllDifferent(variables, Vec.of());
    }

    /**
     * Encodes an {@code all-different} constraint using pseudo-Boolean constraints and/or
     * clauses.
     *
     * @param variables The variables that should all be different.
     * @param except The values not to consider in the constraint.
     *
     * @return The created constraint(s), as internally represented by Sat4j.
     *
     * @throws ContradictionException If encoding the constraint results in a trivial
     *         inconsistency.
     */
    IConstr encodeAllDifferent(IVec<IVariable> variables, IVec<BigInteger> except)
            throws ContradictionException;

    /**
     * Encodes an {@code all-different} constraint using pseudo-Boolean constraints and/or
     * clauses.
     *
     * @param variableMatrix The matrix of variables that should all be different.
     *
     * @return The created constraint(s), as internally represented by Sat4j.
     *
     * @throws ContradictionException If encoding the constraint results in a trivial
     *         inconsistency.
     *
     * @implSpec The default implementation uses an empty {@code except} vector to invoke
     *           {@link #encodeAllDifferentMatrix(IVec, IVec)}.
     *
     * @see #encodeAllDifferentMatrix(IVec, IVec)
     */
    default IConstr encodeAllDifferentMatrix(IVec<IVec<IVariable>> variableMatrix)
            throws ContradictionException {
        return encodeAllDifferentMatrix(variableMatrix, Vec.of());
    }

    /**
     * Encodes an {@code all-different} constraint using pseudo-Boolean constraints and/or
     * clauses.
     *
     * @param variableMatrix The matrix of variables that should all be different.
     * @param except The values not to consider in the constraint.
     *
     * @return The created constraint(s), as internally represented by Sat4j.
     *
     * @throws ContradictionException If encoding the constraint results in a trivial
     *         inconsistency.
     */
    IConstr encodeAllDifferentMatrix(IVec<IVec<IVariable>> variableMatrix, IVec<BigInteger> except)
            throws ContradictionException;

    /**
     * Encodes an {@code all-different} constraint using pseudo-Boolean constraints and/or
     * clauses.
     *
     * @param variableLists The lists of variables that should all be different.
     *
     * @return The created constraint(s), as internally represented by Sat4j.
     *
     * @throws ContradictionException If encoding the constraint results in a trivial
     *         inconsistency.
     */
    IConstr encodeAllDifferentList(IVec<IVec<IVariable>> variableLists)
            throws ContradictionException;

    /**
     * Adds an {@code all-different} constraint to this solver.
     *
     * @param variableLists The lists of variables that should all be different.
     * @param except The values not to consider in the constraint.
     *
     * @throws UniverseContradictionException If adding the constraint results in a
     *         trivial inconsistency.
     */
    IConstr encodeAllDifferentList(IVec<IVec<String>> variableLists, IVec<IVec<BigInteger>> except)
            throws ContradictionException;

}
