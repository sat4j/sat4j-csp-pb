/**
 * Sat4j-CSP, a CSP solver based on the Sat4j platform.
 * Copyright (c) 2021-2022 - Exakis Nelite, Univ Artois & CNRS.
 * All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 * If not, see {@link http://www.gnu.org/licenses}.
 */

package org.sat4j.csp.constraints.encoder.extension;

import java.math.BigInteger;

import org.sat4j.csp.constraints.encoder.IConstraintEncoder;
import org.sat4j.csp.variables.IVariable;
import org.sat4j.specs.ContradictionException;
import org.sat4j.specs.IConstr;
import org.sat4j.specs.IVec;

/**
 * The IExtensionConstraintEncoder defines a strategy for encoding {@code extension}
 * constraints using pseudo-Boolean constraints and/or clauses.
 *
 * @author Thibault Falque
 * @author Romain Wallon
 *
 * @version 0.1.0
 */
public interface IExtensionConstraintEncoder extends IConstraintEncoder {

    /**
     * Encodes an {@code extension} constraint describing the support of a variable using
     * pseudo-Boolean constraints and/or clauses.
     *
     * @param variable The variable for which the support is given.
     * @param allowedValues The values allowed for the variable.
     *
     * @return The created constraint(s), as internally represented by Sat4j.
     *
     * @throws ContradictionException If encoding the constraint results in a trivial
     *         inconsistency.
     */
    IConstr encodeSupport(IVariable variable, IVec<BigInteger> allowedValues)
            throws ContradictionException;

    /**
     * Encodes an {@code extension} constraint describing the support of a tuple of
     * variables using pseudo-Boolean constraints and/or clauses.
     *
     * @param variableTuple The tuple of variables for which the support is given.
     * @param allowedValues The values allowed for the tuple variables.
     *        Values equal to {@code null} are interpreted as "any value" (as in so-called
     *        "starred tuples").
     *
     * @return The created constraint(s), as internally represented by Sat4j.
     *
     * @throws ContradictionException If encoding the constraint results in a trivial
     *         inconsistency.
     */
    IConstr encodeSupport(IVec<IVariable> variableTuple, IVec<IVec<BigInteger>> allowedValues)
            throws ContradictionException;

    /**
     * Encodes an {@code extension} constraint describing the conflicts of a variable
     * using pseudo-Boolean constraints and/or clauses.
     *
     * @param variable The variable for which the conflicts are given.
     * @param forbiddenValues The values forbidden for the variable.
     *
     * @return The created constraint(s), as internally represented by Sat4j.
     *
     * @throws ContradictionException If encoding the constraint results in a trivial
     *         inconsistency.
     */
    IConstr encodeConflicts(IVariable variable, IVec<BigInteger> forbiddenValues)
            throws ContradictionException;

    /**
     * Encodes an {@code extension} constraint describing the conflicts of a tuple of
     * variables using pseudo-Boolean constraints and/or clauses.
     *
     * @param variableTuple The tuple of variables for which the conflicts are given.
     * @param forbiddenValues The values forbidden for the tuple variables.
     *        Values equal to {@code null} are interpreted as "any value" (as in so-called
     *        "starred tuples").
     *
     * @return The created constraint(s), as internally represented by Sat4j.
     *
     * @throws ContradictionException If encoding the constraint results in a trivial
     *         inconsistency.
     */
    IConstr encodeConflicts(IVec<IVariable> variableTuple, IVec<IVec<BigInteger>> forbiddenValues)
            throws ContradictionException;

}
