/**
 * The {@code org.sat4j.csp.constraints.encoder.order} package provides encodings
 * for {@code order}-based constraints.
 *
 * @author Thibault Falque
 * @author Romain Wallon
 *
 * @version 0.1.0
 */

package org.sat4j.csp.constraints.encoder.order;
