/**
 * The {@code org.sat4j.csp.constraints.encoder.minmax} package provides encodings
 * for {@code minimum} and {@code maximum} constraints.
 *
 * @author Thibault Falque
 * @author Romain Wallon
 *
 * @version 0.1.0
 */

package org.sat4j.csp.constraints.encoder.minmax;
