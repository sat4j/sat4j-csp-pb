/**
 * sat4j-csp
 * Copyright (c) 2023 - Romain Wallon.
 * All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 * If not, see {@link http://www.gnu.org/licenses}.
 */

package org.sat4j.csp.constraints.encoder.binpacking;

import java.math.BigInteger;

import org.sat4j.csp.constraints.RelationalOperator;
import org.sat4j.csp.constraints.SetBelongingOperator;
import org.sat4j.csp.constraints.encoder.IConstraintEncoder;
import org.sat4j.csp.variables.IVariable;
import org.sat4j.specs.ContradictionException;
import org.sat4j.specs.IConstr;
import org.sat4j.specs.IVec;

/**
 * The IBinPackingEncoder
 *
 * @author Romain Wallon
 *
 * @version 0.1.0
 */
public interface IBinPackingEncoder extends IConstraintEncoder{
    /**
     * Adds a {@code bin-packing} constraint to this solver.
     *
     * @param variables The variables appearing in the constraint.
     * @param sizes The sizes of the elements to pack.
     * @param operator The operator used to ensure the capacity of the bin.
     * @param value The value of the bins capacity.
     *
     * @throws ContradictionException If adding the constraint results in a
     *         trivial inconsistency.
     */
    IConstr encodeBinPacking(IVec<IVariable> variables, IVec<BigInteger> sizes,
            RelationalOperator operator, BigInteger value) throws ContradictionException;

    /**
     * Adds a {@code bin-packing} constraint to this solver.
     *
     * @param variables The variables appearing in the constraint.
     * @param sizes The sizes of the elements to pack.
     * @param operator The operator used to ensure the capacity of the bins.
     * @param variable The variable encoding the bins capacity.
     *
     * @throws ContradictionException If adding the constraint results in a
     *         trivial inconsistency.
     */
    IConstr encodeBinPacking(IVec<IVariable> variables, IVec<BigInteger> sizes,
            RelationalOperator operator, IVariable variable) throws ContradictionException;

    /**
     * Adds a {@code bin-packing} constraint to this solver.
     *
     * @param variables The variables appearing in the constraint.
     * @param sizes The sizes of the elements to pack.
     * @param operator The operator used to ensure the capacity of the bins.
     * @param min The minimum capacity of the bins.
     * @param max The maximum capacity of the bins.
     *
     * @throws ContradictionException If adding the constraint results in a
     *         trivial inconsistency.
     */
    IConstr encodeBinPacking(IVec<IVariable> variables, IVec<BigInteger> sizes,
            SetBelongingOperator operator, BigInteger min, BigInteger max) throws ContradictionException;

    /**
     * Adds a {@code bin-packing} constraint to this solver.
     *
     * @param variables The variables appearing in the constraint.
     * @param sizes The sizes of the elements to pack.
     * @param operator The operator used to ensure the capacity of the bins.
     * @param set The allowed capacities for the bins.
     *
     * @throws ContradictionException If adding the constraint results in a
     *         trivial inconsistency.
     */
    IConstr encodeBinPacking(IVec<IVariable> variables, IVec<BigInteger> sizes,
            SetBelongingOperator operator, IVec<BigInteger> set) throws ContradictionException;

    /**
     * Adds a {@code bin-packing} constraint to this solver.
     *
     * @param variables The variables appearing in the constraint.
     * @param sizes The sizes of the elements to pack.
     * @param capacities The capacities of each bin.
     * @param loads Whether bin loads should be computed.
     *
     * @throws ContradictionException If adding the constraint results in a
     *         trivial inconsistency.
     */
    IConstr encodeBinPackingWithConstantCapacities(IVec<IVariable> variables, IVec<BigInteger> sizes,
            IVec<BigInteger> capacities, boolean loads) throws ContradictionException;

    /**
     * Adds a {@code bin-packing} constraint to this solver.
     *
     * @param variables The variables appearing in the constraint.
     * @param sizes The sizes of the elements to pack.
     * @param capacities The variables encoding the capacities of each bin.
     * @param loads Whether bin loads should be computed.
     *
     * @throws ContradictionException If adding the constraint results in a
     *         trivial inconsistency.
     */
    IConstr encodeBinPackingWithVariableCapacities(IVec<IVariable> variables, IVec<BigInteger> sizes,
            IVec<IVariable> capacities, boolean loads) throws ContradictionException;
}

