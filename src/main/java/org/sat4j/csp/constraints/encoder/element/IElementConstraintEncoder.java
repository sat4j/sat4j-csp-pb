/**
 * Sat4j-CSP, a CSP solver based on the Sat4j platform.
 * Copyright (c) 2021-2022 - Exakis Nelite, Univ Artois & CNRS.
 * All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 * If not, see {@link http://www.gnu.org/licenses}.
 */

package org.sat4j.csp.constraints.encoder.element;

import java.math.BigInteger;

import org.sat4j.csp.constraints.encoder.IConstraintEncoder;
import org.sat4j.csp.variables.IVariable;
import org.sat4j.specs.ContradictionException;
import org.sat4j.specs.IConstr;
import org.sat4j.specs.IVec;

/**
 * The IElementConstraintEncoder defines a strategy for encoding {@code element}
 * constraints using pseudo-Boolean constraints and/or clauses.
 *
 * @author Thibault Falque
 * @author Romain Wallon
 *
 * @version 0.1.0
 */
public interface IElementConstraintEncoder extends IConstraintEncoder {

    /**
     * Encodes an {@code element} constraint using pseudo-Boolean constraints and/or
     * clauses.
     *
     * @param variables The variables appearing in the constraint.
     * @param value The value that must appear among the variables.
     *
     * @return The created constraint(s), as internally represented by Sat4j.
     *
     * @throws ContradictionException If encoding the constraint results in a trivial
     *         inconsistency.
     */
    IConstr encodeElement(IVec<IVariable> variables, BigInteger value)
            throws ContradictionException;

    /**
     * Encodes an {@code element} constraint using pseudo-Boolean constraints and/or
     * clauses.
     *
     * @param variables The variables appearing in the constraint.
     * @param value The variable encoding the value that must appear among the variables.
     *
     * @return The created constraint(s), as internally represented by Sat4j.
     *
     * @throws ContradictionException If encoding the constraint results in a trivial
     *         inconsistency.
     */
    IConstr encodeElement(IVec<IVariable> variables, IVariable value) throws ContradictionException;

    /**
     * Encodes an {@code element} constraint using pseudo-Boolean constraints and/or
     * clauses.
     *
     * @param values The values among which to look for the variable.
     * @param startIndex The index at which to start looking for the variable.
     * @param index The index at which the variable appears in the values.
     * @param value The value to look for.
     *
     * @return The created constraint(s), as internally represented by Sat4j.
     *
     * @throws ContradictionException If encoding the constraint results in a trivial
     *         inconsistency.
     */
    IConstr encodeElementConstantValues(IVec<BigInteger> values, int startIndex, IVariable index,
            BigInteger value) throws ContradictionException;

    /**
     * Encodes an {@code element} constraint using pseudo-Boolean constraints and/or
     * clauses.
     *
     * @param values The values among which to look for the variable.
     * @param startIndex The index at which to start looking for the variable.
     * @param index The index at which the variable appears in the values.
     * @param variable The variable whose value is to be looked for.
     *
     * @return The created constraint(s), as internally represented by Sat4j.
     *
     * @throws ContradictionException If encoding the constraint results in a trivial
     *         inconsistency.
     */
    IConstr encodeElementConstantValues(IVec<BigInteger> values, int startIndex, IVariable index,
            IVariable variable) throws ContradictionException;

    /**
     * Encodes an {@code element} constraint using pseudo-Boolean constraints and/or
     * clauses.
     *
     * @param variables The variables among which to look for the value.
     * @param startIndex The index at which to start looking for the value.
     * @param index The index at which the value appears in the variables.
     * @param value The value to look for.
     *
     * @return The created constraint(s), as internally represented by Sat4j.
     *
     * @throws ContradictionException If encoding the constraint results in a trivial
     *         inconsistency.
     */
    IConstr encodeElement(IVec<IVariable> variables, int startIndex, IVariable index,
            BigInteger value) throws ContradictionException;

    /**
     * Encodes an {@code element} constraint using pseudo-Boolean constraints and/or
     * clauses.
     *
     * @param values The variables representing the values among which to look for the
     *        variable.
     * @param startIndex The index at which to start looking for the variable.
     * @param index The index at which the variable appears in the values.
     * @param variable The variable whose value is to be looked for.
     *
     * @return The created constraint(s), as internally represented by Sat4j.
     *
     * @throws ContradictionException If encoding the constraint results in a trivial
     *         inconsistency.
     */
    IConstr encodeElement(IVec<IVariable> values, int startIndex, IVariable index,
            IVariable variable) throws ContradictionException;

    /**
     * Encodes an {@code element} constraint using pseudo-Boolean constraints and/or
     * clauses.
     *
     * @param matrix The matrix of values among which the value must appear.
     * @param startRowIndex The index of the row starting from which the value must
     *        appear.
     * @param rowIndex The variable encoding the index of the row at which the value
     *        appears.
     * @param startColIndex The index of the column starting from which the value must
     *        appear.
     * @param colIndex The variable encoding the index of the column at which the value
     *        appears.
     * @param value The value to look for inside the matrix.
     *
     * @return The created constraint(s), as internally represented by Sat4j.
     *
     * @throws ContradictionException If encoding the constraint results in a trivial
     *         inconsistency.
     */
    IConstr encodeElementConstantMatrix(IVec<IVec<BigInteger>> matrix, int startRowIndex,
            IVariable rowIndex, int startColIndex, IVariable colIndex, BigInteger value)
            throws ContradictionException;

    /**
     * Encodes an {@code element} constraint using pseudo-Boolean constraints and/or
     * clauses.
     *
     * @param matrix The matrix of values among which the value must appear.
     * @param startRowIndex The index of the row starting from which the value must
     *        appear.
     * @param rowIndex The variable encoding the index of the row at which the value
     *        appears.
     * @param startColIndex The index of the column starting from which the value must
     *        appear.
     * @param colIndex The variable encoding the index of the column at which the value
     *        appears.
     * @param value The variable encoding the value to look for inside the matrix.
     *
     * @return The created constraint(s), as internally represented by Sat4j.
     *
     * @throws ContradictionException If encoding the constraint results in a trivial
     *         inconsistency.
     */
    IConstr encodeElementConstantMatrix(IVec<IVec<BigInteger>> matrix, int startRowIndex,
            IVariable rowIndex, int startColIndex, IVariable colIndex, IVariable value)
            throws ContradictionException;

    /**
     * Encodes an {@code element} constraint using pseudo-Boolean constraints and/or
     * clauses.
     *
     * @param matrix The matrix of variables among which the value must be assigned.
     * @param startRowIndex The index of the row starting from which the value must
     *        appear.
     * @param rowIndex The variable encoding the index of the row at which the value
     *        appears.
     * @param startColIndex The index of the column starting from which the value must
     *        appear.
     * @param colIndex The variable encoding the index of the column at which the value
     *        appears.
     * @param value The variable encoding the value to look for inside the matrix.
     *
     * @return The created constraint(s), as internally represented by Sat4j.
     *
     * @throws ContradictionException If encoding the constraint results in a trivial
     *         inconsistency.
     */
    IConstr encodeElementMatrix(IVec<IVec<IVariable>> matrix, int startRowIndex, IVariable rowIndex,
            int startColIndex, IVariable colIndex, BigInteger value) throws ContradictionException;

    /**
     * Encodes an {@code element} constraint using pseudo-Boolean constraints and/or
     * clauses.
     *
     * @param matrix The matrix of variables among which the value must be assigned.
     * @param startRowIndex The index of the row starting from which the value must
     *        appear.
     * @param rowIndex The variable encoding the index of the row at which the value
     *        appears.
     * @param startColIndex The index of the column starting from which the value must
     *        appear.
     * @param colIndex The variable encoding the index of the column at which the value
     *        appears.
     * @param value The variable encoding the value to look for inside the matrix.
     *
     * @return The created constraint(s), as internally represented by Sat4j.
     *
     * @throws ContradictionException If encoding the constraint results in a trivial
     *         inconsistency.
     */
    IConstr encodeElementMatrix(IVec<IVec<IVariable>> matrix, int startRowIndex, IVariable rowIndex,
            int startColIndex, IVariable colIndex, IVariable value) throws ContradictionException;

}
