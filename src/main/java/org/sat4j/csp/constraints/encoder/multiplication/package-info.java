/**
 * The {@code org.sat4j.csp.constraints.encoder.multiplication} package provides encodings
 * for multiplicative operations.
 *
 * @author Thibault Falque
 * @author Romain Wallon
 *
 * @version 0.1.0
 */

package org.sat4j.csp.constraints.encoder.multiplication;
