/**
 * Sat4j-CSP, a CSP solver based on the Sat4j platform.
 * Copyright (c) 2021-2022 - Exakis Nelite, Univ Artois & CNRS.
 * All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 * If not, see {@link http://www.gnu.org/licenses}.
 */

package org.sat4j.csp.constraints.encoder.intension;

import org.sat4j.core.Vec;
import org.sat4j.csp.variables.IVariable;
import org.sat4j.specs.ContradictionException;
import org.sat4j.specs.IVec;

/**
 * The NaryEncoder is a functional interface for encoding an n-ary operator as a
 * conjunction of pseudo-Boolean constraints and/or clauses.
 *
 * @author Thibault Falque
 * @author Romain Wallon
 *
 * @version 0.1.0
 */
@FunctionalInterface
public interface NaryEncoder {

    /**
     * Encodes an {@code intension} constraint that applies an operator to several
     * variables.
     *
     * @param variables The variables on which the operator is applied.
     *
     * @return The variable representing the reification of the encoded constraint.
     *
     * @throws ContradictionException If encoding the constraint results in a trivial
     *         inconsistency.
     */
    default IVariable encode(IVariable... variables) throws ContradictionException {
        return encode(Vec.of(variables));
    }

    /**
     * Encodes an {@code intension} constraint that applies an operator to several
     * variables.
     *
     * @param variables The variables on which the operator is applied.
     *
     * @return The variable representing the reification of the encoded constraint.
     *
     * @throws ContradictionException If encoding the constraint results in a trivial
     *         inconsistency.
     */
    IVariable encode(IVec<IVariable> variables) throws ContradictionException;

}
