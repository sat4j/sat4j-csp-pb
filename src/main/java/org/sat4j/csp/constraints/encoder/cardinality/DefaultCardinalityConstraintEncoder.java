/**
 * Sat4j-CSP, a CSP solver based on the Sat4j platform.
 * Copyright (c) 2021-2022 - Exakis Nelite, Univ Artois & CNRS.
 * All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 * If not, see {@link http://www.gnu.org/licenses}.
 */

package org.sat4j.csp.constraints.encoder.cardinality;

import java.math.BigInteger;
import java.util.OptionalInt;
import java.util.function.BiFunction;

import org.sat4j.core.ConstrGroup;
import org.sat4j.core.Vec;
import org.sat4j.core.VecInt;
import org.sat4j.csp.constraints.encoder.AbstractConstraintEncoder;
import org.sat4j.csp.variables.IVariable;
import org.sat4j.specs.ContradictionException;
import org.sat4j.specs.IConstr;
import org.sat4j.specs.IVec;
import org.sat4j.specs.IVecInt;

/**
 * The DefaultCardinalityConstraintEncoder provides a base encoding for
 * {@code cardinality} constraints using pseudo-Boolean constraints and/or clauses.
 *
 * @author Thibault Falque
 * @author Romain Wallon
 *
 * @version 0.1.0
 */
public class DefaultCardinalityConstraintEncoder extends AbstractConstraintEncoder
        implements ICardinalityConstraintEncoder {

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.constraints.encoder.cardinality.ICardinalityConstraintEncoder#
     * encodeCardinalityWithConstantValuesAndConstantCounts(org.sat4j.specs.IVec,
     * org.sat4j.specs.IVec, org.sat4j.specs.IVec, boolean)
     */
    @Override
    public IConstr encodeCardinalityWithConstantValuesAndConstantCounts(IVec<IVariable> variables,
            IVec<BigInteger> values, IVec<BigInteger> occurs, boolean closed)
            throws ContradictionException {
        return encodeCardinalityWithConstantValuesAndConstantIntervalCounts(
                variables, values, occurs, occurs, closed);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.constraints.encoder.cardinality.ICardinalityConstraintEncoder#
     * encodeCardinalityWithConstantValuesAndConstantIntervalCounts(org.sat4j.specs.IVec,
     * org.sat4j.specs.IVec, org.sat4j.specs.IVec, org.sat4j.specs.IVec, boolean)
     */
    @Override
    public IConstr encodeCardinalityWithConstantValuesAndConstantIntervalCounts(
            IVec<IVariable> variables, IVec<BigInteger> values, IVec<BigInteger> occursMin,
            IVec<BigInteger> occursMax, boolean closed) throws ContradictionException {
        return internalEncodeCardinality(
                variables, values, occursMin, occursMax, closed, this::assignmentAsLiteral);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.constraints.encoder.cardinality.ICardinalityConstraintEncoder#
     * encodeCardinalityWithConstantValuesAndVariableCounts(org.sat4j.specs.IVec,
     * org.sat4j.specs.IVec, org.sat4j.specs.IVec, boolean)
     */
    @Override
    public IConstr encodeCardinalityWithConstantValuesAndVariableCounts(IVec<IVariable> variables,
            IVec<BigInteger> values, IVec<IVariable> occurs, boolean closed)
            throws ContradictionException {
        return internalEncodeCardinality(
                variables, values, occurs, closed, this::assignmentAsLiteral);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.constraints.encoder.cardinality.ICardinalityConstraintEncoder#
     * encodeCardinalityWithVariableValuesAndConstantCounts(org.sat4j.specs.IVec,
     * org.sat4j.specs.IVec, org.sat4j.specs.IVec, boolean)
     */
    @Override
    public IConstr encodeCardinalityWithVariableValuesAndConstantCounts(IVec<IVariable> variables,
            IVec<IVariable> values, IVec<BigInteger> occurs, boolean closed)
            throws ContradictionException {
        return encodeCardinalityWithVariableValuesAndConstantIntervalCounts(
                variables, values, occurs, occurs, closed);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.constraints.encoder.cardinality.ICardinalityConstraintEncoder#
     * encodeCardinalityWithVariableValuesAndConstantIntervalCounts(org.sat4j.specs.IVec,
     * org.sat4j.specs.IVec, org.sat4j.specs.IVec, org.sat4j.specs.IVec, boolean)
     */
    @Override
    public IConstr encodeCardinalityWithVariableValuesAndConstantIntervalCounts(
            IVec<IVariable> variables, IVec<IVariable> values, IVec<BigInteger> occursMin,
            IVec<BigInteger> occursMax, boolean closed) throws ContradictionException {
        return internalEncodeCardinality(
                variables, values, occursMin, occursMax, closed, this::assignmentAsLiteral);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.constraints.encoder.cardinality.ICardinalityConstraintEncoder#
     * encodeCardinalityWithVariableValuesAndVariableCounts(org.sat4j.specs.IVec,
     * org.sat4j.specs.IVec, org.sat4j.specs.IVec, boolean)
     */
    @Override
    public IConstr encodeCardinalityWithVariableValuesAndVariableCounts(IVec<IVariable> variables,
            IVec<IVariable> values, IVec<IVariable> occurs, boolean closed)
            throws ContradictionException {
        return internalEncodeCardinality(
                variables, values, occurs, closed, this::assignmentAsLiteral);
    }

    /**
     * Encodes a {@code cardinality} constraint using pseudo-Boolean constraints and/or
     * clauses.
     *
     * @param <V> The type used to represent the values to count (which must be one of
     *        {@code BigInteger} or {@code IVariable}).
     *
     * @param variables The variables to count the assignments of.
     * @param values The assignable values to count.
     * @param occursMin The minimum number of times each value can be assigned.
     * @param occursMax The maximum number of times each value can be assigned.
     * @param closed Whether only the values in {@code value} can be assigned to the
     *        variables.
     * @param assignmentAsLiteral The function used to encode the assignment of a value
     *        to a variable as a single literal.
     *
     * @return The created constraint(s), as internally represented by Sat4j.
     *
     * @throws ContradictionException If encoding the constraint results in a trivial
     *         inconsistency.
     */
    private <V> IConstr internalEncodeCardinality(IVec<IVariable> variables, IVec<V> values,
            IVec<BigInteger> occursMin, IVec<BigInteger> occursMax, boolean closed,
            BiFunction<IVariable, V, OptionalInt> assignmentAsLiteral)
            throws ContradictionException {
        var group = new ConstrGroup();
        var literals = new VecInt(variables.size());
        var closedValues = new Vec<IVecInt>(variables.size());

        for (int val = 0; val < values.size(); val++) {
            var value = values.get(val);

            // Counting the assignments of this value.
            for (int v = 0; v < variables.size(); v++) {
                var variable = variables.get(v);
                var literal = assignmentAsLiteral.apply(variable, value);
                if (!literal.isPresent()) {
                    // The variable cannot take this value, so there is nothing to do.
                    continue;
                }

                // Updating the count.
                literals.push(literal.getAsInt());

                // Dealing with closed values.
                if (closed) {
                    var clause = closedValues.get(v);
                    if (clause == null) {
                        // As variables are iterated in order, the last one is at position v.
                        clause = new VecInt();
                        closedValues.push(clause);
                    }
                    clause.push(literal.getAsInt());
                }
            }

            // Adding the constraints on the count.
            group.add(solver.addAtLeast(literals, occursMin.get(val).intValue()));
            group.add(solver.addAtMost(literals, occursMax.get(val).intValue()));
            literals.clear();
        }

        // Adding the clauses for closed values.
        // If closed is false, then the vector of clauses is empty.
        for (var it = closedValues.iterator(); it.hasNext();) {
            group.add(solver.addClause(it.next()));
        }
        return group;
    }

    /**
     * Encodes a {@code cardinality} constraint using pseudo-Boolean constraints and/or
     * clauses.
     *
     * @param <V> The type used to represent the values to count (which must be one of
     *        {@code BigInteger} or {@code IVariable}).
     *
     * @param variables The variables to count the assignments of.
     * @param values The assignable values to count.
     * @param occurs The variables encoding the number of times each value can be
     *        assigned.
     * @param closed Whether only the values in {@code value} can be assigned to the
     *        variables.
     * @param assignmentAsLiteral The function used to encode the assignment of a value
     *        to a variable as a single literal.
     *
     * @return The created constraint(s), as internally represented by Sat4j.
     *
     * @throws ContradictionException If encoding the constraint results in a trivial
     *         inconsistency.
     */
    private <V> IConstr internalEncodeCardinality(IVec<IVariable> variables, IVec<V> values,
            IVec<IVariable> occurs, boolean closed,
            BiFunction<IVariable, V, OptionalInt> assignmentAsLiteral)
            throws ContradictionException {
        var group = new ConstrGroup();
        var literals = new VecInt(variables.size());
        var coefficients = new Vec<BigInteger>(variables.size());
        var closedValues = new Vec<IVecInt>(variables.size());

        for (int val = 0; val < values.size(); val++) {
            var value = values.get(val);

            // Counting the assignments of this value.
            for (int v = 0; v < variables.size(); v++) {
                var variable = variables.get(v);
                var literal = assignmentAsLiteral.apply(variable, value);
                if (!literal.isPresent()) {
                    // The variable cannot take this value, so there is nothing to do.
                    continue;
                }

                // Updating the count.
                literals.push(literal.getAsInt());
                coefficients.push(BigInteger.ONE);

                // Dealing with closed values.
                if (closed) {
                    var clause = closedValues.get(v);
                    if (clause == null) {
                        clause = new VecInt();
                        closedValues.push(clause);
                    }
                    clause.push(literal.getAsInt());
                }
            }

            // Moving the variables for the count to the left hand side.
            var count = occurs.get(val);
            for (int i = 0; i < count.nVars(); i++) {
                literals.push(count.getVariables().get(i));
                coefficients.push(count.getCoefficients().get(i).negate());
            }

            // Adding the constraints on the count.
            group.add(solver.addExactly(literals, coefficients, count.getShift()));
            literals.clear();
            coefficients.clear();
        }

        // Adding the clauses for closed values.
        // If closed is false, then the vector of clauses is empty.
        for (var it = closedValues.iterator(); it.hasNext();) {
            group.add(solver.addClause(it.next()));
        }
        return group;
    }

}
