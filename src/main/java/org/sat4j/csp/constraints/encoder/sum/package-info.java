/**
 * The {@code org.sat4j.csp.constraints.encoder.sum} package provides encodings for the
 * {@code sum} constraint.
 *
 * @author Thibault Falque
 * @author Romain Wallon
 *
 * @version 0.1.0
 */

package org.sat4j.csp.constraints.encoder.sum;
