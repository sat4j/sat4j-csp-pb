/**
 * Sat4j-CSP, a CSP solver based on the Sat4j platform.
 * Copyright (c) 2021-2022 - Exakis Nelite, Univ Artois & CNRS.
 * All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 * If not, see {@link http://www.gnu.org/licenses}.
 */

package org.sat4j.csp.constraints.encoder.alldifferent;

import java.math.BigInteger;

import org.sat4j.core.ConstrGroup;
import org.sat4j.core.Vec;
import org.sat4j.core.VecInt;
import org.sat4j.csp.constraints.encoder.AbstractConstraintEncoder;
import org.sat4j.csp.variables.IDomain;
import org.sat4j.csp.variables.IVariable;
import org.sat4j.specs.ContradictionException;
import org.sat4j.specs.IConstr;
import org.sat4j.specs.IVec;

/**
 * The DefaultAllDifferentConstraintEncoder provides a base encoding for
 * {@code all-different} constraints using pseudo-Boolean constraints and/or clauses.
 *
 * @author Thibault Falque
 * @author Romain Wallon
 *
 * @version 0.1.0
 */
public class DefaultAllDifferentConstraintEncoder extends AbstractConstraintEncoder
        implements IAllDifferentConstraintEncoder {

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.constraints.encoder.alldifferent.IAllDifferentConstraintEncoder#
     * encodeAllDifferent(org.sat4j.specs.IVec, org.sat4j.specs.IVec)
     */
    @Override
    public IConstr encodeAllDifferent(IVec<IVariable> variables, IVec<BigInteger> except)
            throws ContradictionException {
        var group = new ConstrGroup();
        var literals = new VecInt(variables.size());
        var domain = IDomain.unionOf(getDomainsOf(variables));

        for (var value : domain) {
            if (except.contains(value)) {
                // Exceptions are not constrained.
                continue;
            }

            // Other values can be assigned at most once.
            for (var it = variables.iterator(); it.hasNext();) {
                var variable = it.next();
                var literal = variable.getLiteralForValue(value);
                if (literal.isPresent()) {
                    literals.push(literal.getAsInt());
                }
            }
            group.add(solver.addAtMost(literals, 1));
            literals.clear();
        }

        return group;
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.constraints.encoder.alldifferent.IAllDifferentConstraintEncoder#
     * encodeAllDifferentMatrix(org.sat4j.specs.IVec, org.sat4j.specs.IVec)
     */
    @Override
    public IConstr encodeAllDifferentMatrix(IVec<IVec<IVariable>> variableMatrix,
            IVec<BigInteger> except) throws ContradictionException {
        var group = new ConstrGroup();

        // Encoding the constraints over the rows.
        for (var it = variableMatrix.iterator(); it.hasNext();) {
            group.add(encodeAllDifferent(it.next(), except));
        }

        // Encoding the constraints over the columns.
        var nCols = variableMatrix.get(0).size();
        var column = new Vec<IVariable>(variableMatrix.size());
        for (int i = 0; i < nCols; i++) {
            for (var it = variableMatrix.iterator(); it.hasNext();) {
                column.push(it.next().get(i));
            }
            group.add(encodeAllDifferent(column, except));
            column.clear();
        }

        return group;
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.constraints.encoder.alldifferent.IAllDifferentConstraintEncoder#
     * encodeAllDifferentList(org.sat4j.specs.IVec)
     */
    @Override
    public IConstr encodeAllDifferentList(IVec<IVec<IVariable>> variableLists)
            throws ContradictionException {
        var group = new ConstrGroup();
        var intensionEncoder = solver.getIntensionEncoder();
        var clause = new VecInt(variableLists.get(0).size());

        // Encoding the pairwise difference between the lists.
        for (int i = 0; i < variableLists.size() - 1; i++) {
            var t1 = variableLists.get(i);

            for (int j = i + 1; j < variableLists.size(); j++) {
                var t2 = variableLists.get(j);

                // At least one of the elements of t1 and t2 must differ.
                for (int k = 0; k < t1.size(); k++) {
                    var notEqual = intensionEncoder.encodeDifferent(t1.get(k), t2.get(k));
                    clause.push(notEqual.getVariables().get(0));
                }

                group.add(solver.addClause(clause));
                clause.clear();
            }
        }

        return group;
    }

    @Override
    public IConstr encodeAllDifferentList(IVec<IVec<String>> variableLists,
            IVec<IVec<BigInteger>> except) throws ContradictionException {
        // TODO Auto-generated method stub.
        return null;
    }

}
