/**
 * Sat4j-CSP, a CSP solver based on the Sat4j platform.
 * Copyright (c) 2021-2022 - Exakis Nelite, Univ Artois & CNRS.
 * All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 * If not, see {@link http://www.gnu.org/licenses}.
 */

package org.sat4j.csp.constraints.encoder.cumulative;

import java.math.BigInteger;

import org.sat4j.csp.constraints.RelationalOperator;
import org.sat4j.csp.constraints.encoder.IConstraintEncoder;
import org.sat4j.csp.variables.IVariable;
import org.sat4j.specs.ContradictionException;
import org.sat4j.specs.IConstr;
import org.sat4j.specs.IVec;

/**
 * The ICumulativeConstraintEncoder defines a strategy for encoding {@code cumulative}
 * constraints using pseudo-Boolean constraints and/or clauses.
 *
 * @author Thibault Falque
 * @author Romain Wallon
 *
 * @version 0.1.0
 */
public interface ICumulativeConstraintEncoder extends IConstraintEncoder {

    /**
     * Encodes a {@code cumulative} constraint using pseudo-Boolean constraints and/or
     * clauses.
     *
     * @param origins The variables encoding the origins of the resources.
     * @param lengths The lengths of the tasks to assign.
     * @param heights The heights of the tasks to assign.
     * @param operator The operator to compare the cumulative use with.
     * @param value The value for the cumulative use.
     *
     * @return The created constraint(s), as internally represented by Sat4j.
     *
     * @throws ContradictionException If encoding the constraint results in a trivial
     *         inconsistency.
     */
    IConstr encodeCumulativeConstantLengthsConstantHeights(IVec<IVariable> origins,
            IVec<BigInteger> lengths, IVec<BigInteger> heights, RelationalOperator operator,
            BigInteger value) throws ContradictionException;

    /**
     * Encodes a {@code cumulative} constraint using pseudo-Boolean constraints and/or
     * clauses.
     *
     * @param origins The variables encoding the origins of the resources.
     * @param lengths The lengths of the tasks to assign.
     * @param ends The variables encoding the ends of the resources.
     * @param heights The heights of the tasks to assign.
     * @param operator The operator to compare the cumulative use with.
     * @param value The value for the cumulative use.
     *
     * @return The created constraint(s), as internally represented by Sat4j.
     *
     * @throws ContradictionException If encoding the constraint results in a trivial
     *         inconsistency.
     */
    IConstr encodeCumulativeConstantLengthsConstantHeights(IVec<IVariable> origins,
            IVec<BigInteger> lengths, IVec<IVariable> ends, IVec<BigInteger> heights,
            RelationalOperator operator, BigInteger value) throws ContradictionException;

    /**
     * Encodes a {@code cumulative} constraint using pseudo-Boolean constraints and/or
     * clauses.
     *
     * @param origins The variables encoding the origins of the resources.
     * @param lengths The lengths of the tasks to assign.
     * @param heights The heights of the tasks to assign.
     * @param operator The operator to compare the cumulative use with.
     * @param value The variable encoding the value for the cumulative use.
     *
     * @return The created constraint(s), as internally represented by Sat4j.
     *
     * @throws ContradictionException If encoding the constraint results in a trivial
     *         inconsistency.
     */
    IConstr encodeCumulativeConstantLengthsConstantHeights(IVec<IVariable> origins,
            IVec<BigInteger> lengths, IVec<BigInteger> heights, RelationalOperator operator,
            IVariable value) throws ContradictionException;

    /**
     * Encodes a {@code cumulative} constraint using pseudo-Boolean constraints and/or
     * clauses.
     *
     * @param origins The variables encoding the origins of the resources.
     * @param lengths The lengths of the tasks to assign.
     * @param ends The variables encoding the ends of the resources.
     * @param heights The heights of the tasks to assign.
     * @param operator The operator to compare the cumulative use with.
     * @param value The variable encoding the value for the cumulative use.
     *
     * @return The created constraint(s), as internally represented by Sat4j.
     *
     * @throws ContradictionException If encoding the constraint results in a trivial
     *         inconsistency.
     */
    IConstr encodeCumulativeConstantLengthsConstantHeights(IVec<IVariable> origins,
            IVec<BigInteger> lengths, IVec<IVariable> ends, IVec<BigInteger> heights,
            RelationalOperator operator, IVariable value) throws ContradictionException;

    /**
     * Encodes a {@code cumulative} constraint using pseudo-Boolean constraints and/or
     * clauses.
     *
     * @param origins The variables encoding the origins of the resources.
     * @param lengths The lengths of the tasks to assign.
     * @param heights The variable encoding the heights of the tasks to assign.
     * @param operator The operator to compare the cumulative use with.
     * @param value The value for the cumulative use.
     *
     * @return The created constraint(s), as internally represented by Sat4j.
     *
     * @throws ContradictionException If encoding the constraint results in a trivial
     *         inconsistency.
     */
    IConstr encodeCumulativeConstantLengthsVariableHeights(IVec<IVariable> origins,
            IVec<BigInteger> lengths, IVec<IVariable> heights, RelationalOperator operator,
            BigInteger value) throws ContradictionException;

    /**
     * Encodes a {@code cumulative} constraint using pseudo-Boolean constraints and/or
     * clauses.
     *
     * @param origins The variables encoding the origins of the resources.
     * @param lengths The lengths of the tasks to assign.
     * @param ends The variables encoding the ends of the resources.
     * @param heights The variable encoding the heights of the tasks to assign.
     * @param operator The operator to compare the cumulative use with.
     * @param value The value for the cumulative use.
     *
     * @return The created constraint(s), as internally represented by Sat4j.
     *
     * @throws ContradictionException If encoding the constraint results in a trivial
     *         inconsistency.
     */
    IConstr encodeCumulativeConstantLengthsVariableHeights(IVec<IVariable> origins,
            IVec<BigInteger> lengths, IVec<IVariable> ends, IVec<IVariable> heights,
            RelationalOperator operator, BigInteger value) throws ContradictionException;

    /**
     * Encodes a {@code cumulative} constraint using pseudo-Boolean constraints and/or
     * clauses.
     *
     * @param origins The variables encoding the origins of the resources.
     * @param lengths The lengths of the tasks to assign.
     * @param heights The variable encoding the heights of the tasks to assign.
     * @param operator The operator to compare the cumulative use with.
     * @param value The variable encoding the value for the cumulative use.
     *
     * @return The created constraint(s), as internally represented by Sat4j.
     *
     * @throws ContradictionException If encoding the constraint results in a trivial
     *         inconsistency.
     */
    IConstr encodeCumulativeConstantLengthsVariableHeights(IVec<IVariable> origins,
            IVec<BigInteger> lengths, IVec<IVariable> heights, RelationalOperator operator,
            IVariable value) throws ContradictionException;

    /**
     * Encodes a {@code cumulative} constraint using pseudo-Boolean constraints and/or
     * clauses.
     *
     * @param origins The variables encoding the origins of the resources.
     * @param lengths The lengths of the tasks to assign.
     * @param ends The variables encoding the ends of the resources.
     * @param heights The variable encoding the heights of the tasks to assign.
     * @param operator The operator to compare the cumulative use with.
     * @param value The variable encoding the value for the cumulative use.
     *
     * @return The created constraint(s), as internally represented by Sat4j.
     *
     * @throws ContradictionException If encoding the constraint results in a trivial
     *         inconsistency.
     */
    IConstr encodeCumulativeConstantLengthsVariableHeights(IVec<IVariable> origins,
            IVec<BigInteger> lengths, IVec<IVariable> ends, IVec<IVariable> heights,
            RelationalOperator operator, IVariable value) throws ContradictionException;

    /**
     * Encodes a {@code cumulative} constraint using pseudo-Boolean constraints and/or
     * clauses.
     *
     * @param origins The variables encoding the origins of the resources.
     * @param lengths The variables encoding the lengths of the tasks to assign.
     * @param heights The heights of the tasks to assign.
     * @param operator The operator to compare the cumulative use with.
     * @param value The value for the cumulative use.
     *
     * @return The created constraint(s), as internally represented by Sat4j.
     *
     * @throws ContradictionException If encoding the constraint results in a trivial
     *         inconsistency.
     */
    IConstr encodeCumulativeVariableLengthsConstantHeights(IVec<IVariable> origins,
            IVec<IVariable> lengths, IVec<BigInteger> heights, RelationalOperator operator,
            BigInteger value) throws ContradictionException;

    /**
     * Encodes a {@code cumulative} constraint using pseudo-Boolean constraints and/or
     * clauses.
     *
     * @param origins The variables encoding the origins of the resources.
     * @param lengths The variables encoding the lengths of the tasks to assign.
     * @param ends The variables encoding the ends of the resources.
     * @param heights The heights of the tasks to assign.
     * @param operator The operator to compare the cumulative use with.
     * @param value The value for the cumulative use.
     *
     * @return The created constraint(s), as internally represented by Sat4j.
     *
     * @throws ContradictionException If encoding the constraint results in a trivial
     *         inconsistency.
     */
    IConstr encodeCumulativeVariableLengthsConstantHeights(IVec<IVariable> origins,
            IVec<IVariable> lengths, IVec<IVariable> ends, IVec<BigInteger> heights,
            RelationalOperator operator, BigInteger value) throws ContradictionException;

    /**
     * Encodes a {@code cumulative} constraint using pseudo-Boolean constraints and/or
     * clauses.
     *
     * @param origins The variables encoding the origins of the resources.
     * @param lengths The variables encoding the lengths of the tasks to assign.
     * @param heights The heights of the tasks to assign.
     * @param operator The operator to compare the cumulative use with.
     * @param value The variable encoding the value for the cumulative use.
     *
     * @return The created constraint(s), as internally represented by Sat4j.
     *
     * @throws ContradictionException If encoding the constraint results in a trivial
     *         inconsistency.
     */
    IConstr encodeCumulativeVariableLengthsConstantHeights(IVec<IVariable> origins,
            IVec<IVariable> lengths, IVec<BigInteger> heights, RelationalOperator operator,
            IVariable value) throws ContradictionException;

    /**
     * Encodes a {@code cumulative} constraint using pseudo-Boolean constraints and/or
     * clauses.
     *
     * @param origins The variables encoding the origins of the resources.
     * @param lengths The variables encoding the lengths of the tasks to assign.
     * @param ends The variables encoding the ends of the resources.
     * @param heights The heights of the tasks to assign.
     * @param operator The operator to compare the cumulative use with.
     * @param value The variable encoding the value for the cumulative use.
     *
     * @return The created constraint(s), as internally represented by Sat4j.
     *
     * @throws ContradictionException If encoding the constraint results in a trivial
     *         inconsistency.
     */
    IConstr encodeCumulativeVariableLengthsConstantHeights(IVec<IVariable> origins,
            IVec<IVariable> lengths, IVec<IVariable> ends, IVec<BigInteger> heights,
            RelationalOperator operator, IVariable value) throws ContradictionException;

    /**
     * Encodes a {@code cumulative} constraint using pseudo-Boolean constraints and/or
     * clauses.
     *
     * @param origins The variables encoding the origins of the resources.
     * @param lengths The variables encoding the lengths of the tasks to assign.
     * @param heights The variables encoding the heights of the tasks to assign.
     * @param operator The operator to compare the cumulative use with.
     * @param value The value for the cumulative use.
     *
     * @return The created constraint(s), as internally represented by Sat4j.
     *
     * @throws ContradictionException If encoding the constraint results in a trivial
     *         inconsistency.
     */
    IConstr encodeCumulativeVariableLengthsVariableHeights(IVec<IVariable> origins,
            IVec<IVariable> lengths, IVec<IVariable> heights, RelationalOperator operator,
            BigInteger value) throws ContradictionException;

    /**
     * Encodes a {@code cumulative} constraint using pseudo-Boolean constraints and/or
     * clauses.
     *
     * @param origins The variables encoding the origins of the resources.
     * @param lengths The variables encoding the lengths of the tasks to assign.
     * @param ends The variables encoding the ends of the resources.
     * @param heights The variables encoding the heights of the tasks to assign.
     * @param operator The operator to compare the cumulative use with.
     * @param value The value for the cumulative use.
     *
     * @return The created constraint(s), as internally represented by Sat4j.
     *
     * @throws ContradictionException If encoding the constraint results in a trivial
     *         inconsistency.
     */
    IConstr encodeCumulativeVariableLengthsVariableHeights(IVec<IVariable> origins,
            IVec<IVariable> lengths, IVec<IVariable> ends, IVec<IVariable> heights,
            RelationalOperator operator, BigInteger value) throws ContradictionException;

    /**
     * Encodes a {@code cumulative} constraint using pseudo-Boolean constraints and/or
     * clauses.
     *
     * @param origins The variables encoding the origins of the resources.
     * @param lengths The variables encoding the lengths of the tasks to assign.
     * @param heights The variables encoding the heights of the tasks to assign.
     * @param operator The operator to compare the cumulative use with.
     * @param value The variable encoding the value for the cumulative use.
     *
     * @return The created constraint(s), as internally represented by Sat4j.
     *
     * @throws ContradictionException If encoding the constraint results in a trivial
     *         inconsistency.
     */
    IConstr encodeCumulativeVariableLengthsVariableHeights(IVec<IVariable> origins,
            IVec<IVariable> lengths, IVec<IVariable> heights, RelationalOperator operator,
            IVariable value) throws ContradictionException;

    /**
     * Encodes a {@code cumulative} constraint using pseudo-Boolean constraints and/or
     * clauses.
     *
     * @param origins The variables encoding the origins of the resources.
     * @param lengths The variables encoding the lengths of the tasks to assign.
     * @param ends The variables encoding the ends of the resources.
     * @param heights The variables encoding the heights of the tasks to assign.
     * @param operator The operator to compare the cumulative use with.
     * @param value The variable encoding the value for the cumulative use.
     *
     * @return The created constraint(s), as internally represented by Sat4j.
     *
     * @throws ContradictionException If encoding the constraint results in a trivial
     *         inconsistency.
     */
    IConstr encodeCumulativeVariableLengthsVariableHeights(IVec<IVariable> origins,
            IVec<IVariable> lengths, IVec<IVariable> ends, IVec<IVariable> heights,
            RelationalOperator operator, IVariable value) throws ContradictionException;

}
