/**
 * Sat4j-CSP, a CSP solver based on the Sat4j platform.
 * Copyright (c) 2021-2022 - Exakis Nelite, Univ Artois & CNRS.
 * All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 * If not, see {@link http://www.gnu.org/licenses}.
 */

package org.sat4j.csp.constraints.encoder.multiplication;

import java.math.BigInteger;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.sat4j.core.Vec;
import org.sat4j.core.VecInt;
import org.sat4j.csp.constraints.encoder.AbstractConstraintEncoder;
import org.sat4j.csp.variables.ConstantVariable;
import org.sat4j.csp.variables.IVariable;
import org.sat4j.specs.ContradictionException;
import org.sat4j.specs.IVec;
import org.sat4j.specs.IVecInt;

/**
 * The NaiveMultiplicationConstraintEncoder provides a naive encoding for multiplicative
 * operations using pseudo-Boolean constraints and/or clauses.
 *
 * @author Thibault Falque
 * @author Romain Wallon
 *
 * @version 0.1.0
 */
public class NaiveMultiplicationConstraintEncoder extends AbstractConstraintEncoder
        implements IMultiplicationConstraintEncoder {

    /**
     * The cache used to define only one literal per multiplication, to avoid creating
     * several literals to represent the same thing.
     */
    private Map<Set<Integer>, Integer> cache = new HashMap<>();

    /*
     * (non-Javadoc)
     *
     * @see
     * org.sat4j.csp.constraints.encoder.multiplication.IMultiplicationConstraintEncoder#
     * encodeMultiplication(org.sat4j.specs.IVecInt)
     */
    @Override
    public int encodeMultiplication(IVecInt literals) throws ContradictionException {
        // Making a set from the literals to compute the multiplication of.
        var literalSet = new HashSet<Integer>();
        for (var it = literals.iterator(); it.hasNext();) {
            literalSet.add(it.next());
        }

        // If there is exactly one literal, it is equal to its multiplication by itself.
        if (literalSet.size() == 1) {
            return literals.get(0);
        }

        // Looking for a cached literal, or caching a new literal to represent the
        // multiplication.
        var s = cache.get(literalSet);
        if (s == null) {
            s = solver.newDimacsVariable();
            solver.addSoftAtLeast(s, literals, literals.size());
            cache.put(literalSet, s);
        }
        return s;
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.sat4j.csp.constraints.encoder.multiplication.IMultiplicationConstraintEncoder#
     * encodeMultiplication(org.sat4j.csp.variables.IVariable,
     * org.sat4j.csp.variables.IVariable)
     */
    @Override
    public IVariable encodeMultiplication(IVariable left, IVariable right)
            throws ContradictionException {
        // Creating the variable holding the multiplication.
        var domain = left.getDomain().multiply(right.getDomain());
        var multiplication = domain.newVariableWithThisDomain(solver.getVariableFactory());

        // Creating the data structure for representing the multiplication of the
        // variables.
        var localCache = new HashMap<Integer, Integer>();
        var literals = new VecInt((left.nVars() + 1) * (right.nVars() + 1));
        var coefficients = new Vec<BigInteger>((left.nVars() + 1) * (right.nVars() + 1));
        var offset = multiplication.getShift();

        // Distributing the multiplication over the literals encoding the variables.
        for (int l = 0; l < left.nVars(); l++) {
            var lit = left.getVariables().get(l);
            var coeff = left.getCoefficients().get(l);

            // Multiplying the literal with all those of the other variable.
            for (int r = 0; r < right.nVars(); r++) {
                var multipliedLit = encodeMultiplication(lit, right.getVariables().get(r));
                var multipliedCoeff = coeff.multiply(right.getCoefficients().get(r));
                offset = updateCoefficients(
                        localCache, literals, coefficients, multipliedLit, multipliedCoeff, offset);
            }

            // Multiplying the literal with the shift.
            var multipliedCoeff = coeff.multiply(right.getShift());
            offset = updateCoefficients(
                    localCache, literals, coefficients, lit, multipliedCoeff, offset);
        }

        // Multiplying each literal of the right variable by the shift of the left one.
        for (int r = 0; r < right.nVars(); r++) {
            var lit = right.getVariables().get(r);
            var multipliedCoeff = right.getCoefficients().get(r).multiply(left.getShift());
            offset = updateCoefficients(
                    localCache, literals, coefficients, lit, multipliedCoeff, offset);
        }

        // Moving the variables for the multiplication to the left hand side
        for (int i = 0; i < multiplication.nVars(); i++) {
            var lit = multiplication.getVariables().get(i);
            var coeff = multiplication.getCoefficients().get(i).negate();
            offset = updateCoefficients(
                    localCache, literals, coefficients, lit, coeff, offset);
        }

        // Adding the equality constraint to the solver.
        solver.addExactly(literals, coefficients,
                offset.subtract(left.getShift().multiply(right.getShift())));

        return multiplication;
    }

    /**
     * Updates the coefficient of a literal in a constraint that is being built.
     *
     * @param localCache The cache used to retrieve literals that are already in the
     *        constraint.
     * @param literals The vector of the literals of the constraint being built.
     * @param coefficients The vector of the coefficients of the constraint being built.
     * @param literal The literal to add to the constraint.
     * @param coefficient The coefficient of the literal to add.
     * @param offset The offset applied to the constraint to collect all shift constants.
     *
     * @return The new value of the offset.
     */
    private BigInteger updateCoefficients(Map<Integer, Integer> localCache, VecInt literals,
            Vec<BigInteger> coefficients, int literal, BigInteger coefficient, BigInteger offset) {
        // Checking whether the literal already appears in the constraint.
        var index = localCache.get(literal);
        if (index != null) {
            coefficients.set(index, coefficients.get(index).add(coefficient));
            return offset;
        }

        // Checking whether the negation of the literal already appears in the constraints.
        var indexNot = localCache.get(-literal);
        if (indexNot != null) {
            coefficients.set(indexNot, coefficients.get(indexNot).subtract(coefficient));
            return offset.subtract(coefficient);
        }

        // The literal does not appear in the constraint: we simply add it.
        localCache.put(literal, literals.size());
        literals.push(literal);
        coefficients.push(coefficient);
        return offset;
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.sat4j.csp.constraints.encoder.multiplication.IMultiplicationConstraintEncoder#
     * encodeMultiplication(org.sat4j.specs.IVec)
     */
    @Override
    public IVariable encodeMultiplication(IVec<IVariable> variables) throws ContradictionException {
        var iterator = variables.iterator();

        if (!iterator.hasNext()) {
            return ConstantVariable.ONE;
        }

        var multiplication = iterator.next();
        while (iterator.hasNext()) {
            multiplication = encodeMultiplication(multiplication, iterator.next());
        }
        return multiplication;
    }

}
