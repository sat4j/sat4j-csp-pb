/**
 * Sat4j-CSP, a CSP solver based on the Sat4j platform.
 * Copyright (c) 2021-2022 - Exakis Nelite, Univ Artois & CNRS.
 * All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 * If not, see {@link http://www.gnu.org/licenses}.
 */

package org.sat4j.csp.constraints.encoder.count;

import java.math.BigInteger;
import java.util.OptionalInt;
import java.util.function.BiFunction;

import org.sat4j.core.Vec;
import org.sat4j.core.VecInt;
import org.sat4j.csp.constraints.RelationalOperator;
import org.sat4j.csp.constraints.encoder.AbstractConstraintEncoder;
import org.sat4j.csp.variables.ConstantVariable;
import org.sat4j.csp.variables.IVariable;
import org.sat4j.specs.ContradictionException;
import org.sat4j.specs.IConstr;
import org.sat4j.specs.IVec;

/**
 * The DefaultCountConstraintEncoder provides a base encoding for {@code count}
 * constraints using pseudo-Boolean constraints and/or clauses.
 *
 * @author Thibault Falque
 * @author Romain Wallon
 *
 * @version 0.1.0
 */
public class DefaultCountConstraintEncoder extends AbstractConstraintEncoder
        implements ICountConstraintEncoder {

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.constraints.encoder.count.ICountConstraintEncoder#
     * encodeCountWithConstantValues(org.sat4j.specs.IVec, org.sat4j.specs.IVec,
     * org.sat4j.csp.constraints.RelationalOperator, java.math.BigInteger)
     */
    @Override
    public IConstr encodeCountWithConstantValues(IVec<IVariable> variables, IVec<BigInteger> values,
            RelationalOperator operator, BigInteger count) throws ContradictionException {
        return encodeCountWithConstantValues(
                variables, values, operator, ConstantVariable.of(count));
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.constraints.encoder.count.ICountConstraintEncoder#
     * encodeCountWithConstantValues(org.sat4j.specs.IVec, org.sat4j.specs.IVec,
     * org.sat4j.csp.constraints.RelationalOperator, org.sat4j.csp.variables.IVariable)
     */
    @Override
    public IConstr encodeCountWithConstantValues(IVec<IVariable> variables, IVec<BigInteger> values,
            RelationalOperator operator, IVariable count) throws ContradictionException {
        return internalEncodeCount(variables, values, operator, count, this::assignmentAsLiteral);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.constraints.encoder.count.ICountConstraintEncoder#
     * encodeCountWithVariableValues(org.sat4j.specs.IVec, org.sat4j.specs.IVec,
     * org.sat4j.csp.constraints.RelationalOperator, java.math.BigInteger)
     */
    @Override
    public IConstr encodeCountWithVariableValues(IVec<IVariable> variables, IVec<IVariable> values,
            RelationalOperator operator, BigInteger count) throws ContradictionException {
        return encodeCountWithVariableValues(
                variables, values, operator, ConstantVariable.of(count));
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.constraints.encoder.count.ICountConstraintEncoder#
     * encodeCountWithVariableValues(org.sat4j.specs.IVec, org.sat4j.specs.IVec,
     * org.sat4j.csp.constraints.RelationalOperator, org.sat4j.csp.variables.IVariable)
     */
    @Override
    public IConstr encodeCountWithVariableValues(IVec<IVariable> variables, IVec<IVariable> values,
            RelationalOperator operator, IVariable count) throws ContradictionException {
        return internalEncodeCount(variables, values, operator, count, this::assignmentAsLiteral);
    }

    /**
     * Encodes a {@code count} constraint using pseudo-Boolean constraints and/or clauses.
     *
     * @param <V> The type used to represent the values to count (which must be one of
     *        {@code BigInteger} or {@code IVariable}).
     *
     * @param variables The variables to count the assignments of.
     * @param values The representation of the values to count the assignments of.
     * @param operator The operator to use to compare the number of assignments to their
     *        expected count.
     * @param count The variable encoding the number of times the values can be assigned
     *        among the variables.
     * @param assignmentAsLiteral The function used to encode the assignment of a value
     *        to a variable as a single literal.
     *
     * @return The created constraint(s), as internally represented by Sat4j.
     *
     * @throws ContradictionException If encoding the constraint results in a trivial
     *         inconsistency.
     */
    private <V> IConstr internalEncodeCount(IVec<IVariable> variables, IVec<V> values,
            RelationalOperator operator, IVariable count,
            BiFunction<IVariable, V, OptionalInt> assignmentAsLiteral)
            throws ContradictionException {
        int nLits = variables.size() * values.size() + count.nVars();
        var counterLiterals = new VecInt(nLits);
        var counterCoefficients = new Vec<BigInteger>(nLits);

        for (var itValues = values.iterator(); itValues.hasNext();) {
            var value = itValues.next();

            // Adding the literals representing the assignment of this value.
            for (var itVariable = variables.iterator(); itVariable.hasNext();) {
                var variable = itVariable.next();
                var literal = assignmentAsLiteral.apply(variable, value);

                if (literal.isPresent()) {
                    counterLiterals.push(literal.getAsInt());
                    counterCoefficients.push(BigInteger.ONE);
                }
            }
        }

        // Moving the variables for the count to the left hand side.
        for (int i = 0; i < count.nVars(); i++) {
            counterLiterals.push(count.getVariables().get(i));
            counterCoefficients.push(count.getCoefficients().get(i).negate());
        }

        // Adding the constraint about the count.
        return solver.addPseudoBoolean(
                counterLiterals, counterCoefficients, operator, count.getShift());
    }

}
