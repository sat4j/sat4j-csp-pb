/**
 * Sat4j-CSP, a CSP solver based on the Sat4j platform.
 * Copyright (c) 2021-2022 - Exakis Nelite, Univ Artois & CNRS.
 * All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 * If not, see {@link http://www.gnu.org/licenses}.
 */

package org.sat4j.csp.constraints.encoder.extension;

import java.math.BigInteger;

import org.sat4j.core.ConstrGroup;
import org.sat4j.core.VecInt;
import org.sat4j.csp.constraints.encoder.AbstractConstraintEncoder;
import org.sat4j.csp.variables.IVariable;
import org.sat4j.specs.ContradictionException;
import org.sat4j.specs.IConstr;
import org.sat4j.specs.IVec;

/**
 * The DefaultExtensionConstraintEncoder provides a base encoding for {@code extension}
 * constraints using pseudo-Boolean constraints and/or clauses.
 *
 * @author Thibault Falque
 * @author Romain Wallon
 *
 * @version 0.1.0
 */
public class DefaultExtensionConstraintEncoder extends AbstractConstraintEncoder
        implements IExtensionConstraintEncoder {

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.constraints.encoder.extension.IExtensionConstraintEncoder#
     * encodeSupport(org.sat4j.csp.variables.IVariable, org.sat4j.specs.IVec)
     */
    @Override
    public IConstr encodeSupport(IVariable variable, IVec<BigInteger> allowedValues)
            throws ContradictionException {
        var group = new ConstrGroup(false);
        var clause = new VecInt(allowedValues.size());
        for (var value : variable.getDomain()) {
            var literal = variable.getLiteralForValue(value);
            if (allowedValues.contains(value) && literal.isPresent()) {
                clause.push(literal.getAsInt());

            } else if (literal.isPresent()) {
                group.add(solver.addClause(VecInt.of(-literal.getAsInt())));
            }
        }
        return solver.addClause(clause);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.constraints.encoder.extension.IExtensionConstraintEncoder#
     * encodeSupport(org.sat4j.specs.IVec, org.sat4j.specs.IVec)
     */
    @Override
    public IConstr encodeSupport(IVec<IVariable> variableTuple,
            IVec<IVec<BigInteger>> allowedValues) throws ContradictionException {
        var group = new ConstrGroup(false);
        var cube = new VecInt(variableTuple.size());
        var clause = new VecInt(allowedValues.size());

        for (var it = allowedValues.iterator(); it.hasNext();) {
            var tuple = it.next();
            var clean = true;

            for (int v = 0; v < variableTuple.size(); v++) {
                var variable = variableTuple.get(v);
                var value = tuple.get(v);

                if (value == null) {
                    // Any value would be a support, so we do not add any literal.
                    continue;
                }

                var literal = variable.getLiteralForValue(value);
                if (!literal.isPresent()) {
                    // The tuple is infeasible, so the cube should not be added.
                    clean = false;
                    break;
                }

                // The value must be allowed.
                cube.push(literal.getAsInt());
            }

            // Adding the soft cube, only if it is clean.
            if (clean) {
                int s = solver.newDimacsVariable();
                group.add(solver.addLooseSoftAtLeast(s, cube, cube.size()));
                clause.push(s);
            }
            cube.clear();
        }

        // Forbidding values from the domains that never appear in supports.
        for (int i = 0; i < variableTuple.size(); i++) {
            var variable = variableTuple.get(i);
            for (var value : variable.getDomain()) {
                boolean found = false;
                for (var it = allowedValues.iterator(); it.hasNext();) {
                    var allowedValue = it.next().get(i);
                    if ((allowedValue == null) || allowedValue.equals(value)) {
                        found = true;
                        break;
                    }
                }

                if (!found) {
                    var literal = variable.getLiteralForValue(value);
                    if (literal.isPresent()) {
                        group.add(solver.addClause(VecInt.of(-literal.getAsInt())));
                    }
                }
            }
        }

        // Adding the clause that forces one of the tuple to be a solution.
        group.add(solver.addClause(clause));
        return group;
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.constraints.encoder.extension.IExtensionConstraintEncoder#
     * encodeConflicts(org.sat4j.csp.variables.IVariable, org.sat4j.specs.IVec)
     */
    @Override
    public IConstr encodeConflicts(IVariable variable, IVec<BigInteger> forbiddenValues)
            throws ContradictionException {
        var cube = new VecInt(forbiddenValues.size());
        for (var it = forbiddenValues.iterator(); it.hasNext();) {
            var literal = variable.getLiteralForValue(it.next());
            if (literal.isPresent()) {
                cube.push(-literal.getAsInt());
            }
        }
        return solver.addAtLeast(cube, cube.size());
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.constraints.encoder.extension.IExtensionConstraintEncoder#
     * encodeConflicts(org.sat4j.specs.IVec, org.sat4j.specs.IVec)
     */
    @Override
    public IConstr encodeConflicts(IVec<IVariable> variableTuple,
            IVec<IVec<BigInteger>> forbiddenValues) throws ContradictionException {
        var group = new ConstrGroup(false);
        var clause = new VecInt(variableTuple.size());

        for (var it = forbiddenValues.iterator(); it.hasNext();) {
            var tuple = it.next();
            var clean = true;

            for (int v = 0; v < variableTuple.size(); v++) {
                var variable = variableTuple.get(v);
                var value = tuple.get(v);

                if (value == null) {
                    // Any value would be a conflict, so we do not add any literal.
                    continue;
                }

                var literal = variable.getLiteralForValue(value);
                if (!literal.isPresent()) {
                    // The tuple is infeasible, so the clause should not be added.
                    clean = false;
                    break;
                }

                // The value must be forbidden.
                clause.push(-literal.getAsInt());
            }

            // Adding the clause, only if it is clean.
            if (clean) {
                group.add(solver.addClause(clause));
            }
            clause.clear();
        }

        return group;
    }

}
