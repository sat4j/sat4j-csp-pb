/**
 * The {@code org.sat4j.csp.constraints.encoder} package provides classes for encoding
 * different CSP constraints.
 *
 * @author Thibault Falque
 * @author Romain Wallon
 *
 * @version 0.1.0
 */

package org.sat4j.csp.constraints.encoder;
