/**
 * Sat4j-CSP, a CSP solver based on the Sat4j platform.
 * Copyright (c) 2021-2022 - Exakis Nelite, Univ Artois & CNRS.
 * All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 * If not, see {@link http://www.gnu.org/licenses}.
 */

package org.sat4j.csp.constraints.encoder.intension;

import org.sat4j.core.Vec;
import org.sat4j.csp.constraints.encoder.IConstraintEncoder;
import org.sat4j.csp.variables.IVariable;
import org.sat4j.specs.ContradictionException;
import org.sat4j.specs.IVec;

/**
 * The IIntensionConstraintEncoder defines a strategy for encoding {@code intension}
 * constraints using pseudo-Boolean constraints and/or clauses.
 *
 * @author Thibault Falque
 * @author Romain Wallon
 *
 * @version 0.1.0
 */
public interface IIntensionConstraintEncoder extends IConstraintEncoder {

    /**
     * Encodes an {@code intension} constraint that applies an {@code opposite} operator
     * to a variable.
     *
     * @param variable The variable on which the operator is applied.
     *
     * @return The variable representing the reification of the encoded constraint.
     *
     * @throws ContradictionException If encoding the constraint results in a trivial
     *         inconsistency.
     */
    IVariable encodeOpposite(IVariable variable) throws ContradictionException;

    /**
     * Encodes an {@code intension} constraint that applies an {@code absolute-value}
     * operator to a variable.
     *
     * @param variable The variable on which the operator is applied.
     *
     * @return The variable representing the reification of the encoded constraint.
     *
     * @throws ContradictionException If encoding the constraint results in a trivial
     *         inconsistency.
     */
    IVariable encodeAbsoluteValue(IVariable variable) throws ContradictionException;

    /**
     * Encodes an {@code intension} constraint that applies an {@code absolute-value}
     * operator to a variable.
     *
     * @param abs The variable that must be equal to the absolute value of the variable.
     * @param variable The variable on which the operator is applied.
     *
     * @return The variable representing the reification of the encoded constraint.
     *
     * @throws ContradictionException If encoding the constraint results in a trivial
     *         inconsistency.
     */
    IVariable encodeAbsoluteValue(IVariable abs, IVariable variable) throws ContradictionException;

    /**
     * Encodes an {@code intension} constraint that applies an {@code addition} operator
     * between different variables.
     *
     * @param variables The variables on which the operator is applied.
     *
     * @return The variable representing the reification of the encoded constraint.
     *
     * @throws ContradictionException If encoding the constraint results in a trivial
     *         inconsistency.
     *
     * @implSpec The default implementation transforms the array of variables into
     *           an {@link IVec} to invoke {@link #encodeAddition(IVec)}.
     *
     * @see #encodeAddition(IVec)
     */
    default IVariable encodeAddition(IVariable... variables) throws ContradictionException {
        return encodeAddition(Vec.of(variables));
    }

    /**
     * Encodes an {@code intension} constraint that applies an {@code addition} operator
     * between different variables.
     *
     * @param variables The variables on which the operator is applied.
     *
     * @return The variable representing the reification of the encoded constraint.
     *
     * @throws ContradictionException If encoding the constraint results in a trivial
     *         inconsistency.
     */
    IVariable encodeAddition(IVec<IVariable> variables) throws ContradictionException;

    /**
     * Encodes an {@code intension} constraint that applies a {@code subtraction} operator
     * between two variables.
     *
     * @param left The left variable on which the operator is applied.
     * @param right The right variable on which the operator is applied.
     *
     * @return The variable representing the reification of the encoded constraint.
     *
     * @throws ContradictionException If encoding the constraint results in a trivial
     *         inconsistency.
     */
    IVariable encodeSubtraction(IVariable left, IVariable right) throws ContradictionException;

    /**
     * Encodes an {@code intension} constraint that applies a {@code multiplication}
     * operator between different variables.
     *
     * @param variables The variables on which the operator is applied.
     *
     * @return The variable representing the reification of the encoded constraint.
     *
     * @throws ContradictionException If encoding the constraint results in a trivial
     *         inconsistency.
     *
     * @implSpec The default implementation transforms the array of variables into
     *           an {@link IVec} to invoke {@link #encodeMultiplication(IVec)}.
     *
     * @see #encodeMultiplication(IVec)
     */
    default IVariable encodeMultiplication(IVariable... variables) throws ContradictionException {
        return encodeMultiplication(Vec.of(variables));
    }

    /**
     * Encodes an {@code intension} constraint that applies a {@code multiplication}
     * operator between different variables.
     *
     * @param variables The variables on which the operator is applied.
     *
     * @return The variable representing the reification of the encoded constraint.
     *
     * @throws ContradictionException If encoding the constraint results in a trivial
     *         inconsistency.
     */
    IVariable encodeMultiplication(IVec<IVariable> variables) throws ContradictionException;

    /**
     * Encodes an {@code intension} constraint that applies a {@code division} operator
     * between two variables.
     *
     * @param left The left variable on which the operator is applied.
     * @param right The right variable on which the operator is applied.
     *
     * @return The variable representing the reification of the encoded constraint.
     *
     * @throws ContradictionException If encoding the constraint results in a trivial
     *         inconsistency.
     */
    IVariable encodeDivision(IVariable left, IVariable right) throws ContradictionException;

    /**
     * Encodes an {@code intension} constraint that applies a {@code division} operator
     * between two variables.
     *
     * @param div The variable that must be equal to the division of the two other
     *        variables.
     * @param left The left variable on which the operator is applied.
     * @param right The right variable on which the operator is applied.
     *
     * @return The {@code div} variable.
     *
     * @throws ContradictionException If encoding the constraint results in a trivial
     *         inconsistency.
     */
    IVariable encodeDivision(IVariable div, IVariable left, IVariable right)
            throws ContradictionException;

    /**
     * Encodes an {@code intension} constraint that applies a {@code modulo} operator
     * between two variables.
     *
     * @param left The left variable on which the operator is applied.
     * @param right The right variable on which the operator is applied.
     *
     * @return The variable representing the reification of the encoded constraint.
     *
     * @throws ContradictionException If encoding the constraint results in a trivial
     *         inconsistency.
     */
    IVariable encodeModulo(IVariable left, IVariable right) throws ContradictionException;

    /**
     * Encodes an {@code intension} constraint that applies a {@code modulo} operator
     * between two variables.
     *
     * @param mod The variable that must be equal to the modulo of the two other
     *        variables.
     * @param left The left variable on which the operator is applied.
     * @param right The right variable on which the operator is applied.
     *
     * @return The {@code mod} variable.
     *
     * @throws ContradictionException If encoding the constraint results in a trivial
     *         inconsistency.
     */
    IVariable encodeModulo(IVariable mod, IVariable left, IVariable right)
            throws ContradictionException;

    /**
     * Encodes an {@code intension} constraint that applies both a {@code division} and
     * a {@code modulo} operator between two variables.
     *
     * @param div The variable that must be equal to the division of the two other
     *        variables.
     * @param mod The variable that must be equal to the modulo of the two other
     *        variables.
     * @param left The left variable on which the operator is applied.
     * @param right The right variable on which the operator is applied.
     *
     * @return The {@code div} variable.
     *
     * @throws ContradictionException If encoding the constraint results in a trivial
     *         inconsistency.
     */
    IVariable encodeDivision(IVariable div, IVariable mod, IVariable left, IVariable right)
            throws ContradictionException;

    /**
     * Encodes an {@code intension} constraint that applies a {@code square} operator
     * to a variable.
     *
     * @param variable The variable on which the operator is applied.
     *
     * @return The variable representing the reification of the encoded constraint.
     *
     * @throws ContradictionException If encoding the constraint results in a trivial
     *         inconsistency.
     */
    IVariable encodeSquare(IVariable variable) throws ContradictionException;

    /**
     * Encodes an {@code intension} constraint that applies a {@code power} operator
     * between two variables.
     *
     * @param variable The variable on which the operator is applied.
     * @param exponent The variable used as exponent in the constraint.
     *
     * @return The variable representing the reification of the encoded constraint.
     *
     * @throws ContradictionException If encoding the constraint results in a trivial
     *         inconsistency.
     */
    IVariable encodePower(IVariable variable, IVariable exponent) throws ContradictionException;

    /**
     * Encodes an {@code intension} constraint that applies a {@code power} operator
     * between two variables.
     *
     * @param pow The variable that must be equal to the the variable raised to the power
     *        of the exponent.
     * @param variable The variable on which the operator is applied.
     * @param exponent The variable used as exponent in the constraint.
     *
     * @return The variable representing the reification of the encoded constraint.
     *
     * @throws ContradictionException If encoding the constraint results in a trivial
     *         inconsistency.
     */
    IVariable encodePower(IVariable pow, IVariable variable, IVariable exponent)
            throws ContradictionException;

    /**
     * Encodes an {@code intension} constraint that applies a {@code minimum}
     * operator between different variables.
     *
     * @param variables The variables on which the operator is applied.
     *
     * @return The variable representing the reification of the encoded constraint.
     *
     * @throws ContradictionException If encoding the constraint results in a trivial
     *         inconsistency.
     *
     * @implSpec The default implementation transforms the array of variables into
     *           an {@link IVec} to invoke {@link #encodeMinimum(IVec)}.
     *
     * @see #encodeMinimum(IVec)
     */
    default IVariable encodeMinimum(IVariable... variables) throws ContradictionException {
        return encodeMinimum(Vec.of(variables));
    }

    /**
     * Encodes an {@code intension} constraint that applies a {@code minimum}
     * operator between different variables.
     *
     * @param variables The variables on which the operator is applied.
     *
     * @return The variable representing the reification of the encoded constraint.
     *
     * @throws ContradictionException If encoding the constraint results in a trivial
     *         inconsistency.
     */
    IVariable encodeMinimum(IVec<IVariable> variables) throws ContradictionException;

    /**
     * Encodes an {@code intension} constraint that applies a {@code minimum}
     * operator between different variables.
     *
     * @param min The variable that must be equal to the minimum of the other variables.
     * @param variables The variables on which the operator is applied.
     *
     * @return The variable representing the reification of the encoded constraint.
     *
     * @throws ContradictionException If encoding the constraint results in a trivial
     *         inconsistency.
     *
     * @implSpec The default implementation transforms the array of variables into
     *           an {@link IVec} to invoke {@link #encodeMinimum(IVariable, IVec)}.
     *
     * @see #encodeMinimum(IVariable, IVec)
     */
    default IVariable encodeMinimum(IVariable min, IVariable... variables)
            throws ContradictionException {
        return encodeMinimum(min, Vec.of(variables));
    }

    /**
     * Encodes an {@code intension} constraint that applies a {@code minimum}
     * operator between different variables.
     *
     * @param min The variable that must be equal to the minimum of the other variables.
     * @param variables The variables on which the operator is applied.
     *
     * @return The variable representing the reification of the encoded constraint.
     *
     * @throws ContradictionException If encoding the constraint results in a trivial
     *         inconsistency.
     */
    IVariable encodeMinimum(IVariable min, IVec<IVariable> variables) throws ContradictionException;

    /**
     * Encodes an {@code intension} constraint that applies a {@code maximum}
     * operator between different variables.
     *
     * @param variables The variables on which the operator is applied.
     *
     * @return The variable representing the reification of the encoded constraint.
     *
     * @throws ContradictionException If encoding the constraint results in a trivial
     *         inconsistency.
     *
     * @implSpec The default implementation transforms the array of variables into
     *           an {@link IVec} to invoke {@link #encodeMaximum(IVec)}.
     *
     * @see #encodeMaximum(IVec)
     */
    default IVariable encodeMaximum(IVariable... variables) throws ContradictionException {
        return encodeMaximum(Vec.of(variables));
    }

    /**
     * Encodes an {@code intension} constraint that applies a {@code maximum}
     * operator between different variables.
     *
     * @param variables The variables on which the operator is applied.
     *
     * @return The variable representing the reification of the encoded constraint.
     *
     * @throws ContradictionException If encoding the constraint results in a trivial
     *         inconsistency.
     */
    IVariable encodeMaximum(IVec<IVariable> variables) throws ContradictionException;

    /**
     * Encodes an {@code intension} constraint that applies a {@code maximum}
     * operator between different variables.
     *
     * @param max The variable that must be equal to the maximum of the other variables.
     * @param variables The variables on which the operator is applied.
     *
     * @return The variable representing the reification of the encoded constraint.
     *
     * @throws ContradictionException If encoding the constraint results in a trivial
     *         inconsistency.
     *
     * @implSpec The default implementation transforms the array of variables into
     *           an {@link IVec} to invoke {@link #encodeMaximum(IVariable, IVec)}.
     *
     * @see #encodeMaximum(IVariable, IVec)
     */
    default IVariable encodeMaximum(IVariable max, IVariable... variables)
            throws ContradictionException {
        return encodeMaximum(max, Vec.of(variables));
    }

    /**
     * Encodes an {@code intension} constraint that applies a {@code maximum}
     * operator between different variables.
     *
     * @param max The variable that must be equal to the maximum of the other variables.
     * @param variables The variables on which the operator is applied.
     *
     * @return The variable representing the reification of the encoded constraint.
     *
     * @throws ContradictionException If encoding the constraint results in a trivial
     *         inconsistency.
     */
    IVariable encodeMaximum(IVariable max, IVec<IVariable> variables) throws ContradictionException;

    /**
     * Encodes an {@code intension} constraint that applies a {@code distance} operator
     * between two variables.
     *
     * @param left The left variable on which the operator is applied.
     * @param right The right variable on which the operator is applied.
     *
     * @return The variable representing the reification of the encoded constraint.
     *
     * @throws ContradictionException If encoding the constraint results in a trivial
     *         inconsistency.
     */
    IVariable encodeDistance(IVariable left, IVariable right) throws ContradictionException;

    /**
     * Encodes an {@code intension} constraint that applies a {@code distance} operator
     * between two variables.
     *
     * @param dist The variable that must be equal to the distance between the variables.
     * @param left The left variable on which the operator is applied.
     * @param right The right variable on which the operator is applied.
     *
     * @return The variable representing the reification of the encoded constraint.
     *
     * @throws ContradictionException If encoding the constraint results in a trivial
     *         inconsistency.
     */
    IVariable encodeDistance(IVariable dist, IVariable left, IVariable right)
            throws ContradictionException;

    /**
     * Encodes an {@code intension} constraint that applies a {@code less-than} operator
     * between two variables.
     *
     * @param left The left variable on which the operator is applied.
     * @param right The right variable on which the operator is applied.
     *
     * @return The variable representing the reification of the encoded constraint.
     *
     * @throws ContradictionException If encoding the constraint results in a trivial
     *         inconsistency.
     */
    IVariable encodeLessThan(IVariable left, IVariable right) throws ContradictionException;

    /**
     * Encodes an {@code intension} constraint that applies a {@code less-or-equal}
     * operator between two variables.
     *
     * @param left The left variable on which the operator is applied.
     * @param right The right variable on which the operator is applied.
     *
     * @return The variable representing the reification of the encoded constraint.
     *
     * @throws ContradictionException If encoding the constraint results in a trivial
     *         inconsistency.
     */
    IVariable encodeLessOrEqual(IVariable left, IVariable right) throws ContradictionException;

    /**
     * Encodes an {@code intension} constraint that applies an {@code equal} operator
     * between different variables.
     *
     * @param variables The variables on which the operator is applied.
     *
     * @return The variable representing the reification of the encoded constraint.
     *
     * @throws ContradictionException If encoding the constraint results in a trivial
     *         inconsistency.
     *
     * @implSpec The default implementation transforms the array of variables into
     *           an {@link IVec} to invoke {@link #encodeEqual(IVec)}.
     *
     * @see #encodeEqual(IVec)
     */
    default IVariable encodeEqual(IVariable... variables) throws ContradictionException {
        return encodeEqual(Vec.of(variables));
    }

    /**
     * Encodes an {@code intension} constraint that applies an {@code equal} operator
     * between different variables.
     *
     * @param variables The variables on which the operator is applied.
     *
     * @return The variable representing the reification of the encoded constraint.
     *
     * @throws ContradictionException If encoding the constraint results in a trivial
     *         inconsistency.
     */
    IVariable encodeEqual(IVec<IVariable> variables) throws ContradictionException;

    /**
     * Encodes an {@code intension} constraint that applies a {@code different} operator
     * between two variables.
     *
     * @param left The left variable on which the operator is applied.
     * @param right The right variable on which the operator is applied.
     *
     * @return The variable representing the reification of the encoded constraint.
     *
     * @throws ContradictionException If encoding the constraint results in a trivial
     *         inconsistency.
     */
    IVariable encodeDifferent(IVariable left, IVariable right) throws ContradictionException;

    /**
     * Encodes an {@code intension} constraint that applies a {@code greater-or-equal}
     * operator between two variables.
     *
     * @param left The left variable on which the operator is applied.
     * @param right The right variable on which the operator is applied.
     *
     * @return The variable representing the reification of the encoded constraint.
     *
     * @throws ContradictionException If encoding the constraint results in a trivial
     *         inconsistency.
     */
    IVariable encodeGreaterOrEqual(IVariable left, IVariable right) throws ContradictionException;

    /**
     * Encodes an {@code intension} constraint that applies a {@code greater} operator
     * between two variables.
     *
     * @param left The left variable on which the operator is applied.
     * @param right The right variable on which the operator is applied.
     *
     * @return The variable representing the reification of the encoded constraint.
     *
     * @throws ContradictionException If encoding the constraint results in a trivial
     *         inconsistency.
     */
    IVariable encodeGreaterThan(IVariable left, IVariable right) throws ContradictionException;

    /**
     * Encodes an {@code intension} constraint that applies a {@code not} operator
     * to a (Boolean) variable.
     *
     * @param variable The variable on which the operator is applied.
     *
     * @return The variable representing the reification of the encoded constraint.
     *
     * @throws ContradictionException If encoding the constraint results in a trivial
     *         inconsistency.
     */
    IVariable encodeNot(IVariable variable) throws ContradictionException;

    /**
     * Encodes an {@code intension} constraint that applies a {@code conjunction}
     * operator between different (Boolean) variables.
     *
     * @param variables The variables on which the operator is applied.
     *
     * @return The variable representing the reification of the encoded constraint.
     *
     * @throws ContradictionException If encoding the constraint results in a trivial
     *         inconsistency.
     *
     * @implSpec The default implementation transforms the array of variables into
     *           an {@link IVec} to invoke {@link #encodeConjunction(IVec)}.
     *
     * @see #encodeConjunction(IVec)
     */
    default IVariable encodeConjunction(IVariable... variables) throws ContradictionException {
        return encodeConjunction(Vec.of(variables));
    }

    /**
     * Encodes an {@code intension} constraint that applies a {@code conjunction}
     * operator between different (Boolean) variables.
     *
     * @param variables The variables on which the operator is applied.
     *
     * @return The variable representing the reification of the encoded constraint.
     *
     * @throws ContradictionException If encoding the constraint results in a trivial
     *         inconsistency.
     */
    IVariable encodeConjunction(IVec<IVariable> variables) throws ContradictionException;

    /**
     * Encodes an {@code intension} constraint that applies a {@code disjunction}
     * operator between different (Boolean) variables.
     *
     * @param variables The variables on which the operator is applied.
     *
     * @return The variable representing the reification of the encoded constraint.
     *
     * @throws ContradictionException If encoding the constraint results in a trivial
     *         inconsistency.
     *
     * @implSpec The default implementation transforms the array of variables into
     *           an {@link IVec} to invoke {@link #encodeDisjunction(IVec)}.
     *
     * @see #encodeDisjunction(IVec)
     */
    default IVariable encodeDisjunction(IVariable... variables) throws ContradictionException {
        return encodeDisjunction(Vec.of(variables));
    }

    /**
     * Encodes an {@code intension} constraint that applies a {@code disjunction}
     * operator between different (Boolean) variables.
     *
     * @param variables The variables on which the operator is applied.
     *
     * @return The variable representing the reification of the encoded constraint.
     *
     * @throws ContradictionException If encoding the constraint results in a trivial
     *         inconsistency.
     */
    IVariable encodeDisjunction(IVec<IVariable> variables) throws ContradictionException;

    /**
     * Encodes an {@code intension} constraint that applies an
     * {@code exclusive-disjunction} operator between different (Boolean) variables.
     *
     * @param variables The variables on which the operator is applied.
     *
     * @return The variable representing the reification of the encoded constraint.
     *
     * @throws ContradictionException If encoding the constraint results in a trivial
     *         inconsistency.
     *
     * @implSpec The default implementation transforms the array of variables into
     *           an {@link IVec} to invoke {@link #encodeExclusiveDisjunction(IVec)}.
     *
     * @see #encodeExclusiveDisjunction(IVec)
     */
    default IVariable encodeExclusiveDisjunction(IVariable... variables)
            throws ContradictionException {
        return encodeExclusiveDisjunction(Vec.of(variables));
    }

    /**
     * Encodes an {@code intension} constraint that applies an
     * {@code exclusive-disjunction} operator between different (Boolean) variables.
     *
     * @param variables The variables on which the operator is applied.
     *
     * @return The variable representing the reification of the encoded constraint.
     *
     * @throws ContradictionException If encoding the constraint results in a trivial
     *         inconsistency.
     */
    IVariable encodeExclusiveDisjunction(IVec<IVariable> variables) throws ContradictionException;

    /**
     * Encodes an {@code intension} constraint that applies an {@code equivalence}
     * operator between different (Boolean) variables.
     *
     * @param variables The variables on which the operator is applied.
     *
     * @return The variable representing the reification of the encoded constraint.
     *
     * @throws ContradictionException If encoding the constraint results in a trivial
     *         inconsistency.
     *
     * @implSpec The default implementation transforms the array of variables into
     *           an {@link IVec} to invoke {@link #encodeEquivalence(IVec)}.
     *
     * @see #encodeEquivalence(IVec)
     */
    default IVariable encodeEquivalence(IVariable... variables) throws ContradictionException {
        return encodeEquivalence(Vec.of(variables));
    }

    /**
     * Encodes an {@code intension} constraint that applies an {@code equivalence}
     * operator between different (Boolean) variables.
     *
     * @param variables The variables on which the operator is applied.
     *
     * @return The variable representing the reification of the encoded constraint.
     *
     * @throws ContradictionException If encoding the constraint results in a trivial
     *         inconsistency.
     */
    IVariable encodeEquivalence(IVec<IVariable> variables) throws ContradictionException;

    /**
     * Encodes an {@code intension} constraint that applies an {@code implication}
     * operator between two (Boolean) variables.
     *
     * @param left The left variable on which the operator is applied.
     * @param right The right variable on which the operator is applied.
     *
     * @return The variable representing the reification of the encoded constraint.
     *
     * @throws ContradictionException If encoding the constraint results in a trivial
     *         inconsistency.
     */
    IVariable encodeImplication(IVariable left, IVariable right) throws ContradictionException;

    /**
     * Encodes an {@code intension} constraint that applies an {@code if-then-else}
     * operator on three variables.
     *
     * @param condition The variable representing the condition of the alternative.
     * @param ifTrue The value of the expression if {@code condition} is {@code true}.
     * @param ifFalse The value of the expression if {@code condition} is {@code false}.
     *
     * @return The variable representing the reification of the encoded constraint.
     *
     * @throws ContradictionException If encoding the constraint results in a trivial
     *         inconsistency.
     */
    IVariable encodeIfThenElse(IVariable condition, IVariable ifTrue, IVariable ifFalse)
            throws ContradictionException;

    /**
     * Encodes an {@code intension} constraint that applies an {@code if-then-else}
     * operator on three variables.
     *
     * @param ite The variable that must be equal to the alternative of the other
     *        variables.
     * @param condition The variable representing the condition of the alternative.
     * @param ifTrue The value of the expression if {@code condition} is {@code true}.
     * @param ifFalse The value of the expression if {@code condition} is {@code false}.
     *
     * @return The variable representing the reification of the encoded constraint.
     *
     * @throws ContradictionException If encoding the constraint results in a trivial
     *         inconsistency.
     */
    IVariable encodeIfThenElse(IVariable ite, IVariable condition, IVariable ifTrue,
            IVariable ifFalse) throws ContradictionException;

}
