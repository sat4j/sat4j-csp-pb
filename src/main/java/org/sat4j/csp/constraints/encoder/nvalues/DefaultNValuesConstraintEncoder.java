/**
 * Sat4j-CSP, a CSP solver based on the Sat4j platform.
 * Copyright (c) 2021-2022 - Exakis Nelite, Univ Artois & CNRS.
 * All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 * If not, see {@link http://www.gnu.org/licenses}.
 */

package org.sat4j.csp.constraints.encoder.nvalues;

import java.math.BigInteger;

import org.sat4j.core.Vec;
import org.sat4j.core.VecInt;
import org.sat4j.csp.constraints.RelationalOperator;
import org.sat4j.csp.constraints.encoder.AbstractConstraintEncoder;
import org.sat4j.csp.variables.ConstantVariable;
import org.sat4j.csp.variables.IDomain;
import org.sat4j.csp.variables.IVariable;
import org.sat4j.specs.ContradictionException;
import org.sat4j.specs.IConstr;
import org.sat4j.specs.IVec;

/**
 * The DefaultNValuesConstraintEncoder provides a base encoding for {@code n-values}
 * constraints using pseudo-Boolean constraints and/or clauses.
 *
 * @author Thibault Falque
 * @author Romain Wallon
 *
 * @version 0.1.0
 */
public class DefaultNValuesConstraintEncoder extends AbstractConstraintEncoder
        implements INValuesConstraintEncoder {

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.constraints.encoder.nvalues.INValuesConstraintEncoder#
     * encodeNValuesExcept(org.sat4j.specs.IVec,
     * org.sat4j.csp.constraints.RelationalOperator, java.math.BigInteger,
     * org.sat4j.specs.IVec)
     */
    @Override
    public IConstr encodeNValuesExcept(IVec<IVariable> variables, RelationalOperator operator,
            BigInteger nb, IVec<BigInteger> except) throws ContradictionException {
        var allValuesExcept = computeAllValues(variables, except);
        return internalEncodeCount(variables, allValuesExcept, operator, ConstantVariable.of(nb));
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.sat4j.csp.constraints.encoder.nvalues.INValuesConstraintEncoder#encodeNValues(
     * org.sat4j.specs.IVec)
     */
    @Override
    public IVariable encodeNValues(IVec<IVariable> variables) throws ContradictionException {
        var variable = solver.newVariable(0, variables.size());
        encodeNValues(variables, RelationalOperator.EQ, variable);
        return variable;
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.constraints.encoder.nvalues.INValuesConstraintEncoder#
     * encodeNValuesExcept(org.sat4j.specs.IVec,
     * org.sat4j.csp.constraints.RelationalOperator, org.sat4j.csp.variables.IVariable,
     * org.sat4j.specs.IVec)
     */
    @Override
    public IConstr encodeNValuesExcept(IVec<IVariable> variables, RelationalOperator operator,
            IVariable nb, IVec<BigInteger> except) throws ContradictionException {
        var allValuesExcept = computeAllValues(variables, except);
        return internalEncodeCount(variables, allValuesExcept, operator, nb);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.constraints.encoder.nvalues.INValuesConstraintEncoder#
     * encodeNValuesExcept(org.sat4j.specs.IVec, org.sat4j.specs.IVec)
     */
    @Override
    public IVariable encodeNValuesExcept(IVec<IVariable> variables, IVec<BigInteger> except)
            throws ContradictionException {
        var variable = solver.newVariable(0, variables.size());
        encodeNValuesExcept(variables, RelationalOperator.EQ, variable, except);
        return variable;
    }

    /**
     * Encodes an {@code n-values} constraint using pseudo-Boolean constraints and/or
     * clauses.
     *
     * @param variables The variables appearing in the constraint.
     * @param values The values to count the assignments of.
     * @param operator The relational operator used in the constraint.
     * @param nb The variable counting the number of distinct values.
     *
     * @return The created constraint(s), as internally represented by Sat4j.
     *
     * @throws ContradictionException If encoding the constraint results in a trivial
     *         inconsistency.
     */
    private IConstr internalEncodeCount(IVec<IVariable> variables, IVec<BigInteger> values,
            RelationalOperator operator, IVariable nb) throws ContradictionException {
        var counterLiterals = new VecInt(values.size() + nb.nVars());
        var counterCoefficients = new Vec<BigInteger>(values.size() + nb.nVars());

        // Building the clauses allowing to check whether a given value is assigned.
        var clause = new VecInt(variables.size());
        for (var itValues = values.iterator(); itValues.hasNext();) {
            var value = itValues.next();

            // Adding the literals representing the assignment of this value.
            for (var itVariable = variables.iterator(); itVariable.hasNext();) {
                var variable = itVariable.next();
                var literal = variable.getLiteralForValue(value);

                if (literal.isPresent()) {
                    clause.push(literal.getAsInt());
                }
            }

            // Encoding the counter for this value.
            if (!clause.isEmpty()) {
                int s = solver.newDimacsVariable();
                counterLiterals.push(s);
                counterCoefficients.push(BigInteger.ONE);
                solver.addSoftAtLeast(s, clause, 1);
                clause.clear();
            }
        }

        // Moving the variables for the count to the left hand side.
        for (int i = 0; i < nb.nVars(); i++) {
            counterLiterals.push(nb.getVariables().get(i));
            counterCoefficients.push(nb.getCoefficients().get(i).negate());
        }

        // Adding the constraint about the count.
        return solver.addPseudoBoolean(
                counterLiterals, counterCoefficients, operator, nb.getShift());
    }

    /**
     * Computes the vector of all the values to count from the domain of the
     * given variables.
     *
     * @param variables The variables whose domains are to be considered.
     * @param except The values of the domains that should not be considered.
     *
     * @return The vector of all the values to count.
     */
    private static IVec<BigInteger> computeAllValues(IVec<IVariable> variables,
            IVec<BigInteger> except) {
        var allValues = IDomain.unionOf(getDomainsOf(variables));
        var allValuesExcept = new Vec<BigInteger>(allValues.size());

        for (var value : allValues) {
            if (!except.contains(value)) {
                allValuesExcept.push(value);
            }
        }

        return allValuesExcept;
    }

}
