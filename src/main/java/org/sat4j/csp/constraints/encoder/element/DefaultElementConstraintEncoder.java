/**
 * Sat4j-CSP, a CSP solver based on the Sat4j platform.
 * Copyright (c) 2021-2022 - Exakis Nelite, Univ Artois & CNRS.
 * All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 * If not, see {@link http://www.gnu.org/licenses}.
 */

package org.sat4j.csp.constraints.encoder.element;

import java.math.BigInteger;
import java.util.OptionalInt;
import java.util.function.BiFunction;

import org.sat4j.core.ConstrGroup;
import org.sat4j.core.Vec;
import org.sat4j.core.VecInt;
import org.sat4j.csp.constraints.RelationalOperator;
import org.sat4j.csp.constraints.encoder.AbstractConstraintEncoder;
import org.sat4j.csp.variables.IVariable;
import org.sat4j.specs.ContradictionException;
import org.sat4j.specs.IConstr;
import org.sat4j.specs.IVec;
import org.sat4j.specs.IVecInt;

/**
 * The DefaultElementConstraintEncoder provides a base encoding for {@code element}
 * constraints using pseudo-Boolean constraints and/or clauses.
 *
 * @author Thibault Falque
 * @author Romain Wallon
 *
 * @version 0.1.0
 */
public class DefaultElementConstraintEncoder extends AbstractConstraintEncoder
        implements IElementConstraintEncoder {

    /*
     * (non-Javadoc)
     *
     * @see
     * org.sat4j.csp.constraints.encoder.element.IElementConstraintEncoder#encodeElement(
     * org.sat4j.specs.IVec, java.math.BigInteger)
     */
    @Override
    public IConstr encodeElement(IVec<IVariable> variables, BigInteger value)
            throws ContradictionException {
        return internalEncodeElement(variables, value, this::assignmentAsLiteral);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.sat4j.csp.constraints.encoder.element.IElementConstraintEncoder#encodeElement(
     * org.sat4j.specs.IVec, org.sat4j.csp.variables.IVariable)
     */
    @Override
    public IConstr encodeElement(IVec<IVariable> variables, IVariable value)
            throws ContradictionException {
        return internalEncodeElement(variables, value, this::assignmentAsLiteral);
    }

    /**
     * Encodes an {@code element} constraint using pseudo-Boolean constraints and/or
     * clauses.
     *
     * @param <V> The type used to represent the values to look for (which must be one of
     *        {@code BigInteger} or {@code IVariable}).
     *
     * @param variables The variables appearing in the constraint.
     * @param value The variable encoding the value that must appear among the variables.
     * @param assignmentAsLiteral The function used to encode the assignment of a value
     *        to a variable as a single literal.
     *
     * @return The created constraint(s), as internally represented by Sat4j.
     *
     * @throws ContradictionException If encoding the constraint results in a trivial
     *         inconsistency.
     */
    private <V> IConstr internalEncodeElement(IVec<IVariable> variables, V value,
            BiFunction<IVariable, V, OptionalInt> assignmentAsLiteral)
            throws ContradictionException {
        var clause = new VecInt(variables.size());

        for (var it = variables.iterator(); it.hasNext();) {
            var variable = it.next();
            var literal = assignmentAsLiteral.apply(variable, value);

            if (literal.isPresent()) {
                clause.push(literal.getAsInt());
            }
        }

        return solver.addClause(clause);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.constraints.encoder.element.IElementConstraintEncoder#
     * encodeElementConstantValues(org.sat4j.specs.IVec, int,
     * org.sat4j.csp.variables.IVariable, java.math.BigInteger)
     */
    @Override
    public IConstr encodeElementConstantValues(IVec<BigInteger> values, int startIndex,
            IVariable index, BigInteger value) throws ContradictionException {
        var sumEncoder = solver.getSumEncoder();
        ConstrGroup group = new ConstrGroup();
        IVecInt clause = new VecInt();
        for (int i = startIndex; i < values.size(); i++) {
            if (values.get(i).equals(value)) {
                int s = solver.newDimacsVariable();
                group.add(sumEncoder.encodeSoftSum(s,
                        Vec.of(index), RelationalOperator.EQ, BigInteger.valueOf(i)));
                clause.push(s);
            }
        }
        group.add(solver.addClause(clause));
        return group;
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.constraints.encoder.element.IElementConstraintEncoder#
     * encodeElementConstantValues(org.sat4j.specs.IVec, int,
     * org.sat4j.csp.variables.IVariable, org.sat4j.csp.variables.IVariable)
     */
    @Override
    public IConstr encodeElementConstantValues(IVec<BigInteger> values, int startIndex,
            IVariable index, IVariable variable) throws ContradictionException {
        return internalEncodeElement(
                values, startIndex, index, variable, (val, var) -> assignmentAsLiteral(var, val));
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.sat4j.csp.constraints.encoder.element.IElementConstraintEncoder#encodeElement(
     * org.sat4j.specs.IVec, int, org.sat4j.csp.variables.IVariable, java.math.BigInteger)
     */
    @Override
    public IConstr encodeElement(IVec<IVariable> variables, int startIndex, IVariable index,
            BigInteger value) throws ContradictionException {
        return internalEncodeElement(
                variables, startIndex, index, value, this::assignmentAsLiteral);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.sat4j.csp.constraints.encoder.element.IElementConstraintEncoder#encodeElement(
     * org.sat4j.specs.IVec, int, org.sat4j.csp.variables.IVariable,
     * org.sat4j.csp.variables.IVariable)
     */
    @Override
    public IConstr encodeElement(IVec<IVariable> values, int startIndex, IVariable index,
            IVariable variable) throws ContradictionException {
        return internalEncodeElement(
                values, startIndex, index, variable, this::assignmentAsLiteral);
    }

    /**
     * Encodes an {@code element} constraint using pseudo-Boolean constraints and/or
     * clauses.
     *
     * @param values The representation of the values among which to look for the element.
     * @param startIndex The index at which to start looking for the value.
     * @param index The index at which the value appears in the variables.
     * @param value The value to look for.
     *
     * @return The created constraint(s), as internally represented by Sat4j.
     *
     * @throws ContradictionException If encoding the constraint results in a trivial
     *         inconsistency.
     */
    private <U, V> IConstr internalEncodeElement(IVec<U> values, int startIndex, IVariable index,
            V element, BiFunction<U, V, OptionalInt> assignmentAsLiteral)
            throws ContradictionException {
        var group = new ConstrGroup();
        var somewhere = new VecInt(values.size() - startIndex);

        for (int i = startIndex; i < values.size(); i++) {
            var here = index.getLiteralForValue(BigInteger.valueOf(i));

            if (here.isPresent()) {
                var equal = assignmentAsLiteral.apply(values.get(i), element);

                if (equal.isPresent()) {
                    // If index is i, then the element must be the i-th value.
                    // Said differently, we encode now here => equal.
                    group.add(solver.addClause(VecInt.of(-here.getAsInt(), equal.getAsInt())));
                    somewhere.push(here.getAsInt());
                }
            }
        }

        // The index must point to one of the possible values.
        group.add(solver.addClause(somewhere));

        return group;
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.constraints.encoder.element.IElementConstraintEncoder#
     * encodeElementConstantMatrix(org.sat4j.specs.IVec, int,
     * org.sat4j.csp.variables.IVariable, int, org.sat4j.csp.variables.IVariable,
     * java.math.BigInteger)
     */
    @Override
    public IConstr encodeElementConstantMatrix(IVec<IVec<BigInteger>> matrix, int startRowIndex,
            IVariable rowIndex, int startColIndex, IVariable colIndex, BigInteger value)
            throws ContradictionException {
        var sumEncoder = solver.getSumEncoder();
        ConstrGroup group = new ConstrGroup();
        IVecInt clause = new VecInt();
        for (int i = startRowIndex; i < matrix.size(); i++) {
            for (int j = startColIndex; j < matrix.get(i).size(); j++) {
                if (matrix.get(i).get(j).equals(value)) {
                    int s1 = solver.newDimacsVariable();
                    group.add(sumEncoder.encodeSoftSum(s1,
                            Vec.of(rowIndex), RelationalOperator.EQ, BigInteger.valueOf(i)));
                    int s2 = solver.newDimacsVariable();
                    group.add(sumEncoder.encodeSoftSum(s2,
                            Vec.of(colIndex), RelationalOperator.EQ, BigInteger.valueOf(j)));
                    int s3 = solver.newDimacsVariable();
                    group.add(solver.addSoftAtLeast(s3, VecInt.of(s1, s2), 2));
                    clause.push(s3);
                }
            }
        }
        group.add(solver.addClause(clause));
        return group;
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.constraints.encoder.element.IElementConstraintEncoder#
     * encodeElementConstantMatrix(org.sat4j.specs.IVec, int,
     * org.sat4j.csp.variables.IVariable, int, org.sat4j.csp.variables.IVariable,
     * org.sat4j.csp.variables.IVariable)
     */
    @Override
    public IConstr encodeElementConstantMatrix(IVec<IVec<BigInteger>> matrix, int startRowIndex,
            IVariable rowIndex, int startColIndex, IVariable colIndex, IVariable value)
            throws ContradictionException {
        return internalEncodeElementMatrix(
                matrix, startRowIndex, rowIndex, startColIndex, colIndex, value,
                (val, var) -> assignmentAsLiteral(var, val));
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.constraints.encoder.element.IElementConstraintEncoder#
     * encodeElementMatrix(org.sat4j.specs.IVec, int, org.sat4j.csp.variables.IVariable,
     * int, org.sat4j.csp.variables.IVariable, java.math.BigInteger)
     */
    @Override
    public IConstr encodeElementMatrix(IVec<IVec<IVariable>> matrix, int startRowIndex,
            IVariable rowIndex, int startColIndex, IVariable colIndex, BigInteger value)
            throws ContradictionException {
        return internalEncodeElementMatrix(matrix, startRowIndex, rowIndex, startColIndex, colIndex, value, IVariable::getLiteralForValue);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.constraints.encoder.element.IElementConstraintEncoder#
     * encodeElementMatrix(org.sat4j.specs.IVec, int, org.sat4j.csp.variables.IVariable,
     * int, org.sat4j.csp.variables.IVariable, org.sat4j.csp.variables.IVariable)
     */
    @Override
    public IConstr encodeElementMatrix(IVec<IVec<IVariable>> matrix, int startRowIndex,
            IVariable rowIndex, int startColIndex, IVariable colIndex, IVariable value)
            throws ContradictionException {
        return internalEncodeElementMatrix(matrix, startRowIndex, rowIndex, startColIndex, colIndex, value, this::assignmentAsLiteral);
    }

    /**
     * Encodes an {@code element} constraint using pseudo-Boolean constraints and/or
     * clauses.
     *
     * @param values The representation of the values among which to look for the element.
     * @param startIndex The index at which to start looking for the value.
     * @param index The index at which the value appears in the variables.
     * @param value The value to look for.
     *
     * @return The created constraint(s), as internally represented by Sat4j.
     *
     * @throws ContradictionException If encoding the constraint results in a trivial
     *         inconsistency.
     */
    private <U, V> IConstr internalEncodeElementMatrix(IVec<IVec<U>> values, int startRowIndex,
            IVariable rowIndex, int startColumnIndex, IVariable columnIndex,
            V element, BiFunction<U, V, OptionalInt> assignmentAsLiteral)
            throws ContradictionException {
        var group = new ConstrGroup();
        var somewhere = new VecInt(values.size() - startRowIndex);

        for (int i = startRowIndex; i < values.size(); i++) {
            var hereRow = rowIndex.getLiteralForValue(BigInteger.valueOf(i));

            if (hereRow.isPresent()) {

                for (int j = startColumnIndex; j < values.get(i).size(); j++) {
                    var hereCol = columnIndex.getLiteralForValue(BigInteger.valueOf(j));

                    if (hereCol.isPresent()) {

                        var equal = assignmentAsLiteral.apply(values.get(i).get(j), element);

                        if (equal.isPresent()) {
                            // If index is i, then the element must be the i-th value.
                            // Said differently, we encode now here => equal.
                            group.add(
                                    solver.addClause(
                                            VecInt.of(-hereRow.getAsInt(), -hereCol.getAsInt(), equal.getAsInt())));
                            
                            int s = solver.newDimacsVariable();
                            group.add(solver.addSoftAtLeast(s, VecInt.of(hereRow.getAsInt(),hereCol.getAsInt()), 2));
                            somewhere.push(s);
                        }
                    }
                }
            }
        }

        // The index must point to one of the possible values.
        group.add(solver.addClause(somewhere));

        return group;
    }

}
