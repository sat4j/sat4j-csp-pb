/**
 * Sat4j-CSP, a CSP solver based on the Sat4j platform.
 * Copyright (c) 2021-2022 - Exakis Nelite, Univ Artois & CNRS.
 * All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 * If not, see {@link http://www.gnu.org/licenses}.
 */

package org.sat4j.csp.constraints.encoder.intension;

import java.math.BigInteger;

import org.sat4j.csp.constraints.ArithmeticOperator;
import org.sat4j.csp.constraints.RelationalOperator;
import org.sat4j.csp.constraints.SetBelongingOperator;
import org.sat4j.csp.constraints.encoder.IConstraintEncoder;
import org.sat4j.csp.variables.IVariable;
import org.sat4j.specs.ContradictionException;
import org.sat4j.specs.IConstr;
import org.sat4j.specs.IVec;

/**
 * The IPrimitiveConstraintEncoder defines a strategy for encoding {@code primitive}
 * constraints using pseudo-Boolean constraints and/or clauses.
 *
 * @author Thibault Falque
 * @author Romain Wallon
 *
 * @version 0.1.0
 */
public interface IPrimitiveConstraintEncoder extends IConstraintEncoder {

    /**
     * Encodes a {@code primitive} constraint using pseudo-Boolean constraints and/or
     * clauses.
     *
     * @param variable The variable appearing in the constraint.
     * @param operator The operator used in the constraint.
     * @param value The value to compare the variable with.
     *
     * @return The created constraint(s), as internally represented by Sat4j.
     *
     * @throws ContradictionException If encoding the constraint results in a trivial
     *         inconsistency.
     */
    IConstr encodePrimitive(IVariable variable, RelationalOperator operator, BigInteger value)
            throws ContradictionException;

    /**
     * Encodes a {@code primitive} constraint using pseudo-Boolean constraints and/or
     * clauses.
     *
     * @param variable The variable appearing in the constraint.
     * @param arithOp The arithmetic operator applied on the variable.
     * @param leftHandSide The value on the left-hand side of the constraint.
     * @param relOp The relational operator used to compare the right-hand side with the
     *        left-hand side.
     * @param rightHandSide The value on the right-hand side of the constraint.
     *
     * @return The created constraint(s), as internally represented by Sat4j.
     *
     * @throws ContradictionException If encoding the constraint results in a trivial
     *         inconsistency.
     */
    IConstr encodePrimitive(IVariable variable, ArithmeticOperator arithOp, BigInteger leftHandSide,
            RelationalOperator relOp, BigInteger rightHandSide) throws ContradictionException;

    /**
     * Encodes a {@code primitive} constraint using pseudo-Boolean constraints and/or
     * clauses.
     *
     * @param variable The variable appearing in the constraint.
     * @param arithOp The arithmetic operator applied on the variable.
     * @param leftHandSide The variable on the left-hand side of the constraint.
     * @param relOp The relational operator used to compare the right-hand side with the
     *        left-hand side.
     * @param rightHandSide The value on the right-hand side of the constraint.
     *
     * @return The created constraint(s), as internally represented by Sat4j.
     *
     * @throws ContradictionException If encoding the constraint results in a trivial
     *         inconsistency.
     */
    IConstr encodePrimitive(IVariable variable, ArithmeticOperator arithOp, IVariable leftHandSide,
            RelationalOperator relOp, BigInteger rightHandSide) throws ContradictionException;

    /**
     * Encodes a {@code primitive} constraint using pseudo-Boolean constraints and/or
     * clauses.
     *
     * @param variable The variable appearing in the constraint.
     * @param arithOp The arithmetic operator applied on the variable.
     * @param leftHandSide The value on the left-hand side of the constraint.
     * @param relOp The relational operator used to compare the right-hand side with the
     *        left-hand side.
     * @param rightHandSide The variable on the right-hand side of the constraint.
     *
     * @return The created constraint(s), as internally represented by Sat4j.
     *
     * @throws ContradictionException If encoding the constraint results in a trivial
     *         inconsistency.
     */
    IConstr encodePrimitive(IVariable variable, ArithmeticOperator arithOp, BigInteger leftHandSide,
            RelationalOperator relOp, IVariable rightHandSide) throws ContradictionException;

    /**
     * Encodes a {@code primitive} constraint using pseudo-Boolean constraints and/or
     * clauses.
     *
     * @param variable The variable appearing in the constraint.
     * @param arithOp The arithmetic operator applied on the variable.
     * @param leftHandSide The variable on the left-hand side of the constraint.
     * @param relOp The relational operator used to compare the right-hand side with the
     *        left-hand side.
     * @param rightHandSide The variable on the right-hand side of the constraint.
     *
     * @return The created constraint(s), as internally represented by Sat4j.
     *
     * @throws ContradictionException If encoding the constraint results in a trivial
     *         inconsistency.
     */
    IConstr encodePrimitive(IVariable variable, ArithmeticOperator arithOp, IVariable leftHandSide,
            RelationalOperator relOp, IVariable rightHandSide) throws ContradictionException;

    /**
     * Encodes a {@code primitive} constraint using pseudo-Boolean constraints and/or
     * clauses.
     *
     * @param arithOp The arithmetic operator applied on the variable.
     * @param variable The variable on which the operator is applied.
     * @param rightHandSide The variable on the right-hand side of the constraint.
     *
     * @return The created constraint(s), as internally represented by Sat4j.
     *
     * @throws ContradictionException If encoding the constraint results in a trivial
     *         inconsistency.
     */
    IConstr encodePrimitive(ArithmeticOperator arithOp, IVariable variable, IVariable rightHandSide)
            throws ContradictionException;

    /**
     * Encodes a {@code primitive} constraint using pseudo-Boolean constraints and/or
     * clauses.
     *
     * @param variable The variable appearing in the constraint.
     * @param operator The operator defining whether the values are allowed or forbidden.
     * @param values The set of values on which the operator is applied.
     *
     * @return The created constraint(s), as internally represented by Sat4j.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    IConstr encodePrimitive(IVariable variable, SetBelongingOperator operator,
            IVec<BigInteger> values) throws ContradictionException;

    /**
     * Encodes a {@code primitive} constraint using pseudo-Boolean constraints and/or
     * clauses.
     *
     * @param variable The variable appearing in the constraint.
     * @param operator The operator defining whether the values are allowed or forbidden.
     * @param min The minimum value of the range on which the operator is applied.
     * @param max The maximum value of the range on which the operator is applied.
     *
     * @return The created constraint(s), as internally represented by Sat4j.
     *
     * @throws ContradictionException If adding the constraint results in a trivial
     *         inconsistency.
     */
    IConstr encodePrimitive(IVariable variable, SetBelongingOperator operator, BigInteger min,
            BigInteger max) throws ContradictionException;

}
