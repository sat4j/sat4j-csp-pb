/**
 * Sat4j-CSP, a CSP solver based on the Sat4j platform.
 * Copyright (c) 2021-2022 - Exakis Nelite, Univ Artois & CNRS.
 * All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 * If not, see {@link http://www.gnu.org/licenses}.
 */

package org.sat4j.csp.constraints.encoder.intension;

import org.sat4j.csp.constraints.encoder.IConstraintEncoder;
import org.sat4j.csp.constraints.intension.BinaryIntensionConstraint;
import org.sat4j.csp.constraints.intension.ConstantIntensionConstraint;
import org.sat4j.csp.constraints.intension.IfThenElseIntensionConstraint;
import org.sat4j.csp.constraints.intension.NaryIntensionConstraint;
import org.sat4j.csp.constraints.intension.UnaryIntensionConstraint;
import org.sat4j.csp.constraints.intension.VariableIntensionConstraint;
import org.sat4j.csp.variables.IVariable;
import org.sat4j.specs.ContradictionException;

/**
 * The IIntensionConstraintVisitor visits an intension constraint in order to compute its
 * encoding using pseudo-Boolean constraints.
 *
 * @author Thibault Falque
 * @author Romain Wallon
 *
 * @version 0.1.0
 */
public interface IIntensionConstraintVisitor extends IConstraintEncoder {

    /**
     * Visits a unary constraint that appears in an {@code intension} constraint.
     *
     * @param constr The constraint to visit.
     *
     * @throws ContradictionException If visiting (and encoding) the constraint results in
     *         a trivial inconsistency.
     */
    void visit(UnaryIntensionConstraint constr) throws ContradictionException;

    /**
     * Visits a binary constraint that appears in an {@code intension} constraint.
     *
     * @param constr The constraint to visit.
     *
     * @throws ContradictionException If visiting (and encoding) the constraint results in
     *         a trivial inconsistency.
     */
    void visit(BinaryIntensionConstraint constr) throws ContradictionException;

    /**
     * Visits an n-ary constraint that appears in an {@code intension} constraint.
     *
     * @param constr The constraint to visit.
     *
     * @throws ContradictionException If visiting (and encoding) the constraint results in
     *         a trivial inconsistency.
     */
    void visit(NaryIntensionConstraint constr) throws ContradictionException;

    /**
     * Visits an if-then-else constraint that appears in an {@code intension} constraint.
     *
     * @param ifThenElse The constraint to visit.
     *
     * @throws ContradictionException If visiting (and encoding) the constraint results in
     *         a trivial inconsistency.
     */
    void visit(IfThenElseIntensionConstraint ifThenElse) throws ContradictionException;

    /**
     * Visits a variable that appears in an {@code intension} constraint.
     *
     * @param variable The variable to visit.
     *
     * @throws ContradictionException If visiting (and encoding) the constraint results in
     *         a trivial inconsistency.
     */
    void visit(VariableIntensionConstraint variable) throws ContradictionException;

    /**
     * Visits a constant that appears in an {@code intension} constraint.
     *
     * @param constant The constant to visit.
     *
     * @throws ContradictionException If visiting (and encoding) the constraint results in
     *         a trivial inconsistency.
     */
    void visit(ConstantIntensionConstraint constant) throws ContradictionException;

    /**
     * Gives the (Boolean) variable that represents the satisfaction of the visited
     * intension constraint.
     *
     * @return The variable representing the reification of the encoded constraint.
     */
    IVariable getEncodingVariable();

}
