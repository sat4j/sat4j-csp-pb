/**
 * The {@code org.sat4j.csp.constraints} package provides the representation of the
 * different CSP constraints.
 *
 * @author Thibault Falque
 * @author Romain Wallon
 *
 * @version 0.1.0
 */

package org.sat4j.csp.constraints;
