/**
 * Sat4j-CSP, a CSP solver based on the Sat4j platform.
 * Copyright (c) 2021-2022 - Exakis Nelite, Univ Artois & CNRS.
 * All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 * If not, see {@link http://www.gnu.org/licenses}.
 */

package org.sat4j.csp.variables;

import java.math.BigInteger;

import org.sat4j.core.Vec;
import org.sat4j.core.VecInt;
import org.sat4j.csp.utils.RangeVec;
import org.sat4j.specs.ContradictionException;
import org.sat4j.specs.IVec;
import org.sat4j.specs.IVecInt;

/**
 * The OrderEncodingVariableFactory is an {@link IVariableFactory} that creates variables
 * encoded using the order-encoding.
 *
 * @author Thibault Falque
 * @author Romain Wallon
 *
 * @version 0.1.0
 */
public class OrderEncodingVariableFactory extends AbstractVariableFactory {

    /*
     * (non-Javadoc)
     *
     * @see
     * org.sat4j.csp.variables.AbstractVariableFactory#internalCreateVariable(org.sat4j.
     * specs.IVec)
     */
    @Override
    protected IVariable internalCreateVariable(IVec<BigInteger> values)
            throws ContradictionException {
        // Creating the domain of the variable.
        var domain = EnumeratedDomain.of(values);
        var rangeValues = RangeVec.of(domain.min(), domain.max());
        var nVariables = rangeValues.size() - 1;
        var booleanVariables = createVariablesForDomainOfSize(nVariables);
        var coefficients = new Vec<BigInteger>(nVariables, BigInteger.ONE);

        // Creating the variable itself.
        var variable = new Variable(booleanVariables, coefficients, domain);
        var orderEncodingVariable = new OrderEncodingVariableDecorator(variable, solver);

        // Adding the constraints to forbid values outside the domain.
        for (var it = rangeValues.iterator(); it.hasNext();) {
            var v = it.next();

            if (!domain.contains(v)) {
                var lit = orderEncodingVariable.getLiteralForValue(v, false).orElseThrow();
                solver.addClause(VecInt.of(-lit));
            }
        }

        return orderEncodingVariable;
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.sat4j.csp.variables.AbstractVariableFactory#internalCreateVariable(java.math.
     * BigInteger, java.math.BigInteger)
     */
    @Override
    protected IVariable internalCreateVariable(BigInteger min, BigInteger max)
            throws ContradictionException {
        // Creating the domain of the variable.
        var values = RangeVec.of(min, max);
        var nVariables = values.size() - 1;
        var booleanVariables = createVariablesForDomainOfSize(nVariables);
        var coefficients = new Vec<BigInteger>(nVariables, BigInteger.ONE);

        // Creating the variable.
        var variable = new Variable(booleanVariables, coefficients, RangeDomain.of(min, max));

        // Decorating the variable as an order-encoding one.
        return new OrderEncodingVariableDecorator(variable, solver);
    }

    /**
     * Creates the required Boolean variables to represent a CSP variable with the
     * order-encoding, and adds the constraint for the domain of this variable.
     *
     * @param nVars The number of Boolean variables required to represent the CSP
     *        variable.
     *
     * @return The vector of Boolean variables.
     *
     * @throws ContradictionException If the constraints for the domain are inconsistent.
     */
    private IVecInt createVariablesForDomainOfSize(int nVars) throws ContradictionException {
        // Adding the required Boolean variables to the solvers.
        int last = solver.newDimacsVariables(nVars);
        int left = last - nVars + 1;
        var variables = new VecInt(nVars);

        for (int i = 0; i < nVars - 1; i++) {
            // Adding the left variable to the vector.
            variables.push(left);

            // Adding the constraint for the domain -x_i => -x_{i+1}.
            int right = left + 1;
            solver.addClause(VecInt.of(left, -right));

            left = right;
        }

        // Adding the last variable to the vector.
        variables.push(last);
        return variables;
    }

}
