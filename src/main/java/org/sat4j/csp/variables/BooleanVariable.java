/**
 * Sat4j-CSP, a CSP solver based on the Sat4j platform.
 * Copyright (c) 2021-2022 - Exakis Nelite, Univ Artois & CNRS.
 * All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 * If not, see {@link http://www.gnu.org/licenses}.
 */

package org.sat4j.csp.variables;

import java.math.BigInteger;
import java.util.OptionalInt;

import org.sat4j.core.Vec;
import org.sat4j.core.VecInt;
import org.sat4j.specs.IVec;
import org.sat4j.specs.IVecInt;

/**
 * The BooleanVariable is an {@link IVariable} that represents a CSP variable that is
 * itself a Boolean variable (i.e., whose domain is exactly <code>{0, 1}</code>).
 *
 * @author Thibault Falque
 * @author Romain Wallon
 *
 * @version 0.1.0
 */
public final class BooleanVariable implements IVariable {

    /**
     * The DIMACS identifier of this variable.
     */
    private final int dimacs;

    /**
     * Creates a new BooleanVariable.
     *
     * @param dimacs The DIMACS identifier of the variable.
     */
    private BooleanVariable(int dimacs) {
        this.dimacs = dimacs;
    }

    /**
     * Creates a new BooleanVariable.
     *
     * @param dimacs The DIMACS identifier of the variable.
     *
     * @return The created variable.
     */
    public static IVariable of(int dimacs) {
        return new BooleanVariable(dimacs);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.variables.IVariable#nVars()
     */
    @Override
    public int nVars() {
        return 1;
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.variables.IVariable#getVariables()
     */
    @Override
    public IVecInt getVariables() {
        return VecInt.of(dimacs);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.variables.IVariable#getCoefficients()
     */
    @Override
    public IVec<BigInteger> getCoefficients() {
        return Vec.of(BigInteger.ONE);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.variables.IVariable#getLiteralForValue(java.math.BigInteger)
     */
    @Override
    public OptionalInt getLiteralForValue(BigInteger value) {
        if (value.signum() == 0) {
            return OptionalInt.of(-dimacs);
        }

        if (BigInteger.ONE.equals(value)) {
            return OptionalInt.of(dimacs);
        }

        return OptionalInt.empty();
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.sat4j.csp.variables.IVariable#getLiteralForValueAtLeast(java.math.BigInteger)
     */
    @Override
    public int getLiteralForValueAtLeast(BigInteger value) {
        if (value.signum() <= 0) {
            // The variable can take any (Boolean) value.
            // Returning the literal 1, that is always true in the solver.
            return 1;
        }

        if (BigInteger.ONE.equals(value)) {
            // The variable has to be satisfied.
            return dimacs;
        }

        // It is impossible to be greater than this value.
        // Returning the literal -1, that is always false in the solver.
        return -1;
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.variables.IVariable#getShift()
     */
    @Override
    public BigInteger getShift() {
        return BigInteger.ZERO;
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.variables.IVariable#getDomain()
     */
    @Override
    public IDomain getDomain() {
        return RangeDomain.of(BigInteger.ZERO, BigInteger.ONE);
    }

}
