/**
 * Sat4j-CSP, a CSP solver based on the Sat4j platform.
 * Copyright (c) 2021-2022 - Exakis Nelite, Univ Artois & CNRS.
 * All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 * If not, see {@link http://www.gnu.org/licenses}.
 */

package org.sat4j.csp.variables;

import java.math.BigInteger;

import org.sat4j.core.VecInt;
import org.sat4j.csp.utils.RangeVec;
import org.sat4j.specs.ContradictionException;
import org.sat4j.specs.IVec;
import org.sat4j.specs.IVecInt;

/**
 * The DirectEncodingVariableFactory is an {@link IVariableFactory} that creates variables
 * encoded using the direct-encoding.
 *
 * @author Thibault Falque
 * @author Romain Wallon
 *
 * @version 0.1.0
 */
public class DirectEncodingVariableFactory extends AbstractVariableFactory {

    /*
     * (non-Javadoc)
     *
     * @see
     * org.sat4j.csp.variables.AbstractVariableFactory#internalCreateVariable(org.sat4j.
     * specs.IVec)
     */
    @Override
    protected IVariable internalCreateVariable(IVec<BigInteger> values)
            throws ContradictionException {
        var booleanVariables = createVariablesForDomainOfSize(values.size());
        var variable = new Variable(booleanVariables, values, EnumeratedDomain.of(values));
        return new LazyDirectEncodingVariableProxy(variable, () -> variable, solver);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.sat4j.csp.variables.AbstractVariableFactory#internalCreateVariable(
     * java.math.BigInteger, java.math.BigInteger)
     */
    @Override
    protected IVariable internalCreateVariable(BigInteger min, BigInteger max)
            throws ContradictionException {
        var values = RangeVec.of(min, max);
        var booleanVariables = createVariablesForDomainOfSize(values.size());
        var variable = new Variable(booleanVariables, values, RangeDomain.of(min, max));
        return new LazyDirectEncodingVariableProxy(variable, () -> variable, solver);
    }

    /**
     * Creates the required Boolean variables to represent a CSP variable, and adds
     * the constraint for the domain of this variable.
     *
     * @param nVars The number of Boolean variables required to represent the CSP
     *        variable.
     *
     * @return The vector of the Boolean variables.
     *
     * @throws ContradictionException If the constraint for the domain is inconsistent.
     */
    private IVecInt createVariablesForDomainOfSize(int nVars) throws ContradictionException {
        // Adding the required Boolean variables to the solvers.
        int last = solver.newDimacsVariables(nVars);
        var variables = new VecInt(nVars);
        for (int i = 0; i < nVars; i++) {
            variables.push(last - i);
        }

        // Adding the constraint for the domain.
        solver.addExactly(variables, 1);
        return variables;
    }

}
