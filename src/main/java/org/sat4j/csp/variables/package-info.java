/**
 * The {@code org.sat4j.csp.variables} package provides the classes for encoding CSP
 * variables into combinations of Boolean variables using different approaches.
 *
 * @author Thibault Falque
 * @author Romain Wallon
 *
 * @version 0.1.0
 */

package org.sat4j.csp.variables;
