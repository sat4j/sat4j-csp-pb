/**
 * Sat4j-CSP, a CSP solver based on the Sat4j platform.
 * Copyright (c) 2021-2022 - Exakis Nelite, Univ Artois & CNRS.
 * All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 * If not, see {@link http://www.gnu.org/licenses}.
 */

package org.sat4j.csp.variables;

import static java.math.BigInteger.ONE;

import java.math.BigInteger;

import org.sat4j.core.Vec;
import org.sat4j.core.VecInt;
import org.sat4j.specs.ContradictionException;
import org.sat4j.specs.IVec;
import org.sat4j.specs.IVecInt;

/**
 * The SmartDirecEncodingVariableFactory is an {@link IVariableFactory} that creates
 * variables encoded using a smart implementation of the direct encoding.
 *
 * @author Franck Damien
 * @author Thibault Falque
 * @author Romain Wallon
 *
 * @version 0.1.0
 */
public class SmartDirecEncodingVariableFactory extends AbstractVariableFactory {

    /*
     * (non-Javadoc)
     *
     * @see
     * org.sat4j.csp.variables.AbstractVariableFactory#internalCreateVariable(org.
     * sat4j. specs.IVec)
     */
    @Override
    protected IVariable internalCreateVariable(IVec<BigInteger> values)
            throws ContradictionException {
        // Creating the appropriate variables and coefficients.
        var nVariables = values.size();
        var variables = createVariablesForDomainOfSize(nVariables);
        var coefficients = new Vec<BigInteger>(nVariables);

        // Adding the difference between two consecutive values as coefficients.
        coefficients.push(values.get(0));
        for (int i = 1; i < nVariables; i++) {
            var previousValue = values.get(i - 1);
            var currentValue = values.get(i);
            coefficients.push(currentValue.subtract(previousValue));
        }

        // Creating the variable itself.
        var variable = new Variable(variables, coefficients, EnumeratedDomain.of(values));
        return new SmartDirectEncodingVariableDecorator(variable, solver);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.variables.AbstractVariableFactory#internalCreateVariable(
     * java.math.BigInteger, java.math.BigInteger)
     */
    @Override
    protected IVariable internalCreateVariable(BigInteger min, BigInteger max)
            throws ContradictionException {
        // Creating the appropriate variables and coefficients.
        var nVariables = 1 + min.subtract(max).intValue();
        var variables = createVariablesForDomainOfSize(nVariables);
        var coefficients = new Vec<BigInteger>(nVariables, ONE);

        // The first coefficient has to be the minimum.
        // This allows to make sure the variable is at least equal to this value.
        coefficients.set(0, min);

        // Creating the variable itself.
        var variable = new Variable(variables, coefficients, RangeDomain.of(min, max));
        return new SmartDirectEncodingVariableDecorator(variable, solver);
    }

    /**
     * Creates the required Boolean variables to represent a CSP variable, and adds
     * the constraints for the domain of this variable.
     *
     * @param nVars The number of Boolean variables required to represent the CSP
     *        variable.
     *
     * @return The vector of the Boolean variables.
     *
     * @throws ContradictionException If the constraints for the domain are inconsistent.
     */
    private IVecInt createVariablesForDomainOfSize(int nVars) throws ContradictionException {
        // Adding the required Boolean variables to the solvers.
        int last = solver.newDimacsVariables(nVars);
        int left = last - nVars + 1;
        var variables = new VecInt(nVars);

        for (int i = 0; i < nVars - 1; i++) {
            // Adding the left variable to the vector.
            variables.push(left);

            // Adding the constraint for the domain: x_{i+1} => x_{i}.
            int right = left + 1;
            solver.addClause(VecInt.of(-right, left));

            left = right;
        }

        // Adding the last variable to the vector.
        variables.push(last);

        // At least one of the variables must be satisfied.
        solver.addClause(variables);
        return variables;
    }

}
