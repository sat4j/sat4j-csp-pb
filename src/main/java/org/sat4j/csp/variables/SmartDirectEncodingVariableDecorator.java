/**
 * Sat4j-CSP, a CSP solver based on the Sat4j platform.
 * Copyright (c) 2021-2022 - Exakis Nelite, Univ Artois & CNRS.
 * All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 * If not, see {@link http://www.gnu.org/licenses}.
 */

package org.sat4j.csp.variables;

import static java.math.BigInteger.ONE;
import static java.math.BigInteger.TWO;

import java.math.BigInteger;
import java.util.HashMap;
import java.util.Map;
import java.util.OptionalInt;

import org.sat4j.core.Vec;
import org.sat4j.core.VecInt;
import org.sat4j.csp.constraints.RelationalOperator;
import org.sat4j.csp.core.ICSPSolver;
import org.sat4j.csp.utils.UncheckedContradictionException;
import org.sat4j.specs.ContradictionException;

/**
 * The SmartDirectEncodingVariableDecorator is a decorator for an {@link IVariable} that
 * allows to interpret this variable as being encoded using a smart implementation of the
 * direct encoding.
 *
 * @author Franck Damien
 * @author Thibault Falque
 * @author Romain Wallon
 *
 * @version 0.1.0
 */
public class SmartDirectEncodingVariableDecorator extends AbstractVariableDecorator {

    /**
     * The solver in which to add constraints when computing the literal
     * representing a particular value.
     */
    private final ICSPSolver solver;

    /**
     * The cache for the literals representing the values of the domain of this
     * variable.
     */
    private final Map<BigInteger, Integer> cache = new HashMap<>();

    /**
     * Creates a new SmartDirectEncodingVariableDecorator.
     * 
     * @param variable The variable to decorate.
     * @param solver The solver in which the variable is created.
     */
    protected SmartDirectEncodingVariableDecorator(IVariable variable, ICSPSolver solver) {
        super(variable);
        this.solver = solver;
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.sat4j.csp.variables.AbstractVariableDecorator#getLiteralForValue(java.
     * math. BigInteger)
     */
    @Override
    public OptionalInt getLiteralForValue(BigInteger value) {
        // Checking whether the literal for the value already exists.
        var cachedLiteral = cache.get(value);
        if (cachedLiteral != null) {
            return OptionalInt.of(cachedLiteral);
        }

        try {
            // Looking for the wanted value.
            var equalIndex = -1;
            var totalValue = getShift();

            // Computing the possible values for the variable.
            for (int i = 0; i < nVars(); i++) {
                // Updating the total value, and comparing it with the one to retrieve.
                totalValue = totalValue.add(getCoefficients().get(i));
                int cmp = totalValue.compareTo(value);

                if (cmp == 0) {
                    // We have found the variable representing this value.
                    equalIndex = i;
                    break;
                }

                if (cmp > 0) {
                    // We have been too far: there is no variable representing the value.
                    return OptionalInt.empty();
                }
            }

            if (equalIndex < 0) {
                // We did not find any index: there is no variable representing the value.
                return OptionalInt.empty();
            }

            // We found the wanted index: let us now ensure equality.
            if (equalIndex == (nVars() - 1)) {
                // This is the last variable, so it is enough to ensure equality.
                var v = getVariables().get(equalIndex);
                cache.put(value, v);
                return OptionalInt.of(v);
            }

            // Creating the variable representing the equality.
            int v = solver.newDimacsVariables(1);
            cache.put(value, v);

            // Adding the constraint enforcing the equality.
            int atLeastValue = getVariables().get(equalIndex);
            int atLeastNextValue = getVariables().get(equalIndex + 1);
            solver.addSoftPseudoBoolean(v, VecInt.of(atLeastValue, -atLeastNextValue),
                    Vec.of(ONE, ONE), RelationalOperator.EQ, TWO);

            // Returning the variable for the value.
            return OptionalInt.of(v);

        } catch (ContradictionException e) {
            throw new UncheckedContradictionException(e);
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.sat4j.csp.variables.AbstractVariableDecorator#getLiteralForValueAtLeast(
     * java. math.BigInteger)
     */
    @Override
    public int getLiteralForValueAtLeast(BigInteger value) {
        var totalValue = getShift();

        // Computing the possible values for the variable.
        for (int i = 0; i < nVars(); i++) {
            totalValue = totalValue.add(getCoefficients().get(i));

            if (totalValue.compareTo(value) >= 0) {
                // We have found the first assignable value greater than the given one.
                return getVariables().get(i);
            }
        }

        // The value is greater than the maximum that can be assigned to this variable.
        return -1;
    }

}
