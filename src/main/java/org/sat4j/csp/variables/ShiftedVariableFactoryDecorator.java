/**
 * Sat4j-CSP, a CSP solver based on the Sat4j platform.
 * Copyright (c) 2021-2022 - Exakis Nelite, Univ Artois & CNRS.
 * All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 * If not, see {@link http://www.gnu.org/licenses}.
 */

package org.sat4j.csp.variables;

import java.math.BigInteger;

import org.sat4j.csp.core.ICSPSolver;
import org.sat4j.csp.utils.UncheckedContradictionException;
import org.sat4j.specs.IVec;

/**
 * The ShiftedVariableFactoryDecorator is a decorator for an {@link IVariableFactory} that
 * allows to shift all created variables to ensure that their domains starts at {@code 0}.
 *
 * @author Thibault Falque
 * @author Romain Wallon
 *
 * @version 0.1.0
 */
public final class ShiftedVariableFactoryDecorator implements IVariableFactory {

    /**
     * The decorated variable factory.
     */
    private final IVariableFactory decorated;

    /**
     * Creates a new ShiftedVariableFactoryDecorator.
     *
     * @param decorated The variable factory to decorate.
     */
    public ShiftedVariableFactoryDecorator(IVariableFactory decorated) {
        this.decorated = decorated;
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.sat4j.csp.variables.IVariableFactory#setSolver(org.sat4j.csp.core.ICSPSolver)
     */
    @Override
    public void setSolver(ICSPSolver solver) {
        decorated.setSolver(solver);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.variables.IVariableFactory#createVariable(
     * org.sat4j.specs.IVec)
     */
    @Override
    public IVariable createVariable(IVec<BigInteger> values) {
        if (values.isEmpty()) {
            throw new UncheckedContradictionException("Creating variable with empty domain!");
        }

        // Looking for the minimum value.
        var min = values.get(0);
        for (int i = 1; i < values.size(); i++) {
            if (values.get(i).compareTo(min) < 0) {
                min = values.get(i);
            }
        }

        if (min.signum() == 0) {
            // In this case, there is no shift to apply.
            return decorated.createVariable(values);
        }

        // Shifting the values.
        for (int i = 0; i < values.size(); i++) {
            values.set(i, values.get(i).subtract(min));
        }
        return new ShiftedVariableDecorator(decorated.createVariable(values), min);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.variables.IVariableFactory#createVariable(
     * java.math.BigInteger, java.math.BigInteger)
     */
    @Override
    public IVariable createVariable(BigInteger min, BigInteger max) {
        if (min.signum() == 0) {
            // In this case, there is no shift to apply.
            return decorated.createVariable(min, max);
        }

        return new ShiftedVariableDecorator(
                decorated.createVariable(BigInteger.ZERO, max.subtract(min)), min);
    }

}
