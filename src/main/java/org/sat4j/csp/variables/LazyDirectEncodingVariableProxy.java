/**
 * Sat4j-CSP, a CSP solver based on the Sat4j platform.
 * Copyright (c) 2021-2022 - Exakis Nelite, Univ Artois & CNRS.
 * All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 * If not, see {@link http://www.gnu.org/licenses}.
 */

package org.sat4j.csp.variables;

import java.math.BigInteger;
import java.util.OptionalInt;
import java.util.function.Supplier;

import org.sat4j.core.Vec;
import org.sat4j.core.VecInt;
import org.sat4j.csp.core.ICSPSolver;
import org.sat4j.csp.utils.UncheckedContradictionException;
import org.sat4j.specs.ContradictionException;

/**
 * The LazyDirectEncodingVariableProxy is a proxy for an {@link IVariable} that allows to
 * switch from any encoding to the direct-encoding of this variable so as to allow
 * efficient encoding of constraints that require an access to a particular value for the
 * variable.
 * The direct encoding of the variable is only computed if it is needed, i.e., when the
 * method {@link #getLiteralForValue(BigInteger)} is invoked.
 *
 * @author Thibault Falque
 * @author Romain Wallon
 *
 * @version 0.1.0
 */
public class LazyDirectEncodingVariableProxy extends AbstractVariableDecorator {

    /**
     * The variable encoded with the direct-encoding.
     * It is {@code null} until this encoding is (lazily) computed.
     */
    private IVariable directEncodedVariable;

    /**
     * The supplier for encoding the variable using the direct-encoding.
     */
    private final Supplier<IVariable> directEncoderForVariable;

    /**
     * The solver in which to add constraints when computing the direct-encoding.
     */
    private final ICSPSolver solver;

    /**
     * Creates a new LazyDirectEncodingVariableProxy.
     *
     * @param originalVariable The original variable, which uses any encoding.
     * @param directEncoderForVariable The supplier for encoding the variable using the
     *        direct-encoding.
     * @param solver The solver in which to add constraints when computing the
     *        direct-encoding.
     */
    public LazyDirectEncodingVariableProxy(IVariable originalVariable,
            Supplier<IVariable> directEncoderForVariable, ICSPSolver solver) {
        super(originalVariable);
        this.directEncoderForVariable = directEncoderForVariable;
        this.solver = solver;
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.sat4j.csp.variables.AbstractVariableDecorator#getLiteralForValue(java.math.
     * BigInteger)
     */
    @Override
    public OptionalInt getLiteralForValue(BigInteger value) {
        // The literal is that having the value as coefficient in the direct-encoding.
        for (int i = 0; i < getDirectEncodedVariable().nVars(); i++) {
            if (directEncodedVariable.getCoefficients().get(i).equals(value)) {
                return OptionalInt.of(directEncodedVariable.getVariables().get(i));
            }
        }
        return OptionalInt.empty();
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.sat4j.csp.variables.AbstractVariableDecorator#getLiteralForValueAtLeast(java.
     * math.BigInteger)
     */
    @Override
    public int getLiteralForValueAtLeast(BigInteger value) {
        throw new UnsupportedOperationException();
    }

    /**
     * Gives the variable encoded with the direct-encoding.
     * If the direct-encoding of the variable has not been computed yet, it is computed
     * now.
     *
     * @return The variable encoded with the direct-encoding.
     */
    private IVariable getDirectEncodedVariable() {
        try {
            if (directEncodedVariable == null) {
                directEncodedVariable = directEncoderForVariable.get();
                if (directEncodedVariable != variable) {
                    addEqualityConstraint();
                }
            }
            return directEncodedVariable;

        } catch (ContradictionException e) {
            throw new UncheckedContradictionException(e);
        }
    }

    /**
     * Adds an equality constraint to make sure that the direct-encoding and the original
     * encoding of the variable indeed encode the same variable.
     *
     * @throws ContradictionException If adding the constraints results in a trivial
     *         inconsistency.
     */
    private void addEqualityConstraint() throws ContradictionException {
        var allLiterals = new VecInt(variable.nVars() + directEncodedVariable.nVars());
        var allCoefficients = new Vec<BigInteger>(variable.nVars() + directEncodedVariable.nVars());

        // Adding the variables and coefficients of the original encoding.
        for (int i = 0; i < variable.nVars(); i++) {
            allLiterals.push(variable.getVariables().get(i));
            allCoefficients.push(variable.getCoefficients().get(i));
        }

        // Adding the variables and coefficients of the direct-encoding.
        for (int i = 0; i < directEncodedVariable.nVars(); i++) {
            allLiterals.push(directEncodedVariable.getVariables().get(i));
            allCoefficients.push(directEncodedVariable.getCoefficients().get(i).negate());
        }

        // Adding the constraint to the solver.
        solver.addExactly(allLiterals, allCoefficients, BigInteger.ZERO);
    }

}
