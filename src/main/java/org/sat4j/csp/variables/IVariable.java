/**
 * Sat4j-CSP, a CSP solver based on the Sat4j platform.
 * Copyright (c) 2021-2022 - Exakis Nelite, Univ Artois & CNRS.
 * All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 * If not, see {@link http://www.gnu.org/licenses}.
 */

package org.sat4j.csp.variables;

import java.math.BigInteger;
import java.util.OptionalInt;

import org.sat4j.specs.IVec;
import org.sat4j.specs.IVecInt;

/**
 * The IVariable defines an interface for representing a (CSP) variable using
 * Boolean variables.
 *
 * @author Thibault Falque
 * @author Romain Wallon
 *
 * @version 0.1.0
 */
public interface IVariable {

    /**
     * Gives the number of Boolean variables needed to represent this variable.
     *
     * @return The number of needed Boolean variables.
     */
    int nVars();

    /**
     * Gives the DIMACS identifiers of the Boolean variables used to represent this
     * variable.
     *
     * @return The Boolean variables representing this variable.
     */
    IVecInt getVariables();

    /**
     * Gives the coefficients of the Boolean variables used in the representation of
     * this variable.
     *
     * @return The coefficients of the Boolean variables representing this variable.
     */
    IVec<BigInteger> getCoefficients();

    /**
     * Gives the literal to satisfy to ensure that this variable has the given value.
     *
     * @param value The value to get the literal for.
     *
     * @return The literal associated to the value.
     */
    OptionalInt getLiteralForValue(BigInteger value);

    /**
     * Gives the literal to satisfy to ensure that this variable has a value at least
     * equal to the given one.
     *
     * @param value The value to get the literal for.
     *
     * @return The literal associated to the value.
     */
    int getLiteralForValueAtLeast(BigInteger value);

    /**
     * Gives the shift applied to this variable to make its domain start at a
     * different value.
     *
     * @return The shift applied to this variable.
     */
    BigInteger getShift();

    /**
     * Gives the domain of this variable, describing the values this variable can
     * take.
     *
     * @return The domain of this variable.
     */
    IDomain getDomain();

    /**
     * Gives a variable that represents the opposite of this variable.
     *
     * @return The variable that represents {@code -this}.
     */
    default IVariable negate() {
        return new MultipliedVariableDecorator(this, BigInteger.ONE.negate());
    }

    /**
     * Decodes the value of this variable, based on the assignment of the Boolean
     * variables that encode it, as provided by the given model.
     *
     * @param model The model of the CSP problem.
     *
     * @return The value of this variable, as provided by the model.
     */
    default BigInteger decode(int[] model) {
        var variables = getVariables();
        var coefficients = getCoefficients();
        var value = getShift();

        for (int i = 0; i < nVars(); i++) {
            int variable = variables.get(i);
            var coefficient = coefficients.get(i);

            if (model[variable - 1] > 0) {
                value = value.add(coefficient);
            }
        }

        return value;
    }

}
