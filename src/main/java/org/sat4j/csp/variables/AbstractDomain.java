/**
 * Sat4j-CSP, a CSP solver based on the Sat4j platform.
 * Copyright (c) 2021-2022 - Exakis Nelite, Univ Artois & CNRS.
 * All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 * If not, see {@link http://www.gnu.org/licenses}.
 */

package org.sat4j.csp.variables;

import java.math.BigInteger;

import org.sat4j.core.Vec;
import org.sat4j.specs.IVec;

/**
 * The AbstractDomain is the base class for all implementations of {@link IDomain}.
 *
 * @author Thibault Falque
 * @author Romain Wallon
 *
 * @version 0.1.0
 */
public abstract class AbstractDomain implements IDomain {

    /**
     * The minimum value of the domain.
     */
    private final BigInteger min;

    /**
     * The maximum value of the domain.
     */
    private final BigInteger max;

    /**
     * The vector of all the values of the domain.
     */
    private final IVec<BigInteger> values;

    /**
     * Creates a new AbstractDomain.
     *
     * @param min The minimum value of the domain.
     * @param max The maximum value of the domain.
     * @param values The vector of all the values of the domain.
     */
    protected AbstractDomain(BigInteger min, BigInteger max, IVec<BigInteger> values) {
        this.min = min;
        this.max = max;
        this.values = values;
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.variables.IDomain#min()
     */
    @Override
    public BigInteger min() {
        return min;
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.variables.IDomain#max()
     */
    @Override
    public BigInteger max() {
        return max;
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.variables.IDomain#allValues()
     */
    @Override
    public IVec<BigInteger> allValues() {
        return values;
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.variables.IDomain#add(org.sat4j.csp.variables.IDomain)
     */
    @Override
    public IDomain add(IDomain other) {
        return add((AbstractDomain) other);
    }

    /**
     * Computes the domain obtained by adding all the values in this domain to those of
     * the other.
     *
     * @param other The domain with which to compute the addition.
     *
     * @return The domain representing the addition of the domains.
     */
    protected abstract IDomain add(AbstractDomain other);

    /**
     * Computes the domain obtained by adding all the values in this domain to those of
     * the other.
     *
     * @param other The domain with which to compute the addition.
     *
     * @return The domain representing the addition of the domains.
     */
    protected abstract IDomain add(RangeDomain other);

    /**
     * Computes the domain obtained by adding all the values in this domain to those of
     * the other.
     *
     * @param other The domain with which to compute the addition.
     *
     * @return The domain representing the addition of the domains.
     */
    protected abstract IDomain add(EnumeratedDomain other);

    /**
     * Computes the domain obtained by adding all the values in this domain to those of
     * the other, by enumerating all values from both domains.
     *
     * @param other The domain with which to compute the addition.
     *
     * @return The domain representing the addition of the domains.
     */
    protected final IDomain defaultAdd(IDomain other) {
        var values = new Vec<BigInteger>(size() * other.size());
        for (var thisValue : this) {
            for (var otherValue : other) {
                values.push(thisValue.add(otherValue));
            }
        }
        values.sortUnique(BigInteger::compareTo);
        return EnumeratedDomain.of(values);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.variables.IDomain#multiply(org.sat4j.csp.variables.IDomain)
     */
    @Override
    public IDomain multiply(IDomain other) {
        return multiply((AbstractDomain) other);
    }

    /**
     * Computes the domain obtained by multiplying all the values in this domain to
     * those of the other.
     *
     * @param other The domain with which to compute the multiplication.
     *
     * @return The domain representing the multiplication of the domains.
     */
    protected abstract IDomain multiply(AbstractDomain other);

    /**
     * Computes the domain obtained by multiplying all the values in this domain to
     * those of the other.
     *
     * @param other The domain with which to compute the multiplication.
     *
     * @return The domain representing the multiplication of the domains.
     */
    protected abstract IDomain multiply(RangeDomain other);

    /**
     * Computes the domain obtained by multiplying all the values in this domain to
     * those of the other.
     *
     * @param other The domain with which to compute the multiplication.
     *
     * @return The domain representing the multiplication of the domains.
     */
    protected abstract IDomain multiply(EnumeratedDomain other);

    /**
     * Computes the domain obtained by multiplying all the values in this domain to
     * those of the other, by enumerating all values from both domains.
     *
     * @param other The domain with which to compute the multiplication.
     *
     * @return The domain representing the multiplication of the domains.
     */
    protected final IDomain defaultMultiply(IDomain other) {
        var values = new Vec<BigInteger>(size() * other.size());
        for (var thisValue : this) {
            for (var otherValue : other) {
                values.push(thisValue.multiply(otherValue));
            }
        }
        values.sortUnique(BigInteger::compareTo);
        return EnumeratedDomain.of(values);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.variables.IDomain#union(org.sat4j.csp.variables.IDomain)
     */
    @Override
    public IDomain union(IDomain other) {
        return union((AbstractDomain) other);
    }

    /**
     * Computes the union between this domain and the other.
     *
     * @param other The domain with which to compute the union.
     *
     * @return The domain representing the union of the domains.
     */
    protected abstract IDomain union(AbstractDomain other);

    /**
     * Computes the union between this domain and the other.
     *
     * @param other The domain with which to compute the union.
     *
     * @return The domain representing the union of the domains.
     */
    protected abstract IDomain union(RangeDomain other);

    /**
     * Computes the union between this domain and the other.
     *
     * @param other The domain with which to compute the union.
     *
     * @return The domain representing the union of the domains.
     */
    protected abstract IDomain union(EnumeratedDomain other);

    /**
     * Computes the union between this domain and the other, by enumerating all values
     * from both domains.
     *
     * @param other The domain with which to compute the union.
     *
     * @return The domain representing the union of the domains.
     */
    protected final IDomain defaultUnion(IDomain other) {
        // Looking for the values that do not appear in the other domain.
        var notContained = new Vec<BigInteger>();
        for (var value : this) {
            if (!other.contains(value)) {
                notContained.push(value);
            }
        }

        if (notContained.isEmpty()) {
            // This domain is fully contained in the other domain.
            return other;
        }

        // Copying the values of the other domain and the ones in notContained in the union.
        var bothValues = new Vec<BigInteger>(other.size() + notContained.size());
        other.allValues().copyTo(bothValues);
        notContained.copyTo(bothValues);
        bothValues.sortUnique(BigInteger::compareTo);
        return EnumeratedDomain.of(bothValues);
    }

}
