/**
 * Sat4j-CSP, a CSP solver based on the Sat4j platform.
 * Copyright (c) 2021-2022 - Exakis Nelite, Univ Artois & CNRS.
 * All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 * If not, see {@link http://www.gnu.org/licenses}.
 */

package org.sat4j.csp.variables;

import static java.math.BigInteger.ONE;

import java.math.BigInteger;

import org.sat4j.core.Vec;
import org.sat4j.core.VecInt;
import org.sat4j.csp.core.ICSPSolver;
import org.sat4j.specs.ContradictionException;
import org.sat4j.specs.IVec;

/**
 * The LogEncodingVariableFactory is an {@link IVariableFactory} that creates variables
 * encoded using the log-encoding.
 *
 * @author Thibault Falque
 * @author Romain Wallon
 *
 * @version 0.1.0
 */
public class LogEncodingVariableFactory extends AbstractVariableFactory {

    /**
     * The direct-encoding variable factory used to compute the direct-encoding of
     * variables when needed, through a {@link LazyDirectEncodingVariableProxy}.
     */
    private final IVariableFactory directEncodingVariableFactory = new DirectEncodingVariableFactory();

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.variables.AbstractVariableFactory#setSolver(org.sat4j.csp.core.
     * ICSPSolver)
     */
    @Override
    public void setSolver(ICSPSolver solver) {
        super.setSolver(solver);
        directEncodingVariableFactory.setSolver(solver);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.sat4j.csp.variables.AbstractVariableFactory#internalCreateVariable(
     * org.sat4j.specs.IVec)
     */
    @Override
    protected IVariable internalCreateVariable(IVec<BigInteger> values)
            throws ContradictionException {
        // Adding the right number of variables.
        var min = values.get(0);
        var max = values.last();
        int nBits = max.bitLength();
        int last = solver.newDimacsVariables(nBits);

        // Computing the variables and coefficients.
        var variables = new VecInt(nBits);
        var coefficients = new Vec<BigInteger>(nBits);
        var current = ONE;
        for (int i = 0; i < nBits; i++) {
            variables.push(last - i);
            coefficients.push(current);
            current = current.shiftLeft(1);
        }

        // Adding the constraints for the domain.
        int allowedValueIndex = 1;
        var clause = new VecInt(nBits);
        for (var value = min.add(ONE); value.compareTo(max) < 0; value = value.add(ONE)) {
            if (value.equals(values.get(allowedValueIndex))) {
                // This value is allowed.
                allowedValueIndex++;
                continue;
            }

            // The value is not allowed: a clause must forbid it.
            for (int i = 0; i < nBits; i++) {
                if (value.testBit(i)) {
                    clause.push(-variables.get(i));
                } else {
                    clause.push(variables.get(i));
                }
            }
            solver.addClause(clause);
            clause.clear();
        }

        // Creating the variable.
        var variable = new Variable(variables, coefficients, EnumeratedDomain.of(values));
        return new LazyDirectEncodingVariableProxy(variable,
                () -> directEncodingVariableFactory.createVariable(values),
                solver);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.sat4j.csp.variables.AbstractVariableFactory#internalCreateVariable(
     * java.math.BigInteger, java.math.BigInteger)
     */
    @Override
    protected IVariable internalCreateVariable(BigInteger min, BigInteger max)
            throws ContradictionException {
        // Adding the right number of variables.
        int nBits = max.bitLength();
        int last = solver.newDimacsVariables(nBits);

        // Computing the variables and coefficients.
        var variables = new VecInt(nBits);
        var coefficients = new Vec<BigInteger>(nBits);
        var current = ONE;
        for (int i = 0; i < nBits; i++) {
            variables.push(last - i);
            coefficients.push(current);
            current = current.shiftLeft(1);
        }

        // Adding the constraints for the domain.
        solver.addAtLeast(variables, coefficients, min);
        solver.addAtMost(variables, coefficients, max);

        // Creating the variable.
        var variable = new Variable(variables, coefficients, RangeDomain.of(min, max));
        return new LazyDirectEncodingVariableProxy(variable,
                () -> directEncodingVariableFactory.createVariable(min, max),
                solver);
    }

}
