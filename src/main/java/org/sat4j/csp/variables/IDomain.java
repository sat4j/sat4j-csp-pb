/**
 * Sat4j-CSP, a CSP solver based on the Sat4j platform.
 * Copyright (c) 2021-2022 - Exakis Nelite, Univ Artois & CNRS.
 * All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 * If not, see {@link http://www.gnu.org/licenses}.
 */

package org.sat4j.csp.variables;

import java.math.BigInteger;
import java.util.Iterator;
import java.util.List;

import org.sat4j.csp.utils.UncheckedContradictionException;
import org.sat4j.specs.IVec;

/**
 * The IDomain defines an interface for dealing with the domains of the variables of the
 * CSP problem.
 *
 * @author Thibault Falque
 * @author Romain Wallon
 *
 * @version 0.1.0
 */
public interface IDomain extends Iterable<BigInteger> {

    /**
     * Gives the minimum value in this domain.
     *
     * @return The minimum value in this domain.
     */
    BigInteger min();

    /**
     * Gives the maximum value in this domain.
     *
     * @return The maximum value in this domain.
     */
    BigInteger max();

    /**
     * Gives all the values in this domain.
     *
     * @return The vector of all the values in this domain.
     */
    IVec<BigInteger> allValues();

    /**
     * Gives the size of this domain, i.e., the number of values it contains.
     *
     * @return The size of this domain.
     */
    default int size() {
        return allValues().size();
    }

    /**
     * Checks whether a value is present in this domain.
     *
     * @param value The value to check the presence of.
     *
     * @return Whether this domain contains the value.
     */
    default boolean contains(BigInteger value) {
        return allValues().contains(value);
    }

    /**
     * Provides an iterator over the values of this domain.
     *
     * @return An iterator over the values of this domain.
     */
    @Override
    default Iterator<BigInteger> iterator() {
        return allValues().iterator();
    }

    /**
     * Shifts this domain by a certain value.
     *
     * @param value The value by which to shift the values in this domain.
     *
     * @return The shifted domain.
     */
    IDomain shift(BigInteger value);

    /**
     * Multiplies this domain by a coefficient.
     *
     * @param coefficient The coefficient by which to multiply the values in this domain.
     *
     * @return The multiplied domain.
     */
    IDomain multiply(BigInteger coefficient);

    /**
     * Computes the domain obtained by adding all the values in this domain to those of
     * the other.
     *
     * @param other The domain with which to compute the addition.
     *
     * @return The domain representing the addition of the domains.
     */
    IDomain add(IDomain other);

    /**
     * Computes the domain obtained by multiplying all the values in this domain to
     * those of the other.
     *
     * @param other The domain with which to compute the multiplication.
     *
     * @return The domain representing the multiplication of the domains.
     */
    IDomain multiply(IDomain other);

    /**
     * Computes the domain obtained by adding together the values of all the given
     * domains.
     *
     * @param domains The domains to compute the addition of.
     *
     * @return The domain representing the addition of the domains.
     */
    static IDomain additionOf(IDomain... domains) {
        return additionOf(List.of(domains));
    }

    /**
     * Computes the domain obtained by adding together the values of all the given
     * domains.
     *
     * @param domains The domains to compute the addition of.
     *
     * @return The domain representing the addition of the domains.
     */
    static IDomain additionOf(List<IDomain> domains) {
        if (domains.isEmpty()) {
            throw new UncheckedContradictionException("Creating empty domain!");
        }

        var addition = domains.get(0);
        for (int i = 1; i < domains.size(); i++) {
            addition = addition.add(domains.get(i));
        }
        return addition;
    }

    /**
     * Computes the union between this domain and the other.
     *
     * @param other The domain with which to compute the union.
     *
     * @return The domain representing the union of the domains.
     */
    IDomain union(IDomain other);

    /**
     * Computes the union of the given domains.
     *
     * @param domains The domains to compute the union of.
     *
     * @return The domain representing the union of the domains.
     */
    static IDomain unionOf(IDomain... domains) {
        return unionOf(List.of(domains));
    }

    /**
     * Computes the union of the given domains.
     *
     * @param domains The domains to compute the union of.
     *
     * @return The domain representing the union of the domains.
     */
    static IDomain unionOf(List<IDomain> domains) {
        if (domains.isEmpty()) {
            throw new UncheckedContradictionException("Creating empty domain!");
        }

        var union = domains.get(0);
        for (int i = 1; i < domains.size(); i++) {
            union = union.union(domains.get(i));
        }
        return union;
    }

    /**
     * Creates a variable that has this domain.
     *
     * @param variableFactory The variable factory to use to instantiate the variable.
     *
     * @return The created variable.
     */
    IVariable newVariableWithThisDomain(IVariableFactory variableFactory);

}
