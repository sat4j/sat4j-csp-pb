/**
 * Sat4j-CSP, a CSP solver based on the Sat4j platform.
 * Copyright (c) 2021-2022 - Exakis Nelite, Univ Artois & CNRS.
 * All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 * If not, see {@link http://www.gnu.org/licenses}.
 */

package org.sat4j.csp.variables;

import java.math.BigInteger;
import java.util.OptionalInt;

import org.sat4j.specs.IVec;
import org.sat4j.specs.IVecInt;

/**
 * The Variable is a POJO implementation of {@link IVariable} that contains all the
 * information needed to represent a (CSP) variable as a linear combination of Boolean
 * variables.
 *
 * @author Thibault Falque
 * @author Romain Wallon
 *
 * @version 0.1.0
 */
public final class Variable implements IVariable {

    /**
     * The Boolean variables used to encode this variable.
     */
    private final IVecInt variables;

    /**
     * The coefficients of the Boolean variables in the encoding of this variable.
     */
    private final IVec<BigInteger> coefficients;

    /**
     * The domain of this variable, describing the values it can take.
     */
    private final IDomain domain;

    /**
     * Creates a new Variable.
     *
     * @param variables The Boolean variables used to encode the variable.
     * @param coefficients The coefficients of the Boolean variables in the encoding of
     *        this variable.
     * @param domain The domain of this variable, describing the values it can take.
     */
    public Variable(IVecInt variables, IVec<BigInteger> coefficients, IDomain domain) {
        this.variables = variables;
        this.coefficients = coefficients;
        this.domain = domain;
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.variables.IVariable#nVars()
     */
    @Override
    public int nVars() {
        return variables.size();
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.variables.IVariable#getVariables()
     */
    @Override
    public IVecInt getVariables() {
        return variables;
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.variables.IVariable#getCoefficients()
     */
    @Override
    public IVec<BigInteger> getCoefficients() {
        return coefficients;
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.variables.IVariable#getShift()
     */
    @Override
    public BigInteger getShift() {
        return BigInteger.ZERO;
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.variables.IVariable#getLiteralForValue(java.math.BigInteger)
     */
    @Override
    public OptionalInt getLiteralForValue(BigInteger value) {
        throw new UnsupportedOperationException();
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.sat4j.csp.variables.IVariable#getLiteralForValueAtLeast(java.math.BigInteger)
     */
    @Override
    public int getLiteralForValueAtLeast(BigInteger value) {
        throw new UnsupportedOperationException();
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.variables.IVariable#getDomain()
     */
    @Override
    public IDomain getDomain() {
        return domain;
    }

}
