/**
 * Sat4j-CSP, a CSP solver based on the Sat4j platform.
 * Copyright (c) 2021-2022 - Exakis Nelite, Univ Artois & CNRS.
 * All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 * If not, see {@link http://www.gnu.org/licenses}.
 */

package org.sat4j.csp.variables;

import java.math.BigInteger;

import org.sat4j.csp.core.ICSPSolver;
import org.sat4j.specs.IVec;

/**
 * The SmartEncodingVariableFactory is an {@link IVariableFactory} that creates variables
 * encoded using different encodings.
 * The most suitable encoding is chosen based on the values that are allowed for the
 * variable to create.
 *
 * @author Thibault Falque
 * @author Romain Wallon
 *
 * @version 0.1.0
 */
public final class SmartEncodingVariableFactory implements IVariableFactory {

    /**
     * The factory for creating variables encoded with the direct-encoding.
     */
    private final IVariableFactory directEncoding = new DirectEncodingVariableFactory();

    /**
     * The factory for creating variables encoded with the log-encoding.
     */
    private final IVariableFactory logEncoding = new LogEncodingVariableFactory();

    /*
     * (non-Javadoc)
     *
     * @see
     * org.sat4j.csp.variables.IVariableFactory#setSolver(org.sat4j.csp.core.ICSPSolver)
     */
    @Override
    public void setSolver(ICSPSolver solver) {
        directEncoding.setSolver(solver);
        logEncoding.setSolver(solver);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.variables.IVariableFactory#createVariable(
     * org.sat4j.specs.IVec)
     */
    @Override
    public IVariable createVariable(IVec<BigInteger> values) {
        return directEncoding.createVariable(values);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.variables.IVariableFactory#createVariable(
     * java.math.BigInteger, java.math.BigInteger)
     */
    @Override
    public IVariable createVariable(BigInteger min, BigInteger max) {
        return logEncoding.createVariable(min, max);
    }

}
