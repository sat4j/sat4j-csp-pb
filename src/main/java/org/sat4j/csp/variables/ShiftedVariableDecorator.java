/**
 * Sat4j-CSP, a CSP solver based on the Sat4j platform.
 * Copyright (c) 2021-2022 - Exakis Nelite, Univ Artois & CNRS.
 * All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 * If not, see {@link http://www.gnu.org/licenses}.
 */

package org.sat4j.csp.variables;

import java.math.BigInteger;
import java.util.OptionalInt;

/**
 * The ShiftedVariableDecorator is a decorator for an {@code IVariable} that allows
 * to shift the domain of this variable with a particular value.
 *
 * @author Thibault Falque
 * @author Romain Wallon
 *
 * @version 0.1.0
 */
public final class ShiftedVariableDecorator extends AbstractVariableDecorator {

    /**
     * The shift to apply to the variable.
     */
    private final BigInteger shift;

    /**
     * Creates a new ShiftedVariableDecorator.
     *
     * @param variable The variable to decorate.
     * @param shift The shift to apply to the variable.
     */
    public ShiftedVariableDecorator(IVariable variable, BigInteger shift) {
        super(variable);
        this.shift = shift;
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.variables.IVariable#getLiteralForValue(java.math.BigInteger)
     */
    @Override
    public OptionalInt getLiteralForValue(BigInteger value) {
        return super.getLiteralForValue(value.subtract(shift));
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.sat4j.csp.variables.AbstractVariableDecorator#getLiteralForValueAtLeast(java.
     * math.BigInteger)
     */
    @Override
    public int getLiteralForValueAtLeast(BigInteger value) {
        return super.getLiteralForValueAtLeast(value.subtract(shift));
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.variables.IVariable#getShift()
     */
    @Override
    public BigInteger getShift() {
        return shift.add(super.getShift());
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.variables.IVariable#getDomain()
     */
    @Override
    public IDomain getDomain() {
        return super.getDomain().shift(shift);
    }

}
