/**
 * Sat4j-CSP, a CSP solver based on the Sat4j platform.
 * Copyright (c) 2021-2022 - Exakis Nelite, Univ Artois & CNRS.
 * All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 * If not, see {@link http://www.gnu.org/licenses}.
 */

package org.sat4j.csp.variables;

import java.math.BigInteger;

import org.sat4j.core.Vec;
import org.sat4j.csp.utils.RangeVec;

/**
 * The RangeDomain is an {@link IDomain} that contains a range of values.
 *
 * @author Thibault Falque
 * @author Romain Wallon
 *
 * @version 0.1.0
 */
public class RangeDomain extends AbstractDomain {

    /**
     * Creates a new RangeDomain.
     *
     * @param min The minimum value of the domain.
     * @param max The maximum value of the domain.
     */
    private RangeDomain(BigInteger min, BigInteger max) {
        super(min, max, RangeVec.of(min, max));
    }

    /**
     * Creates a new RangeDomain.
     *
     * @param min The minimum value of the domain.
     * @param max The maximum value of the domain.
     *
     * @return The created domain.
     */
    public static IDomain of(BigInteger min, BigInteger max) {
        return new RangeDomain(min, max);
    }

    /**
     * Creates a new RangeDomain containing a single value.
     *
     * @param value The only value of the domain.
     *
     * @return The created domain.
     */
    public static IDomain singleton(BigInteger value) {
        return new RangeDomain(value, value);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.sat4j.csp.variables.IDomain#newVariableWithThisDomain(org.sat4j.csp.variables.
     * IVariableFactory)
     */
    @Override
    public IVariable newVariableWithThisDomain(IVariableFactory variableFactory) {
        return variableFactory.createVariable(min(), max());
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.variables.IDomain#shift(java.math.BigInteger)
     */
    @Override
    public IDomain shift(BigInteger value) {
        return RangeDomain.of(min().add(value), max().add(value));
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.variables.IDomain#multiply(java.math.BigInteger)
     */
    @Override
    public IDomain multiply(BigInteger coefficient) {
        if (coefficient.signum() >= 0) {
            return RangeDomain.of(min().multiply(coefficient), max().multiply(coefficient));
        }
        return RangeDomain.of(max().multiply(coefficient), min().multiply(coefficient));
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.sat4j.csp.variables.AbstractDomain#add(org.sat4j.csp.variables.AbstractDomain)
     */
    @Override
    protected IDomain add(AbstractDomain other) {
        return other.add(this);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.sat4j.csp.variables.AbstractDomain#add(org.sat4j.csp.variables.RangeDomain)
     */
    @Override
    protected IDomain add(RangeDomain other) {
        return RangeDomain.of(min().add(other.min()), max().add(other.max()));
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.variables.AbstractDomain#add(org.sat4j.csp.variables.
     * EnumeratedDomain)
     */
    @Override
    protected IDomain add(EnumeratedDomain other) {
        return defaultAdd(other);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.variables.AbstractDomain#multiply(org.sat4j.csp.variables.
     * AbstractDomain)
     */
    @Override
    protected IDomain multiply(AbstractDomain other) {
        return defaultMultiply(other);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.variables.AbstractDomain#multiply(org.sat4j.csp.variables.
     * RangeDomain)
     */
    @Override
    protected IDomain multiply(RangeDomain other) {
        var possibleBounds = Vec.of(
                min().multiply(other.min()),
                min().multiply(other.max()),
                max().multiply(other.min()),
                max().multiply(other.max()));
        possibleBounds.sort(BigInteger::compareTo);
        return RangeDomain.of(possibleBounds.get(0), possibleBounds.last());
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.variables.AbstractDomain#multiply(org.sat4j.csp.variables.
     * EnumeratedDomain)
     */
    @Override
    protected IDomain multiply(EnumeratedDomain other) {
        return defaultMultiply(other);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.variables.AbstractDomain#union(org.sat4j.csp.variables.
     * AbstractDomain)
     */
    @Override
    protected IDomain union(AbstractDomain other) {
        return other.union(this);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.sat4j.csp.variables.AbstractDomain#union(org.sat4j.csp.variables.RangeDomain)
     */
    @Override
    protected IDomain union(RangeDomain other) {
        if (other.contains(min()) && other.contains(max())) {
            // This domain is contained in the other.
            return other;
        }

        if (this.contains(other.min()) && this.contains(other.max())) {
            // The other domain is contained in this domain.
            return this;
        }

        if (other.contains(min()) || other.contains(max())) {
            // One of the bounds of this domain is contained in the other.
            return new RangeDomain(other.min().min(min()), other.max().max(max()));
        }

        if (this.contains(other.min()) || this.contains(other.max())) {
            // One of the bounds of the other domain is contained in this domain.
            return RangeDomain.of(other.min().min(min()), other.max().max(max()));
        }

        // There is no intersection between the two domains.
        // We must thus enumerate all the values of both domains.
        return defaultUnion(other);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.variables.AbstractDomain#union(org.sat4j.csp.variables.
     * EnumeratedDomain)
     */
    @Override
    protected IDomain union(EnumeratedDomain other) {
        return other.defaultUnion(this);
    }

}
