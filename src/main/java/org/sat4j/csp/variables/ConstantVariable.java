/**
 * Sat4j-CSP, a CSP solver based on the Sat4j platform.
 * Copyright (c) 2021-2022 - Exakis Nelite, Univ Artois & CNRS.
 * All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 * If not, see {@link http://www.gnu.org/licenses}.
 */

package org.sat4j.csp.variables;

import java.math.BigInteger;
import java.util.OptionalInt;

import org.sat4j.core.Vec;
import org.sat4j.core.VecInt;
import org.sat4j.specs.IVec;
import org.sat4j.specs.IVecInt;

/**
 * The ConstantVariable is an {@link IVariable} that represents a CSP variable whose
 * domain contains exactly one value.
 * Such a variable does not need any Boolean variable to be represented.
 * Actually, this class is more an adapter for a {@link BigInteger}.
 *
 * @author Thibault Falque
 * @author Romain Wallon
 *
 * @version 0.1.0
 */
public final class ConstantVariable implements IVariable {

    /**
     * The {@link ConstantVariable} for the value {@code -1}.
     */
    public static final IVariable MINUS_ONE = new ConstantVariable(BigInteger.ONE.negate());

    /**
     * The {@link ConstantVariable} for the value {@code 0}.
     */
    public static final IVariable ZERO = new ConstantVariable(BigInteger.ZERO);

    /**
     * The {@link ConstantVariable} for the value {@code 1}.
     */
    public static final IVariable ONE = new ConstantVariable(BigInteger.ONE);

    /**
     * The {@link ConstantVariable} for the value {@code 2}.
     */
    public static final IVariable TWO = new ConstantVariable(BigInteger.TWO);

    /**
     * The value of this variable.
     */
    private final BigInteger value;

    /**
     * Creates a new ConstantVariable.
     *
     * @param value The value of the variable.
     */
    private ConstantVariable(BigInteger value) {
        this.value = value;
    }

    /**
     * Creates a new ConstantVariable.
     *
     * @param value The value of the variable.
     *
     * @return The created variable.
     */
    public static IVariable of(long value) {
        return new ConstantVariable(BigInteger.valueOf(value));
    }

    /**
     * Creates a new ConstantVariable.
     *
     * @param value The value of the variable.
     *
     * @return The created variable.
     */
    public static IVariable of(BigInteger value) {
        return new ConstantVariable(value);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.variables.IVariable#nVars()
     */
    @Override
    public int nVars() {
        return 0;
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.variables.IVariable#getVariables()
     */
    @Override
    public IVecInt getVariables() {
        return VecInt.of();
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.variables.IVariable#getCoefficients()
     */
    @Override
    public IVec<BigInteger> getCoefficients() {
        return Vec.of();
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.variables.IVariable#getLiteralForValue(java.math.BigInteger)
     */
    @Override
    public OptionalInt getLiteralForValue(BigInteger value) {
        if (this.value.equals(value)) {
            // Returning the literal 1, that is always true in the solver.
            return OptionalInt.of(1);
        }

        // The value does not match this constant.
        return OptionalInt.empty();
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.sat4j.csp.variables.IVariable#getLiteralForValueAtLeast(java.math.BigInteger)
     */
    @Override
    public int getLiteralForValueAtLeast(BigInteger value) {
        if (this.value.compareTo(value) >= 0) {
            // This constant is indeed greater than the value.
            // Returning the literal 1, that is always true in the solver.
            return 1;
        }

        // It is impossible to be at least equal to this value.
        // Returning the literal -1, that is always false in the solver.
        return -1;
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.variables.IVariable#getShift()
     */
    @Override
    public BigInteger getShift() {
        return value;
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.variables.IVariable#getDomain()
     */
    @Override
    public IDomain getDomain() {
        return RangeDomain.singleton(value);
    }

}
