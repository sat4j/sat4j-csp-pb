/**
 * Sat4j-CSP, a CSP solver based on the Sat4j platform.
 * Copyright (c) 2021-2022 - Exakis Nelite, Univ Artois & CNRS.
 * All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 * If not, see {@link http://www.gnu.org/licenses}.
 */

package org.sat4j.csp.variables;

import java.math.BigInteger;
import java.util.OptionalInt;

import org.sat4j.csp.utils.MultipliedVec;
import org.sat4j.specs.IVec;

/**
 * The MultipliedVariableDecorator is a decorator for an {@code IVariable} that allows
 * to multiply the domain of this variable by a certain value.
 *
 * @author Thibault Falque
 * @author Romain Wallon
 *
 * @version 0.1.0
 */
public final class MultipliedVariableDecorator extends AbstractVariableDecorator {

    /**
     * The multiplicative coefficient to apply to the variable.
     */
    private final BigInteger coefficient;

    /**
     * The coefficients of the Boolean variables, once the multiplication has been
     * applied.
     */
    private final IVec<BigInteger> multipliedCoefficients;

    /**
     * Creates a new MultipliedVariableDecorator.
     *
     * @param variable The variable to decorate.
     * @param coefficient The multiplicative coefficient to apply to the variable.
     */
    public MultipliedVariableDecorator(IVariable variable, BigInteger coefficient) {
        super(variable);
        this.coefficient = coefficient;
        this.multipliedCoefficients = new MultipliedVec(variable.getCoefficients(), coefficient);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.variables.IVariable#getCoefficients()
     */
    @Override
    public IVec<BigInteger> getCoefficients() {
        return multipliedCoefficients;
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.variables.IVariable#getLiteralForValue(java.math.BigInteger)
     */
    @Override
    public OptionalInt getLiteralForValue(BigInteger value) {
        return super.getLiteralForValue(value.divide(coefficient));
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.sat4j.csp.variables.AbstractVariableDecorator#getLiteralForValueAtLeast(java.
     * math.BigInteger)
     */
    @Override
    public int getLiteralForValueAtLeast(BigInteger value) {
        if (coefficient.signum() > 0) {
            // The sign has not changed by applying the multiplication.
            return super.getLiteralForValueAtLeast(value.divide(coefficient));
        }

        // The sign has changed after the multiplication, so we must change the value.
        var minOver = super.getLiteralForValueAtLeast(
                value.divide(coefficient).add(BigInteger.ONE));
        return -minOver;
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.variables.IVariable#getShift()
     */
    @Override
    public BigInteger getShift() {
        return coefficient.multiply(super.getShift());
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.variables.IVariable#getDomain()
     */
    @Override
    public IDomain getDomain() {
        return super.getDomain().multiply(coefficient);
    }

}
