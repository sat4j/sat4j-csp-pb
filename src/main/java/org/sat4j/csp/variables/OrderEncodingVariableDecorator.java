/**
 * Sat4j-CSP, a CSP solver based on the Sat4j platform.
 * Copyright (c) 2021-2022 - Exakis Nelite, Univ Artois & CNRS.
 * All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 * If not, see {@link http://www.gnu.org/licenses}.
 */

package org.sat4j.csp.variables;

import static java.math.BigInteger.ONE;
import static java.math.BigInteger.TWO;

import java.math.BigInteger;
import java.util.HashMap;
import java.util.Map;
import java.util.OptionalInt;

import org.sat4j.core.Vec;
import org.sat4j.core.VecInt;
import org.sat4j.csp.constraints.RelationalOperator;
import org.sat4j.csp.core.ICSPSolver;
import org.sat4j.csp.utils.UncheckedContradictionException;
import org.sat4j.specs.ContradictionException;

/**
 * The OrderEncodingVariableDecorator is a decorator for an {@link IVariable} that allows
 * to interpret this variable as being encoded with the order encoding.
 *
 * @author Thibault Falque
 * @author Romain Wallon
 *
 * @version 0.1.0
 */
public class OrderEncodingVariableDecorator extends AbstractVariableDecorator {

    /**
     * The solver in which to add constraints when computing the literal representing a
     * particular value.
     */
    private final ICSPSolver solver;

    /**
     * The cache for the literals representing the values of the domain of this variable.
     */
    private final Map<BigInteger, Integer> cache = new HashMap<>();

    /**
     * Creates a new OrderEncodingVariableDecorator.
     *
     * @param variable The variable to decorate.
     * @param solver The solver in which the variable is created.
     */
    public OrderEncodingVariableDecorator(IVariable variable, ICSPSolver solver) {
        super(variable);
        this.solver = solver;
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.sat4j.csp.variables.AbstractVariableDecorator#getLiteralForValue(java.math.
     * BigInteger)
     */
    @Override
    public OptionalInt getLiteralForValue(BigInteger value) {
        return getLiteralForValue(value, true);
    }

    /**
     * Gives the literal to satisfy to ensure that this variable has the given value.
     * This method should only be used internally.
     *
     * @param value The value to get the literal for.
     * @param checkDomain Whether the existence of the value of the domain must be checked
     *        before looking for the literal.
     *
     * @return The literal associated to the value.
     */
    protected OptionalInt getLiteralForValue(BigInteger value, boolean checkDomain) {
        // Checking whether the value is allowed.
        if (checkDomain && (!variable.getDomain().contains(value))) {
            return OptionalInt.empty();
        }

        // Checking whether the literal for the value already exists.
        var cachedLiteral = cache.get(value);
        if (cachedLiteral != null) {
            return OptionalInt.of(cachedLiteral);
        }

        try {
            // Computing the representation of the value.
            int atLeastValue = getLiteralForValueAtLeast(value);
            int atLeastValuePlus1 = getLiteralForValueAtLeast(value.add(ONE));

            if (atLeastValue == -1) {
                // The value is too big, and cannot be represented.
                // Returning the literal -1, that is always false in the solver.
                cache.put(value, -1);
                return OptionalInt.of(-1);
            }

            if ((atLeastValue == 1) && ((atLeastValuePlus1 == 1) || (atLeastValuePlus1 == -1))) {
                // The value is too small, and cannot be represented.
                // Returning the literal -1, that is always false in the solver.
                cache.put(value, -1);
                return OptionalInt.of(-1);
            }

            if (atLeastValue == 1) {
                // The required value is actually the minimum of the domain.
                cache.put(value, -atLeastValuePlus1);
                return OptionalInt.of(-atLeastValuePlus1);
            }

            if (atLeastValuePlus1 == -1) {
                // The required value is actually the maximum of the domain.
                cache.put(value, atLeastValue);
                return OptionalInt.of(atLeastValue);
            }

            // The value on the inside of the domain of the variable.
            // Creating the variable representing the value.
            int v = solver.newDimacsVariables(1);
            cache.put(value, v);

            // Adding the constraint enforcing the value iff v is satisfied.
            solver.addSoftPseudoBoolean(v, VecInt.of(atLeastValue, -atLeastValuePlus1),
                    Vec.of(ONE, ONE), RelationalOperator.EQ, TWO);

            // Returning the variable for the value.
            return OptionalInt.of(v);

        } catch (ContradictionException e) {
            throw new UncheckedContradictionException(e);
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.sat4j.csp.variables.AbstractVariableDecorator#getLiteralForValueAtLeast(java.
     * math.BigInteger)
     */
    @Override
    public int getLiteralForValueAtLeast(BigInteger value) {
        // Switching to fixed precision integers for more efficiency.
        // If arbitrary precision were required, the variable could not have been encoded.
        var min = variable.getDomain().min().intValue();
        var max = variable.getDomain().max().intValue();
        var intValue = value.intValue();

        if (intValue <= min) {
            // The variable can take any value of its domain.
            // Returning the literal 1, that is always true in the solver.
            return 1;
        }

        if (max < intValue) {
            // It is impossible to be at least equal to this value.
            // Returning the literal -1, that is always false in the solver.
            return -1;
        }

        // Looking for the appropriate variable.
        var varIndex = intValue - min - 1;
        return getVariables().get(varIndex);
    }

}
