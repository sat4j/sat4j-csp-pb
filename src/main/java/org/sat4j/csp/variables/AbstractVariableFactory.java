/**
 * Sat4j-CSP, a CSP solver based on the Sat4j platform.
 * Copyright (c) 2021-2022 - Exakis Nelite, Univ Artois & CNRS.
 * All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 * If not, see {@link http://www.gnu.org/licenses}.
 */

package org.sat4j.csp.variables;

import static java.math.BigInteger.ONE;

import java.math.BigInteger;

import org.sat4j.core.ReadOnlyVec;
import org.sat4j.csp.core.ICSPSolver;
import org.sat4j.csp.utils.UncheckedContradictionException;
import org.sat4j.specs.ContradictionException;
import org.sat4j.specs.IVec;

/**
 * The AbstractVariableFactory defines the common behavior of the different
 * implementations of {@link IVariableFactory}.
 *
 * @author Thibault Falque
 * @author Romain Wallon
 *
 * @version 0.1.0
 */
public abstract class AbstractVariableFactory implements IVariableFactory {

    /**
     * The solver in which variables are created.
     */
    protected ICSPSolver solver;

    /*
     * (non-Javadoc)
     *
     * @see
     * org.sat4j.csp.variables.IVariableFactory#setSolver(org.sat4j.csp.core.ICSPSolver)
     */
    @Override
    public void setSolver(ICSPSolver solver) {
        this.solver = solver;
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.variables.IVariableFactory#createVariable(org.sat4j.specs.IVec)
     */
    @Override
    public final IVariable createVariable(IVec<BigInteger> values) {
        try {
            values.sortUnique(BigInteger::compareTo);
            if (isContiguousDomain(values)) {
                return createVariable(values.get(0), values.last());
            }

            return internalCreateVariable(new ReadOnlyVec<>(values));

        } catch (ContradictionException e) {
            // Re-throwing the exception as an unchecked one.
            throw new UncheckedContradictionException(e);
        }
    }

    /**
     * Creates a new variable.
     *
     * @param values The allowed values for the variable.
     *
     * @return The created variable.
     *
     * @throws ContradictionException If creating the variable produces a trivial
     *         inconsistency.
     */
    protected abstract IVariable internalCreateVariable(IVec<BigInteger> values)
            throws ContradictionException;

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.variables.IVariableFactory#createVariable(
     * java.math.BigInteger, java.math.BigInteger)
     */
    @Override
    public final IVariable createVariable(BigInteger min, BigInteger max) {
        try {
            if (min.equals(max)) {
                // There is a single value in the domain.
                return ConstantVariable.of(min);
            }

            if ((min.signum() == 0) && ONE.equals(max)) {
                // This is a Boolean variable.
                int dimacs = solver.newDimacsVariable();
                return BooleanVariable.of(dimacs);
            }

            return internalCreateVariable(min, max);

        } catch (ContradictionException e) {
            // Re-throwing the exception as an unchecked one.
            throw new UncheckedContradictionException(e);
        }
    }

    /**
     * Creates a new variable.
     *
     * @param min The minimum value that is allowed for the variable.
     * @param max The maximum value that is allowed for the variable.
     *
     * @return The created variable.
     *
     * @throws ContradictionException If creating the variable produces a trivial
     *         inconsistency.
     */
    protected abstract IVariable internalCreateVariable(BigInteger min, BigInteger max)
            throws ContradictionException;

    /**
     * Checks whether the given values define a contiguous domain.
     * The values are supposed to be sorted in ascending order.
     *
     * @param values The values to check.
     *
     * @return Whether the given values define a contiguous domain.
     */
    private static boolean isContiguousDomain(IVec<BigInteger> values) {
        for (int i = 1; i < values.size(); i++) {
            if (!values.get(i).equals(values.get(i - 1).add(ONE))) {
                return false;
            }
        }
        return true;
    }

}
