/**
 * Sat4j-CSP, a CSP solver based on the Sat4j platform.
 * Copyright (c) 2021-2022 - Exakis Nelite, Univ Artois & CNRS.
 * All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 * If not, see {@link http://www.gnu.org/licenses}.
 */

package org.sat4j.csp.variables;

import java.math.BigInteger;

import org.sat4j.core.Vec;
import org.sat4j.csp.utils.MultipliedVec;
import org.sat4j.csp.utils.ShiftedVec;
import org.sat4j.specs.IVec;

/**
 * The EnumeratedDomain is an {@link IDomain} that contains a sequence of (enumerated)
 * values.
 *
 * @author Thibault Falque
 * @author Romain Wallon
 *
 * @version 0.1.0
 */
public class EnumeratedDomain extends AbstractDomain {

    /**
     * Creates a new EnumeratedDomain.
     *
     * @param values The vector of all the values of the domain.
     */
    private EnumeratedDomain(IVec<BigInteger> values) {
        super(values.get(0), values.last(), values);
    }

    /**
     * Creates a new EnumeratedDomain.
     *
     * @param values The vector of all the values of the domain.
     *
     * @return The created domain.
     */
    public static IDomain of(IVec<BigInteger> values) {
        return new EnumeratedDomain(values);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.sat4j.csp.variables.IDomain#newVariableWithThisDomain(org.sat4j.csp.variables.
     * IVariableFactory)
     */
    @Override
    public IVariable newVariableWithThisDomain(IVariableFactory variableFactory) {
        var copyOfValues = new Vec<BigInteger>(allValues().size());
        allValues().copyTo(copyOfValues);
        return variableFactory.createVariable(copyOfValues);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.variables.IDomain#shift(java.math.BigInteger)
     */
    @Override
    public IDomain shift(BigInteger value) {
        return EnumeratedDomain.of(new ShiftedVec(allValues(), value));
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.variables.IDomain#multiply(java.math.BigInteger)
     */
    @Override
    public IDomain multiply(BigInteger coefficient) {
        if (coefficient.signum() >= 0) {
            // Multiplying this domain will preserve the order of the values in it.
            return EnumeratedDomain.of(new MultipliedVec(allValues(), coefficient));
        }

        // Multiplying this domain will modify the order of the values.
        // We thus need to reverse the vector of values to create the domain.
        // Doing so, we also apply the multiplication rather than decorating the vector.
        var reversedValues = new Vec<BigInteger>(size());
        for (int i = size() - 1; i >= 0; i--) {
            reversedValues.push(allValues().get(i).multiply(coefficient));
        }
        return EnumeratedDomain.of(reversedValues);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.sat4j.csp.variables.AbstractDomain#add(org.sat4j.csp.variables.AbstractDomain)
     */
    @Override
    protected IDomain add(AbstractDomain other) {
        return other.add(this);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.sat4j.csp.variables.AbstractDomain#add(org.sat4j.csp.variables.RangeDomain)
     */
    @Override
    protected IDomain add(RangeDomain other) {
        return defaultAdd(other);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.variables.AbstractDomain#add(org.sat4j.csp.variables.
     * EnumeratedDomain)
     */
    @Override
    protected IDomain add(EnumeratedDomain other) {
        return defaultAdd(other);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.variables.AbstractDomain#multiply(org.sat4j.csp.variables.
     * AbstractDomain)
     */
    @Override
    protected IDomain multiply(AbstractDomain other) {
        return other.multiply(this);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.variables.AbstractDomain#multiply(org.sat4j.csp.variables.
     * RangeDomain)
     */
    @Override
    protected IDomain multiply(RangeDomain other) {
        return defaultMultiply(other);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.variables.AbstractDomain#multiply(org.sat4j.csp.variables.
     * EnumeratedDomain)
     */
    @Override
    protected IDomain multiply(EnumeratedDomain other) {
        return defaultMultiply(other);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.variables.AbstractDomain#union(org.sat4j.csp.variables.
     * AbstractDomain)
     */
    @Override
    protected IDomain union(AbstractDomain other) {
        return other.union(this);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sat4j.csp.variables.AbstractDomain#union(org.sat4j.csp.variables.
     * AbstractDomain)
     */
    @Override
    protected IDomain union(RangeDomain other) {
        return this.defaultUnion(other);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.sat4j.csp.variables.AbstractDomain#union(org.sat4j.csp.variables.RangeDomain)
     */
    @Override
    protected IDomain union(EnumeratedDomain other) {
        return this.defaultUnion(other);
    }

}
