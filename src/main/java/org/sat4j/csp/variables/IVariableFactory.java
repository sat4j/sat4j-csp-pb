/**
 * Sat4j-CSP, a CSP solver based on the Sat4j platform.
 * Copyright (c) 2021-2022 - Exakis Nelite, Univ Artois & CNRS.
 * All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 * If not, see {@link http://www.gnu.org/licenses}.
 */

package org.sat4j.csp.variables;

import java.math.BigInteger;
import java.util.List;

import org.sat4j.core.Vec;
import org.sat4j.csp.core.ICSPSolver;
import org.sat4j.specs.IVec;
import org.sat4j.specs.IVecInt;

/**
 * The IVariableFactory is an abstract factory for creating instances of
 * {@link IVariable}.
 *
 * @author Thibault Falque
 * @author Romain Wallon
 *
 * @version 0.1.0
 */
public interface IVariableFactory {

    /**
     * Sets the solver in which variables must be created.
     *
     * @param solver The solver to set.
     */
    void setSolver(ICSPSolver solver);

    /**
     * Creates a new variable.
     *
     * @param values The allowed values for the variable.
     *
     * @return The created variable.
     */
    default IVariable createVariable(int... values) {
        var bigValues = new Vec<BigInteger>(values.length);
        for (int v : values) {
            bigValues.push(BigInteger.valueOf(v));
        }
        return createVariable(bigValues);
    }

    /**
     * Creates a new variable.
     *
     * @param values The allowed values for the variable.
     *
     * @return The created variable.
     */
    default IVariable createVariable(IVecInt values) {
        var bigValues = new Vec<BigInteger>(values.size());
        for (var it = values.iterator(); it.hasNext();) {
            bigValues.push(BigInteger.valueOf(it.next()));
        }
        return createVariable(bigValues);
    }

    /**
     * Creates a new variable.
     *
     * @param values The allowed values for the variable.
     *
     * @return The created variable.
     */
    default IVariable createVariable(BigInteger... values) {
        return createVariable(Vec.of(values));
    }

    /**
     * Creates a new variable.
     *
     * @param values The allowed values for the variable.
     *
     * @return The created variable.
     */
    default IVariable createVariable(List<BigInteger> values) {
        return createVariable(Vec.of(values));
    }

    /**
     * Creates a new variable.
     *
     * @param values The allowed values for the variable.
     *
     * @return The created variable.
     */
    IVariable createVariable(IVec<BigInteger> values);

    /**
     * Creates a new variable.
     *
     * @param min The minimum value that is allowed for the variable.
     * @param max The maximum value that is allowed for the variable.
     *
     * @return The created variable.
     */
    default IVariable createVariable(int min, int max) {
        return createVariable(BigInteger.valueOf(min), BigInteger.valueOf(max));
    }

    /**
     * Creates a new variable.
     *
     * @param min The minimum value that is allowed for the variable.
     * @param max The maximum value that is allowed for the variable.
     *
     * @return The created variable.
     */
    IVariable createVariable(BigInteger min, BigInteger max);

}
